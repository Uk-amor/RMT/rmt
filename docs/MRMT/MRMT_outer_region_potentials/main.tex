\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{fullpage}

\newcommand{\chnl}[2]{\Phi_{#1}^{\Gamma^{#2}}({\mathbf{x}}_{N};{\mathbf{r}}_{N+1}\sigma_{N+1})}
\newcommand{\rsh}[1]{{\cal Y}_{l_{#1},m_{#1}}}
\newcommand{\tgt}[1]{\Phi_{#1}({\mathbf{x}}_{N})}

\title{MRMT outer region potentials}
\author{Zden\v{e}k Ma\v{s}\'{i}n}
\date{first version July 2016, updated: October 2017}

\begin{document}

\maketitle

\section{Preliminaries}

The Schroedinger equation describing laser-molecule interaction is
\begin{eqnarray}\label{eq1}
[H_{0} + D(\mathbf{E};t)]\Psi(\mathbf{X}_{N+1},t)= \imath\frac{d\Psi(\mathbf{X}_{N+1},t)}{dt},
\end{eqnarray}
\begin{eqnarray}
H_{0} &=& H_{N} + \underbrace{\sum_{p=1}^{N}\frac{1}{r_{p(N+1)}}-\sum_{k=1}^{Nuclei}\frac{Z_{k}}{\rho_{k(N+1)}}}_{H_{N+1}},\label{we}\\
D(\mathbf{E};t) &=& \underbrace{\sum_{i=1}^{N} \mathbf{E}(t).\mathbf{r}_{i}}_{D_{N}} + \underbrace{\mathbf{E}(t).\mathbf{r}_{N+1}}_{D_{N+1}}\label{efield},
\end{eqnarray}
where $H_{0}$ is the field-free non-relativistic Hamiltonian of the molecule in the fixed-nuclei approximation and $D(\mathbf{E};t)$ represents the dipole approximation to the interaction of the molecule's electrons with the electric field $\mathbf{E}(t)$ and we have used the length gauge to describe it; $\mathbf{X}_{N+1}$ stands for all spin-space coordinates of the $N+1$ electrons in the system. We have written the terms $H_{0}$ and $D(\mathbf{E};t)$ in form convenient for derivation of the coupling potentials: the contribution to $H_{0}$ and $D(\mathbf{E};t)$ of the $(N+1)$th electron has been separated out.

We assume in the derivations that the electric field is linearly polarized: $\mathbf{E}(t) = E(t)(E_{x},E_{y},E_{z})$, where $E_{x},E_{y},E_{z}$ are real components of a unit vector defining the direction of the field in the molecular frame and $E(t)$ defines the time-dependent amplitude of the laser pulse.

\section{Potentials for outer region propagation}

The potentials describing interaction of the outer region electron with the field and with the residual $N$-electron target are expressed in the basis of the channel functions. These have the form:

\begin{eqnarray}
\vert \chnl{i}{}\rangle &=& \sum_{M_{S_{i}}}\sum_{m_{i}} (S_{i}M_{S_{i}},\frac{1}{2}m_{i}\vert SM)\Phi_{i}({\mathbf{x}}_{N}){\cal Y}_{l_{i},m_{i}}(\hat{\mathbf{x}}_{N+1}),
\end{eqnarray}
where $\Gamma$ stands for the total spatial symmetry (irreducible representation) of the $N+1$ wavefunction, $(S_{i}M_{S_{i}},\frac{1}{2}m_{i}\vert SM)$ is the Clebsch-Gordan coefficient ensuring correct spin-coupling of the target state $\Phi_{i}({\mathbf{x}}_{N})$ and the unbound electron to the total spin $S$ and $M$. However, since spin plays only a passive role in our calculations we have omitted the spin variables on the left hand side of the equation. We assume that only wavefunctions corresponding to the same total spin have been included in the calculation. The channels are labelled by the collective index $i \equiv \{t_{i},l_{i},m_{i}\}$, where $t_{i}$ stands for the index of the target state and $l_{i},m_{i}$ are the appropriate angular momentum quantum numbers of the \textbf{real} spherical harmonic ${\cal Y}_{l_{i},m_{i}}(\hat{\mathbf{x}}_{N+1})$ coupled to the target state $t_{i}$ producing a channel function with the correct total symmetry $\Gamma$.

We note that as opposed to the atomic case the wavefunction of total symmetry $\Gamma$ and the target states $\Phi_{i}({\mathbf{x}}_{N})$ do not posses a definite value of angular momentum and its projection on the z-axis: in case of linear molecules the projection of the angular momentum on the molecular axis is a conserved quantity but our codes only use Abelian point groups which do not include point groups of linear molecules. Therefore there is no geometrical dependence of the molecular wavefunctions that can be factored out in the form a single spherical harmonic (or Clebsch-Gordan coefficients in case of matrix elements).

The potential for the outer region electron is split into three terms
\begin{eqnarray}
V(\mathbf{r},t) = W^{E}(\mathbf{r}) + W^{D}(\mathbf{r},t) + W^{P}(\mathbf{r},t).
\end{eqnarray}

\input{app1.tex}

\subsection{$W^{D}$ potentials for laser-target interaction}

The potentials describing interaction of the laser with the residual target are defined as the matrix elements:

\begin{eqnarray}
W^{D}_{ij}(\mathbf{r},t) &=& \left(\chnl{i}{i}\vert D_{N}\vert\chnl{j}{j}\right).\label{wddef}
\end{eqnarray}

Expanding the $D_{N}$ operator and separating out the integration over the target's electrons we obtain:
\begin{eqnarray}
W^{D}_{ij}(\mathbf{r},t) &=& \left(\chnl{i}{i}\vert \sum_{i=1}^{N} \mathbf{E}(t).\mathbf{r}_{i}\vert\chnl{j}{j}\right) = \nonumber\\
&=& \mathbf{E}(t).\langle\tgt{t_{i}}\vert\sum_{i=1}^{N}\mathbf{r}_{i}\vert\tgt{t_{j}}\rangle \langle\rsh{i}\vert\rsh{j}\rangle=\nonumber\\
&=& \mathbf{E}(t).\mathbf{d}_{t_{i},t_{j}}\delta_{l_{i},l_{j}}\delta_{m_{i},m_{j}},\label{wdeq}
\end{eqnarray}
where the term $\mathbf{E}(t).\mathbf{d}_{t_{i},t_{j}}$ is the dot product between the x,y,z components of the electric field and the corresponding components of the permanent or transition dipole moments for the $N$-electron target states $t_i$ and $t_j$.

We can see that this potential is diagonal in $\{l,m\}$ but if at least two target states of different symmetries are included it will couple wavefunctions of different total symmetries. If the electric field is linearly polarized we can simplify equation~(\ref{wdeq}) as:
\begin{eqnarray}
W^{D}_{ij}(\mathbf{r},t) &=& E(t)w^{d}_{ij},\\
w^{d}_{ij}&=& \left(\hat{\mathbf{E}}.\mathbf{d}_{t_{i},t_{j}}\right)\delta_{l_{i},l_{j}}\delta_{m_{i},m_{j}},
\end{eqnarray}
where $\hat{\mathbf{E}} = (E_{x},E_{y},E_{z})$ is the unit vector in the direction of the field. Therefore the $w^{d}_{ij}$ terms containing the dot product $\hat{\mathbf{E}}.\mathbf{d}_{t_{i},t_{j}}$ is computed only once (at the beginning of the calculation) and no other changes to the code are required to accommodate an arbitrary direction of the linearly polarized field in the $W^{D}$ potential.

\subsection{$W^{P}$ potentials for laser-electron interaction}

The last potential is the one describing interaction of the unbound electron with the laser field:

\begin{eqnarray}
W^{P}_{ij}(\mathbf{r},t) &=& \left(\chnl{i}{i}\vert D_{N+1}\vert\chnl{j}{j}\right).\label{wpdef}
\end{eqnarray}

Following the same steps as in evaluation of the $W^{D}$ potential we obtain:
\begin{eqnarray}
W^{P}_{ij}(\mathbf{r},t) &=& \left(\chnl{i}{i}\vert \mathbf{E}(t).\mathbf{r}_{N+1}\vert\chnl{j}{j}\right) = \nonumber\\
&=& \langle\tgt{t_{i}}\vert\tgt{t_{j}}\rangle\mathbf{E}(t).\langle\rsh{i}\vert\mathbf{r}_{N+1}\vert \rsh{j}\rangle=\nonumber\\
&=& \delta_{t_{i},t_{j}}r_{N+1}\sqrt{\frac{4\pi}{3}}[E_{x}(t).\langle\rsh{i}\vert {\cal Y}_{1,1}\vert\rsh{j}\rangle +\nonumber\\ &+&E_{y}(t).\langle\rsh{i}\vert {\cal Y}_{1,-1} \vert\rsh{j}\rangle+E_{z}(t).\langle\rsh{i}\vert {\cal Y}_{1,0}\vert\rsh{j}\rangle],\label{wp}
\end{eqnarray}
where we took advantage of orthonormality of the target states and the form of the real spherical harmonics for $l=1$:
\begin{eqnarray}
{\cal Y}_{1,m}(\mathbf{r}) = 
\begin{cases} \sqrt{\frac{3}{4\pi}}\frac{y}{r} &,m = -1, \\ 
\sqrt{\frac{3}{4\pi}}\frac{z}{r} &,m = 0, \\ 
\sqrt{\frac{3}{4\pi}}\frac{x}{r} &,m = 1.
\end{cases}
\end{eqnarray}
The $W^{P}$ potential is diagonal in the target space and can couple wavefunctions of different total symmetries. If the field is linearly polarized then the potential can be rewritten as:
\begin{eqnarray}
W^{P}_{ij}(\mathbf{r},t) &=& E(t)r_{N+1}w^{p}_{ij},\\
w^{p}_{ij}&=&\delta_{t_{i},t_{j}}r_{N+1}\sqrt{\frac{4\pi}{3}}[\langle\rsh{i}\vert E_{x}.{\cal Y}_{1,1}+E_{y}.{\cal Y}_{1,-1}+ E_{z}.{\cal Y}_{1,0}\vert\rsh{j}\rangle\rangle],
\end{eqnarray}
where $(E_{x},E_{y},E_{z})$ is the unit vector in the direction of the field. The $w^{p}_{ij}$ terms can be calculated only once and no other changes to the code are needed to accommodate an arbitrary direction of the linearly polarized field in the $W^{P}$ potential.

We conclude that to construct the molecular potential $V(\mathbf{r})$ we require the potential coupling coefficients $a_{ij\lambda}$ from the field-free molecular calculation, dipole transition moments for the target states and the coupling coefficients for three real spherical harmonics, see equation (\ref{rg}): these are sufficient to construct the potentials for an arbitrary direction of the linearly polarized field.

\section{Inner region and the arbitrary direction of the linearly polarized field}

In the inner region the total wavefunction of the system is expanded into the R-matrix basis functions:
\begin{eqnarray}
\Psi(\mathbf{x}_{N+1},t) = \sum_{k} A_{k}(t)\Psi_{k}(\mathbf{x}_{N+1}),
\end{eqnarray}
where $A_{k}(t)$ are the time-dependent coefficients determined by the R-matrix time-propagation scheme. The inner-region Hamiltonian has the form:
\begin{eqnarray}
H(t) = \left(H_{N+1} + {\cal{L}}_{N+1}\right) + D(\mathbf{E};t),
\end{eqnarray}
where ${\cal{L}}_{N+1}$ is the usual Bloch operator, $D(\mathbf{E};t)$ is the dipole operator describing the electric field, see equation~(\ref{efield}), and $\Psi_{k}(\mathbf{x}_{N+1})$ are eigenfunctions of $\left(H_{N+1} + {\cal{L}}_{N+1}\right)$. Therefore the matrix elements of $H(t)$ in the basis of the R-matrix functions are
\begin{eqnarray}
\langle \Psi_{k'}\vert H(t)\vert \Psi_{k}\rangle &=& \delta_{k,k'}E_{k} + \langle \Psi_{k'}\vert D(\mathbf{E};t)\vert \Psi_{k}\rangle,\\
\langle \Psi_{k'}\vert D(\mathbf{E};t) \vert \Psi_{k}\rangle &=& \langle \Psi_{k'}\vert \sum_{i=1}^{N+1} \mathbf{E}(t).\mathbf{r}_{i}\vert \Psi_{k}\rangle.
\end{eqnarray}
If the electric field is linearly polarized then $\mathbf{E}(t) = E(t)\hat{\mathbf{E}} = E(t)(E_{x},E_{y},E_{z})$ and the matrix element of the dipole operator can be written as:
\begin{eqnarray}
\langle \Psi_{k'}\vert D(\mathbf{E};t) \vert \Psi_{k}\rangle &=& E(t)D_{k',k},\\
D_{k',k}&=&\left(E_{x}\langle \Psi_{k'}\vert \sum_{i=1}^{N+1} x_{i}\vert \Psi_{k}\rangle + E_{y}\langle \Psi_{k'}\vert \sum_{i=1}^{N+1} y_{i}\vert \Psi_{k}\rangle + E_{z}\langle \Psi_{k'}\vert \sum_{i=1}^{N+1} z_{i}\vert \Psi_{k}\rangle\right),
\end{eqnarray}
where the $x,y,z$ dipole matrix elements are obtained from the CDENPROP code and $\hat{\mathbf{E}} = (E_{x},E_{y},E_{z})$ is the unit vector in the direction of the field. Therefore we can first contract, for each pair of the R-matrix basis functions, the elementary $x,y,z$ dipole matrix elements with the field direction as given by the expression in the round brackets and then multiply the result by the time-dependent field amplitude. The code only requires the knowledge of the dipole blocks $D_{k',k}$ so we need to do the contraction with the field direction only once (at the beginning of the calculation) and no other changes to the code are needed to accommodate an arbitrary direction of the linearly polarized field.

\end{document}
