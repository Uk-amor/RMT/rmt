OR_size command
***********************

.. automodule:: rmt_utilities.OR_size
.. argparse::
   :module: rmt_utilities.OR_size
   :func: read_command_line
   :prog: RMT_OR_size