=============
RMT utilities
=============

.. image:: https://gitlab.com/UK-AMOR/RMT/rmt/badges/master/pipeline.svg
        :target: https://gitlab.com/UK-AMOR/RMT/rmt/-/commits/master

.. image:: https://gitlab.com/UK-AMOR/RMT/rmt/badges/master/coverage.svg
        :target: https://gitlab.com/UK-AMOR/RMT/rmt/-/commits/master

.. image:: https://img.shields.io/pypi/v/rmt-utilities.svg
        :target: https://pypi.python.org/pypi/rmt-utilities


Python package for interacting with the high performance, parallel fortran code R-matrix with Time `(RMT) <https://gitlab.com/Uk-amor/RMT/rmt>`_


* Free software: `GPL-v3 <https://www.gnu.org/licenses/gpl-3.0.en.html>`_
* `Package repository <https://gitlab.com/UK-AMOR/RMT/rmt>`_
