===============
Release History
===============

v0.1 (29-04-22)
---------------
The initial release contained basic classes for working with RMT output files
and reading input configurations. 

`View v0.1 on PyPI <https://pypi.org/project/rmt-utilities/0.1/>`_

v1.0 (30-08-22)
---------------
v1.0 provides command line utilities for mopst of the major post-processing
tasks for RMT output. On the physics side this includes producing high harmonic and absorption
spectra, estimating ionisation cross sections, plotting electron densities and
momentum distributions. On the development side, v1.0 also includes utilities
for regression testing RMT calculations, in particular a command line tool
`rmt_compare` which reports on the agreement between output files from two RMT
calculations, and the `rmt_utilities.regress` module which allows the
programmatic execution and comparison of calculations.

`View v1.0 on PyPI <https://pypi.org/project/rmt-utilities/1.0/>`_
