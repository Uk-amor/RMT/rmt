#!/bin/bash

mkdir -p outputs
rm -f *.dat fort.* dyson_*

# ----------------------------------------------------------------------------------------------------------------------------------
echo "H2O2: cdenprop"
# ----------------------------------------------------------------------------------------------------------------------------------

ln -s ../1-integrals/moints               fort.17

ln -s ../2-inner/target-ci-coeffs.dat             fort.266
ln -s ../2-inner/target-properties.dat            fort.24

ln -s ../2-inner/scattering-ci-coeffs.dat         fort.25
ln -s ../2-inner/scattering-configurations-A.dat  fort.70
ln -s ../2-inner/scattering-configurations-B.dat  fort.71

cdenprop < inputs/cdenprop.inp \
               1> cdenprop.out \
               2> cdenprop.err || exit 1

mv fort.624 cdenprop-inner-dipoles.dat
mv dyson_orbitals.ukrmolp0 cdenprop-dyson_orbitals_ukrmolp0.dat
mv target.phases.data cdenprop-target.phases.dat

rm -f fort.*

# ----------------------------------------------------------------------------------------------------------------------------------
echo "H2O2: outer"
# ----------------------------------------------------------------------------------------------------------------------------------

ln -s ../1-integrals/moints        fort.22

ln -s ../2-inner/target-properties.dat     fort.24
ln -s ../2-inner/scattering-ci-coeffs.dat  fort.25

ln -s cdenprop-inner-dipoles.dat           fort.624

mpiexec -n 1 outer-run swintf rsolve_photo < inputs/outer.inp 1> outer.out 2> outer.err || exit 1

mv fort.142  outer-partial-wave-dipoles.dat

rm -f fort.*
