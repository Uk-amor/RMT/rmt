import pytest
import unittest
from rmt_utilities.dipole_cli import get_command_line as gcl
from rmt_utilities.gen_hhg import get_hhg_spectrum as ghg
from types import SimpleNamespace
import os
import matplotlib.pyplot as plt


dummyargs = {'x': False,
             'y': False,
             'z': True,
             'plot': False,
             'output': True,
             'pad_factor': 8,
             'solutions_list': None,
             'units': 'eV',
             'window': None}


class genhhg(unittest.TestCase):
    @classmethod
    def setUp(self):
        from subprocess import call
        call(['mkdir', 'tests/utilities_tests/spawn'])
        call(['cp', 'tests/utilities_tests/data/expec_z_all.neon00001000', 'tests/utilities_tests/spawn/'])
        call(['cp', 'tests/utilities_tests/data/expec_z_all.neon00001000',
              'tests/utilities_tests/spawn/expec_v_all.neon00001000'])

    @classmethod
    def tearDown(self):
        from subprocess import call
        call(['rm', '-rf', 'tests/utilities_tests/spawn'])

    @pytest.mark.slow
    def test_gcl_defaults(self):
        """This test isn't actually slow, but breaks if the --skip_slow argument
        is passed"""
        args = gcl('harmonic')
        assert args.files == ['.']
        dargs = dict(dummyargs)
        del dargs['window']
        for arg in dargs:
            assert getattr(args, arg) == dummyargs[arg]

    def test_get_hhg(self):
        newargs = SimpleNamespace(**dummyargs)
        newargs.files = ['tests/utilities_tests/spawn']
        ax = ghg(newargs)
        assert ax
        plt.close()

    def test_output(self):
        newargs = SimpleNamespace(**dummyargs)
        newargs.files = ['tests/utilities_tests/spawn']
        ghg(newargs)
        assert os.path.isfile('tests/utilities_tests/spawn/Harm_len_0001')
        assert os.path.isfile('tests/utilities_tests/spawn/Harm_len_0002')
        assert os.path.isfile('tests/utilities_tests/spawn/Harm_vel_0001')
        assert os.path.isfile('tests/utilities_tests/spawn/Harm_vel_0002')
        plt.close()

    def test_plot(self):
        newargs = SimpleNamespace(**dummyargs)
        newargs.files = ['tests/utilities_tests/spawn']
        newargs.plot = True
        newargs.output = False
        plt.ion()
        ax = ghg(newargs)
        assert len(ax.get_lines()) == 4
        assert ax.get_xlabel() == 'Frequency (eV)'
        plt.close()
