import unittest
from rmt_utilities.OR_size import get_OR_size as OR
from mock import Mock

calc = Mock()
calc.config = {'x_last_others': 240,
               'x_last_master': 200,
               'no_of_pes_to_use_outer': 8}
hfile = Mock()
hfile.rmatr = 15


class ORtests(unittest.TestCase):
    def test_get_OR_size_defaults(self):
        assert OR(calc, hfile) == 165.4

    def test_get_OR_size_deltar(self):
        calc.config["deltar"] = 0.16
        try:
            assert OR(calc, hfile) == 315.8
        except AssertionError as E:
            raise E
        finally:
            del calc.config["deltar"]

    def test_get_OR_size_pes_per_sector(self):
        calc.config["no_of_pes_per_sector"] = 2
        try:
            assert OR(calc, hfile) == 98.2
        except AssertionError as E:
            raise E
        finally:
            del calc.config["no_of_pes_per_sector"]

    def test_read_command_line(self):
        from rmt_utilities.OR_size import read_command_line
        pp = read_command_line().parse_args()
        assert pp.file == "."
