import numpy as np
import pytest
from rmt_utilities import windowing as w


@pytest.mark.parametrize("window", [w.LinearProportion(0.7), w.LinearStep(600), w.ExponentialStep(1, 400)])
def test_end_close_to_zero(window):
    data = window.window(1000)
    assert data[-1] < 1e-16


@pytest.mark.parametrize(
    "window,index", [(w.LinearProportion(0.7), 700), (w.LinearStep(600), 600), (w.ExponentialStep(1, 400), 400)]
)
def test_all_less_than_one(window, index):
    data = window.window(1000)
    assert np.all(data[index + 1:] < 1)
