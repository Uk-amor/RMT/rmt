MODULE test_recoupling 
    USE ISO_FORTRAN_ENV, ONLY: REAL64    
    USE testdrive, ONLY: error_type, new_unittest, unittest_type, test_failed
    USE testing_utilities, ONLY: compare, format_context

    IMPLICIT NONE
    PRIVATE
    PUBLIC :: collect_recoupling   

    CONTAINS

    SUBROUTINE collect_recoupling(testsuite)   
        TYPE(unittest_type), DIMENSION(:), ALLOCATABLE, INTENT(OUT) :: testsuite

        testsuite = [ &
                    new_unittest("Decouple LS channels: ", test_decoupleLS), &
                    new_unittest("Decouple jK channels: ", test_decouplejK), &
                    new_unittest("Extract Quantum Numbers (LS Coupled): ", test_GetQnumsLS), &
                    new_unittest("Extract Quantum Numbers (LS Decoupled): ", test_GetQnumsLSUnc), &
                    new_unittest("Extract Quantum Numbers (jK Decoupled): ", test_GetQnumsjKUnc) &
                    ]      

    END SUBROUTINE collect_recoupling

    SUBROUTINE test_decoupleLS(error) 
        USE recoupling, only: Recouple
        USE modChannel, only: AddChannel, Channel
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error
        TYPE(Channel), ALLOCATABLE :: ChannelsLS(:)
        TYPE(Channel), ALLOCATABLE :: ChannelsUN(:)
        COMPLEX*16, ALLOCATABLE :: wv(:, :, :)
        REAL(REAL64), ALLOCATABLE :: expected_wv(:, :, :)
        CHARACTER(len=:), ALLOCATABLE :: context
        INTEGER :: ii
        INTEGER :: expected(9)
        INTEGER, ALLOCATABLE :: new_shape(:)

        ALLOCATE(ChannelsLS(0), ChannelsUN(0))
        ALLOCATE(wv(2, 1, 3))
        wv = (1.0_REAL64, 0.0_REAL64)
        ALLOCATE(expected_wv(2, 6, 3))
        expected_wv = (0.0_REAL64, 0.0_REAL64)
        expected_wv(:, 1, :) = 0.5_REAL64
        expected_wv(:, 2, :) = -0.5_REAL64
        expected_wv(:, 5, :) = -0.5_REAL64
        expected_wv(:, 6, :) = 0.5_REAL64

        CALL AddChannel(Target_Counter=1, &
        l_of_Target=1, &
        l_in_current_sym=1, &
        Current_L=1, &
        ml=0, &
        pty=0, &
        Current_S=1, &
        s_of_target=2, &
        coupling_id=1, &
        channelsIn=ChannelsLS)

        context = "" 

        CALL Recouple(ChannelsLS, wv)

        IF (.NOT. compare(ubound(ChannelsLS, 1), 6)) THEN
            CALL format_context(context, "Number of channels", ubound(ChannelsLS, 1), 6)
        END IF

        expected = [1, 1, -1, 1, 1, 1, -1, 1, 0]
        CALL compare_channel(ChannelsLS(1), expected, ii) 
        IF (ii /= 0) THEN
            CALL format_context(context, ChannelsLS(1)%Label(ii), ChannelsLS(1)%QNumber(ii), expected(ii))
        END IF

        expected =[1, 1, 1, 1, -1, 1, 1, -1, 0]
        CALL compare_channel(ChannelsLS(6), expected, ii) 
        IF (ii /= 0) THEN
            CALL format_context(context, ChannelsLS(6)%Label(ii), ChannelsLS(6)%QNumber(ii), expected(ii))
        END IF

        new_shape = shape(wv)
        IF (.NOT. compare(new_shape, [2,6,3])) THEN
            CALL format_context(context, "Decoupled Wfn wrong shape", new_shape, [2,6,3])
        END IF

        IF (.NOT. compare(real(wv, REAL64), expected_wv)) THEN
            CALL format_context(context, "Decoupled Wfn wrong value", real(wv, REAL64), expected_wv)
        END IF

        IF (context /= "") THEN
            CALL test_failed(error, "LS channel decoupled incorrectly!", context)
        END IF

    END SUBROUTINE test_decoupleLS

    SUBROUTINE test_decouplejK(error) 
        USE recoupling, only: Recouple
        USE modChannel, only: AddChannel, Channel
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error
        TYPE(Channel), ALLOCATABLE :: ChannelsjK(:)
        TYPE(Channel), ALLOCATABLE :: ChannelsUN(:)
        COMPLEX*16, ALLOCATABLE :: wv(:, :, :)
        REAL(REAL64), ALLOCATABLE :: expected_wv(:, :, :)
        CHARACTER(len=:), ALLOCATABLE :: context
        INTEGER :: ii
        INTEGER :: expected(8)
        INTEGER, ALLOCATABLE :: new_shape(:)

        ALLOCATE(ChannelsjK(0), ChannelsUN(0))
        ALLOCATE(wv(2, 1, 3))
        wv = (1.0_REAL64, 0.0_REAL64)
        ALLOCATE(expected_wv(2, 4, 3))
        expected_wv = 0.0_REAL64
        expected_wv(:, 1, :) = -SQRT(4.0_REAL64/30.0_REAL64)
        expected_wv(:, 2, :) = SQRT(1.0_REAL64/5.0_REAL64)
        expected_wv(:, 3, :) = SQRT(1.0_REAL64/5.0_REAL64)
        expected_wv(:, 4, :) = -SQRT(4.0_REAL64/30.0_REAL64)

        CALL AddChannel(Target_Counter=1, &
        l_of_Target=2, &
        l_in_current_sym=2, &
        Current_L=1, &
        ml=0, &
        pty=0, &
        Current_S=1, &
        s_of_target=2, &
        coupling_id=2, &
        channelsIn=ChannelsjK)

        context = "" 

        CALL Recouple(ChannelsjK, wv)

        IF (.NOT. compare(ubound(ChannelsjK, 1), 4)) THEN
            CALL format_context(context, "Number of channels", ubound(ChannelsjK, 1), 4)
        END IF

        expected = [1, 2, 1, 2, 0, 1, -1, 0]
        CALL compare_channel(ChannelsjK(1), expected, ii) 
        IF (ii /= 0) THEN
            CALL format_context(context, ChannelsjK(1)%Label(ii), ChannelsjK(1)%QNumber(ii), expected(ii))
        END IF

        expected = [1, 2, 1, 2, -1, 1, 1, 0]
        CALL compare_channel(ChannelsjK(3), expected, ii) 
        IF (ii /= 0) THEN
            CALL format_context(context, ChannelsjK(3)%Label(ii), ChannelsjK(3)%QNumber(ii), expected(ii))
        END IF

        new_shape = shape(wv)
        IF (.NOT. compare(new_shape, [2,4,3])) THEN
            CALL format_context(context, "Decoupled Wfn wrong shape", new_shape, [2,4,3])
        END IF

        IF (.NOT. compare(real(wv, REAL64), expected_wv)) THEN
            CALL format_context(context, "Decoupled Wfn wrong value", real(wv, REAL64), expected_wv)
        END IF

        IF (context /= "") THEN
            CALL test_failed(error, "jK channel decoupled incorrectly!", context)
        END IF

    END SUBROUTINE test_decouplejK

    SUBROUTINE test_GetQnumsLSUnc(error)
        USE recoupling, only: GetQnums
        USE modChannel, only: Channel
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error
        TYPE(Channel) :: ChannelIn
        CHARACTER(len=:), ALLOCATABLE :: context
        INTEGER :: ii, elecL, elecML
        REAL(REAL64) :: coupl

        context = ""
        ALLOCATE(ChannelIn%Qnumber(9))
        DO ii = 1, 9
            ChannelIn%QNumber(ii) = ii
        END DO

        ChannelIn%Species = 3 ! Uncoupled LS channel 
        CALL GetQnums(ChannelIn=ChannelIn, &
                      TargetL = 2, &
                      TargetML = 2, &
                      elecMS = 2, &
                      elecL = elecL, &
                      elecML = elecML, &
                      coupl = coupl)

        IF (.NOT. compare(elecL, 4)) THEN
            CALL format_context(context, "elecL:", elecL, 4)
        END IF

        IF (.NOT. compare(elecML, 5)) THEN
            CALL format_context(context, "elecML:", elecML, 5)
        END IF

        IF (.NOT. compare(coupl, 0.0_REAL64)) THEN
            CALL format_context(context, "coupl:", coupl, 0.0_REAL64)
        END IF

        CALL GetQnums(ChannelIn=ChannelIn, &
                      TargetL = 2, &
                      TargetML = 3, &
                      elecMS = 8, &
                      elecL = elecL, &
                      elecML = elecML, &
                      coupl = coupl)

        IF (.NOT. compare(coupl, 1.0_REAL64)) THEN
            CALL format_context(context, "coupl:", coupl, 0.0_REAL64)
        END IF

        IF (context /= "") THEN
            CALL test_failed(error, "GetQnums returned incorrect values for decoupled LS Channel", context)
        END IF

    
    END SUBROUTINE test_GetQnumsLSUnc

    SUBROUTINE test_GetQnumsjKUnc(error)
        USE recoupling, only: GetQnums
        USE modChannel, only: Channel
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error
        TYPE(Channel) :: ChannelIn
        CHARACTER(len=:), ALLOCATABLE :: context
        INTEGER :: ii, elecL, elecML
        REAL(REAL64) :: coupl

        context = ""
        ALLOCATE(ChannelIn%Qnumber(8))
        DO ii = 1, 8
            ChannelIn%QNumber(ii) = ii
        END DO

        ChannelIn%Species = 4 ! Uncoupled jK channel 
        CALL GetQnums(ChannelIn=ChannelIn, &
                      TargetL = 2, &
                      TargetML = 2, &
                      elecMS = 2, &
                      elecL = elecL, &
                      elecML = elecML, &
                      coupl = coupl)

        IF (.NOT. compare(elecL, 4)) THEN
            CALL format_context(context, "elecL:", elecL, 4)
        END IF

        IF (.NOT. compare(elecML, 5)) THEN
            CALL format_context(context, "elecML:", elecML, 5)
        END IF

        IF (.NOT. compare(coupl, 0.0_REAL64)) THEN
            CALL format_context(context, "coupl:", coupl, 0.0_REAL64)
        END IF

        CALL GetQnums(ChannelIn=ChannelIn, &
                      TargetL = 2, &
                      TargetML = 3, &
                      elecMS = 7, &
                      elecL = elecL, &
                      elecML = elecML, &
                      coupl = coupl)

        IF (.NOT. compare(coupl, 1.0_REAL64)) THEN
            CALL format_context(context, "coupl:", coupl, 1.0_REAL64)
        END IF

        IF (context /= "") THEN
            CALL test_failed(error, "GetQnums returned incorrect values for decoupled jK Channel", context)
        END IF

    
    END SUBROUTINE test_GetQnumsjKUnc

    SUBROUTINE test_GetQnumsLS(error)
        USE recoupling, only: GetQnums
        USE modChannel, only: Channel
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error
        TYPE(Channel) :: ChannelIn
        CHARACTER(len=:), ALLOCATABLE :: context
        INTEGER :: elecL, elecML
        REAL(REAL64) :: coupl, expected_coupl

        context = ""
        ALLOCATE(ChannelIn%Qnumber(8))
        ChannelIn%QNumber = [1, 2, 3, 4, 4, 0, 0, 0]

        ChannelIn%Species = 1 ! Coupled LS channel 
        CALL GetQnums(ChannelIn=ChannelIn, &
                      TargetL = 2, &
                      TargetML = 2, &
                      elecMS = 2, &
                      elecL = elecL, &
                      elecML = elecML, &
                      coupl = coupl)

        IF (.NOT. compare(elecL, 3)) THEN
            CALL format_context(context, "elecL:", elecL, 3)
        END IF

        IF (.NOT. compare(elecML, 2)) THEN
            CALL format_context(context, "elecML:", elecML, 2)
        END IF

        expected_coupl = sqrt(2.0_REAL64 / 5.0_REAL64)
        IF (.NOT. compare(coupl, expected_coupl)) THEN
            CALL format_context(context, "coupl:", coupl, expected_coupl)
        END IF

        CALL GetQnums(ChannelIn=ChannelIn, &
                      TargetL = 2, &
                      TargetML = 3, &
                      elecMS = 2, &
                      elecL = elecL, &
                      elecML = elecML, &
                      coupl = coupl)

        IF (.NOT. compare(coupl, 0.0_REAL64)) THEN
            CALL format_context(context, "coupl:", coupl, 0.0_REAL64)
        END IF

        IF (context /= "") THEN
            CALL test_failed(error, "GetQnums returned incorrect values for coupled LS Channel", context)
        END IF

    
    END SUBROUTINE test_GetQnumsLS

    SUBROUTINE compare_channel(channelIn, expected, index) 
        USE modChannel, ONLY: Channel
        TYPE(channel), INTENT(IN) :: channelIn
        INTEGER, INTENT(IN) :: expected(:)
        INTEGER, INTENT(OUT) :: index

        DO index=1,ubound(ChannelIn%QNumber, 1)
            IF (.NOT. compare(channelIn%QNumber(index), expected(index))) THEN
                RETURN
            END IF
        END DO
        index = 0
    END SUBROUTINE compare_channel

END MODULE test_recoupling



