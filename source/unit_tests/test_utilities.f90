MODULE test_utilities
    USE ISO_FORTRAN_ENV, ONLY: INT32, REAL64

    USE testdrive, ONLY: check, error_type, new_unittest, test_failed, unittest_type

    USE rmt_utilities, ONLY: int_to_char

    IMPLICIT NONE

    PRIVATE
    PUBLIC :: collect_utilities
CONTAINS
    SUBROUTINE collect_utilities(testsuite)
        TYPE(unittest_type), DIMENSION(:), ALLOCATABLE, INTENT(OUT) :: testsuite

        testsuite = [ &
                    new_unittest("int-to-char::zero", test_int_to_char_zero), &
                    new_unittest("int-to-char::one-digit", test_int_to_char_one_digit), &
                    new_unittest("int-to-char::one-digit-negative", test_int_to_char_one_digit_negative), &
                    new_unittest("int-to-char::two-digits", test_int_to_char_two_digits), &
                    new_unittest("int-to-char::two-digits-negative", test_int_to_char_two_digits_negative), &
                    new_unittest("int-to-char::ten-digits", test_int_to_char_ten_digits), &
                    new_unittest("int-to-char::ten-digits-negative", test_int_to_char_ten_digits_negative) &
                    ]
    END SUBROUTINE

    SUBROUTINE test_int_to_char_zero(error)
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error

        CALL check(error, int_to_char(0), "0")
    END SUBROUTINE

    SUBROUTINE test_int_to_char_one_digit(error)
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error

        CALL check(error, int_to_char(9), "9")
    END SUBROUTINE

    SUBROUTINE test_int_to_char_one_digit_negative(error)
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error

        CALL check(error, int_to_char(-1), "-1")
    END SUBROUTINE

    SUBROUTINE test_int_to_char_two_digits(error)
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error

        CALL check(error, int_to_char(98), "98")
    END SUBROUTINE

    SUBROUTINE test_int_to_char_two_digits_negative(error)
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error

        CALL check(error, int_to_char(-21), "-21")
    END SUBROUTINE

    SUBROUTINE test_int_to_char_ten_digits(error)
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error

        CALL check(error, int_to_char(1234567890), "1234567890")
    END SUBROUTINE

    SUBROUTINE test_int_to_char_ten_digits_negative(error)
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error

        CALL check(error, int_to_char(-1234567890), "-1234567890")
    END SUBROUTINE
END MODULE
