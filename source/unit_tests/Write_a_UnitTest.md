
# Steps To Create Your Own Unit Test.

## The Unit Test File Code Necessities



* __In the [CMakeLists.txt](CMakeLists.txt#L11) create a unit test name and add it to the bottom of the test list.__

* __Copy [test_template.f90](test_template.f90), this is a compilable boiler plate example of a unit test file and its practicalities. Rename test_template copy.f90 and the module name and subroutines it features to suit your unit test__

* __In [main.f90](main.f90#L10) include the main subroutine from your unit test and append your subroutine to the testsuite as shown [here](test_template.f90#L21)__

* __For further documentation on the examples described in test_template.f90 see [here](). For information on the test drive framework used see [testing framework](https://github.com/fortran-lang/test-drive)__

