MODULE test_dipole_input_file_parse_RM1 ! rename module to suit your unit test (match file name)
    USE ISO_FORTRAN_ENV, ONLY: INT32, REAL64    !  Use standard fortran library  *include info*

    USE testdrive, ONLY: check, error_type, new_unittest, unittest_type, test_failed

    USE testing_utilities, ONLY: compare, &
                                data_file, &
                                format_context, &
                                int_to_char, &
                                range, &
                                subarray, &
                                subarray_integer_int32, &
                                subarray_real_real64

    USE dipole_input_file_RM1, ONLY: D_file, dipole_block, parse_D_file_RM1
    USE initial_conditions, ONLY: debug

    USE precisn, ONLY: wp

    IMPLICIT NONE  ! all types must be explicitly declared

    INTERFACE check
        MODULE PROCEDURE check_d_file
    END INTERFACE

    TYPE expected_D_file
        INTEGER(INT32):: ntarg
        INTEGER(INT32):: fintr
        INTEGER(INT32):: intr
        TYPE(expected_dipole_block), DIMENSION(:), ALLOCATABLE:: blocks
    END TYPE expected_D_file

    TYPE expected_dipole_block
        INTEGER(INT32):: noterm, isi, inch, ILrgL, IPty, ISpn, jsi, jnch, JLrgL
        INTEGER(INT32):: lgsf, lglf, lgpf, lgsi, lgli, lgpi ! quantum numbers initial/final
        TYPE(subarray_real_real64) :: temp_dipsto ! length gauge reduced dipole transition matrix
        TYPE(subarray_real_real64) :: temp_dipsto_v ! velocity gauge reduced dipole transition matrix
        INTEGER(INT32), ALLOCATABLE :: iidip(:)
        INTEGER(INT32), ALLOCATABLE :: ifdip(:)
    END TYPE expected_dipole_block

    PRIVATE
    PUBLIC :: collect_parse_D_file_RM1 
CONTAINS
    SUBROUTINE collect_parse_D_file_RM1(testsuite)   !  This subroutine will collect the outcomes of tests performed
        TYPE(unittest_type), DIMENSION(:), ALLOCATABLE, INTENT(OUT) :: testsuite    ! Type of unit test type with member as initialised as testsuite

        !  testsuite is a list of each test performed make sure to use line continuation characters (&)
        testsuite = [new_unittest("atomic::small:example_Ar_jk", test_RMatrix1), &
                    new_unittest("atomic::small:iodine", test_RMatrix1_iodine), &
                    new_unittest("atomic::big:Ar_jk", test_RMatrix1_Ar_jk), &
                    new_unittest("atomic::big:Ar_ls", test_RMatrix1_Ar_LS)]      

    END SUBROUTINE collect_parse_D_file_RM1

    SUBROUTINE check_D_file(error, actual, expected, loc_of_blocks)
        ! comparisons for D file go here
        TYPE(error_type), ALLOCATABLE, INTENT(out) :: error
        TYPE(D_file), INTENT(in) :: actual
        TYPE(expected_D_file), INTENT(in) :: expected
        CHARACTER(len=:), ALLOCATABLE       :: context
        INTEGER, DIMENSION(:), ALLOCATABLE  :: expected_shape
        INTEGER, INTENT(IN)                 :: loc_of_blocks(:)
        INTEGER                             :: i

        context = ""

        IF (actual%ntarg /= expected%ntarg) THEN
            CALL format_context(context, "ntarg", actual%ntarg, expected%ntarg)
        END IF

        IF (actual%fintr /= expected%fintr) THEN
            CALL format_context(context, "fintr", actual%fintr, expected%fintr)
        END IF

        IF (actual%intr /= expected%intr) THEN
            CALL format_context(context, "intr", actual%intr, expected%intr)
        END IF

        expected_shape = [actual%ntarg, actual%ntarg]

        DO i = 1, SIZE(loc_of_blocks)
            CALL check_dipole_block(context, &
                                    loc_of_blocks(i), &
                                    actual%blocks(loc_of_blocks(i)), &
                                    expected%blocks(i))
        END DO

        PRINT *, "context: ", context

        IF (context /= "") THEN
            CALL test_failed(error, "D file parsed incorrectly!", context)
        END IF


    END SUBROUTINE check_D_file

    SUBROUTINE check_dipole_block(context, index, actual, expected)
        ! comparisons for D00n file go here
        TYPE(dipole_block), INTENT(in) :: actual
        TYPE(expected_dipole_block), INTENT(in) :: expected
        INTEGER, INTENT(IN) :: index

        CHARACTER(len=:), ALLOCATABLE :: context, prefix
        INTEGER, DIMENSION(:), ALLOCATABLE :: expected_shape

        prefix = "dipole_block("//int_to_char(index)//")%"

        IF (actual%noterm /= expected%noterm) THEN
            CALL format_context(context, prefix//"noterm", actual%noterm, expected%noterm)
        END IF

        IF (actual%isi /= expected%isi) THEN
            CALL format_context(context, prefix//"isi", actual%isi, expected%isi)
        END IF

        IF (actual%inch /= expected%inch) THEN
            CALL format_context(context, prefix//"inch", actual%inch, expected%inch)
        END IF

        IF (actual%jsi /= expected%jsi) THEN
            CALL format_context(context, prefix//"jsi", actual%jsi, expected%jsi)
        END IF

        IF (actual%jnch /= expected%jnch) THEN
            CALL format_context(context, prefix//"jnch", actual%jnch, expected%jnch)
        END IF

        IF (actual%lgsf /= expected%lgsf) THEN
            CALL format_context(context, prefix//"lgsf", actual%lgsf, expected%lgsf)
        END IF

        IF (actual%lglf /= expected%lglf) THEN
            CALL format_context(context, prefix//"lglf", actual%lglf, expected%lglf)
        END IF

        IF (actual%lgpf /= expected%lgpf) THEN
            CALL format_context(context, prefix//"lgpf", actual%lgpf, expected%lgpf)
        END IF

        IF (actual%lgsi /= expected%lgsi) THEN
            CALL format_context(context, prefix//"lgsi", actual%lgsi, expected%lgsi)
        END IF

        IF (actual%lgli /= expected%lgli) THEN
            CALL format_context(context, prefix//"lgli", actual%lgli, expected%lgli)
        END IF

        IF (actual%lgpi /= expected%lgpi) THEN
            CALL format_context(context, prefix//"lgpi", actual%lgpi, expected%lgpi)
        END IF
        
        IF (.NOT. compare(actual%iidip(index), expected%iidip(1))) THEN
            CALL format_context(context, "iidip", actual%iidip(index), expected%iidip(1))
        END IF

        IF (.NOT. compare(actual%ifdip(index), expected%ifdip(1))) THEN
            CALL format_context(context, "ifdip", actual%ifdip(index), expected%ifdip(1))
        END IF

        ! Check that array shapes are _internally_ consistent
        

        IF (.NOT. compare(actual%temp_dipsto, expected%temp_dipsto)) THEN
            CALL format_context(context, "temp_dipsto", actual%temp_dipsto, expected%temp_dipsto)
        END IF

        IF (.NOT. compare(actual%temp_dipsto_v, expected%temp_dipsto_v)) THEN
            CALL format_context(context, "temp_dipsto_v", actual%temp_dipsto_v, expected%temp_dipsto_v)
        END IF

        expected_shape = [actual%isi, actual%jsi]

        IF(.NOT. compare(SHAPE(actual%temp_dipsto), expected_shape)) THEN
            CALL format_context(context, "shape("//prefix//"%temp_dipsto)", SHAPE(actual%temp_dipsto), expected_shape)
        END IF

        IF(.NOT. compare(SHAPE(actual%temp_dipsto_v), expected_shape)) THEN
            CALL format_context(context, "shape("//prefix//"%temp_dipsto_v)", SHAPE(actual%temp_dipsto_v), expected_shape)
        END IF



    END SUBROUTINE check_dipole_block

    SUBROUTINE test_D_file(&
                            error, &
                            path, &
                            expected, &
                            lplusp, &
                            crlv, &
                            block_ind, &
                            dipole_coupled, &
                            wd_read_store, &
                            wd_read_store_v, &
                            LML_wp_read_store, &
                            LML_wd_read_store, &
                            LML_wd_read_store_v, &
                            L_block_nchan, &
                            L_block_tot_nchan, &
                            LML_block_nchan, &
                            LML_block_tot_nchan, &
                            ntarg, &
                            nstmx, &
                            no_of_L_blocks, &
                            no_of_LML_blocks, &
                            coupling_id, &
                            dipole_velocity_output, &
                            L_block_lrgl, &
                            L_block_nspn, &
                            L_block_npty, &
                            LML_block_nspn, &
                            LML_block_lrgl, &
                            LML_block_l2p, &
                            LML_block_npty, &
                            LML_block_ml, &
                            LML_block_nconat, &
                            ML_Max, &
                            xy_plane_desired, &
                            debug, &
                            mat_size_wd, &
                            mat_size_wp, &
                            loc_of_blocks)

        ! call parse function from input file, call check_D_file

        TYPE(error_type), ALLOCATABLE, INTENT(out) :: error
        TYPE(expected_D_file), INTENT(INOUT) :: expected
        TYPE(D_file) :: file
        
        CHARACTER(len=*), INTENT(in) :: path
        CHARACTER(len=:), ALLOCATABLE :: relative_path_header, D_file_path_header, relative_path_data, D_file_path_data

        INTEGER, INTENT(IN) :: lplusp
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: crlv(:, :)
        !INTEGER, ALLOCATABLE, INTENT(INOUT) :: iidip(:)
        !INTEGER, ALLOCATABLE, INTENT(INOUT) :: ifdip(:)
        INTEGER, ALLOCATABLE, INTENT(INOUT) :: dipole_coupled(:, :)
        INTEGER, ALLOCATABLE, INTENT(INOUT) :: block_ind(:, :, :)

        REAL(wp), ALLOCATABLE :: wp_read_store(:, :)
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: wd_read_store(:, :)
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: wd_read_store_v(:, :)
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: LML_wp_read_store(:, :)
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: LML_wd_read_store(:, :)
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: LML_wd_read_store_v(:, :)
        
        INTEGER, ALLOCATABLE, INTENT(INOUT) :: L_block_nchan(:)
        INTEGER, ALLOCATABLE, INTENT(INOUT) :: LML_block_nchan(:)
        INTEGER, INTENT(IN) :: L_block_tot_nchan
        INTEGER, INTENT(IN) :: LML_block_tot_nchan

        INTEGER, INTENT(IN)                  :: L_block_lrgl(:)
        INTEGER, INTENT(IN)                  :: L_block_nspn(:)
        INTEGER, INTENT(IN)                  :: L_block_npty(:)

        INTEGER, INTENT(IN)                  :: LML_block_lrgl(:)
        INTEGER, INTENT(IN)                  :: LML_block_l2p(:, :)
        INTEGER, INTENT(IN)                  :: LML_block_npty(:)
        INTEGER, INTENT(IN)                  :: LML_block_nspn(:)
        INTEGER, INTENT(IN)                  :: LML_block_ml(:)
        INTEGER, INTENT(IN)                  :: LML_block_nconat(:, :)
        LOGICAL, INTENT(IN)                  :: xy_plane_desired(:)

        INTEGER, INTENT(IN)                  :: coupling_id
        INTEGER, INTENT(IN)                  :: no_of_L_blocks
        INTEGER, INTENT(IN)                  :: no_of_LML_blocks
        INTEGER, INTENT(IN)                  :: nstmx
        INTEGER, INTENT(IN)                  :: ntarg
        INTEGER, INTENT(IN)                  :: ML_Max

        LOGICAL, INTENT(IN)                  :: debug
        LOGICAL, INTENT(IN)                  :: dipole_velocity_output 
        
        INTEGER, INTENT(INOUT) :: mat_size_wd
        INTEGER, INTENT(INOUT) :: mat_size_wp

        INTEGER, INTENT(IN)                     :: loc_of_blocks(:)

        relative_path_header = path//'D00'
        relative_path_data = path//'D'
        D_file_path_header = data_file(relative_path_header)
        D_file_path_data = data_file(relative_path_data) 
        
        CALL parse_D_file_RM1(&
                                D_file_path_header, &
                                D_file_path_data, &
                                file, &
                                lplusp, &
                                crlv, &
                                block_ind, &
                                dipole_coupled, &
                                wp_read_store, &
                                wd_read_store, &
                                wd_read_store_v, &
                                LML_wp_read_store, &
                                LML_wd_read_store, &
                                LML_wd_read_store_v, &
                                L_block_nchan, &
                                L_block_tot_nchan, &
                                LML_block_nchan, &
                                LML_block_tot_nchan, &
                                ntarg, &
                                nstmx, &
                                no_of_L_blocks, &
                                no_of_LML_blocks, &
                                coupling_id, &
                                dipole_velocity_output, &
                                L_block_lrgl, &
                                L_block_nspn, &
                                L_block_npty, &
                                LML_block_nspn, &
                                LML_block_lrgl, &
                                LML_block_l2p, &
                                LML_block_npty, &
                                LML_block_ml, &
                                LML_block_nconat, &
                                ML_Max, &
                                xy_plane_desired, &
                                debug, &
                                mat_size_wd, &
                                mat_size_wp)

        CALL check(error, file, expected, loc_of_blocks)
    END SUBROUTINE test_D_file

    SUBROUTINE test_RMatrix1(error)
        ! expected data goes here
        
        TYPE(error_type), ALLOCATABLE, INTENT(out) :: error
        TYPE(expected_D_file)               :: expected
        
        INTEGER(INT32) :: lplusp
        REAL(wp), ALLOCATABLE  :: crlv(:, :)
        INTEGER, ALLOCATABLE  :: iidip(:)
        INTEGER, ALLOCATABLE  :: ifdip(:)
        INTEGER, ALLOCATABLE  :: dipole_coupled(:, :)
        INTEGER, ALLOCATABLE  :: block_ind(:, :, :)

        !REAL(wp), ALLOCATABLE  :: wp_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: wd_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: wd_read_store_v(:, :)
        REAL(wp), ALLOCATABLE  :: LML_wp_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: LML_wd_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: LML_wd_read_store_v(:, :)
        INTEGER :: mat_size_wd
        INTEGER :: mat_size_wp
        
        INTEGER, ALLOCATABLE  :: L_block_nchan(:)
        INTEGER, ALLOCATABLE  :: LML_block_nchan(:)
        INTEGER(INT32) :: L_block_tot_nchan
        INTEGER(INT32) :: LML_block_tot_nchan

        INTEGER, ALLOCATABLE                  :: L_block_lrgl(:)
        INTEGER, ALLOCATABLE                  :: L_block_nspn(:)
        INTEGER, ALLOCATABLE                  :: L_block_npty(:)

        INTEGER, ALLOCATABLE                  :: LML_block_lrgl(:)
        INTEGER, ALLOCATABLE                  :: LML_block_l2p(:, :)
        INTEGER, ALLOCATABLE                  :: LML_block_npty(:)
        INTEGER, ALLOCATABLE                  :: LML_block_nspn(:)
        INTEGER, ALLOCATABLE                  :: LML_block_ml(:)
        INTEGER, ALLOCATABLE                  :: LML_block_nconat(:, :)

        INTEGER(INT32)                  :: coupling_id
        INTEGER(INT32)                  :: no_of_L_blocks
        INTEGER(INT32)                  :: no_of_LML_blocks
        INTEGER(INT32)                  :: nstmx
        INTEGER(INT32)                  :: ntarg
        INTEGER(INT32)                  :: ML_Max

        LOGICAL                   :: debug
        LOGICAL                   :: dipole_velocity_output 
        LOGICAL, ALLOCATABLE                ::xy_plane_desired(:)

        INTEGER, ALLOCATABLE                :: loc_of_blocks(:)

        loc_of_blocks = [6, 3]
        lplusp = 0
        L_block_nchan = [2,2,5,5,6,6]
        L_block_tot_nchan = 26
        LML_block_nchan = [2,5,5,5,6,6,6,6,6]
        LML_block_tot_nchan = 47
        ntarg = 2
        nstmx = 318
        no_of_L_blocks = 6
        no_of_LML_blocks = 9
        coupling_id = 2
        dipole_velocity_output = .FALSE.
        L_block_lrgl = [0,0,2,2,4,4]
        L_block_nspn = [0,0,0,0,0,0]
        L_block_npty = [0,1,0,1,0,1]
        LML_block_nspn = [0,0,0,0,0,0,0,0,0]
        LML_block_lrgl = [0,2,2,2,4,4,4,4,4]
        LML_block_l2p =  RESHAPE([1,1,0,0,0,0,1,1,3,1,1,0,2,0,2,0,2,0,2,0,2,0,2,0,1,3,&
                            1,3,1,3,1,3,1,3,1,3,1,3,1,3,1,3,0,2,2,4,2,2,0,2,2,4,2,2], &
                            [6,9])
        LML_block_npty = [0,0,1,1,0,0,0,1,1]
        LML_block_ml = [0,0,-2,2,-4,0,4,-2,2]
        LML_block_nconat = RESHAPE([1,1,3,2,3,2,3,2,4,2,4,2,4,2,4,2,4,2], [2,9])
        ML_Max = 4
        xy_plane_desired = [.true.]
        debug = .FALSE.
        mat_size_wd = 44
        mat_size_wp = 44

        expected = expected_D_file( &
                                    ntarg = 2_INT32, &
                                    fintr = 6_INT32, &
                                    intr = 6_INT32, &

                                    blocks = [ &
                                        expected_dipole_block( &
                                        noterm = 45_INT32, &
                                        isi = 318_INT32, &
                                        inch = 6_INT32, &
                                        ILrgL = 4_INT32, &
                                        IPty = 1_INT32, &
                                        ISpn = 0_INT32, &
                                        jsi = 286_INT32, &
                                        jnch = 6_INT32, &
                                        JLrgL = 4_INT32, &
                                        lgsf = 0_INT32, &
                                        lglf = 4_INT32, &
                                        lgpf = 0_INT32, &
                                        lgsi = 0_INT32, &
                                        lgli = 4_INT32, &
                                        lgpi = 1_INT32, &
                                        iidip=[6_INT32], &
                                        ifdip=[5_INT32], &
                                        temp_dipsto=subarray( &
                                            [ &
                                                -2.604242E-05_REAL64, &
                                                5.532279E-06_REAL64, &
                                                -9.953791E-06_REAL64, &
                                                -7.842459E-06_REAL64, &
                                                4.196564E-06_REAL64, &
                                                -2.645052E-05_REAL64, &
                                                -8.169160E-05_REAL64, &
                                                4.788274E-05_REAL64, &
                                                -7.141468E-05_REAL64, &
                                                -6.761512E-05_REAL64, &
                                                -2.428795E-06_REAL64, &
                                                4.817715E-05_REAL64, &
                                                -2.774499E-05_REAL64, &
                                                -2.362459E-05_REAL64, &
                                                3.827272E-05_REAL64, &
                                                -2.632454E-05_REAL64, &
                                                1.299342E-06_REAL64, &
                                                2.358964E-05_REAL64, &
                                                -1.462524E-05_REAL64, &
                                                1.795396E-05_REAL64, &
                                                -1.618866E-05_REAL64, &
                                                -5.807354E-06_REAL64, &
                                                4.573831E-07_REAL64, &
                                                -3.520304E-06_REAL64, &
                                                -2.518161E-06_REAL64, &
                                                1.861118E-06_REAL64, &
                                                -1.776840E-06_REAL64 &                                                               
                                            ], &
                                            [range(100,126), range(1,1)], &
                                            [318, 286] &
                                        ), &
                                        temp_dipsto_v=subarray( &
                                            [ &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64 &
                                            ], &
                                            [range(100,126), range(1,1)], &
                                            [318, 286] &
                                            ) &
                                        ), &

                                        expected_dipole_block( &
                                        noterm = 45_INT32, &
                                        isi = 264_INT32, &
                                        inch = 5_INT32, &
                                        ILrgL = 2_INT32, &
                                        IPty = 1_INT32, &
                                        ISpn = 0_INT32, &
                                        jsi = 239_INT32, &
                                        jnch = 5_INT32, &
                                        JLrgL = 2_INT32, &
                                        lgsf = 0_INT32, &
                                        lglf = 2_INT32, &
                                        lgpf = 0_INT32, &
                                        lgsi = 0_INT32, &
                                        lgli = 2_INT32, &
                                        lgpi = 1_INT32, &
                                        iidip=[4_INT32], &
                                        ifdip=[3_INT32], &
                                        temp_dipsto=subarray( &
                                            [ &
                                                2.261346E-09_REAL64, &
                                                4.871482E-08_REAL64, &
                                                1.994579E-08_REAL64, &
                                                8.226698E-09_REAL64, &
                                                1.337868E-09_REAL64, &
                                                4.264024E-09_REAL64, &
                                                -3.349140E-08_REAL64, &
                                                -1.420389E-08_REAL64, &
                                                -5.021653E-09_REAL64, &
                                                9.871002E-10_REAL64, &
                                                -3.595584E-09_REAL64, &
                                                3.355490E-08_REAL64, &
                                                1.516189E-08_REAL64, &
                                                4.300978E-09_REAL64, &
                                                -2.668514E-09_REAL64, &
                                                3.482180E-09_REAL64, &
                                                -3.206456E-08_REAL64, &
                                                -1.670800E-08_REAL64, &
                                                3.831373E-09_REAL64, &
                                                -2.719073E-09_REAL64, &
                                                3.825981E-09_REAL64, &
                                                -8.903322E-09_REAL64, &
                                                3.350593E-08_REAL64, &
                                                -1.227080E-08_REAL64, &
                                                -7.564951E-09_REAL64, &
                                                7.780812E-09_REAL64, &
                                                -1.797794E-08_REAL64, &
                                                1.131258E-08_REAL64, &
                                                9.860690E-09_REAL64, &
                                                -6.603192E-09_REAL64, &
                                                8.916447E-09_REAL64, &
                                                1.511151E-08_REAL64, &
                                                6.367654E-09_REAL64, &
                                                1.127193E-08_REAL64, &
                                                1.006711E-09_REAL64, &
                                                1.907566E-08_REAL64, &
                                                4.126247E-09_REAL64, &
                                                -5.623592E-09_REAL64, &
                                                -2.970439E-10_REAL64, &
                                                3.742253E-09_REAL64, &
                                                4.608860E-09_REAL64, &
                                                -8.530392E-10_REAL64, &
                                                -2.873481E-11_REAL64, &
                                                2.342264E-09_REAL64, &
                                                4.140302E-09_REAL64, &
                                                2.849067E-09_REAL64, &
                                                -3.598918E-09_REAL64, &
                                                7.378183E-10_REAL64, &
                                                -3.466569E-09_REAL64, &
                                                2.884705E-09_REAL64, &
                                                -7.522486E-10_REAL64 &                                                          
                                            ], &
                                            [range(150,200), range(1,1)], &
                                            [264, 239] &
                                        ), &
                                        temp_dipsto_v=subarray( &
                                            [ &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64 &
                                            ], &
                                            [range(150,200), range(1,1)], &
                                            [264, 239] &
                                            ) &
                                        ) &
                                    ] &
                                )

        CALL test_D_file(error, &
                        "atomic_tests/small_tests/Example_Ar_jk/inputs/", &
                        expected, &
                        lplusp, &
                        crlv, &
                        block_ind, &
                        dipole_coupled, &
                        !wp_read_store, &
                        wd_read_store, &
                        wd_read_store_v, &
                        LML_wp_read_store, &
                        LML_wd_read_store, &
                        LML_wd_read_store_v, &
                        L_block_nchan, &
                        L_block_tot_nchan, &
                        LML_block_nchan, &
                        LML_block_tot_nchan, &
                        ntarg, &
                        nstmx, &
                        no_of_L_blocks, &
                        no_of_LML_blocks, &
                        coupling_id, &
                        dipole_velocity_output, &
                        L_block_lrgl, &
                        L_block_nspn, &
                        L_block_npty, &
                        LML_block_nspn, &
                        LML_block_lrgl, &
                        LML_block_l2p, &
                        LML_block_npty, &
                        LML_block_ml, &
                        LML_block_nconat, &
                        ML_Max, &
                        xy_plane_desired, &
                        debug, &
                        mat_size_wd, &
                        mat_size_wp, &
                        loc_of_blocks)
    END SUBROUTINE test_RMatrix1

    SUBROUTINE test_RMatrix1_iodine(error) 
        
        TYPE(error_type), ALLOCATABLE, INTENT(out) :: error
        TYPE(expected_D_file)               :: expected
        
        INTEGER(INT32) :: lplusp
        REAL(wp), ALLOCATABLE  :: crlv(:, :)
        INTEGER, ALLOCATABLE  :: iidip(:)
        INTEGER, ALLOCATABLE  :: ifdip(:)
        INTEGER, ALLOCATABLE  :: dipole_coupled(:, :)
        INTEGER, ALLOCATABLE  :: block_ind(:, :, :)

        !REAL(wp), ALLOCATABLE  :: wp_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: wd_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: wd_read_store_v(:, :)
        REAL(wp), ALLOCATABLE  :: LML_wp_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: LML_wd_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: LML_wd_read_store_v(:, :)
        INTEGER :: mat_size_wd
        INTEGER :: mat_size_wp
        
        INTEGER, ALLOCATABLE  :: L_block_nchan(:)
        INTEGER, ALLOCATABLE  :: LML_block_nchan(:)
        INTEGER(INT32) :: L_block_tot_nchan
        INTEGER(INT32) :: LML_block_tot_nchan

        INTEGER, ALLOCATABLE                  :: L_block_lrgl(:)
        INTEGER, ALLOCATABLE                  :: L_block_nspn(:)
        INTEGER, ALLOCATABLE                  :: L_block_npty(:)

        INTEGER, ALLOCATABLE                  :: LML_block_lrgl(:)
        INTEGER, ALLOCATABLE                  :: LML_block_l2p(:, :)
        INTEGER, ALLOCATABLE                  :: LML_block_npty(:)
        INTEGER, ALLOCATABLE                  :: LML_block_nspn(:)
        INTEGER, ALLOCATABLE                  :: LML_block_ml(:)
        INTEGER, ALLOCATABLE                  :: LML_block_nconat(:, :)

        INTEGER(INT32)                  :: coupling_id
        INTEGER(INT32)                  :: no_of_L_blocks
        INTEGER(INT32)                  :: no_of_LML_blocks
        INTEGER(INT32)                  :: nstmx
        INTEGER(INT32)                  :: ntarg
        INTEGER(INT32)                  :: ML_Max

        LOGICAL                   :: debug
        LOGICAL                   :: dipole_velocity_output 
        LOGICAL, ALLOCATABLE           :: xy_plane_desired(:)

        INTEGER, ALLOCATABLE                :: loc_of_blocks(:)

        loc_of_blocks = [7, 3]
        lplusp = 0
        L_block_nchan = [5,5,8,8,9,9]
        L_block_tot_nchan = 44
        LML_block_nchan = [5,5,5,5,8,8,8,8,8,8,8,8,9,9,9,9,9,9,9,9]
        LML_block_tot_nchan = 156
        ntarg = 3
        nstmx = 396
        no_of_L_blocks = 6
        no_of_LML_blocks = 20
        coupling_id = 2
        dipole_velocity_output = .TRUE.
        L_block_lrgl = [1,1,3,3,5,5]
        L_block_nspn = [0,0,0,0,0,0]
        L_block_npty = [0,1,0,1,0,1]
        LML_block_nspn = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        LML_block_lrgl = [1,1,1,1,3,3,3,3,3,3,3,3,5,5,5,5,5,5,5,5]
        LML_block_l2p =  RESHAPE([2,2,0,0,2,0,0,0,0,2,2,0,0,2,0,0,0, &
                                    0,1,3,1,1,1,0,0,0,0,1,3,1,1,1,0,0, &
                                    0,0,2,0,2,4,2,0,2,2,0,2,0,2,4,2,0,2, &
                                    2,0,2,0,2,4,2,0,2,2,0,2,0,2,4,2,0,2,2, &
                                    0,1,3,1,3,1,1,1,3,0,1,3,1,3,1,1,1,3,0,1, &
                                    3,1,3,1,1,1,3,0,1,3,1,3,1,1,1,3,0,0,2,4,2, &
                                    4,2,2,2,4,0,2,4,2,4,2,2,2,4,0,2,4,2,4,2,2,2, &
                                    4,0,2,4,2,4,2,2,2,4,1,3,1,3,5,3,1,3,3,1,3,1,3, &
                                    5,3,1,3,3,1,3,1,3,5,3,1,3,3,1,3,1,3,5,3,1,3,3],[9,20])
        LML_block_npty = [0,0,1,1,0,0,0,0,1,1,1,1,0,0,0,0,1,1,1,1]
        LML_block_ml = [-1,1,-1,1,-3,-1,1,3,-3,-1,1,3,-3,-1,1,3,-3,-1,1,3]
        LML_block_nconat = RESHAPE([2,1,2,2,1,2,2,1,2,2,1,2,4,1,3,4, &
                                    1,3,4,1,3,4,1,3,4,1,3,4,1,3,4,1,3, & 
                                    4,1,3,5,1,3,5,1,3,5,1,3,5,1,3,5,1,3, &
                                    5,1,3,5,1,3,5,1,3], [3,20])
        ML_Max = 3
        xy_plane_desired = [.FALSE.]
        debug = .FALSE.
        mat_size_wd = 119
        mat_size_wp = 119

        expected = expected_D_file( &
                                    ntarg = 3_INT32, &
                                    fintr = 7_INT32, &
                                    intr = 7_INT32, &

                                    blocks = [ &
                                        expected_dipole_block( &
                                        noterm = 44_INT32, &
                                        isi = 396_INT32, &
                                        inch = 9_INT32, &
                                        ILrgL = 5_INT32, &
                                        IPty = 1_INT32, &
                                        ISpn = 0_INT32, &
                                        jsi = 396_INT32, &
                                        jnch = 9_INT32, &
                                        JLrgL = 5_INT32, &
                                        lgsf = 0_INT32, &
                                        lglf = 5_INT32, &
                                        lgpf = 0_INT32, &
                                        lgsi = 0_INT32, &
                                        lgli = 5_INT32, &
                                        lgpi = 1_INT32, &
                                        iidip=[6_INT32], &
                                        ifdip=[5_INT32], &
                                        temp_dipsto=subarray( &
                                            [ &
                                                -5.668147E-07_REAL64, &
                                                -2.397423E-07_REAL64, &
                                                -4.168313E-07_REAL64, &
                                                1.311586E-03_REAL64, &
                                                -8.550166E-03_REAL64, &
                                                8.109363E-03_REAL64, &
                                                6.447128E-11_REAL64, &
                                                6.774633E-07_REAL64, &
                                                3.824381E-07_REAL64, &
                                                -1.075862E-07_REAL64, &
                                                2.794659E-09_REAL64, &
                                                2.014559E-07_REAL64, &
                                                1.727244E-03_REAL64, &
                                                7.782738E-03_REAL64, &
                                                7.479141E-03_REAL64, &
                                                3.351302E-08_REAL64, &
                                                -2.586902E-06_REAL64, &
                                                -4.817205E-07_REAL64, &
                                                1.896272E-06_REAL64, &
                                                -1.869922E-06_REAL64, &
                                                4.259233E-07_REAL64, &
                                                3.275544E-06_REAL64, &
                                                1.588784E-04_REAL64, &
                                                4.414661E-04_REAL64, &
                                                -2.438509E-08_REAL64, &
                                                -4.530332E-06_REAL64 &                                                             
                                            ], &
                                            [range(50,75), range(1,1)], &
                                            [396, 396] &
                                        ), &
                                        temp_dipsto_v=subarray( &
                                            [ &
                                                -6.253620E-08_REAL64, &
                                                1.400523E-08_REAL64, &
                                                5.544304E-08_REAL64, &
                                                -1.362857E-09_REAL64, &
                                                1.824852E-05_REAL64, &
                                                1.137791E-04_REAL64, &
                                                -1.571734E-04_REAL64, &
                                                -8.709737E-07_REAL64, &
                                                -8.016802E-07_REAL64, &
                                                6.898078E-07_REAL64, &
                                                -2.432627E-07_REAL64, &
                                                3.680345E-07_REAL64, &
                                                -2.072975E-09_REAL64, &
                                                -1.086698E-05_REAL64, &
                                                -6.093338E-05_REAL64, &
                                                8.424174E-05_REAL64, &
                                                -5.967976E-07_REAL64, &
                                                7.565061E-07_REAL64, &
                                                -5.790019E-07_REAL64, &
                                                2.905502E-07_REAL64, &
                                                -2.958708E-07_REAL64, &
                                                8.637693E-10_REAL64, &
                                                1.285515E-06_REAL64, &
                                                5.725379E-06_REAL64, &
                                                -6.552988E-06_REAL64, &
                                                1.457326E-09_REAL64 &
                                            ], &
                                            [range(200,225), range(1,1)], &
                                            [396, 396] &
                                            ) &
                                        ), &
                                        expected_dipole_block( &
                                        noterm = 44_INT32, &
                                        isi = 221_INT32, &
                                        inch = 5_INT32, &
                                        ILrgL = 1_INT32, &
                                        IPty = 1_INT32, &
                                        ISpn = 0_INT32, &
                                        jsi = 352_INT32, &
                                        jnch = 8_INT32, &
                                        JLrgL = 3_INT32, &
                                        lgsf = 0_INT32, &
                                        lglf = 3_INT32, &
                                        lgpf = 0_INT32, &
                                        lgsi = 0_INT32, &
                                        lgli = 1_INT32, &
                                        lgpi = 1_INT32, &
                                        iidip=[2_INT32], &
                                        ifdip=[3_INT32], &
                                        temp_dipsto=subarray( &
                                            [ &
                                                -1.162138E-07_REAL64, &
                                                -3.114269E-07_REAL64, &
                                                8.301986E-07_REAL64, &
                                                7.794539E-08_REAL64, &
                                                -3.837095E-07_REAL64, &
                                                1.389312E-07_REAL64, &
                                                -1.398199E-07_REAL64, &
                                                -7.738288E-07_REAL64, &
                                                2.773268E-07_REAL64, &
                                                3.322201E-07_REAL64, &
                                                -9.847763E-08_REAL64, &
                                                -6.504981E-08_REAL64, &
                                                8.033558E-07_REAL64, &
                                                -2.259968E-07_REAL64, &
                                                -3.161438E-07_REAL64, &
                                                -5.935717E-08_REAL64, &
                                                7.156708E-08_REAL64, &
                                                -8.067814E-07_REAL64, &
                                                8.545304E-08_REAL64, &
                                                2.945576E-07_REAL64, &
                                                -4.292197E-08_REAL64, &
                                                -6.441531E-08_REAL64, &
                                                7.866623E-07_REAL64, &
                                                6.406923E-08_REAL64, &
                                                -2.751864E-07_REAL64, &
                                                3.330157E-08_REAL64, &
                                                5.449162E-08_REAL64, &
                                                -7.422650E-07_REAL64, &
                                                -1.724730E-07_REAL64, &
                                                2.550040E-07_REAL64, &
                                                -2.646989E-08_REAL64, &
                                                -4.532875E-08_REAL64, &
                                                6.892062E-07_REAL64, &
                                                2.362177E-07_REAL64, &
                                                -2.335342E-07_REAL64, &
                                                2.299389E-08_REAL64, &
                                                4.009501E-08_REAL64, &
                                                -6.252357E-07_REAL64, &
                                                -2.803430E-07_REAL64, &
                                                2.129804E-07_REAL64, &
                                                1.426564E-08_REAL64, &
                                                -4.049565E-08_REAL64, &
                                                9.432768E-08_REAL64, &
                                                5.919122E-07_REAL64, &
                                                2.008457E-07_REAL64, &
                                                1.826069E-07_REAL64, &
                                                -2.388854E-08_REAL64, &
                                                5.238904E-08_REAL64, &
                                                -5.161634E-07_REAL64, &
                                                -2.240889E-07_REAL64, &
                                                -1.533803E-07_REAL64 &                                                                
                                            ], &
                                            [range(150,200), range(1,1)], &
                                            [221, 352] &
                                        ), &
                                        temp_dipsto_v=subarray( &
                                            [ &
                                                1.272147E-03_REAL64, &
                                                2.212323E-03_REAL64, &
                                                -8.356221E-06_REAL64, &
                                                7.160750E-06_REAL64, &
                                                -1.381003E-03_REAL64, &
                                                2.799034E-03_REAL64, &
                                                3.296870E-05_REAL64, &
                                                -2.516362E-05_REAL64, &
                                                1.359037E-03_REAL64, &
                                                -1.491050E-05_REAL64, &
                                                -3.927381E-03_REAL64, &
                                                1.642881E-10_REAL64, &
                                                7.988255E-09_REAL64, &
                                                -1.749685E-03_REAL64, &
                                                2.177652E-05_REAL64, &
                                                4.943199E-03_REAL64, &
                                                3.244329E-08_REAL64, &
                                                -5.592362E-08_REAL64, &
                                                2.099992E-03_REAL64, &
                                                -1.950329E-05_REAL64, &
                                                5.992491E-03_REAL64, &
                                                1.762893E-06_REAL64, &
                                                -1.259365E-04_REAL64, &
                                                -2.031475E-04_REAL64, &
                                                3.469507E-08_REAL64, &
                                                1.088618E-06_REAL64, &
                                                -7.349944E-07_REAL64, &
                                                2.064889E-07_REAL64, &
                                                2.301919E-03_REAL64, &
                                                1.284449E-04_REAL64, &
                                                -6.543648E-03_REAL64, &
                                                -4.562781E-05_REAL64, &
                                                4.648585E-07_REAL64, &
                                                2.229099E-03_REAL64, &
                                                -4.245161E-04_REAL64, &
                                                -6.430046E-03_REAL64, &
                                                -6.067898E-04_REAL64, &
                                                1.673909E-07_REAL64, &
                                                1.867541E-03_REAL64, &
                                                -8.781675E-04_REAL64, &
                                                5.062239E-03_REAL64, &
                                                -3.125103E-03_REAL64, &
                                                -4.938148E-05_REAL64, &
                                                3.533068E-04_REAL64, &
                                                7.012291E-07_REAL64, &
                                                -2.918717E-06_REAL64, &
                                                -6.816487E-06_REAL64, &
                                                4.683406E-06_REAL64, &
                                                -1.283709E-03_REAL64, &
                                                -1.185190E-03_REAL64 &
                                            ], &
                                            [range(1,50), range(1,1)], &
                                            [221, 352] &
                                            ) &
                                        ) &
                                    ] &
                                )

        CALL test_D_file(error, &
                        "atomic_tests/small_tests/iodine/inputs/", &
                        expected, &
                        lplusp, &
                        crlv, &
                        block_ind, &
                        dipole_coupled, &
                        !wp_read_store, &
                        wd_read_store, &
                        wd_read_store_v, &
                        LML_wp_read_store, &
                        LML_wd_read_store, &
                        LML_wd_read_store_v, &
                        L_block_nchan, &
                        L_block_tot_nchan, &
                        LML_block_nchan, &
                        LML_block_tot_nchan, &
                        ntarg, &
                        nstmx, &
                        no_of_L_blocks, &
                        no_of_LML_blocks, &
                        coupling_id, &
                        dipole_velocity_output, &
                        L_block_lrgl, &
                        L_block_nspn, &
                        L_block_npty, &
                        LML_block_nspn, &
                        LML_block_lrgl, &
                        LML_block_l2p, &
                        LML_block_npty, &
                        LML_block_ml, &
                        LML_block_nconat, &
                        ML_Max, &
                        xy_plane_desired, &
                        debug, &
                        mat_size_wd, &
                        mat_size_wp, &
                        loc_of_blocks)
    END SUBROUTINE test_RMatrix1_iodine

    SUBROUTINE test_RMatrix1_Ar_jk(error)

        TYPE(error_type), ALLOCATABLE, INTENT(out) :: error
        TYPE(expected_D_file)               :: expected
        
        INTEGER(INT32) :: lplusp
        REAL(wp), ALLOCATABLE  :: crlv(:, :)
        INTEGER, ALLOCATABLE  :: iidip(:)
        INTEGER, ALLOCATABLE  :: ifdip(:)
        INTEGER, ALLOCATABLE  :: dipole_coupled(:, :)
        INTEGER, ALLOCATABLE  :: block_ind(:, :, :)

        !REAL(wp), ALLOCATABLE  :: wp_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: wd_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: wd_read_store_v(:, :)
        REAL(wp), ALLOCATABLE  :: LML_wp_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: LML_wd_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: LML_wd_read_store_v(:, :)
        INTEGER :: mat_size_wd
        INTEGER :: mat_size_wp
        
        INTEGER, ALLOCATABLE  :: L_block_nchan(:)
        INTEGER, ALLOCATABLE  :: LML_block_nchan(:)
        INTEGER(INT32) :: L_block_tot_nchan
        INTEGER(INT32) :: LML_block_tot_nchan

        INTEGER, ALLOCATABLE                  :: L_block_lrgl(:)
        INTEGER, ALLOCATABLE                  :: L_block_nspn(:)
        INTEGER, ALLOCATABLE                  :: L_block_npty(:)

        INTEGER, ALLOCATABLE                  :: LML_block_lrgl(:)
        INTEGER, ALLOCATABLE                  :: LML_block_l2p(:, :)
        INTEGER, ALLOCATABLE                  :: LML_block_npty(:)
        INTEGER, ALLOCATABLE                  :: LML_block_nspn(:)
        INTEGER, ALLOCATABLE                  :: LML_block_ml(:)
        INTEGER, ALLOCATABLE                  :: LML_block_nconat(:, :)

        INTEGER(INT32)                  :: coupling_id
        INTEGER(INT32)                  :: no_of_L_blocks
        INTEGER(INT32)                  :: no_of_LML_blocks
        INTEGER(INT32)                  :: nstmx
        INTEGER(INT32)                  :: ntarg
        INTEGER(INT32)                  :: ML_Max

        LOGICAL                   :: debug
        LOGICAL                   :: dipole_velocity_output 
        LOGICAL, ALLOCATABLE           :: xy_plane_desired(:)

        INTEGER, ALLOCATABLE                :: loc_of_blocks(:)

        loc_of_blocks = [3]
        lplusp = 0
        L_block_nchan = [3,3,7,7,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8]
        L_block_tot_nchan = 196
        LML_block_nchan = [3,7,7,7,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8, &
                            8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8, &
                            8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8, &
                            8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8, &
                            8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8, &
                            8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8, &
                            8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8, &
                            8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8, &
                            8,8,8,8,8,8,8,8,8]
        LML_block_tot_nchan = 1344
        ntarg = 3
        nstmx = 368
        no_of_L_blocks = 26
        no_of_LML_blocks = 169
        coupling_id = 2
        dipole_velocity_output = .FALSE.
        L_block_lrgl = [0, 0, 2, 2, 4, 4, 6, 6, 8, 8,10,10,12,12,14,14,16,16,18,18,20,20,22,22,24,24]
        L_block_nspn = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        L_block_npty = [0,  1,  0,  1,  0,  1,  0,  1,  0,  1,  0,  1,  0,  1,  0,  1,  0,  1,  0,  1,  0,  1,  0,  1,  0,  1]
        LML_block_nspn = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, &
                            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, &
                            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, &
                            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, &
                            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, &
                            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, &
                            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, &
                            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, &
                            0,0,0,0,0,0,0,0,0]
        LML_block_lrgl = [0, 2, 2, 2, 4, 4, 4, 4, 4, 6, 6, 6, 6, 6, 6, 6, 8, 8, 8, 8, &
                            8, 8, 8, 8, 8,10,10,10,10,10,10,10,10,10,10,10,12,12,12,12, &
                            12,12,12,12,12,12,12,12,12,14,14,14,14,14,14,14,14,14,14,14, &
                            14,14,14,14,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16, &
                            16,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18, &
                            20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20, &
                            20,22,22,22,22,22,22,22,22,22,22,22,22,22,22,22,22,22,22,22, &
                            22,22,22,22,24,24,24,24,24,24,24,24,24,24,24,24,24,24,24,24, &
                            24,24,24,24,24,24,24,24,24]
        LML_block_l2p =  RESHAPE([1,1,0,0,0,0,0,0,1,1,3,1,1,0,2,0,2,0,2, &
                                    0,2,1,1,0,2,0,2,0,2,1,1,0,1,3,1,3,1,3,2, &
                                    2,1,3,1,3,1,3,2,2,1,3,1,3,1,3,2,2,0,2,2, &
                                    4,2,2,1,3,0,2,2,4,2,2,1,3,1,3,3,5,3,3,2, &
                                    4,1,3,3,5,3,3,2,4,1,3,3,5,3,3,2,4,2,4,2, &
                                    4,2,4,3,3,2,4,2,4,2,4,3,3,2,4,2,4,2,4,3, &
                                    3,2,4,2,4,2,4,3,3,3,5,3,5,3,5,4,4,3,5,3, &
                                    5,3,5,4,4,3,5,3,5,3,5,4,4,3,5,3,5,3,5,4, &
                                    4,3,5,3,5,3,5,4,4,2,4,4,6,4,4,3,5,2,4,4, &
                                    6,4,4,3,5,2,4,4,6,4,4,3,5,2,4,4,6,4,4,3, &
                                    5,3,5,5,7,5,5,4,6,3,5,5,7,5,5,4,6,3,5,5, &
                                    7,5,5,4,6,3,5,5,7,5,5,4,6,3,5,5,7,5,5,4, &
                                    6,4,6,4,6,4,6,5,5,4,6,4,6,4,6,5,5,4,6,4, &
                                    6,4,6,5,5,4,6,4,6,4,6,5,5,4,6,4,6,4,6,5, &
                                    5,4,6,4,6,4,6,5,5,5,7,5,7,5,7,6,6,5,7,5, &
                                    7,5,7,6,6,5,7,5,7,5,7,6,6,5,7,5,7,5,7,6, &
                                    6,5,7,5,7,5,7,6,6,5,7,5,7,5,7,6,6,5,7,5, &
                                    7,5,7,6,6,4,6,6,8,6,6,5,7,4,6,6,8,6,6,5, &
                                    7,4,6,6,8,6,6,5,7,4,6,6,8,6,6,5,7,4,6,6, &
                                    8,6,6,5,7,4,6,6,8,6,6,5,7,5,7,7,9,7,7,6, &
                                    8,5,7,7,9,7,7,6,8,5,7,7,9,7,7,6,8,5,7,7, &
                                    9,7,7,6,8,5,7,7,9,7,7,6,8,5,7,7,9,7,7,6, &
                                    8,5,7,7,9,7,7,6,8,6,8,6,8,6,8,7,7,6,8,6, &
                                    8,6,8,7,7,6,8,6,8,6,8,7,7,6,8,6,8,6,8,7, &
                                    7,6,8,6,8,6,8,7,7,6,8,6,8,6,8,7,7,6,8,6, &
                                    8,6,8,7,7,6,8,6,8,6,8,7,7,7,9,7,9,7,9,8, &
                                    8,7,9,7,9,7,9,8,8,7,9,7,9,7,9,8,8,7,9,7, &
                                    9,7,9,8,8,7,9,7,9,7,9,8,8,7,9,7,9,7,9,8, &
                                    8,7,9,7,9,7,9,8,8,7,9,7,9,7,9,8,8,7,9,7, &
                                    9,7,9,8,8,6,8,8,10,8,8,7,9,6,8,8,10,8,8,7, &
                                    9,6,8,8,10,8,8,7,9,6,8,8,10,8,8,7,9,6,8,8, &
                                    10,8,8,7,9,6,8,8,10,8,8,7,9,6,8,8,10,8,8,7, &
                                    9,6,8,8,10,8,8,7,9,7,9,9,11,9,9,8,10,7,9,9, &
                                    11,9,9,8,10,7,9,9,11,9,9,8,10,7,9,9,11,9,9,8, &
                                    10,7,9,9,11,9,9,8,10,7,9,9,11,9,9,8,10,7,9,9, &
                                    11,9,9,8,10,7,9,9,11,9,9,8,10,7,9,9,11,9,9,8, &
                                    10,8,10,8,10,8,10,9,9,8,10,8,10,8,10,9,9,8,10,8, &
                                    10,8,10,9,9,8,10,8,10,8,10,9,9,8,10,8,10,8,10,9, &
                                    9,8,10,8,10,8,10,9,9,8,10,8,10,8,10,9,9,8,10,8, &
                                    10,8,10,9,9,8,10,8,10,8,10,9,9,8,10,8,10,8,10,9, &
                                    9,9,11,9,11,9,11,10,10,9,11,9,11,9,11,10,10,9,11,9, &
                                    11,9,11,10,10,9,11,9,11,9,11,10,10,9,11,9,11,9,11,10, &
                                    10,9,11,9,11,9,11,10,10,9,11,9,11,9,11,10,10,9,11,9, &
                                    11,9,11,10,10,9,11,9,11,9,11,10,10,9,11,9,11,9,11,10, &
                                    10,9,11,9,11,9,11,10,10,8,10,10,12,10,10,9,11,8,10,10, &
                                    12,10,10,9,11,8,10,10,12,10,10,9,11,8,10,10,12,10,10,9, &
                                    11,8,10,10,12,10,10,9,11,8,10,10,12,10,10,9,11,8,10,10, &
                                    12,10,10,9,11,8,10,10,12,10,10,9,11,8,10,10,12,10,10,9, &
                                    11,8,10,10,12,10,10,9,11,9,11,11,13,11,11,10,12,9,11,11, &
                                    13,11,11,10,12,9,11,11,13,11,11,10,12,9,11,11,13,11,11,10, &
                                    12,9,11,11,13,11,11,10,12,9,11,11,13,11,11,10,12,9,11,11, &
                                    13,11,11,10,12,9,11,11,13,11,11,10,12,9,11,11,13,11,11,10, &
                                    12,9,11,11,13,11,11,10,12,9,11,11,13,11,11,10,12,10,12,10, &
                                    12,10,12,11,11,10,12,10,12,10,12,11,11,10,12,10,12,10,12,11, &
                                    11,10,12,10,12,10,12,11,11,10,12,10,12,10,12,11,11,10,12,10, &
                                    12,10,12,11,11,10,12,10,12,10,12,11,11,10,12,10,12,10,12,11, &
                                    11,10,12,10,12,10,12,11,11,10,12,10,12,10,12,11,11,10,12,10, &
                                    12,10,12,11,11,10,12,10,12,10,12,11,11,11,13,11,13,11,13,12, &
                                    12,11,13,11,13,11,13,12,12,11,13,11,13,11,13,12,12,11,13,11, &
                                    13,11,13,12,12,11,13,11,13,11,13,12,12,11,13,11,13,11,13,12, &
                                    12,11,13,11,13,11,13,12,12,11,13,11,13,11,13,12,12,11,13,11, &
                                    13,11,13,12,12,11,13,11,13,11,13,12,12,11,13,11,13,11,13,12, &
                                    12,11,13,11,13,11,13,12,12,11,13,11,13,11,13,12,12,10,12,12, &
                                    14,12,12,11,13,10,12,12,14,12,12,11,13,10,12,12,14,12,12,11, &
                                    13,10,12,12,14,12,12,11,13,10,12,12,14,12,12,11,13,10,12,12, &
                                    14,12,12,11,13,10,12,12,14,12,12,11,13,10,12,12,14,12,12,11, &
                                    13,10,12,12,14,12,12,11,13,10,12,12,14,12,12,11,13,10,12,12, &
                                    14,12,12,11,13,10,12,12,14,12,12,11,13],[8,169])
        LML_block_npty = [0,0,1,1,0,0,0,1,1,0,0,0,1,1,1,1,0,0,0,0, &
                            0,1,1,1,1,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0, &
                            0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,1,1,1,1, &
                            1,1,1,1,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1, &
                            1,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1, &
                            0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1, &
                            1,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1, &
                            1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1, &
                            1,1,1,1,1,1,1,1,1]
        LML_block_ml = [0, 0,  -2,  2,  -4,  0,  4,  -2,  2,  -4,  0,  4,  -6,  -2,  2,  6,  -8,  -4,  0,  4,  8,  &
                        -6,  -2,  2,  6,  -8,  -4,  0,  4,  8,  -10,  -6,  -2,  2,  6,  10,  -12,  -8,  -4,  0,  4, &
                        8,  12,  -10,  -6,  -2,  2,  6,  10,  -12,  -8,  -4,  0,  4,  8,  12,  -14,  -10,  -6,  -2, &
                        2,  6,  10,  14,  -16,  -12,  -8,  -4,  0,  4,  8,  12,  16,  -14,  -10,  -6,  -2,  2,  6, &
                        10,  14,  -16,  -12,  -8,  -4,  0,  4,  8,  12,  16,  -18,  -14,  -10,  -6,  -2,  2,  6,  10, &
                        14,  18,  -20,  -16,  -12,  -8,  -4,  0,  4,  8,  12,  16,  20,  -18,  -14,  -10,  -6,  -2, &
                        2,  6,  10,  14,  18,  -20,  -16,  -12,  -8,  -4,  0,  4,  8,  12,  16,  20,  -22,  -18,  -14, &
                        -10,  -6,  -2,  2,  6,  10,  14,  18,  22,  -24,  -20,  -16,  -12,  -8,  -4,  0,  4,  8,  12, &
                        16,  20,  24,  -22,  -18,  -14,  -10,  -6,  -2,  2,  6,  10,  14,  18,  22]
        LML_block_nconat = RESHAPE([1,1,1,3,2,2,3,2,2,3,2,2,4,2,2,4,2,2,4,2, &
                                    2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4, &
                                    2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2, &
                                    4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2, &
                                    2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4, &
                                    2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2, &
                                    4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2, &
                                    2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4, &
                                    2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2, &
                                    4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2, &
                                    2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4, &
                                    2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2, &
                                    4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2, &
                                    2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4, &
                                    2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2, &
                                    4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2, &
                                    2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4, &
                                    2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2, &
                                    4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2, &
                                    2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4, &
                                    2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2, &
                                    4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2, &
                                    2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4, &
                                    2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2, &
                                    4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2, &
                                    2,4,2,2,4,2,2], [3,169])
        ML_Max = 24
        xy_plane_desired = [.TRUE.]
        debug = .FALSE.
        mat_size_wd = 303
        mat_size_wp = 301

        expected = expected_D_file( &
                                    ntarg = 3_INT32, &
                                    fintr = 36_INT32, &
                                    intr = 36_INT32, &

                                    blocks = [ &
                                        expected_dipole_block( &
                                        noterm = 46_INT32, &
                                        isi = 322_INT32, &
                                        inch = 7_INT32, &
                                        ILrgL = 2_INT32, &
                                        IPty = 1_INT32, &
                                        ISpn = 0_INT32, &
                                        jsi = 322_INT32, &
                                        jnch = 7_INT32, &
                                        JLrgL = 2_INT32, &
                                        lgsf = 0_INT32, &
                                        lglf = 2_INT32, &
                                        lgpf = 0_INT32, &
                                        lgsi = 0_INT32, &
                                        lgli = 2_INT32, &
                                        lgpi = 1_INT32, &
                                        iidip=[4_INT32], &
                                        ifdip=[3_INT32], &
                                        temp_dipsto=subarray( &
                                            [ &
                                                1.585730E-15_REAL64, &
                                                -9.245093E-07_REAL64, &
                                                1.568950E-15_REAL64, &
                                                -1.184946E-06_REAL64, &
                                                6.570379E-15_REAL64, &
                                                -5.481835E-06_REAL64, &
                                                4.692580E-18_REAL64, &
                                                8.134832E-16_REAL64, &
                                                2.527686E-06_REAL64, &
                                                1.951664E-15_REAL64, &
                                                1.693885E-06_REAL64, &
                                                6.909509E-15_REAL64, &
                                                1.604539E-06_REAL64, &
                                                -1.101734E-17_REAL64, &
                                                9.198439E-16_REAL64, &
                                                -2.916285E-07_REAL64, &
                                                1.270240E-15_REAL64, &
                                                -1.483220E-07_REAL64, &
                                                1.385280E-15_REAL64, &
                                                2.987805E-07_REAL64, &
                                                -2.308563E-18_REAL64, &
                                                2.267579E-16_REAL64, &
                                                7.480676E-08_REAL64, &
                                                5.904216E-17_REAL64, &
                                                -5.910295E-08_REAL64, &
                                                1.216815E-15_REAL64, &
                                                4.641272E-07_REAL64, &
                                                -1.729094E-19_REAL64, &
                                                1.253517E-15_REAL64, &
                                                -5.696797E-08_REAL64, &
                                                9.389814E-16_REAL64, &
                                                -3.831352E-08_REAL64, &
                                                -3.858860E-16_REAL64, &
                                                -3.700782E-07_REAL64, &
                                                7.438390E-18_REAL64, &
                                                -6.881709E-17_REAL64, &
                                                3.916091E-08_REAL64, &
                                                2.458421E-16_REAL64, &
                                                2.268827E-08_REAL64, &
                                                -6.216278E-16_REAL64, &
                                                -9.680433E-08_REAL64, &
                                                -9.255444E-19_REAL64, &
                                                1.017038E-15_REAL64, &
                                                7.175825E-09_REAL64, &
                                                5.607666E-16_REAL64, &
                                                5.577526E-09_REAL64, &
                                                1.610995E-17_REAL64, &
                                                1.028142E-07_REAL64, &
                                                -8.548218E-19_REAL64, &
                                                2.964070E-16_REAL64, &
                                                7.871432E-09_REAL64 &
                                            ], &
                                            [range(150,200), range(1,1)], &
                                            [322, 322] &
                                        ), &
                                        temp_dipsto_v=subarray( &
                                            [ &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64 &
                                                ], &
                                                [range(1,50), range(1,1)], &
                                                [322, 322] &
                                                ) &
                                            ) &
                                        ] &
                                    )

        CALL test_D_file(error, &
                        "atomic_tests/big_tests/Ar_jK/inputs/", &
                        expected, &
                        lplusp, &
                        crlv, &
                        block_ind, &
                        dipole_coupled, &
                        !wp_read_store, &
                        wd_read_store, &
                        wd_read_store_v, &
                        LML_wp_read_store, &
                        LML_wd_read_store, &
                        LML_wd_read_store_v, &
                        L_block_nchan, &
                        L_block_tot_nchan, &
                        LML_block_nchan, &
                        LML_block_tot_nchan, &
                        ntarg, &
                        nstmx, &
                        no_of_L_blocks, &
                        no_of_LML_blocks, &
                        coupling_id, &
                        dipole_velocity_output, &
                        L_block_lrgl, &
                        L_block_nspn, &
                        L_block_npty, &
                        LML_block_nspn, &
                        LML_block_lrgl, &
                        LML_block_l2p, &
                        LML_block_npty, &
                        LML_block_ml, &
                        LML_block_nconat, &
                        ML_Max, &
                        xy_plane_desired, &
                        debug, &
                        mat_size_wd, &
                        mat_size_wp, &
                        loc_of_blocks)

    END SUBROUTINE test_RMatrix1_Ar_jk

    SUBROUTINE test_RMatrix1_Ar_LS(error)

        TYPE(error_type), ALLOCATABLE, INTENT(out) :: error
        TYPE(expected_D_file)               :: expected
        
        INTEGER(INT32) :: lplusp
        REAL(wp), ALLOCATABLE  :: crlv(:, :)
        INTEGER, ALLOCATABLE  :: iidip(:)
        INTEGER, ALLOCATABLE  :: ifdip(:)
        INTEGER, ALLOCATABLE  :: dipole_coupled(:, :)
        INTEGER, ALLOCATABLE  :: block_ind(:, :, :)

        !REAL(wp), ALLOCATABLE  :: wp_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: wd_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: wd_read_store_v(:, :)
        REAL(wp), ALLOCATABLE  :: LML_wp_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: LML_wd_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: LML_wd_read_store_v(:, :)
        INTEGER :: mat_size_wd
        INTEGER :: mat_size_wp
        
        INTEGER, ALLOCATABLE  :: L_block_nchan(:)
        INTEGER, ALLOCATABLE  :: LML_block_nchan(:)
        INTEGER(INT32) :: L_block_tot_nchan
        INTEGER(INT32) :: LML_block_tot_nchan

        INTEGER, ALLOCATABLE                  :: L_block_lrgl(:)
        INTEGER, ALLOCATABLE                  :: L_block_nspn(:)
        INTEGER, ALLOCATABLE                  :: L_block_npty(:)

        INTEGER, ALLOCATABLE                  :: LML_block_lrgl(:)
        INTEGER, ALLOCATABLE                  :: LML_block_l2p(:, :)
        INTEGER, ALLOCATABLE                  :: LML_block_npty(:)
        INTEGER, ALLOCATABLE                  :: LML_block_nspn(:)
        INTEGER, ALLOCATABLE                  :: LML_block_ml(:)
        INTEGER, ALLOCATABLE                  :: LML_block_nconat(:, :)

        INTEGER(INT32)                  :: coupling_id
        INTEGER(INT32)                  :: no_of_L_blocks
        INTEGER(INT32)                  :: no_of_LML_blocks
        INTEGER(INT32)                  :: nstmx
        INTEGER(INT32)                  :: ntarg
        INTEGER(INT32)                  :: ML_Max

        LOGICAL                   :: debug
        LOGICAL                   :: dipole_velocity_output 
        LOGICAL, ALLOCATABLE           :: xy_plane_desired(:)

        INTEGER, ALLOCATABLE                :: loc_of_blocks(:)

        loc_of_blocks = [10]
        lplusp = 0
        L_block_nchan = [2,1,3,3,1,1,3,3,1,1,3,3,1,1,3,3,1,1,3,3,1,1,3,3,1,1,3]
        L_block_tot_nchan = 54
        LML_block_nchan = [2,1,3,3,3,3,3,1,1,1,1,1,3,3,3,3,3,3,3,3, &
                            3,1,1,1,1,1,1,1,1,1,3,3,3,3,3,3,3,3,3,3, &    
                            3,3,3,1,1,1,1,1,1,1,1,1,1,1,1,1,3,3,3,3, &    
                            3,3,3,3,3,3,3,3,3,3,3,3,3,1,1,1,1,1,1,1, &    
                            1,1,1,1,1,1,1,1,1,1,3,3,3,3,3,3,3,3,3,3, &    
                            3,3,3,3,3,3,3,3,3,3,3,1,1,1,1,1,1,1,1,1, &    
                            1,1,1,1,1,1,1,1,1,1,1,1,3,3,3,3,3,3,3,3, &    
                            3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,1,1,1, &    
                            1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, &    
                            1,1,3,3,3,3,3,3,3,3,3,3,3,3,3,3]
        LML_block_tot_nchan = 405
        ntarg = 2
        nstmx = 138
        no_of_L_blocks = 27
        no_of_LML_blocks = 196
        coupling_id = 1
        dipole_velocity_output = .FALSE.
        L_block_lrgl = [0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9,10,10,11,11,12,12,13,13]
        L_block_nspn = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
        L_block_npty = [0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1]
        LML_block_nspn = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, &
                        1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, &
                        1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, &
                        1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, &
                        1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, &
                        1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, &
                        1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, &
                        1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, &
                        1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, &
                        1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
        LML_block_lrgl = [0, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, &
                        4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, &
                        6, 6, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, &
                        7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, &
                        8, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, &
                        10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10, &
                        10,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, &
                        11,11,11,11,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12, &
                        12,12,12,12,12,12,12,12,12,13,13,13,13,13,13,13,13,13,13,13, &
                        13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13]
        LML_block_l2p =  RESHAPE([1,0,0,1,0,0,0,2,1,0,2,1,1,3,2,1,3,2,1,3, &
                        2,2,0,0,2,0,0,3,0,0,3,0,0,3,0,0,2,4,3,2, &
                        4,3,2,4,3,2,4,3,3,5,4,3,5,4,3,5,4,3,5,4, &
                        3,5,4,4,0,0,4,0,0,4,0,0,4,0,0,5,0,0,5,0, &
                        0,5,0,0,5,0,0,5,0,0,4,6,5,4,6,5,4,6,5,4, &
                        6,5,4,6,5,4,6,5,5,7,6,5,7,6,5,7,6,5,7,6, &
                        5,7,6,5,7,6,5,7,6,6,0,0,6,0,0,6,0,0,6,0, &
                        0,6,0,0,6,0,0,7,0,0,7,0,0,7,0,0,7,0,0,7, &
                        0,0,7,0,0,7,0,0,6,8,7,6,8,7,6,8,7,6,8,7, &
                        6,8,7,6,8,7,6,8,7,6,8,7,7,9,8,7,9,8,7,9, &
                        8,7,9,8,7,9,8,7,9,8,7,9,8,7,9,8,7,9,8,8, &
                        0,0,8,0,0,8,0,0,8,0,0,8,0,0,8,0,0,8,0,0, &
                        8,0,0,9,0,0,9,0,0,9,0,0,9,0,0,9,0,0,9,0, &
                        0,9,0,0,9,0,0,9,0,0,8,10,9,8,10,9,8,10,9,8, &
                        10,9,8,10,9,8,10,9,8,10,9,8,10,9,8,10,9,8,10,9, &
                        9,11,10,9,11,10,9,11,10,9,11,10,9,11,10,9,11,10,9,11, &
                        10,9,11,10,9,11,10,9,11,10,9,11,10,10,0,0,10,0,0,10, &
                        0,0,10,0,0,10,0,0,10,0,0,10,0,0,10,0,0,10,0,0, &
                        10,0,0,11,0,0,11,0,0,11,0,0,11,0,0,11,0,0,11,0, &
                        0,11,0,0,11,0,0,11,0,0,11,0,0,11,0,0,10,12,11,10, &
                        12,11,10,12,11,10,12,11,10,12,11,10,12,11,10,12,11,10,12,11, &
                        10,12,11,10,12,11,10,12,11,10,12,11,11,13,12,11,13,12,11,13, &
                        12,11,13,12,11,13,12,11,13,12,11,13,12,11,13,12,11,13,12,11, &
                        13,12,11,13,12,11,13,12,11,13,12,12,0,0,12,0,0,12,0,0, &
                        12,0,0,12,0,0,12,0,0,12,0,0,12,0,0,12,0,0,12,0, &
                        0,12,0,0,12,0,0,13,0,0,13,0,0,13,0,0,13,0,0,13, &
                        0,0,13,0,0,13,0,0,13,0,0,13,0,0,13,0,0,13,0,0, &
                        13,0,0,13,0,0,12,14,13,12,14,13,12,14,13,12,14,13,12,14, &
                        13,12,14,13,12,14,13,12,14,13,12,14,13,12,14,13,12,14,13,12, &
                        14,13,12,14,13,12,14,13],[3,196])
        LML_block_npty = [0,0,1,1,0,0,0,1,1,0,0,0,1,1,1,1,0,0,0,0, &
                        0,1,1,1,1,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0, &
                        0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,1,1,1,1, &
                        1,1,1,1,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1, &
                        1,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1, &
                        0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1, &
                        1,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1, &
                        1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1, &
                        1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0, &
                        0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
        LML_block_ml = [0, 0,-1, 1,-2, 0, 2,-1, 1,-2, 0, 2,-3,-1, 1, 3,-4,-2, 0, 2, &
                        4,-3,-1, 1, 3,-4,-2, 0, 2, 4,-5,-3,-1, 1, 3, 5,-6,-4,-2, 0, &
                        2, 4, 6,-5,-3,-1, 1, 3, 5,-6,-4,-2, 0, 2, 4, 6,-7,-5,-3,-1, &
                        1, 3, 5, 7,-8,-6,-4,-2, 0, 2, 4, 6, 8,-7,-5,-3,-1, 1, 3, 5, &
                        7,-8,-6,-4,-2, 0, 2, 4, 6, 8,-9,-7,-5,-3,-1, 1, 3, 5, 7, 9, &
                        -10,-8,-6,-4,-2, 0, 2, 4, 6, 8,10,-9,-7,-5,-3,-1, 1, 3, 5, 7, &
                        9,-10,-8,-6,-4,-2, 0, 2, 4, 6, 8,10,-11,-9,-7,-5,-3,-1, 1, 3, &
                        5, 7, 9,11,-12,-10,-8,-6,-4,-2, 0, 2, 4, 6, 8,10,12,-11,-9,-7, &
                        -5,-3,-1, 1, 3, 5, 7, 9,11,-12,-10,-8,-6,-4,-2, 0, 2, 4, 6, 8, &
                        10,12,-13,-11,-9,-7,-5,-3,-1, 1, 3, 5, 7, 9,11,13]
        LML_block_nconat = RESHAPE([1,1,1,0,2,1,2,1,2,1,2,1,2,1,1,0,1,0,1,0, &
                            1,0,1,0,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1, &
                            2,1,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0, &
                            2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1, &
                            2,1,2,1,2,1,1,0,1,0,1,0,1,0,1,0,1,0,1,0, &
                            1,0,1,0,1,0,1,0,1,0,1,0,2,1,2,1,2,1,2,1, &
                            2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1, &
                            2,1,2,1,2,1,1,0,1,0,1,0,1,0,1,0,1,0,1,0, &
                            1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0, &
                            2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1, &
                            2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1, &
                            2,1,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0, &
                            1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0, &
                            1,0,1,0,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1, &
                            2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1, &
                            2,1,2,1,2,1,2,1,2,1,2,1,2,1,1,0,1,0,1,0, &
                            1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0, &
                            1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0, &
                            1,0,1,0,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1, &
                            2,1,2,1,2,1,2,1,2,1,2,1], [2,196])
        ML_Max = 13
        xy_plane_desired = [.TRUE.]
        debug = .FALSE.
        mat_size_wd = 87
        mat_size_wp = 87

        expected = expected_D_file( &
                                    ntarg = 2_INT32, &
                                    fintr = 38_INT32, &
                                    intr = 38_INT32, &

                                    blocks = [ &
                                        expected_dipole_block( &
                                        noterm = 46_INT32, &
                                        isi = 138_INT32, &
                                        inch = 3_INT32, &
                                        ILrgL = 4_INT32, &
                                        IPty = 0_INT32, &
                                        ISpn = 1_INT32, &
                                        jsi = 138_INT32, &
                                        jnch = 3_INT32, &
                                        JLrgL = 3_INT32, &
                                        lgsf = 1_INT32, &
                                        lglf = 3_INT32, &
                                        lgpf = 1_INT32, &
                                        lgsi = 1_INT32, &
                                        lgli = 4_INT32, &
                                        lgpi = 0_INT32, &
                                        iidip=[8_INT32], &
                                        ifdip=[7_INT32], &
                                        temp_dipsto=subarray( &
                                            [ &
                                                -2.976347E-06_REAL64, &
                                                -1.028847E-05_REAL64, &
                                                -3.296402E-07_REAL64, &
                                                9.186798E-07_REAL64, &
                                                -1.134212E-06_REAL64, &
                                                3.523515E-07_REAL64, &
                                                2.624457E-07_REAL64, &
                                                2.996311E-06_REAL64, &
                                                -8.752347E-07_REAL64, &
                                                -2.917601E-07_REAL64, &
                                                1.808327E-06_REAL64, &
                                                -1.077417E-06_REAL64, &
                                                1.677850E-07_REAL64, &
                                                -5.381149E-07_REAL64, &
                                                8.572756E-07_REAL64, &
                                                -7.410903E-08_REAL64, &
                                                1.269094E-07_REAL64, &
                                                3.736379E-07_REAL64, &
                                                -2.730899E-08_REAL64, &
                                                3.214277E-08_REAL64, &
                                                -1.221533E-07_REAL64, &
                                                8.028970E-09_REAL64, &
                                                -7.980473E-09_REAL64, &
                                                -2.939127E-08_REAL64, &
                                                1.801876E-09_REAL64, &
                                                -1.603469E-09_REAL64, &
                                                2.650773E-09_REAL64, &
                                                7.449642E-12_REAL64, &
                                                -7.760355E-11_REAL64, &
                                                2.344671E-09_REAL64, &
                                                -1.353881E-10_REAL64, &
                                                5.730513E-10_REAL64, &
                                                -1.398500E-09_REAL64, &
                                                1.473336E-09_REAL64, &
                                                -2.001759E-10_REAL64, &
                                                -2.139428E-10_REAL64, &
                                                1.048888E-09_REAL64, &
                                                -4.996039E-11_REAL64, &
                                                6.738702E-11_REAL64, &
                                                4.587710E-10_REAL64, &
                                                -4.635270E-11_REAL64, &
                                                4.594878E-11_REAL64, &
                                                3.477594E-11_REAL64, &
                                                -1.862037E-10_REAL64, &
                                                -2.075974E-12_REAL64, &
                                                4.804736E-12_REAL64, &
                                                4.144789E-11_REAL64, &
                                                -7.534656E-11_REAL64, &
                                                1.182066E-11_REAL64, &
                                                -7.656496E-13_REAL64, &
                                                -6.659431E-15_REAL64 &                                            
                                            ], &
                                            [range(75,125), range(1,1)], &
                                            [138, 138] &
                                        ), &
                                        temp_dipsto_v=subarray( &
                                            [ &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64 &
                                                ], &
                                                [range(75,125), range(1,1)], &
                                                [138, 138] &
                                                ) &
                                            ) &
                                        ] &
                                    )

        CALL test_D_file(error, &
                        "atomic_tests/big_tests/Ar_LS/inputs/", &
                        expected, &
                        lplusp, &
                        crlv, &
                        block_ind, &
                        dipole_coupled, &
                        !wp_read_store, &
                        wd_read_store, &
                        wd_read_store_v, &
                        LML_wp_read_store, &
                        LML_wd_read_store, &
                        LML_wd_read_store_v, &
                        L_block_nchan, &
                        L_block_tot_nchan, &
                        LML_block_nchan, &
                        LML_block_tot_nchan, &
                        ntarg, &
                        nstmx, &
                        no_of_L_blocks, &
                        no_of_LML_blocks, &
                        coupling_id, &
                        dipole_velocity_output, &
                        L_block_lrgl, &
                        L_block_nspn, &
                        L_block_npty, &
                        LML_block_nspn, &
                        LML_block_lrgl, &
                        LML_block_l2p, &
                        LML_block_npty, &
                        LML_block_ml, &
                        LML_block_nconat, &
                        ML_Max, &
                        xy_plane_desired, &
                        debug, &
                        mat_size_wd, &
                        mat_size_wp, &
                        loc_of_blocks)

    END SUBROUTINE test_RMatrix1_Ar_LS

END MODULE