MODULE test_dipole_utilities ! rename module to suit your unit test (match file name)
    USE ISO_FORTRAN_ENV, ONLY: INT32, REAL64    !  Use standard fortran library  *include info*

    USE testdrive, ONLY: check, error_type, new_unittest, unittest_type, test_failed

    USE testing_utilities, ONLY: compare, format_context

    USE dipole_utilities, ONLY: finind_L, finind_LML

    IMPLICIT NONE  ! all types must be explicitly declared

    PRIVATE
    PUBLIC :: collect_dipole_utilities

CONTAINS

    SUBROUTINE collect_dipole_utilities(testsuite)   !  This subroutine will collect the outcomes of tests performed
        TYPE(unittest_type), DIMENSION(:), ALLOCATABLE, INTENT(OUT) :: testsuite    ! Type of unit test type with member as initialised as testsuite

        !  testsuite is a list of each test performed. Make sure to use line continuation characters (&)
        testsuite = [ &
                    new_unittest("finind_L: ", test_finind_L), &
                    new_unittest("finind_LML: ", test_finind_LML) &
                    ]      

    END SUBROUTINE

    SUBROUTINE check_finind_L(error, actual_sym_no, expected_sym_no)
        TYPE(error_type), ALLOCATABLE, INTENT(out) :: error
        CHARACTER(len=:), ALLOCATABLE :: context, prefix
        INTEGER, INTENT(in) :: actual_sym_no
        INTEGER, INTENT(in) :: expected_sym_no

        context = ""

        IF (actual_sym_no /= expected_sym_no) THEN
            CALL format_context(context, prefix//"sym_no", actual_sym_no, expected_sym_no)
        END IF

        PRINT *, "context: ", context

        IF (context /= "") THEN
            CALL test_failed(error, "D file parsed incorrectly!", context)
        END IF
    END SUBROUTINE check_finind_L

    !  This subroutine calls the previous one with arguments passed, 
    !  here there are INTs, in this case they are going to be equal and the test will pass, changing their values to be different will show the outcome of a failed test
    SUBROUTINE test_finind_L(error)
        TYPE(error_type), ALLOCATABLE, INTENT(out) :: error

        INTEGER :: actual_sym_no
        INTEGER :: expected_lgsf, expected_lglf, expected_lgpf, expected_no_of_L_blocks, expected_sym_no
        INTEGER, ALLOCATABLE  :: expected_L_block_nspn(:)
        INTEGER, ALLOCATABLE  :: expected_L_block_lrgl(:)
        INTEGER, ALLOCATABLE  :: expected_L_block_npty(:)

        expected_lgsf = 0
        expected_lglf = 0
        expected_lgpf = 0
        expected_sym_no = 1
        expected_no_of_L_blocks = 6
        expected_L_block_nspn = [0,0,0,0,0,0]
        expected_L_block_lrgl = [0,0,2,2,4,4]
        expected_L_block_npty = [0,1,0,1,0,1]

        CALL finind_L(expected_lgsf, expected_lglf, expected_lgpf, actual_sym_no, expected_no_of_L_blocks, &
                    expected_L_block_nspn, expected_L_block_lrgl, expected_L_block_npty)

        CALL check_finind_L(error, actual_sym_no, expected_sym_no)
    END SUBROUTINE test_finind_L

    SUBROUTINE test_finind_LML(error)
        TYPE(error_type), ALLOCATABLE, INTENT(out) :: error

        INTEGER :: actual_sym_no
        INTEGER :: expected_lgsf, expected_lglf, expected_lgpf, expected_no_of_LML_blocks, expected_sym_no, expected_lgmlf
        INTEGER, ALLOCATABLE  :: expected_LML_block_nspn(:)
        INTEGER, ALLOCATABLE  :: expected_LML_block_lrgl(:)
        INTEGER, ALLOCATABLE  :: expected_LML_block_npty(:)
        INTEGER, ALLOCATABLE  :: expected_LML_block_ml(:)

        expected_lgsf = 0
        expected_lglf = 0
        expected_lgpf = 0
        expected_lgmlf = 0
        expected_sym_no = 1
        expected_no_of_LML_blocks = 9
        expected_LML_block_nspn = [0,0,0,0,0,0,0,0,0]
        expected_LML_block_lrgl = [0,2,2,2,4,4,4,4,4]
        expected_LML_block_npty = [0,0,1,1,0,0,0,1,1]
        expected_LML_block_ml = [0,0,-2,2,-4,0,4,-2,2]

        CALL finind_LML(expected_lgsf, expected_lglf, expected_lgpf, expected_lgmlf, actual_sym_no, expected_no_of_LML_blocks, &
                    expected_LML_block_nspn, expected_LML_block_lrgl, expected_LML_block_npty, expected_LML_block_ml)

        CALL check_finind_L(error, actual_sym_no, expected_sym_no)
    END SUBROUTINE test_finind_LML
END MODULE