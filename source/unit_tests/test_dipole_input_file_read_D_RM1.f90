MODULE test_dipole_input_file_read_D_RM1 ! rename module to suit your unit test (match file name)
    USE ISO_FORTRAN_ENV, ONLY: INT32, REAL64    !  Use standard fortran library  *include info*

    USE testdrive, ONLY: check, error_type, new_unittest, unittest_type, test_failed

    USE testing_utilities, ONLY: compare, &
                                data_file, &
                                format_context, &
                                int_to_char, &
                                range, &
                                subarray, &
                                subarray_integer_int32, &
                                subarray_real_real64

    USE dipole_input_file_RM1, ONLY: D_file, dipole_block, read_D_file_RM1

    USE precisn, ONLY: wp

    IMPLICIT NONE  ! all types must be explicitly declared

    INTERFACE check
        MODULE PROCEDURE check_d_file
    END INTERFACE

    TYPE expected_D_file
        INTEGER(INT32):: ntarg
        INTEGER(INT32):: fintr
        INTEGER(INT32):: intr
        TYPE(expected_dipole_block), DIMENSION(:), ALLOCATABLE:: blocks
    END TYPE expected_D_file

    TYPE expected_dipole_block
        INTEGER(INT32):: noterm, isi, inch, ILrgL, IPty, ISpn, jsi, jnch, JLrgL
        INTEGER(INT32):: lgsf, lglf, lgpf, lgsi, lgli, lgpi ! quantum numbers initial/final
        TYPE(subarray_real_real64) :: dipsto !dipsto ! length gauge reduced dipole transition matrix
        TYPE(subarray_real_real64) :: dipsto_v !dipsto_v ! velocity gauge reduced dipole transition matrix
        INTEGER(INT32), ALLOCATABLE :: iidip(:)
        INTEGER(INT32), ALLOCATABLE :: ifdip(:)
    END TYPE expected_dipole_block

    PRIVATE
    PUBLIC :: collect_read_D_file_RM1 !  rename this subroutine to suit your unit test (also line 15)
CONTAINS
    SUBROUTINE collect_read_D_file_RM1(testsuite)   !  This subroutine will collect the outcomes of tests performed
        TYPE(unittest_type), DIMENSION(:), ALLOCATABLE, INTENT(OUT) :: testsuite    ! Type of unit test type with member as initialised as testsuite

        !  testsuite is a list of each test performed make sure to use line continuation characters (&)
        testsuite = [new_unittest("atomic::small:example_Ar_jk", test_RMatrix1), &
                    new_unittest("atomic::small:iodine", test_RMatrix1_iodine), &
                    new_unittest("atomic::big:Ar_jk", test_RMatrix1_Ar_jk), &
                    new_unittest("atomic::big:Ar_ls", test_RMatrix1_Ar_LS)]      

    END SUBROUTINE collect_read_D_file_RM1

    SUBROUTINE check_D_file(error, &
                            actual_ntarg, &
                            actual_fintr, &
                            actual_iidip, &
                            actual_ifdip, &
                            nstmx, &
                            actual_dipsto, &
                            actual_dipsto_v, &
                            expected, &
                            loc_of_blocks, &
                            dipole_velocity_output)
        ! comparisons for D file go here
        TYPE(error_type), ALLOCATABLE, INTENT(out) :: error
        TYPE(expected_D_file), INTENT(in) :: expected
        CHARACTER(len=:), ALLOCATABLE       :: context
        INTEGER, DIMENSION(:), ALLOCATABLE  :: expected_shape
        INTEGER, INTENT(IN)                 :: loc_of_blocks(:)
        INTEGER, INTENT(INOUT)              :: actual_ntarg
        INTEGER, INTENT(INOUT)              :: actual_fintr
        INTEGER, INTENT(INOUT)              :: nstmx
        INTEGER                             :: i
        LOGICAL, INTENT(INOUT)              :: dipole_velocity_output

        INTEGER, ALLOCATABLE, INTENT(INOUT)  :: actual_iidip(:)
        INTEGER, ALLOCATABLE, INTENT(INOUT)  :: actual_ifdip(:)
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: actual_dipsto(:, :, :)
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: actual_dipsto_v(:, :, :)

        context = ""

        IF (actual_ntarg /= expected%ntarg) THEN
            CALL format_context(context, "ntarg", actual_ntarg, expected%ntarg)
        END IF

        IF (actual_fintr /= expected%fintr) THEN
            CALL format_context(context, "fintr", actual_fintr, expected%fintr)
        END IF

        expected_shape = [actual_ntarg, actual_ntarg]

        DO i = 1, SIZE(loc_of_blocks)
            CALL check_dipole_block(context, &
                                    loc_of_blocks(i), &
                                    actual_fintr, &
                                    actual_iidip, &
                                    actual_ifdip, &
                                    nstmx, &
                                    actual_dipsto, &
                                    actual_dipsto_v, &
                                    expected%blocks(i), &
                                    dipole_velocity_output)
        END DO

        PRINT *, "context: ", context

        IF (context /= "") THEN
            CALL test_failed(error, "D file read incorrectly!", context)
        END IF


    END SUBROUTINE check_D_file

    SUBROUTINE check_dipole_block(context, &
                                    index, &
                                    actual_fintr, &
                                    actual_iidip, &
                                    actual_ifdip, &
                                    nstmx, &
                                    actual_dipsto, &
                                    actual_dipsto_v, &
                                    expected, &
                                    dipole_velocity_output)

        ! comparisons for D00n file go here
        TYPE(expected_dipole_block), INTENT(in) :: expected
        INTEGER, INTENT(IN) :: index
        INTEGER, INTENT(INOUT)              :: nstmx
        INTEGER, INTENT(INOUT)              :: actual_fintr
        LOGICAL, INTENT(IN)                 :: dipole_velocity_output

        INTEGER, ALLOCATABLE, INTENT(INOUT)  :: actual_iidip(:)
        INTEGER, ALLOCATABLE, INTENT(INOUT)  :: actual_ifdip(:)
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: actual_dipsto(:, :, :)
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: actual_dipsto_v(:, :, :)

        CHARACTER(len=:), ALLOCATABLE :: context, prefix
        INTEGER, DIMENSION(:), ALLOCATABLE :: expected_shape

        prefix = "dipole_block("//int_to_char(index)//")%"
      
        IF (.NOT. compare(actual_iidip(index), expected%iidip(1))) THEN
            CALL format_context(context, "iidip", actual_iidip(index), expected%iidip(1))
        END IF

        IF (.NOT. compare(actual_ifdip(index), expected%ifdip(1))) THEN
            CALL format_context(context, "ifdip", actual_ifdip(index), expected%ifdip(1))
        END IF

        ! Check that array shapes are _internally_ consistent

        IF (.NOT. compare(actual_dipsto, expected%dipsto)) THEN
            CALL format_context(context, "dipsto", actual_dipsto, expected%dipsto)
        END IF

        IF (dipole_velocity_output) THEN
            IF (.NOT. compare(actual_dipsto_v, expected%dipsto_v)) THEN
                CALL format_context(context, "dipsto_v", actual_dipsto_v, expected%dipsto_v)
            END IF
        END IF

        expected_shape = [nstmx, nstmx, actual_fintr]

        IF(.NOT. compare(SHAPE(actual_dipsto), expected_shape)) THEN
            CALL format_context(context, "shape("//prefix//"%dipsto)", SHAPE(actual_dipsto), expected_shape)
        END IF

        IF (dipole_velocity_output) THEN
            IF(.NOT. compare(SHAPE(actual_dipsto_v), expected_shape)) THEN
                CALL format_context(context, "shape("//prefix//"%dipsto_v)", SHAPE(actual_dipsto_v), expected_shape)
            END IF
        END IF

    END SUBROUTINE check_dipole_block

    SUBROUTINE test_D_file(&
                            error, &
                            path, &
                            expected, &
                            lplusp, &
                            crlv, &
                            iidip, &
                            ifdip, &
                            block_ind, &
                            dipole_coupled, &
                            wp_read_store, &
                            wd_read_store, &
                            wd_read_store_v, &
                            LML_wp_read_store, &
                            LML_wd_read_store, &
                            LML_wd_read_store_v, &
                            L_block_nchan, &
                            L_block_tot_nchan, &
                            LML_block_nchan, &
                            LML_block_tot_nchan, &
                            ntarg, &
                            nstmx, &
                            no_of_L_blocks, &
                            no_of_LML_blocks, &
                            coupling_id, &
                            dipole_velocity_output, &
                            L_block_lrgl, &
                            L_block_nspn, &
                            L_block_npty, &
                            LML_block_nspn, &
                            LML_block_lrgl, &
                            LML_block_l2p, &
                            LML_block_npty, &
                            LML_block_ml, &
                            LML_block_nconat, &
                            ML_Max, &
                            xy_plane_desired, &
                            fintr, &
                            dipsto, &
                            dipsto_v, &
                            debug, &
                            mat_size_wd, &
                            mat_size_wp, &
                            loc_of_blocks)

        ! call read function from input file, call check_D_file

        TYPE(error_type), ALLOCATABLE, INTENT(out) :: error
        TYPE(expected_D_file), INTENT(INOUT) :: expected
        
        CHARACTER(len=*), INTENT(in) :: path
        CHARACTER(len=:), ALLOCATABLE :: relative_path_header, D_file_path_header, relative_path_data, D_file_path_data

        INTEGER, INTENT(IN) :: lplusp
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: crlv(:, :)
        INTEGER, ALLOCATABLE, INTENT(INOUT) :: iidip(:)
        INTEGER, ALLOCATABLE, INTENT(INOUT) :: ifdip(:)
        INTEGER, ALLOCATABLE, INTENT(INOUT) :: dipole_coupled(:, :)
        INTEGER, ALLOCATABLE, INTENT(INOUT) :: block_ind(:, :, :)

        REAL(wp), ALLOCATABLE :: wp_read_store(:, :)
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: wd_read_store(:, :)
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: wd_read_store_v(:, :)
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: LML_wp_read_store(:, :)
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: LML_wd_read_store(:, :)
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: LML_wd_read_store_v(:, :)
        
        INTEGER, ALLOCATABLE, INTENT(INOUT) :: L_block_nchan(:)
        INTEGER, ALLOCATABLE, INTENT(INOUT) :: LML_block_nchan(:)
        INTEGER, INTENT(IN) :: L_block_tot_nchan
        INTEGER, INTENT(IN) :: LML_block_tot_nchan

        INTEGER, INTENT(IN)                  :: L_block_lrgl(:)
        INTEGER, INTENT(IN)                  :: L_block_nspn(:)
        INTEGER, INTENT(IN)                  :: L_block_npty(:)

        INTEGER, INTENT(IN)                  :: LML_block_lrgl(:)
        INTEGER, INTENT(IN)                  :: LML_block_l2p(:, :)
        INTEGER, INTENT(IN)                  :: LML_block_npty(:)
        INTEGER, INTENT(IN)                  :: LML_block_nspn(:)
        INTEGER, INTENT(IN)                  :: LML_block_ml(:)
        INTEGER, INTENT(IN)                  :: LML_block_nconat(:, :)
        LOGICAL, INTENT(IN)                  :: xy_plane_desired(:)

        INTEGER, INTENT(IN)                  :: coupling_id
        INTEGER, INTENT(IN)                  :: no_of_L_blocks
        INTEGER, INTENT(IN)                  :: no_of_LML_blocks
        INTEGER, INTENT(INOUT)                  :: nstmx
        INTEGER, INTENT(INOUT)                  :: ntarg
        INTEGER, INTENT(IN)                  :: ML_Max
        INTEGER, INTENT(INOUT)               :: fintr

        LOGICAL, INTENT(IN)                  :: debug
        LOGICAL, INTENT(INOUT)               :: dipole_velocity_output 
        
        INTEGER, INTENT(INOUT) :: mat_size_wd
        INTEGER, INTENT(INOUT) :: mat_size_wp

        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: dipsto(:, :, :)
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: dipsto_v(:, :, :)

        INTEGER, INTENT(IN)                     :: loc_of_blocks(:)
        
        relative_path_header = path//'D00'
        relative_path_data = path//'D'
        D_file_path_header = data_file(relative_path_header)
        D_file_path_data = data_file(relative_path_data) 

        CALL read_D_file_RM1(&
                            D_file_path_header, &
                            D_file_path_data, &
                            lplusp, &
                            crlv, &
                            iidip, &
                            ifdip, &
                            block_ind, &
                            dipole_coupled, &
                            wp_read_store, &
                            wd_read_store, &
                            wd_read_store_v, &
                            LML_wp_read_store, &
                            LML_wd_read_store, &
                            LML_wd_read_store_v, &
                            L_block_nchan, &
                            L_block_tot_nchan, &
                            LML_block_nchan, &
                            LML_block_tot_nchan, &
                            ntarg, &
                            nstmx, &
                            no_of_L_blocks, &
                            no_of_LML_blocks, &
                            coupling_id, &
                            dipole_velocity_output, &
                            L_block_lrgl, &
                            L_block_nspn, &
                            L_block_npty, &
                            LML_block_nspn, &
                            LML_block_lrgl, &
                            LML_block_l2p, &
                            LML_block_npty, &
                            LML_block_ml, &
                            LML_block_nconat, &
                            ML_Max, &
                            xy_plane_desired, &
                            fintr, &
                            dipsto, &
                            dipsto_v, &
                            debug, &
                            mat_size_wd, &
                            mat_size_wp)

            CALL check_D_file(error, &
                        ntarg, &
                        fintr, &
                        iidip, &
                        ifdip, &
                        nstmx, &
                        dipsto, &
                        dipsto_v, &
                        expected, &
                        loc_of_blocks, &
                        dipole_velocity_output)
    END SUBROUTINE test_D_file

    SUBROUTINE test_RMatrix1(error)
        ! expected data goes here
        
        TYPE(error_type), ALLOCATABLE, INTENT(out) :: error
        TYPE(expected_D_file)               :: expected
        
        INTEGER(INT32) :: lplusp
        REAL(wp), ALLOCATABLE  :: crlv(:, :)
        INTEGER, ALLOCATABLE  :: iidip(:)
        INTEGER, ALLOCATABLE  :: ifdip(:)
        INTEGER, ALLOCATABLE  :: dipole_coupled(:, :)
        INTEGER, ALLOCATABLE  :: block_ind(:, :, :)

        REAL(wp), ALLOCATABLE  :: wp_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: wd_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: wd_read_store_v(:, :)
        REAL(wp), ALLOCATABLE  :: LML_wp_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: LML_wd_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: LML_wd_read_store_v(:, :)
        INTEGER :: mat_size_wd
        INTEGER :: mat_size_wp
        
        INTEGER, ALLOCATABLE  :: L_block_nchan(:)
        INTEGER, ALLOCATABLE  :: LML_block_nchan(:)
        INTEGER(INT32) :: L_block_tot_nchan
        INTEGER(INT32) :: LML_block_tot_nchan

        INTEGER, ALLOCATABLE                  :: L_block_lrgl(:)
        INTEGER, ALLOCATABLE                  :: L_block_nspn(:)
        INTEGER, ALLOCATABLE                  :: L_block_npty(:)

        INTEGER, ALLOCATABLE                  :: LML_block_lrgl(:)
        INTEGER, ALLOCATABLE                  :: LML_block_l2p(:, :)
        INTEGER, ALLOCATABLE                  :: LML_block_npty(:)
        INTEGER, ALLOCATABLE                  :: LML_block_nspn(:)
        INTEGER, ALLOCATABLE                  :: LML_block_ml(:)
        INTEGER, ALLOCATABLE                  :: LML_block_nconat(:, :)

        INTEGER(INT32)                  :: coupling_id
        INTEGER(INT32)                  :: no_of_L_blocks
        INTEGER(INT32)                  :: no_of_LML_blocks
        INTEGER(INT32)                  :: nstmx
        INTEGER(INT32)                  :: ntarg
        INTEGER(INT32)                  :: ML_Max
        INTEGER(INT32)                  :: fintr

        LOGICAL                   :: debug
        LOGICAL                   :: dipole_velocity_output 
        LOGICAL, ALLOCATABLE                ::xy_plane_desired(:)

        INTEGER, ALLOCATABLE                :: loc_of_blocks(:)

        REAL(wp), ALLOCATABLE :: dipsto(:, :, :)
        REAL(wp), ALLOCATABLE :: dipsto_v(:, :, :)

        loc_of_blocks = [6,3]
        lplusp = 0
        L_block_nchan = [2,2,5,5,6,6]
        L_block_tot_nchan = 26
        LML_block_nchan = [2,5,5,5,6,6,6,6,6]
        LML_block_tot_nchan = 47
        ntarg = 2
        nstmx = 318
        no_of_L_blocks = 6
        no_of_LML_blocks = 9
        coupling_id = 2
        dipole_velocity_output = .FALSE.
        L_block_lrgl = [0,0,2,2,4,4]
        L_block_nspn = [0,0,0,0,0,0]
        L_block_npty = [0,1,0,1,0,1]
        LML_block_nspn = [0,0,0,0,0,0,0,0,0]
        LML_block_lrgl = [0,2,2,2,4,4,4,4,4]
        LML_block_l2p =  RESHAPE([1,1,0,0,0,0,1,1,3,1,1,0,2,0,2,0,2,0,2,0,2,0,2,0,1,3,&
                            1,3,1,3,1,3,1,3,1,3,1,3,1,3,1,3,0,2,2,4,2,2,0,2,2,4,2,2], &
                            [6,9])
        LML_block_npty = [0,0,1,1,0,0,0,1,1]
        LML_block_ml = [0,0,-2,2,-4,0,4,-2,2]
        LML_block_nconat = RESHAPE([1,1,3,2,3,2,3,2,4,2,4,2,4,2,4,2,4,2], [2,9])
        ML_Max = 4
        xy_plane_desired = [.true.]
        debug = .FALSE.
        mat_size_wd = 44
        mat_size_wp = 44

        expected = expected_D_file( &
                                    ntarg = 2_INT32, &
                                    fintr = 6_INT32, &
                                    intr = 6_INT32, &

                                    blocks = [ &
                                        expected_dipole_block( &
                                        noterm = 45_INT32, &
                                        isi = 264_INT32, &
                                        inch = 5_INT32, &
                                        ILrgL = 2_INT32, &
                                        IPty = 1_INT32, &
                                        ISpn = 0_INT32, &
                                        jsi = 239_INT32, &
                                        jnch = 5_INT32, &
                                        JLrgL = 2_INT32, &
                                        lgsf = 0_INT32, &
                                        lglf = 2_INT32, &
                                        lgpf = 0_INT32, &
                                        lgsi = 0_INT32, &
                                        lgli = 2_INT32, &
                                        lgpi = 1_INT32, &
                                        iidip=[6_INT32], &
                                        ifdip=[5_INT32], &
                                        dipsto=subarray( &
                                            [ &
                                                2.968454E-01_REAL64, &
                                                1.119497E+00_REAL64, &
                                                2.649335E-03_REAL64, &
                                                3.227341E-01_REAL64, &
                                                -4.580487E-01_REAL64, &
                                                -2.883736E-01_REAL64, &
                                                -4.292470E-01_REAL64, &
                                                -7.891044E-02_REAL64, &
                                                4.763692E-01_REAL64, &
                                                6.333439E-01_REAL64, &
                                                -2.043249E-01_REAL64, &
                                                3.503587E-01_REAL64, &
                                                5.324659E-02_REAL64, &
                                                2.606031E-01_REAL64, &
                                                -1.085060E+00_REAL64, &
                                                3.055652E-01_REAL64, &
                                                -3.914073E-01_REAL64, &
                                                3.531661E-02_REAL64, &
                                                -1.331457E-01_REAL64, &
                                                -1.321953E+00_REAL64, &
                                                1.544227E-01_REAL64, &
                                                -2.579616E-01_REAL64, &
                                                3.358922E-01_REAL64, &
                                                9.700001E-02_REAL64, &
                                                1.438600E+00_REAL64, &
                                                4.003961E-02_REAL64, &
                                                2.251641E-01_REAL64, &
                                                8.935978E-02_REAL64, &
                                                -3.167352E-01_REAL64, &
                                                -1.477855E+00_REAL64, &
                                                -3.623115E-02_REAL64, &
                                                -7.844892E-02_REAL64, &
                                                1.862261E-01_REAL64, &
                                                -2.782352E-01_REAL64, &
                                                1.437301E+00_REAL64, &
                                                3.055805E-02_REAL64, &
                                                7.375800E-02_REAL64, &
                                                -1.619865E-01_REAL64, &
                                                -2.676810E-01_REAL64, &
                                                -1.245696E+00_REAL64, &
                                                -2.486171E-02_REAL64, &
                                                -7.080685E-02_REAL64, &
                                                8.783027E-01_REAL64, &
                                                1.235775E-01_REAL64, &
                                                -1.864934E-01_REAL64, &
                                                1.811610E-02_REAL64, &
                                                -7.017005E-02_REAL64, &
                                                -4.317229E-01_REAL64, &
                                                -9.051521E-03_REAL64, &
                                                7.638614E-03_REAL64 &                                                                                                        
                                            ], &
                                            [range(1,50), range(1,1), range(1,1)], &
                                            [318, 318, 6] &
                                        ), &
                                        dipsto_v=subarray( &
                                            [ &
                                                0.000000E+00_REAL64 &
                                            ], &
                                            [range(1,1), range(1,1), range(1,1)], &
                                            [1, 1, 1] &
                                            ) &
                                        ), &
                                        expected_dipole_block( &
                                        noterm = 45_INT32, &
                                        isi = 264_INT32, &
                                        inch = 5_INT32, &
                                        ILrgL = 2_INT32, &
                                        IPty = 1_INT32, &
                                        ISpn = 0_INT32, &
                                        jsi = 239_INT32, &
                                        jnch = 5_INT32, &
                                        JLrgL = 2_INT32, &
                                        lgsf = 0_INT32, &
                                        lglf = 2_INT32, &
                                        lgpf = 0_INT32, &
                                        lgsi = 0_INT32, &
                                        lgli = 2_INT32, &
                                        lgpi = 1_INT32, &
                                        iidip=[4_INT32], &
                                        ifdip=[3_INT32], &
                                        dipsto=subarray( &
                                        [ &
                                            7.317744E-03_REAL64, &
                                            -3.799379E-02_REAL64, &
                                            -1.913395E-01_REAL64, &
                                            5.573969E-02_REAL64, &
                                            -1.073182E-01_REAL64, &
                                            -1.220165E-02_REAL64, &
                                            1.183353E-01_REAL64, &
                                            1.610282E-01_REAL64, &
                                            -4.953164E-02_REAL64, &
                                            9.049006E-02_REAL64, &
                                            1.609585E-02_REAL64, &
                                            -1.683948E-01_REAL64, &
                                            -1.029064E-01_REAL64, &
                                            4.416040E-02_REAL64, &
                                            -7.785078E-02_REAL64, &
                                            1.858218E-02_REAL64, &
                                            -1.736664E-01_REAL64, &
                                            -7.582162E-02_REAL64, &
                                            3.955426E-02_REAL64, &
                                            -6.770331E-02_REAL64, &
                                            -1.972385E-02_REAL64, &
                                            1.673705E-01_REAL64, &
                                            6.219355E-02_REAL64, &
                                            -3.559965E-02_REAL64, &
                                            5.937799E-02_REAL64, &
                                            -1.982944E-02_REAL64, &
                                            1.572764E-01_REAL64, &
                                            5.349273E-02_REAL64, &
                                            -3.219696E-02_REAL64, &
                                            5.250156E-02_REAL64, &
                                            1.924921E-02_REAL64, &
                                            -1.458181E-01_REAL64, &
                                            -4.702372E-02_REAL64, &
                                            2.926697E-02_REAL64, &
                                            4.681346E-02_REAL64, &
                                            1.826832E-02_REAL64, &
                                            -1.340335E-01_REAL64, &
                                            -4.177888E-02_REAL64, &
                                            2.674403E-02_REAL64, &
                                            -4.226306E-02_REAL64, &
                                            -1.705924E-02_REAL64, &
                                            1.228442E-01_REAL64, &
                                            3.728194E-02_REAL64, &
                                            2.454916E-02_REAL64, &
                                            3.941014E-02_REAL64, &
                                            -1.563450E-02_REAL64, &
                                            1.139477E-01_REAL64, &
                                            3.311700E-02_REAL64, &
                                            2.253232E-02_REAL64, &
                                            3.845846E-02_REAL64, &
                                            -1.396529E-02_REAL64 &                                                                                                                                                 
                                            ], &
                                            [range(100,150), range(1,1), range(1,1)], &
                                            [318, 318, 6] &
                                        ), &
                                        dipsto_v=subarray( &
                                            [ &
                                                0.000000E+00_REAL64 &
                                            ], &
                                            [range(1,1), range(1,1), range(1,1)], &
                                            [1, 1, 1] &
                                            ) &
                                        ) &
                                    ] &
                                )

        CALL test_D_file(error, &
                            "atomic_tests/small_tests/Example_Ar_jk/inputs/", &
                            expected, &
                            lplusp, &
                            crlv, &
                            iidip, &
                            ifdip, &
                            block_ind, &
                            dipole_coupled, &
                            wp_read_store, &
                            wd_read_store, &
                            wd_read_store_v, &
                            LML_wp_read_store, &
                            LML_wd_read_store, &
                            LML_wd_read_store_v, &
                            L_block_nchan, &
                            L_block_tot_nchan, &
                            LML_block_nchan, &
                            LML_block_tot_nchan, &
                            ntarg, &
                            nstmx, &
                            no_of_L_blocks, &
                            no_of_LML_blocks, &
                            coupling_id, &
                            dipole_velocity_output, &
                            L_block_lrgl, &
                            L_block_nspn, &
                            L_block_npty, &
                            LML_block_nspn, &
                            LML_block_lrgl, &
                            LML_block_l2p, &
                            LML_block_npty, &
                            LML_block_ml, &
                            LML_block_nconat, &
                            ML_Max, &
                            xy_plane_desired, &
                            fintr, &
                            dipsto, &
                            dipsto_v, &
                            debug, &
                            mat_size_wd, &
                            mat_size_wp, &
                            loc_of_blocks)
    END SUBROUTINE test_RMatrix1

    SUBROUTINE test_RMatrix1_iodine(error)
        ! expected data goes here
        
        TYPE(error_type), ALLOCATABLE, INTENT(out) :: error
        TYPE(expected_D_file)               :: expected
        
        INTEGER(INT32) :: lplusp
        REAL(wp), ALLOCATABLE  :: crlv(:, :)
        INTEGER, ALLOCATABLE  :: iidip(:)
        INTEGER, ALLOCATABLE  :: ifdip(:)
        INTEGER, ALLOCATABLE  :: dipole_coupled(:, :)
        INTEGER, ALLOCATABLE  :: block_ind(:, :, :)

        REAL(wp), ALLOCATABLE  :: wp_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: wd_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: wd_read_store_v(:, :)
        REAL(wp), ALLOCATABLE  :: LML_wp_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: LML_wd_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: LML_wd_read_store_v(:, :)
        INTEGER :: mat_size_wd
        INTEGER :: mat_size_wp
        
        INTEGER, ALLOCATABLE  :: L_block_nchan(:)
        INTEGER, ALLOCATABLE  :: LML_block_nchan(:)
        INTEGER(INT32) :: L_block_tot_nchan
        INTEGER(INT32) :: LML_block_tot_nchan

        INTEGER, ALLOCATABLE                  :: L_block_lrgl(:)
        INTEGER, ALLOCATABLE                  :: L_block_nspn(:)
        INTEGER, ALLOCATABLE                  :: L_block_npty(:)

        INTEGER, ALLOCATABLE                  :: LML_block_lrgl(:)
        INTEGER, ALLOCATABLE                  :: LML_block_l2p(:, :)
        INTEGER, ALLOCATABLE                  :: LML_block_npty(:)
        INTEGER, ALLOCATABLE                  :: LML_block_nspn(:)
        INTEGER, ALLOCATABLE                  :: LML_block_ml(:)
        INTEGER, ALLOCATABLE                  :: LML_block_nconat(:, :)

        INTEGER(INT32)                  :: coupling_id
        INTEGER(INT32)                  :: no_of_L_blocks
        INTEGER(INT32)                  :: no_of_LML_blocks
        INTEGER(INT32)                  :: nstmx
        INTEGER(INT32)                  :: ntarg
        INTEGER(INT32)                  :: ML_Max
        INTEGER(INT32)                  :: fintr

        LOGICAL                   :: debug
        LOGICAL                   :: dipole_velocity_output 
        LOGICAL, ALLOCATABLE                ::xy_plane_desired(:)

        INTEGER, ALLOCATABLE                :: loc_of_blocks(:)

        REAL(wp), ALLOCATABLE :: dipsto(:, :, :)
        REAL(wp), ALLOCATABLE :: dipsto_v(:, :, :)

        loc_of_blocks = [3,5]
        lplusp = 0
        L_block_nchan = [5,5,8,8,9,9]
        L_block_tot_nchan = 44
        LML_block_nchan = [5,5,5,5,8,8,8,8,8,8,8,8,9,9,9,9,9,9,9,9]
        LML_block_tot_nchan = 156
        ntarg = 3
        nstmx = 396
        no_of_L_blocks = 6
        no_of_LML_blocks = 20
        coupling_id = 2
        dipole_velocity_output = .TRUE.
        L_block_lrgl = [1,1,3,3,5,5]
        L_block_nspn = [0,0,0,0,0,0]
        L_block_npty = [0,1,0,1,0,1]
        LML_block_nspn = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        LML_block_lrgl = [1,1,1,1,3,3,3,3,3,3,3,3,5,5,5,5,5,5,5,5]
        LML_block_l2p =  RESHAPE([2,2,0,0,2,0,0,0,0,2,2,0,0,2,0,0,0,0,1,3, &
                                1,1,1,0,0,0,0,1,3,1,1,1,0,0,0,0,2,0,2,4, &
                                2,0,2,2,0,2,0,2,4,2,0,2,2,0,2,0,2,4,2,0, &
                                2,2,0,2,0,2,4,2,0,2,2,0,1,3,1,3,1,1,1,3, &
                                0,1,3,1,3,1,1,1,3,0,1,3,1,3,1,1,1,3,0,1, &
                                3,1,3,1,1,1,3,0,0,2,4,2,4,2,2,2,4,0,2,4, &
                                2,4,2,2,2,4,0,2,4,2,4,2,2,2,4,0,2,4,2,4, &
                                2,2,2,4,1,3,1,3,5,3,1,3,3,1,3,1,3,5,3,1, &
                                3,3,1,3,1,3,5,3,1,3,3,1,3,1,3,5,3,1,3,3], &
                                [9,20])
        LML_block_npty = [0,0,1,1,0,0,0,0,1,1,1,1,0,0,0,0,1,1,1,1]
        LML_block_ml = [-1,1,-1,1,-3,-1,1,3,-3,-1,1,3,-3,-1,1,3,-3,-1,1,3]
        LML_block_nconat = RESHAPE([2,1,2,2,1,2,2,1,2,2,1,2,4,1,3,4,1,3,4,1, &
                                    3,4,1,3,4,1,3,4,1,3,4,1,3,4,1,3,5,1,3,5, &
                                    1,3,5,1,3,5,1,3,5,1,3,5,1,3,5,1,3,5,1,3], [3,20])
        ML_Max = 3
        xy_plane_desired = [.false.]
        debug = .FALSE.
        mat_size_wd = 119
        mat_size_wp = 119

        expected = expected_D_file( &
                                    ntarg = 3_INT32, &
                                    fintr = 7_INT32, &
                                    intr = 7_INT32, &

                                    blocks = [ &
                                        expected_dipole_block( &
                                        noterm = 44_INT32, &
                                        isi = 221_INT32, &
                                        inch = 5_INT32, &
                                        ILrgL = 1_INT32, &
                                        IPty = 1_INT32, &
                                        ISpn = 0_INT32, &
                                        jsi = 352_INT32, &
                                        jnch = 8_INT32, &
                                        JLrgL = 3_INT32, &
                                        lgsf = 0_INT32, &
                                        lglf = 3_INT32, &
                                        lgpf = 0_INT32, &
                                        lgsi = 0_INT32, &
                                        lgli = 1_INT32, &
                                        lgpi = 1_INT32, &
                                        iidip=[2_INT32], &
                                        ifdip=[3_INT32], &
                                        dipsto=subarray( &
                                            [ &
                                                8.491978E-03_REAL64, &
                                                4.762430E-03_REAL64, &
                                                2.169019E-03_REAL64, &
                                                -8.632324E-04_REAL64, &
                                                -2.792914E-03_REAL64, &
                                                -7.202964E-03_REAL64, &
                                                -3.974577E-03_REAL64, &
                                                1.792236E-03_REAL64, &
                                                1.318328E-03_REAL64, &
                                                2.576310E-03_REAL64, &
                                                6.824016E-03_REAL64, &
                                                3.712379E-03_REAL64, &
                                                -1.832640E-03_REAL64, &
                                                -1.219772E-03_REAL64, &
                                                -3.279045E-03_REAL64, &
                                                -6.505346E-03_REAL64, &
                                                3.359558E-03_REAL64, &
                                                1.588583E-03_REAL64, &
                                                1.742180E-03_REAL64, &
                                                3.805570E-03_REAL64, &
                                                6.872276E-03_REAL64, &
                                                -3.100251E-03_REAL64, &
                                                -1.583866E-03_REAL64, &
                                                -1.004606E-03_REAL64, &
                                                3.898823E-04_REAL64, &
                                                3.516229E-03_REAL64, &
                                                5.117227E-05_REAL64, &
                                                8.657559E-04_REAL64, &
                                                1.739250E-03_REAL64, &
                                                -3.728120E-03_REAL64, &
                                                -6.854085E-03_REAL64, &
                                                2.673884E-03_REAL64, &
                                                1.253576E-03_REAL64, &
                                                -1.995353E-03_REAL64, &
                                                -3.428397E-03_REAL64, &
                                                6.642845E-03_REAL64, &
                                                2.337138E-03_REAL64, &
                                                -9.733171E-04_REAL64, &
                                                2.035737E-03_REAL64, &
                                                -7.399492E-05_REAL64, &
                                                1.553909E-03_REAL64, &
                                                -2.541253E-04_REAL64, &
                                                2.648791E-04_REAL64, &
                                                -5.129374E-04_REAL64, &
                                                3.055192E-03_REAL64, &
                                                6.120135E-03_REAL64, &
                                                -1.972317E-03_REAL64, &
                                                7.789899E-04_REAL64, &
                                                -1.919885E-03_REAL64, &
                                                2.709114E-03_REAL64, &
                                                -5.756223E-03_REAL64 &                                                                                                                                                
                                            ], &
                                            [range(100,150), range(1,1), range(1,1)], &
                                            [396, 396, 7] &
                                        ), &
                                        dipsto_v=subarray( &
                                            [ &
                                                1.238849E-02_REAL64, &
                                                1.328512E-01_REAL64, &
                                                1.291532E-02_REAL64, &
                                                4.115512E-02_REAL64, &
                                                -5.123284E-03_REAL64, &
                                                -7.759811E-02_REAL64, &
                                                -1.156068E-02_REAL64, &
                                                2.261811E-02_REAL64, &
                                                1.199474E-02_REAL64, &
                                                -3.770273E-02_REAL64, &
                                                2.271832E-02_REAL64, &
                                                6.146494E-03_REAL64, &
                                                -6.083502E-03_REAL64, &
                                                -7.802773E-03_REAL64, &
                                                3.589926E-02_REAL64, &
                                                -1.717907E-02_REAL64, &
                                                -3.180889E-03_REAL64, &
                                                2.005398E-02_REAL64, &
                                                5.945795E-03_REAL64, &
                                                -2.877109E-02_REAL64, &
                                                1.070091E-02_REAL64, &
                                                1.845000E-03_REAL64, &
                                                -2.522148E-02_REAL64, &
                                                -5.545420E-03_REAL64, &
                                                2.864623E-02_REAL64, &
                                                -6.211557E-03_REAL64 &
                                            ], &
                                            [range(25,50), range(1,1), range(1,1)], &
                                            [396, 396, 7] &
                                            ) &
                                        ) , &
                                        expected_dipole_block( &
                                        noterm = 44_INT32, &
                                        isi = 396_INT32, &
                                        inch = 9_INT32, &
                                        ILrgL = 5_INT32, &
                                        IPty = 1_INT32, &
                                        ISpn = 0_INT32, &
                                        jsi = 352_INT32, &
                                        jnch = 8_INT32, &
                                        JLrgL = 3_INT32, &
                                        lgsf = 0_INT32, &
                                        lglf = 3_INT32, &
                                        lgpf = 0_INT32, &
                                        lgsi = 0_INT32, &
                                        lgli = 5_INT32, &
                                        lgpi = 1_INT32, &
                                        iidip=[6_INT32], &
                                        ifdip=[3_INT32], &
                                        dipsto=subarray( &
                                            [ &
                                            -1.313815E-02_REAL64, &
                                            -9.588801E-03_REAL64, &
                                            -2.980992E-03_REAL64, &
                                            -9.996403E-04_REAL64, &
                                            3.564385E-03_REAL64, &
                                            1.325842E-02_REAL64, &
                                            7.473378E-03_REAL64, &
                                            2.973212E-03_REAL64, &
                                            -9.507988E-04_REAL64, &
                                            -3.113817E-03_REAL64, &
                                            -1.106231E-02_REAL64, &
                                            -6.052261E-03_REAL64, &
                                            -2.459953E-03_REAL64, &
                                            -1.050593E-04_REAL64, &
                                            3.257207E-03_REAL64, &
                                            1.051289E-02_REAL64, &
                                            5.852631E-03_REAL64, &
                                            2.532528E-03_REAL64, &
                                            -1.755326E-04_REAL64, &
                                            -2.881104E-03_REAL64, &
                                            -8.813960E-03_REAL64, &
                                            -4.864091E-03_REAL64, &
                                            -2.100811E-03_REAL64, &
                                            9.078925E-04_REAL64, &
                                            3.078957E-03_REAL64, &
                                            8.491978E-03_REAL64, &
                                            4.762430E-03_REAL64, &
                                            2.169019E-03_REAL64, &
                                            -8.632324E-04_REAL64, &
                                            -2.792914E-03_REAL64, &
                                            -7.202964E-03_REAL64, &
                                            -3.974577E-03_REAL64, &
                                            1.792236E-03_REAL64, &
                                            1.318328E-03_REAL64, &
                                            2.576310E-03_REAL64, &
                                            6.824016E-03_REAL64, &
                                            3.712379E-03_REAL64, &
                                            -1.832640E-03_REAL64, &
                                            -1.219772E-03_REAL64, &
                                            -3.279045E-03_REAL64, &
                                            -6.505346E-03_REAL64, &
                                            3.359558E-03_REAL64, &
                                            1.588583E-03_REAL64, &
                                            1.742180E-03_REAL64, &
                                            3.805570E-03_REAL64, &
                                            6.872276E-03_REAL64, &
                                            -3.100251E-03_REAL64, &
                                            -1.583866E-03_REAL64, &
                                            -1.004606E-03_REAL64, &
                                            3.898823E-04_REAL64, &
                                            3.516229E-03_REAL64 &                                                                                                                                                                              
                                            ], &
                                            [range(75,125), range(1,1), range(1,1)], &
                                            [396, 396, 7] &
                                        ), &
                                        dipsto_v=subarray( &
                                            [ &
                                                -4.115356E-06_REAL64, &
                                                8.074375E-05_REAL64, &
                                                -1.554106E-04_REAL64, &
                                                3.829130E-05_REAL64, &
                                                6.443743E-05_REAL64, &
                                                2.303743E-03_REAL64, &
                                                2.088519E-03_REAL64, &
                                                1.697441E-04_REAL64, &
                                                -4.526507E-04_REAL64, &
                                                -2.502488E-04_REAL64, &
                                                -2.137282E-03_REAL64, &
                                                6.089645E-04_REAL64, &
                                                -8.850998E-06_REAL64, &
                                                2.167838E-04_REAL64, &
                                                1.055919E-04_REAL64, &
                                                1.299209E-03_REAL64, &
                                                -2.490183E-04_REAL64, &
                                                2.757140E-05_REAL64, &
                                                2.149033E-05_REAL64, &
                                                -2.907970E-05_REAL64, &
                                                -2.601592E-06_REAL64, &
                                                -2.791354E-05_REAL64, &
                                                5.342202E-05_REAL64, &
                                                -1.486568E-05_REAL64, &
                                                2.201424E-05_REAL64, &
                                                6.836390E-04_REAL64, &
                                                -1.242605E-04_REAL64, &
                                                1.523403E-05_REAL64, &
                                                -8.097904E-05_REAL64, &
                                                -4.677352E-06_REAL64, &
                                                -2.851734E-04_REAL64, &
                                                5.093266E-05_REAL64, &
                                                6.401943E-06_REAL64, &
                                                -5.279033E-05_REAL64, &
                                                -2.434422E-07_REAL64, &
                                                9.996379E-05_REAL64, &
                                                1.772426E-05_REAL64, &
                                                2.956427E-06_REAL64, &
                                                -2.941238E-05_REAL64, &
                                                -7.398605E-08_REAL64, &
                                                -1.534143E-06_REAL64, &
                                                -5.619339E-07_REAL64, &
                                                -9.268413E-07_REAL64, &
                                                4.363376E-08_REAL64, &
                                                7.322117E-07_REAL64, &
                                                -3.275969E-07_REAL64, &
                                                4.501966E-07_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64 &                                            
                                            ], &
                                            [range(175,225), range(1,1), range(1,1)], &
                                            [396, 396, 7] &
                                            ) &
                                        ) &
                                    ] &
                                )

        CALL test_D_file(error, &
                            "atomic_tests/small_tests/iodine/inputs/", &
                            expected, &
                            lplusp, &
                            crlv, &
                            iidip, &
                            ifdip, &
                            block_ind, &
                            dipole_coupled, &
                            wp_read_store, &
                            wd_read_store, &
                            wd_read_store_v, &
                            LML_wp_read_store, &
                            LML_wd_read_store, &
                            LML_wd_read_store_v, &
                            L_block_nchan, &
                            L_block_tot_nchan, &
                            LML_block_nchan, &
                            LML_block_tot_nchan, &
                            ntarg, &
                            nstmx, &
                            no_of_L_blocks, &
                            no_of_LML_blocks, &
                            coupling_id, &
                            dipole_velocity_output, &
                            L_block_lrgl, &
                            L_block_nspn, &
                            L_block_npty, &
                            LML_block_nspn, &
                            LML_block_lrgl, &
                            LML_block_l2p, &
                            LML_block_npty, &
                            LML_block_ml, &
                            LML_block_nconat, &
                            ML_Max, &
                            xy_plane_desired, &
                            fintr, &
                            dipsto, &
                            dipsto_v, &
                            debug, &
                            mat_size_wd, &
                            mat_size_wp, &
                            loc_of_blocks)
    END SUBROUTINE test_RMatrix1_iodine

    SUBROUTINE test_RMatrix1_Ar_jk(error)
        ! expected data goes here
        
        TYPE(error_type), ALLOCATABLE, INTENT(out) :: error
        TYPE(expected_D_file)               :: expected
        
        INTEGER(INT32) :: lplusp
        REAL(wp), ALLOCATABLE  :: crlv(:, :)
        INTEGER, ALLOCATABLE  :: iidip(:)
        INTEGER, ALLOCATABLE  :: ifdip(:)
        INTEGER, ALLOCATABLE  :: dipole_coupled(:, :)
        INTEGER, ALLOCATABLE  :: block_ind(:, :, :)

        REAL(wp), ALLOCATABLE  :: wp_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: wd_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: wd_read_store_v(:, :)
        REAL(wp), ALLOCATABLE  :: LML_wp_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: LML_wd_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: LML_wd_read_store_v(:, :)
        INTEGER :: mat_size_wd
        INTEGER :: mat_size_wp
        
        INTEGER, ALLOCATABLE  :: L_block_nchan(:)
        INTEGER, ALLOCATABLE  :: LML_block_nchan(:)
        INTEGER(INT32) :: L_block_tot_nchan
        INTEGER(INT32) :: LML_block_tot_nchan

        INTEGER, ALLOCATABLE                  :: L_block_lrgl(:)
        INTEGER, ALLOCATABLE                  :: L_block_nspn(:)
        INTEGER, ALLOCATABLE                  :: L_block_npty(:)

        INTEGER, ALLOCATABLE                  :: LML_block_lrgl(:)
        INTEGER, ALLOCATABLE                  :: LML_block_l2p(:, :)
        INTEGER, ALLOCATABLE                  :: LML_block_npty(:)
        INTEGER, ALLOCATABLE                  :: LML_block_nspn(:)
        INTEGER, ALLOCATABLE                  :: LML_block_ml(:)
        INTEGER, ALLOCATABLE                  :: LML_block_nconat(:, :)

        INTEGER(INT32)                  :: coupling_id
        INTEGER(INT32)                  :: no_of_L_blocks
        INTEGER(INT32)                  :: no_of_LML_blocks
        INTEGER(INT32)                  :: nstmx
        INTEGER(INT32)                  :: ntarg
        INTEGER(INT32)                  :: ML_Max
        INTEGER(INT32)                  :: fintr

        LOGICAL                   :: debug
        LOGICAL                   :: dipole_velocity_output 
        LOGICAL, ALLOCATABLE                ::xy_plane_desired(:)

        INTEGER, ALLOCATABLE                :: loc_of_blocks(:)

        REAL(wp), ALLOCATABLE :: dipsto(:, :, :)
        REAL(wp), ALLOCATABLE :: dipsto_v(:, :, :)

        loc_of_blocks = [2]
        lplusp = 0
        L_block_nchan = [3,3,7,7,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8]
        L_block_tot_nchan = 196
        LML_block_nchan = [3,7,7,7,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8, &
                            8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8, &
                            8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8, &
                            8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8, &
                            8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8, &
                            8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8, &
                            8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8, &
                            8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8, &
                            8,8,8,8,8,8,8,8,8]
        LML_block_tot_nchan = 1344
        ntarg = 3
        nstmx = 368
        no_of_L_blocks = 26
        no_of_LML_blocks = 169
        coupling_id = 2
        dipole_velocity_output = .FALSE.
        L_block_lrgl = [0,0,2,2,4,4,6,6,8,8,10,10,12,12,14,14,16,16,18,18, &
                        20,20,22,22,24,24]
        L_block_nspn = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, &
                        0,0,0,0,0,0]
        L_block_npty = [0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1, &
                        0,1,0,1,0,1]
        LML_block_nspn = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, &
                            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, &
                            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, &
                            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, &
                            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, &
                            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, &
                            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, &
                            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, &
                            0,0,0,0,0,0,0,0,0]
        LML_block_lrgl = [0,2,2,2,4,4,4,4,4,6,6,6,6,6,6,6,8,8,8,8, &
                            8,8,8,8,8,10,10,10,10,10,10,10,10,10,10,10,12,12,12,12, &
                            12,12,12,12,12,12,12,12,12,14,14,14,14,14,14,14,14,14,14,14, &
                            14,14,14,14,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16, &
                            16,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18, &
                            20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20, &
                            20,22,22,22,22,22,22,22,22,22,22,22,22,22,22,22,22,22,22,22, &
                            22,22,22,22,24,24,24,24,24,24,24,24,24,24,24,24,24,24,24,24, &
                            24,24,24,24,24,24,24,24,24]
        LML_block_l2p =  RESHAPE([1,1,0,0,0,0,0,0,1,1,3,1,1,0,2,0,2,0,2,0, &
                                    2,1,1,0,2,0,2,0,2,1,1,0,1,3,1,3,1,3,2,2, &
                                    1,3,1,3,1,3,2,2,1,3,1,3,1,3,2,2,0,2,2,4, &
                                    2,2,1,3,0,2,2,4,2,2,1,3,1,3,3,5,3,3,2,4, &
                                    1,3,3,5,3,3,2,4,1,3,3,5,3,3,2,4,2,4,2,4, &
                                    2,4,3,3,2,4,2,4,2,4,3,3,2,4,2,4,2,4,3,3, &
                                    2,4,2,4,2,4,3,3,3,5,3,5,3,5,4,4,3,5,3,5, &
                                    3,5,4,4,3,5,3,5,3,5,4,4,3,5,3,5,3,5,4,4, &
                                    3,5,3,5,3,5,4,4,2,4,4,6,4,4,3,5,2,4,4,6, &
                                    4,4,3,5,2,4,4,6,4,4,3,5,2,4,4,6,4,4,3,5, &
                                    3,5,5,7,5,5,4,6,3,5,5,7,5,5,4,6,3,5,5,7, &
                                    5,5,4,6,3,5,5,7,5,5,4,6,3,5,5,7,5,5,4,6, &
                                    4,6,4,6,4,6,5,5,4,6,4,6,4,6,5,5,4,6,4,6, &
                                    4,6,5,5,4,6,4,6,4,6,5,5,4,6,4,6,4,6,5,5, &
                                    4,6,4,6,4,6,5,5,5,7,5,7,5,7,6,6,5,7,5,7, &
                                    5,7,6,6,5,7,5,7,5,7,6,6,5,7,5,7,5,7,6,6, &
                                    5,7,5,7,5,7,6,6,5,7,5,7,5,7,6,6,5,7,5,7, &
                                    5,7,6,6,4,6,6,8,6,6,5,7,4,6,6,8,6,6,5,7, &
                                    4,6,6,8,6,6,5,7,4,6,6,8,6,6,5,7,4,6,6,8, &
                                    6,6,5,7,4,6,6,8,6,6,5,7,5,7,7,9,7,7,6,8, &
                                    5,7,7,9,7,7,6,8,5,7,7,9,7,7,6,8,5,7,7,9, &
                                    7,7,6,8,5,7,7,9,7,7,6,8,5,7,7,9,7,7,6,8, &
                                    5,7,7,9,7,7,6,8,6,8,6,8,6,8,7,7,6,8,6,8, &
                                    6,8,7,7,6,8,6,8,6,8,7,7,6,8,6,8,6,8,7,7, &
                                    6,8,6,8,6,8,7,7,6,8,6,8,6,8,7,7,6,8,6,8, &
                                    6,8,7,7,6,8,6,8,6,8,7,7,7,9,7,9,7,9,8,8, &
                                    7,9,7,9,7,9,8,8,7,9,7,9,7,9,8,8,7,9,7,9, &
                                    7,9,8,8,7,9,7,9,7,9,8,8,7,9,7,9,7,9,8,8, &
                                    7,9,7,9,7,9,8,8,7,9,7,9,7,9,8,8,7,9,7,9, &
                                    7,9,8,8,6,8,8,10,8,8,7,9,6,8,8,10,8,8,7,9, &
                                    6,8,8,10,8,8,7,9,6,8,8,10,8,8,7,9,6,8,8,10, &
                                    8,8,7,9,6,8,8,10,8,8,7,9,6,8,8,10,8,8,7,9, &
                                    6,8,8,10,8,8,7,9,7,9,9,11,9,9,8,10,7,9,9,11, &
                                    9,9,8,10,7,9,9,11,9,9,8,10,7,9,9,11,9,9,8,10, &
                                    7,9,9,11,9,9,8,10,7,9,9,11,9,9,8,10,7,9,9,11, &
                                    9,9,8,10,7,9,9,11,9,9,8,10,7,9,9,11,9,9,8,10, &
                                    8,10,8,10,8,10,9,9,8,10,8,10,8,10,9,9,8,10,8,10, &
                                    8,10,9,9,8,10,8,10,8,10,9,9,8,10,8,10,8,10,9,9, &
                                    8,10,8,10,8,10,9,9,8,10,8,10,8,10,9,9,8,10,8,10, &
                                    8,10,9,9,8,10,8,10,8,10,9,9,8,10,8,10,8,10,9,9, &
                                    9,11,9,11,9,11,10,10,9,11,9,11,9,11,10,10,9,11,9,11, &
                                    9,11,10,10,9,11,9,11,9,11,10,10,9,11,9,11,9,11,10,10, &
                                    9,11,9,11,9,11,10,10,9,11,9,11,9,11,10,10,9,11,9,11, &
                                    9,11,10,10,9,11,9,11,9,11,10,10,9,11,9,11,9,11,10,10, &
                                    9,11,9,11,9,11,10,10,8,10,10,12,10,10,9,11,8,10,10,12, &
                                    10,10,9,11,8,10,10,12,10,10,9,11,8,10,10,12,10,10,9,11, &
                                    8,10,10,12,10,10,9,11,8,10,10,12,10,10,9,11,8,10,10,12, &
                                    10,10,9,11,8,10,10,12,10,10,9,11,8,10,10,12,10,10,9,11, &
                                    8,10,10,12,10,10,9,11,9,11,11,13,11,11,10,12,9,11,11,13, &
                                    11,11,10,12,9,11,11,13,11,11,10,12,9,11,11,13,11,11,10,12, &
                                    9,11,11,13,11,11,10,12,9,11,11,13,11,11,10,12,9,11,11,13, &
                                    11,11,10,12,9,11,11,13,11,11,10,12,9,11,11,13,11,11,10,12, &
                                    9,11,11,13,11,11,10,12,9,11,11,13,11,11,10,12,10,12,10,12, &
                                    10,12,11,11,10,12,10,12,10,12,11,11,10,12,10,12,10,12,11,11, &
                                    10,12,10,12,10,12,11,11,10,12,10,12,10,12,11,11,10,12,10,12, &
                                    10,12,11,11,10,12,10,12,10,12,11,11,10,12,10,12,10,12,11,11, &
                                    10,12,10,12,10,12,11,11,10,12,10,12,10,12,11,11,10,12,10,12, &
                                    10,12,11,11,10,12,10,12,10,12,11,11,11,13,11,13,11,13,12,12, &
                                    11,13,11,13,11,13,12,12,11,13,11,13,11,13,12,12,11,13,11,13, &
                                    11,13,12,12,11,13,11,13,11,13,12,12,11,13,11,13,11,13,12,12, &
                                    11,13,11,13,11,13,12,12,11,13,11,13,11,13,12,12,11,13,11,13, &
                                    11,13,12,12,11,13,11,13,11,13,12,12,11,13,11,13,11,13,12,12, &
                                    11,13,11,13,11,13,12,12,11,13,11,13,11,13,12,12,10,12,12,14, &
                                    12,12,11,13,10,12,12,14,12,12,11,13,10,12,12,14,12,12,11,13, &
                                    10,12,12,14,12,12,11,13,10,12,12,14,12,12,11,13,10,12,12,14, &
                                    12,12,11,13,10,12,12,14,12,12,11,13,10,12,12,14,12,12,11,13, &
                                    10,12,12,14,12,12,11,13,10,12,12,14,12,12,11,13,10,12,12,14, &
                                    12,12,11,13,10,12,12,14,12,12,11,13], &
                            [8,169])
        LML_block_npty = [0,0,1,1,0,0,0,1,1,0,0,0,1,1,1,1,0,0,0,0, &
                            0,1,1,1,1,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0, &
                            0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,1,1,1,1, &
                            1,1,1,1,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1, &
                            1,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1, &
                            0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1, &
                            1,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1, &
                            1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1, &
                            1,1,1,1,1,1,1,1,1]
        LML_block_ml = [0,0,-2,2,-4,0,4,-2,2,-4,0,4,-6,-2,2,6,-8,-4,0,4, &
                        8,-6,-2,2,6,-8,-4,0,4,8,-10,-6,-2,2,6,10,-12,-8,-4,0, &
                        4,8,12,-10,-6,-2,2,6,10,-12,-8,-4,0,4,8,12,-14,-10,-6,-2, &       
                        2,6,10,14,-16,-12,-8,-4,0,4,8,12,16,-14,-10,-6,-2,2,6,10, &       
                        14,-16,-12,-8,-4,0,4,8,12,16,-18,-14,-10,-6,-2,2,6,10,14,18, &    
                        -20,-16,-12,-8,-4,0,4,8,12,16,20,-18,-14,-10,-6,-2,2,6,10,14, &   
                        18,-20,-16,-12,-8,-4,0,4,8,12,16,20,-22,-18,-14,-10,-6,-2,2,6, &  
                        10,14,18,22,-24,-20,-16,-12,-8,-4,0,4,8,12,16,20,24,-22,-18,-14, &
                        -10,-6,-2,2,6,10,14,18,22]
        LML_block_nconat = RESHAPE([1,1,1,3,2,2,3,2,2,3,2,2,4,2,2,4,2,2,4,2, &
                                    2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4, &
                                    2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2, &
                                    4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2, &
                                    2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4, &
                                    2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2, &
                                    4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2, &
                                    2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4, &
                                    2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2, &
                                    4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2, &
                                    2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4, &
                                    2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2, &
                                    4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2, &
                                    2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4, &
                                    2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2, &
                                    4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2, &
                                    2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4, &
                                    2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2, &
                                    4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2, &
                                    2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4, &
                                    2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2, &
                                    4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2, &
                                    2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4, &
                                    2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2, &
                                    4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2,2,4,2, &
                                    2,4,2,2,4,2,2], [3,169])
        ML_Max = 24
        xy_plane_desired = [.true.]
        debug = .FALSE.
        mat_size_wd = 301
        mat_size_wp = 303

        expected = expected_D_file( &
                                    ntarg = 3_INT32, &
                                    fintr = 36_INT32, &
                                    intr = 36_INT32, &

                                    blocks = [ &
                                        expected_dipole_block( &
                                        noterm = 46_INT32, &
                                        isi = 138_INT32, &
                                        inch = 3_INT32, &
                                        ILrgL = 0_INT32, &
                                        IPty = 1_INT32, &
                                        ISpn = 0_INT32, &
                                        jsi = 322_INT32, &
                                        jnch = 7_INT32, &
                                        JLrgL = 2_INT32, &
                                        lgsf = 0_INT32, &
                                        lglf = 2_INT32, &
                                        lgpf = 0_INT32, &
                                        lgsi = 0_INT32, &
                                        lgli = 0_INT32, &
                                        lgpi = 1_INT32, &
                                        iidip=[2_INT32], &
                                        ifdip=[3_INT32], &
                                        dipsto=subarray( &
                                            [ &
                                                9.616419E-12_REAL64, &
                                                1.071279E+00_REAL64, &
                                                -8.034980E-13_REAL64, &
                                                -1.021591E-11_REAL64, &
                                                -1.104909E-12_REAL64, &
                                                2.402170E-01_REAL64, &
                                                6.333426E-01_REAL64, &
                                                -3.999584E-12_REAL64, &
                                                -1.596450E-11_REAL64, &
                                                -3.025882E-11_REAL64, &
                                                -4.746334E-01_REAL64, &
                                                -6.511749E-01_REAL64, &
                                                -7.300838E-12_REAL64, &
                                                1.508283E-11_REAL64, &
                                                -1.083321E+00_REAL64, &
                                                1.019583E-11_REAL64, &
                                                1.711114E-11_REAL64, &
                                                4.777351E-01_REAL64, &
                                                -4.410836E-12_REAL64, &
                                                1.409994E+00_REAL64, &
                                                -2.964210E-12_REAL64, &
                                                1.611224E-12_REAL64, &
                                                6.690817E-13_REAL64, &
                                                -4.326511E-01_REAL64, &
                                                -1.630530E+00_REAL64, &
                                                -1.654532E-12_REAL64, &
                                                1.723205E-12_REAL64, &
                                                -9.635784E-12_REAL64, &
                                                4.038915E-01_REAL64, &
                                                1.586438E+00_REAL64, &
                                                -1.149791E-12_REAL64, &
                                                2.438878E-11_REAL64, &
                                                -7.629598E-13_REAL64, &
                                                -6.171910E-01_REAL64, &
                                                -1.017302E+00_REAL64, &
                                                2.437662E-12_REAL64, &
                                                -2.394250E-12_REAL64, &
                                                9.796570E-02_REAL64, &
                                                1.902973E-11_REAL64, &
                                                6.670927E-01_REAL64, &
                                                -9.628908E-14_REAL64, &
                                                -5.801930E-12_REAL64, &
                                                2.098677E-01_REAL64, &
                                                1.218193E-01_REAL64, &
                                                -5.630798E-12_REAL64, &
                                                1.566689E-01_REAL64, &
                                                2.707960E-12_REAL64, &
                                                -1.091750E-01_REAL64, &
                                                3.616554E-13_REAL64, &
                                                2.273142E-12_REAL64 &                                                                                                                                                 
                                            ], &
                                            [range(1,50), range(1,1), range(1,1)], &
                                            [368, 368, 36] &
                                        ), &
                                        dipsto_v=subarray( &
                                            [ &
                                                0.000000E+00_REAL64 &
                                            ], &
                                            [range(1,1), range(1,1), range(1,1)], &
                                            [1, 1, 1] &
                                            ) &
                                        ), &
                                        expected_dipole_block( &
                                        noterm = 46_INT32, &
                                        isi = 368_INT32, &
                                        inch = 8_INT32, &
                                        ILrgL = 20_INT32, &
                                        IPty = 1_INT32, &
                                        ISpn = 0_INT32, &
                                        jsi = 368_INT32, &
                                        jnch = 8_INT32, &
                                        JLrgL = 20_INT32, &
                                        lgsf = 0_INT32, &
                                        lglf = 20_INT32, &
                                        lgpf = 0_INT32, &
                                        lgsi = 0_INT32, &
                                        lgli = 20_INT32, &
                                        lgpi = 1_INT32, &
                                        iidip=[22_INT32], &
                                        ifdip=[21_INT32], &
                                        dipsto=subarray( &
                                            [ &
                                                5.267557E-11_REAL64, &
                                                -6.673492E-02_REAL64, &
                                                1.196201E-12_REAL64, &
                                                -1.351871E-01_REAL64, &
                                                -7.042421E-12_REAL64, &
                                                -2.423184E-01_REAL64, &
                                                -2.071001E-11_REAL64, &
                                                4.820383E-11_REAL64, &
                                                2.667035E-02_REAL64, &
                                                -9.004975E-13_REAL64, &
                                                1.221344E-01_REAL64, &
                                                -1.412005E-11_REAL64, &
                                                -2.067976E-01_REAL64, &
                                                2.002716E-12_REAL64, &
                                                -7.542973E-11_REAL64, &
                                                1.389617E-01_REAL64, &
                                                -3.853737E-12_REAL64, &
                                                1.120991E-01_REAL64, &
                                                1.422031E-11_REAL64, &
                                                1.295361E-01_REAL64, &
                                                4.086517E-12_REAL64, &
                                                2.048542E-01_REAL64, &
                                                -4.354570E-12_REAL64, &
                                                8.377089E-12_REAL64, &
                                                -1.123104E-01_REAL64, &
                                                -1.326163E-10_REAL64, &
                                                8.954446E-02_REAL64, &
                                                6.609727E-13_REAL64, &
                                                -2.067467E-01_REAL64, &
                                                -1.355475E-11_REAL64, &
                                                -5.199483E-11_REAL64, &
                                                -1.107185E-01_REAL64, &
                                                -2.031666E-11_REAL64, &
                                                5.893431E-02_REAL64, &
                                                4.086361E-12_REAL64, &
                                                -1.978214E-01_REAL64, &
                                                1.295748E-12_REAL64, &
                                                -3.951166E-11_REAL64, &
                                                8.769186E-02_REAL64, &
                                                3.171663E-12_REAL64, &
                                                -6.822928E-02_REAL64, &
                                                2.834318E-13_REAL64, &
                                                1.853836E-01_REAL64, &
                                                -2.524351E-12_REAL64, &
                                                1.447588E-10_REAL64, &
                                                -7.505349E-02_REAL64, &
                                                1.756138E-12_REAL64, &
                                                6.488320E-02_REAL64, &
                                                2.000014E-12_REAL64, &
                                                1.718021E-01_REAL64, &
                                                -4.160482E-12_REAL64 &                                                                                                                                                                                                                
                                            ], &
                                            [range(75,125), range(1,1), range(1,1)], &
                                            [368, 368, 36] &
                                        ), &
                                        dipsto_v=subarray( &
                                            [ &
                                                0.000000E+00_REAL64 &                                      
                                            ], &
                                            [range(1,1), range(1,1), range(1,1)], &
                                            [1, 1, 1] &
                                            ) &
                                        ) &
                                    ] &
                                )

        CALL test_D_file(error, &
                            "atomic_tests/big_tests/Ar_jK/inputs/", &
                            expected, &
                            lplusp, &
                            crlv, &
                            iidip, &
                            ifdip, &
                            block_ind, &
                            dipole_coupled, &
                            wp_read_store, &
                            wd_read_store, &
                            wd_read_store_v, &
                            LML_wp_read_store, &
                            LML_wd_read_store, &
                            LML_wd_read_store_v, &
                            L_block_nchan, &
                            L_block_tot_nchan, &
                            LML_block_nchan, &
                            LML_block_tot_nchan, &
                            ntarg, &
                            nstmx, &
                            no_of_L_blocks, &
                            no_of_LML_blocks, &
                            coupling_id, &
                            dipole_velocity_output, &
                            L_block_lrgl, &
                            L_block_nspn, &
                            L_block_npty, &
                            LML_block_nspn, &
                            LML_block_lrgl, &
                            LML_block_l2p, &
                            LML_block_npty, &
                            LML_block_ml, &
                            LML_block_nconat, &
                            ML_Max, &
                            xy_plane_desired, &
                            fintr, &
                            dipsto, &
                            dipsto_v, &
                            debug, &
                            mat_size_wd, &
                            mat_size_wp, &
                            loc_of_blocks)
    END SUBROUTINE test_RMatrix1_Ar_jk

    SUBROUTINE test_RMatrix1_Ar_LS(error)
        ! expected data goes here
        
        TYPE(error_type), ALLOCATABLE, INTENT(out) :: error
        TYPE(expected_D_file)               :: expected
        
        INTEGER(INT32) :: lplusp
        REAL(wp), ALLOCATABLE  :: crlv(:, :)
        INTEGER, ALLOCATABLE  :: iidip(:)
        INTEGER, ALLOCATABLE  :: ifdip(:)
        INTEGER, ALLOCATABLE  :: dipole_coupled(:, :)
        INTEGER, ALLOCATABLE  :: block_ind(:, :, :)

        REAL(wp), ALLOCATABLE  :: wp_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: wd_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: wd_read_store_v(:, :)
        REAL(wp), ALLOCATABLE  :: LML_wp_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: LML_wd_read_store(:, :)
        REAL(wp), ALLOCATABLE  :: LML_wd_read_store_v(:, :)
        INTEGER :: mat_size_wd
        INTEGER :: mat_size_wp
        
        INTEGER, ALLOCATABLE  :: L_block_nchan(:)
        INTEGER, ALLOCATABLE  :: LML_block_nchan(:)
        INTEGER(INT32) :: L_block_tot_nchan
        INTEGER(INT32) :: LML_block_tot_nchan

        INTEGER, ALLOCATABLE                  :: L_block_lrgl(:)
        INTEGER, ALLOCATABLE                  :: L_block_nspn(:)
        INTEGER, ALLOCATABLE                  :: L_block_npty(:)

        INTEGER, ALLOCATABLE                  :: LML_block_lrgl(:)
        INTEGER, ALLOCATABLE                  :: LML_block_l2p(:, :)
        INTEGER, ALLOCATABLE                  :: LML_block_npty(:)
        INTEGER, ALLOCATABLE                  :: LML_block_nspn(:)
        INTEGER, ALLOCATABLE                  :: LML_block_ml(:)
        INTEGER, ALLOCATABLE                  :: LML_block_nconat(:, :)

        INTEGER(INT32)                  :: coupling_id
        INTEGER(INT32)                  :: no_of_L_blocks
        INTEGER(INT32)                  :: no_of_LML_blocks
        INTEGER(INT32)                  :: nstmx
        INTEGER(INT32)                  :: ntarg
        INTEGER(INT32)                  :: ML_Max
        INTEGER(INT32)                  :: fintr

        LOGICAL                   :: debug
        LOGICAL                   :: dipole_velocity_output 
        LOGICAL, ALLOCATABLE                ::xy_plane_desired(:)

        INTEGER, ALLOCATABLE                :: loc_of_blocks(:)

        REAL(wp), ALLOCATABLE :: dipsto(:, :, :)
        REAL(wp), ALLOCATABLE :: dipsto_v(:, :, :)

        loc_of_blocks = [5]
        lplusp = 0
        L_block_nchan = [2,1,3,3,1,1,3,3,1,1,3,3,1,1,3,3,1,1,3,3, &
                            1,1,3,3,1,1,3]
        L_block_tot_nchan = 54
        LML_block_nchan = [2,1,3,3,3,3,3,1,1,1,1,1,3,3,3,3,3,3,3,3, &
                            3,1,1,1,1,1,1,1,1,1,3,3,3,3,3,3,3,3,3,3, &
                            3,3,3,1,1,1,1,1,1,1,1,1,1,1,1,1,3,3,3,3, &
                            3,3,3,3,3,3,3,3,3,3,3,3,3,1,1,1,1,1,1,1, &
                            1,1,1,1,1,1,1,1,1,1,3,3,3,3,3,3,3,3,3,3, &
                            3,3,3,3,3,3,3,3,3,3,3,1,1,1,1,1,1,1,1,1, &
                            1,1,1,1,1,1,1,1,1,1,1,1,3,3,3,3,3,3,3,3, &
                            3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,1,1,1, &
                            1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, &
                            1,1,3,3,3,3,3,3,3,3,3,3,3,3,3,3]
        LML_block_tot_nchan = 405
        ntarg = 2
        nstmx = 138
        no_of_L_blocks = 27
        no_of_LML_blocks = 196
        coupling_id = 1
        dipole_velocity_output = .FALSE.
        L_block_lrgl = [0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10, &
                            10,11,11,12,12,13,13]
        L_block_nspn = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, &
                            1,1,1,1,1,1,1]
        L_block_npty = [0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0, &
                            1,0,1,0,1,0,1]
        LML_block_nspn = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, &
                            1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, &
                            1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, &
                            1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, &
                            1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, &
                            1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, &
                            1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, &
                            1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, &
                            1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, &
                            1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
        LML_block_lrgl = [0,1,1,1,2,2,2,2,2,3,3,3,3,3,3,3,4,4,4,4, &
                            4,4,4,4,4,5,5,5,5,5,5,5,5,5,5,5,6,6,6,6, &
                            6,6,6,6,6,6,6,6,6,7,7,7,7,7,7,7,7,7,7,7, &
                            7,7,7,7,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8, &
                            8,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9, &
                            10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10, &
                            10,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, &
                            11,11,11,11,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12, &
                            12,12,12,12,12,12,12,12,12,13,13,13,13,13,13,13,13,13,13,13, &
                            13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13]
        LML_block_l2p =  RESHAPE([1,0,0,1,0,0,0,2,1,0,2,1,1,3,2,1,3,2,1,3, &
                                    2,2,0,0,2,0,0,3,0,0,3,0,0,3,0,0,2,4,3,2, &
                                    4,3,2,4,3,2,4,3,3,5,4,3,5,4,3,5,4,3,5,4, &
                                    3,5,4,4,0,0,4,0,0,4,0,0,4,0,0,5,0,0,5,0, &
                                    0,5,0,0,5,0,0,5,0,0,4,6,5,4,6,5,4,6,5,4, &
                                    6,5,4,6,5,4,6,5,5,7,6,5,7,6,5,7,6,5,7,6, &
                                    5,7,6,5,7,6,5,7,6,6,0,0,6,0,0,6,0,0,6,0, &
                                    0,6,0,0,6,0,0,7,0,0,7,0,0,7,0,0,7,0,0,7, &
                                    0,0,7,0,0,7,0,0,6,8,7,6,8,7,6,8,7,6,8,7, &
                                    6,8,7,6,8,7,6,8,7,6,8,7,7,9,8,7,9,8,7,9, &
                                    8,7,9,8,7,9,8,7,9,8,7,9,8,7,9,8,7,9,8,8, &
                                    0,0,8,0,0,8,0,0,8,0,0,8,0,0,8,0,0,8,0,0, &
                                    8,0,0,9,0,0,9,0,0,9,0,0,9,0,0,9,0,0,9,0, &
                                    0,9,0,0,9,0,0,9,0,0,8,10,9,8,10,9,8,10,9,8, &
                                    10,9,8,10,9,8,10,9,8,10,9,8,10,9,8,10,9,8,10,9, &
                                    9,11,10,9,11,10,9,11,10,9,11,10,9,11,10,9,11,10,9,11, &
                                    10,9,11,10,9,11,10,9,11,10,9,11,10,10,0,0,10,0,0,10, &
                                    0,0,10,0,0,10,0,0,10,0,0,10,0,0,10,0,0,10,0,0, &
                                    10,0,0,11,0,0,11,0,0,11,0,0,11,0,0,11,0,0,11,0, &
                                    0,11,0,0,11,0,0,11,0,0,11,0,0,11,0,0,10,12,11,10, &
                                    12,11,10,12,11,10,12,11,10,12,11,10,12,11,10,12,11,10,12,11, &
                                    10,12,11,10,12,11,10,12,11,10,12,11,11,13,12,11,13,12,11,13, &
                                    12,11,13,12,11,13,12,11,13,12,11,13,12,11,13,12,11,13,12,11, &
                                    13,12,11,13,12,11,13,12,11,13,12,12,0,0,12,0,0,12,0,0, &
                                    12,0,0,12,0,0,12,0,0,12,0,0,12,0,0,12,0,0,12,0, &
                                    0,12,0,0,12,0,0,13,0,0,13,0,0,13,0,0,13,0,0,13, &
                                    0,0,13,0,0,13,0,0,13,0,0,13,0,0,13,0,0,13,0,0, &
                                    13,0,0,13,0,0,12,14,13,12,14,13,12,14,13,12,14,13,12,14, &
                                    13,12,14,13,12,14,13,12,14,13,12,14,13,12,14,13,12,14,13,12, &
                                    14,13,12,14,13,12,14,13], &
                                    [3,196])
        LML_block_npty = [0,0,1,1,0,0,0,1,1,0,0,0,1,1,1,1,0,0,0,0, &
                            0,1,1,1,1,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0, &
                            0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,1,1,1,1, &
                            1,1,1,1,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1, &
                            1,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1, &
                            0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1, &
                            1,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1, &
                            1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1, &
                            1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0, &
                            0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
        LML_block_ml = [0,0,-1,1,-2,0,2,-1,1,-2,0,2,-3,-1,1,3,-4,-2,0,2, &
                            4,-3,-1,1,3,-4,-2,0,2,4,-5,-3,-1,1,3,5,-6,-4,-2,0, &
                            2,4,6,-5,-3,-1,1,3,5,-6,-4,-2,0,2,4,6,-7,-5,-3,-1, &
                            1,3,5,7,-8,-6,-4,-2,0,2,4,6,8,-7,-5,-3,-1,1,3,5, &
                            7,-8,-6,-4,-2,0,2,4,6,8,-9,-7,-5,-3,-1,1,3,5,7,9, &
                            -10,-8,-6,-4,-2,0,2,4,6,8,10,-9,-7,-5,-3,-1,1,3,5,7, &
                            9,-10,-8,-6,-4,-2,0,2,4,6,8,10,-11,-9,-7,-5,-3,-1,1,3, &
                            5,7,9,11,-12,-10,-8,-6,-4,-2,0,2,4,6,8,10,12,-11,-9,-7, &
                            -5,-3,-1,1,3,5,7,9,11,-12,-10,-8,-6,-4,-2,0,2,4,6,8, &
                            10,12,-13,-11,-9,-7,-5,-3,-1,1,3,5,7,9,11,13]
        LML_block_nconat = RESHAPE([1,1,1,0,2,1,2,1,2,1,2,1,2,1,1,0,1,0,1,0, &
                                1,0,1,0,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1, &
                                2,1,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0, &
                                2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1, &
                                2,1,2,1,2,1,1,0,1,0,1,0,1,0,1,0,1,0,1,0, &
                                1,0,1,0,1,0,1,0,1,0,1,0,2,1,2,1,2,1,2,1, &
                                2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1, &
                                2,1,2,1,2,1,1,0,1,0,1,0,1,0,1,0,1,0,1,0, &
                                1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0, &
                                2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1, &
                                2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1, &
                                2,1,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0, &
                                1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0, &
                                1,0,1,0,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1, &
                                2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1, &
                                2,1,2,1,2,1,2,1,2,1,2,1,2,1,1,0,1,0,1,0, &
                                1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0, &
                                1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0, &
                                1,0,1,0,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1, &
                                2,1,2,1,2,1,2,1,2,1,2,1], [2,196])
        ML_Max = 13
        xy_plane_desired = [.true.]
        debug = .FALSE.
        mat_size_wd = 87
        mat_size_wp = 87

        expected = expected_D_file( &
                                    ntarg = 2_INT32, &
                                    fintr = 38_INT32, &
                                    intr = 38_INT32, &

                                    blocks = [ &
                                        expected_dipole_block( &
                                        noterm = 46_INT32, &
                                        isi = 138_INT32, &
                                        inch = 3_INT32, &
                                        ILrgL = 10_INT32, &
                                        IPty = 0_INT32, &
                                        ISpn = 1_INT32, &
                                        jsi = 138_INT32, &
                                        jnch = 3_INT32, &
                                        JLrgL = 11_INT32, &
                                        lgsf = 1_INT32, &
                                        lglf = 11_INT32, &
                                        lgpf = 1_INT32, &
                                        lgsi = 1_INT32, &
                                        lgli = 10_INT32, &
                                        lgpi = 0_INT32, &
                                        iidip=[4_INT32], &
                                        ifdip=[5_INT32], &
                                        dipsto=subarray( &
                                            [ &
                                                7.382771E-03_REAL64, &
                                                2.614563E-03_REAL64, &
                                                6.011997E-03_REAL64, &
                                                -6.095297E-03_REAL64, &
                                                5.026517E-03_REAL64, &
                                                -5.847902E-03_REAL64, &
                                                -5.436139E-03_REAL64, &
                                                5.214474E-03_REAL64, &
                                                1.052881E-03_REAL64, &
                                                5.518426E-03_REAL64, &
                                                -9.706889E-04_REAL64, &
                                                -4.483457E-03_REAL64, &
                                                -5.504876E-03_REAL64, &
                                                -3.985440E-03_REAL64, &
                                                -5.047033E-03_REAL64, &
                                                -3.472089E-03_REAL64, &
                                                -4.239534E-03_REAL64, &
                                                -3.044363E-03_REAL64, &
                                                -3.187364E-03_REAL64, &
                                                -2.721560E-03_REAL64, &
                                                3.350340E-05_REAL64, &
                                                -9.106844E-06_REAL64, &
                                                -2.084410E-03_REAL64, &
                                                -2.507614E-03_REAL64, &
                                                1.190792E-03_REAL64, &
                                                2.267109E-03_REAL64, &
                                                6.874197E-04_REAL64, &
                                                1.737668E-03_REAL64, &
                                                4.840077E-04_REAL64, &
                                                9.419574E-04_REAL64, &
                                                1.425939E-05_REAL64, &
                                                2.821134E-08_REAL64, &
                                                -2.583672E-04_REAL64, &
                                                3.628497E-04_REAL64, &
                                                6.344675E-05_REAL64, &
                                                9.949539E-05_REAL64, &
                                                1.086628E-05_REAL64, &
                                                1.169795E-05_REAL64, &
                                                1.590668E-06_REAL64, &
                                                6.349802E-07_REAL64, &
                                                1.090773E-07_REAL64, &
                                                2.369410E-08_REAL64, &
                                                -1.369248E-08_REAL64, &
                                                2.452612E-09_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64 &                                                                                                                                                                                                                                     
                                            ], &
                                            [range(50,100), range(1,1), range(1,1)], &
                                            [138, 138, 38] &
                                        ), &
                                        dipsto_v=subarray( &
                                            [ &
                                                0.000000E+00_REAL64 &
                                            ], &
                                            [range(1,1), range(1,1), range(1,1)], &
                                            [1, 1, 1] &
                                            ) &
                                        ) &
                                        
                                    ] &
                                )

        CALL test_D_file(error, &
                            "atomic_tests/big_tests/Ar_LS/inputs/", &
                            expected, &
                            lplusp, &
                            crlv, &
                            iidip, &
                            ifdip, &
                            block_ind, &
                            dipole_coupled, &
                            wp_read_store, &
                            wd_read_store, &
                            wd_read_store_v, &
                            LML_wp_read_store, &
                            LML_wd_read_store, &
                            LML_wd_read_store_v, &
                            L_block_nchan, &
                            L_block_tot_nchan, &
                            LML_block_nchan, &
                            LML_block_tot_nchan, &
                            ntarg, &
                            nstmx, &
                            no_of_L_blocks, &
                            no_of_LML_blocks, &
                            coupling_id, &
                            dipole_velocity_output, &
                            L_block_lrgl, &
                            L_block_nspn, &
                            L_block_npty, &
                            LML_block_nspn, &
                            LML_block_lrgl, &
                            LML_block_l2p, &
                            LML_block_npty, &
                            LML_block_ml, &
                            LML_block_nconat, &
                            ML_Max, &
                            xy_plane_desired, &
                            fintr, &
                            dipsto, &
                            dipsto_v, &
                            debug, &
                            mat_size_wd, &
                            mat_size_wp, &
                            loc_of_blocks)
    END SUBROUTINE test_RMatrix1_Ar_LS

END MODULE test_dipole_input_file_read_D_RM1