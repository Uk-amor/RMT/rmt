MODULE test_inner_to_outer_interface 
    USE ISO_FORTRAN_ENV, ONLY: INT32

    USE testdrive, ONLY: check, error_type, new_unittest, unittest_type, test_failed

    USE testing_utilities, ONLY: compare, format_context

    USE inner_to_outer_interface, ONLY: setup_row_counters

    IMPLICIT NONE  ! all types must be explicitly declared

    PRIVATE
    PUBLIC :: collect_inner_to_outer_interface   
CONTAINS
    SUBROUTINE collect_inner_to_outer_interface(testsuite)
        TYPE(unittest_type), DIMENSION(:), ALLOCATABLE, INTENT(OUT) :: testsuite

        testsuite = [ &
                    new_unittest("Test setup row counters: ", test_setup_row_counters) &
                    ]      

    END SUBROUTINE collect_inner_to_outer_interface

    SUBROUTINE test_setup_row_counters(error) 
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error
        INTEGER, ALLOCATABLE :: rowbeg_array(:), rowend_array(:)
        CHARACTER(len=:), ALLOCATABLE :: context

        CALL setup_row_counters(my_num_LML_blocks=1, &
                                numrows=[2], &
                                rowbeg=1, &
                                rowend=2, &
                                numrows_sum=2, &
                                rowbeg_array=rowbeg_array, &
                                rowend_array=rowend_array)
        context = ""

        IF (.NOT. compare(rowbeg_array, [1])) THEN
            CALL format_context(context, "rowbeg_array", rowbeg_array, [1])
        END IF

        IF (.NOT. compare(rowend_array, [2])) THEN
            CALL format_context(context, "rowend_array", rowend_array, [2])
        END IF

        IF (context /= "") THEN
            CALL test_failed(error, "row_counters set incorrectly for my_num_LML_blocks=1!", context)
        END IF

        DEALLOCATE(rowbeg_array, rowend_array)

        CALL setup_row_counters(my_num_LML_blocks=2, &
                                numrows=[2,3], &
                                rowbeg=1, &
                                rowend=2, &
                                numrows_sum=5, &
                                rowbeg_array=rowbeg_array, &
                                rowend_array=rowend_array)
        context = ""

        IF (.NOT. compare(rowbeg_array, [1,1])) THEN
            CALL format_context(context, "rowbeg_array", rowbeg_array, [1,3])
        END IF

        IF (.NOT. compare(rowend_array, [2,5])) THEN
            CALL format_context(context, "rowend_array", rowend_array, [2,5])
        END IF

        IF (context /= "") THEN
            CALL test_failed(error, "row_counters set incorrectly for my_num_LML_blocks=2!", context)
        END IF
    END SUBROUTINE test_setup_row_counters

END MODULE test_inner_to_outer_interface