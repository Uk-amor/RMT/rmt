! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Cleans up at the end of an RMT calculation. Frees memory, shuts down MPI
!> communications, and closes output files.

MODULE finalise

    IMPLICIT NONE

CONTAINS

    SUBROUTINE end_program

        USE mpi_communications,       ONLY: i_am_in_outer_region, &
                                            i_am_in_inner_region, &
                                            i_am_in_double_outer_region, &
                                            i_am_double_outer_master, &
                                            i_am_outer_master, &
                                            i_am_inner_master, &
                                            i_am_outer_rank1, &
                                            helium_finalize, &
                                            all_processor_barrier
        USE initial_conditions,       ONLY: double_ionization
        USE io_files,                 ONLY: close_output_files
        USE mpi_layer_lblocks,        ONLY: i_am_block_master
        USE lrpots,                   ONLY: deallocate_lrpots, &
                                            deallocate_region_iii_lrpots
        USE outer_propagators,        ONLY: deallocate_outer_propagators, &
                                            deallocate_double_outer_region_arrays
        USE local_ham_matrix,         ONLY: dealloc_local_ham_matrix
        USE inner_propagators,        ONLY: deallocate_inner_propagators
        USE outer_to_inner_interface, ONLY: deallocate_fderivs
        USE inner_to_outer_interface, ONLY: dealloc_psi_at_inner_fd_pts
        USE setup_bspline_basis,      ONLY: dealloc_R_basis_for_fd_points
        USE setup_wv_data,            ONLY: deallocate_distribute_wv_data
        USE inner_parallelisation,    ONLY: deallocate_inner_region_parallelisation_2
        USE readhd,                   ONLY: deallocate_readhd, &
                                            deallocate_read_spline_files
        USE kernel,                   ONLY: deallocate_psi_inner_on_grid, &
                                            deallocate_psi_double_outer_inside_bndry
        USE wavefunction,             ONLY: deallocate_wavefunction
        USE outer_to_double_outer_interface, ONLY: deallocate_two_electron_psi_near_bndry, &
                                                   deallocate_overlap_and_surf_amps, &
                                                   deallocate_one_elec_basis_near_bndry
        USE double_outer_to_outer_interface, ONLY: deallocate_first_deriv_at_bndry

        CALL all_processor_barrier
        CALL close_output_files(i_am_double_outer_master, i_am_outer_master, &
                                i_am_inner_master, i_am_outer_rank1)
        CALL deallocate_wavefunction
        CALL deallocate_psi_inner_on_grid

        ! Deallocations:
        IF (i_am_in_double_outer_region) THEN
            CALL deallocate_region_iii_lrpots
!           CALL deallocate_overlap_and_surf_amps
            CALL deallocate_first_deriv_at_bndry
            CALL dealloc_local_ham_matrix
            CALL deallocate_double_outer_region_arrays
        END IF

        IF (i_am_in_outer_region) THEN
            CALL deallocate_lrpots
            CALL deallocate_outer_propagators
            CALL dealloc_local_ham_matrix
            IF (double_ionization) THEN
                CALL deallocate_psi_double_outer_inside_bndry
                CALL dealloc_R_basis_for_fd_points
            !   CALL deallocate_read_spline_files
                CALL deallocate_one_elec_basis_near_bndry
                CALL deallocate_overlap_and_surf_amps
                CALL deallocate_two_electron_psi_near_bndry
            END IF
        END IF

        IF (i_am_in_inner_region) THEN
            CALL deallocate_inner_propagators

            CALL deallocate_fderivs
!           CALL dealloc_setup_Lblock_comm_size
            CALL dealloc_psi_at_inner_fd_pts
            IF (i_am_block_master) CALL deallocate_distribute_wv_data
            CALL deallocate_inner_region_parallelisation_2
        END IF
        CALL deallocate_readhd(i_am_inner_master, &
                               i_am_in_outer_region)

        CALL helium_finalize

    END SUBROUTINE end_program

END MODULE finalise
