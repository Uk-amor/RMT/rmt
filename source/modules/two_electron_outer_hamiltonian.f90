! Copyright 2018
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Performs operations associated with the two-electron outer region Hamiltonian 
!> (i.e. performing the Hamiltonian-wavefunction multiplication, incrementing with the Laplacian,
!> the centrifugal terms, and the long range potential matrices).


MODULE two_electron_outer_hamiltonian

    USE calculation_parameters,    ONLY: nfdm
    USE communications_parameters, ONLY: col_id_1st, &
                                         col_id_last, &
                                         row_id_1st, &
                                         row_id_last
    USE coupling_rules,            ONLY: field_contraction
    USE double_outer_to_outer_interface, ONLY: send_boundary_derivative_double_outer_to_outer
    USE global_data,               ONLY: zero
    USE grid_parameters,           ONLY: x_1st, &
                                         x_last
    USE initial_conditions,        ONLY: numsols => no_of_field_confs
    USE local_ham_matrix,          ONLY: RR, Rinverse, Rinverse2, &
                                         r_nplus1, r_nplus2, &
                                         one_over_r_nplus1, one_over_r_nplus2, &
                                         one_over_r_nplus1_sq, one_over_r_nplus2_sq, &
                                         r_greater, r_less_over_r_greater, &
                                         get_1st_deriv, &
                                         one_over_delR
    USE lrpots,                    ONLY: region_three_we_coupled, &
                                         region_three_wp_coupled
    USE mpi_communications,        ONLY: get_my_block_id, &
                                         get_region_three_row_and_col_id, &
                                         i_am_along_diagonal, &
                                         nbr_block_id, &
                                         pe_id_for, &
                                         mpi_comm_region
    USE potentials,                ONLY: makewemat_reg3, &
                                         makewpmat_reg3
    USE precisn,                   ONLY: wp
    USE readhd,                    ONLY: first_l, &
                                         second_l, &
                                         ion_e, &
                                         tot_l, &
                                         tot_ml, &
                                         two_electron_l, &
                                         two_electron_nchan, &
                                         two_electron_spin, &
                                         two_electron_xphase, &
                                         exchange_channel
    USE rmt_assert,                ONLY: assert

    USE MPI
    IMPLICIT NONE

    INTEGER, ALLOCATABLE, SAVE     :: twoe_wp_neighbour_count(:)
    INTEGER, ALLOCATABLE, SAVE     :: twoe_we_neighbour_count(:)
    INTEGER, ALLOCATABLE, SAVE     :: twoe_wp_neighbour_store(:,:)
    INTEGER, ALLOCATABLE, SAVE     :: twoe_we_neighbour_store(:,:)

    PUBLIC setup_region_three_neighbour_counts_and_store

CONTAINS

!---------------------------------------------------------------------

    !> \brief   perform hamiltonian times wavefunction multiplication for two electron outer region
    !> \authors G Armstrong
    !> \date    2022
    !>
    !> This multiplication is
    !> \f[ \left[ -\frac{1}{2}\frac{d^2}{dr_{N+1}^2} - frac{1}{2}\frac{d^2}{dr_{N+2}^2}
    !>            + \frac{l_a(la + 1)}{2r_{N+1}^2} + \frac{lb(lb + 1)}{2r_{N+2}^2}
    !>            - \frac{Z-N}{r_{N+1}} - \frac{Z-N}{r_{N+2}} + 0.5*k^2
    !>    \right]\Psi_p
    !>    + \sum_{p'} \left[W^E_{pp'} + W^P_{pp'} + W^D_{pp'}\right]\Psi_{p'}
    !>  \f]
    !> where $p,p'$ run over all two-electron channels.
    !>
    !> For each solution (wavefunction) and each channel, apply the centrigual barrier term,
    !> the energy shift and the long range potential matrices.
    !> 
    !> \param[in]     delR               Outer region grid spacing
    !> \param[in]     Z_minus_N          Net charge of the residual ion
    !> \param[in]     field_strength     Electric Field Strength in a.u.
    !> \param[in]     psi_double_outer   Wavefunction on local portion of the two-electron grid.
    !>                                   dimensions (grid points, grid points, channels, solutions)
    !> \param[out]    h_psi_double_outer Hamiltonian times the wavefunction on local portion of the two-electron grid.
    !>                                   dimensions (grid points, grid points, channels, solutions)
    !>
    !######################################
    !>
    !> \param[inout]  wf_halo_left       Wavefunction on two grid points inside the one-electron outer region (horizontal axis)
    !> \param[inout]  wf_halo_right      Wavefunction on two grid points beyond the outer edge of the two-electron outer region (horizontal axis)
    !>
    !> \param[inout]  wf_halo_bottom     Wavefunction on two grid points inside the one-electron outer region (vertical axis)
    !> \param[inout]  wf_halo_top        Wavefunction on two grid points beyond the outer edge of the two-electron outer region (vertical axis)
    !>
    !######################################
    !> \param[in]     WE_store_reg3      Saved values for the WE long range potential matrix
    !>                                   dimensions (grid points, grid points, width, channels)
    !>
    SUBROUTINE ham_x_vector_double_outer(delR, Z_minus_N, field_strength, psi_double_outer, h_psi_double_outer, &
                                         wf_halo_left, wf_halo_right, &
                                         wf_halo_bottom, wf_halo_top, &
                                         first_deriv_of_psi_at_b, t_step2, &
                                         i_am_along_outer_boundary, &
                                         WE_store_reg3)

      USE lrpots, ONLY: region_three_we_width

      REAL(wp), INTENT(IN)       :: delR, Z_minus_N, t_step2
      REAL(wp), INTENT(IN)       :: field_strength(3, numsols)
      COMPLEX(wp), INTENT(IN)    :: psi_double_outer(x_1st:x_last, x_1st:x_last, two_electron_nchan, numsols)
      COMPLEX(wp), INTENT(OUT)   :: h_psi_double_outer(x_1st:x_last, x_1st:x_last, two_electron_nchan, numsols)
      COMPLEX(wp), INTENT(INOUT) :: wf_halo_left(x_1st-nfdm:x_1st-1, x_1st:x_last, two_electron_nchan, numsols)
      COMPLEX(wp), INTENT(INOUT) :: wf_halo_right(x_last+1:x_last+nfdm, x_1st:x_last, two_electron_nchan, numsols)
      COMPLEX(wp), INTENT(INOUT) :: wf_halo_bottom(x_1st:x_last, x_1st-nfdm:x_1st-1, two_electron_nchan, numsols)
      COMPLEX(wp), INTENT(INOUT) :: wf_halo_top(x_1st:x_last, x_last+1:x_last+nfdm, two_electron_nchan, numsols)
      COMPLEX(wp), INTENT(OUT)   :: first_deriv_of_psi_at_b(x_1st:x_last, 1:two_electron_nchan, 1:numsols)
      LOGICAL, INTENT(IN)        :: i_am_along_outer_boundary
      REAL(wp), INTENT(IN)       :: WE_store_reg3(x_1st:x_last, x_1st:x_last, region_three_we_width, two_electron_nchan)

      REAL(wp)                   :: z_minus_n_over_r_nplus1(x_1st:x_last)
      REAL(wp)                   :: z_minus_n_over_r_nplus2(x_1st:x_last)
      INTEGER                    :: channel_id, i, isol
      INTEGER                    :: la, lb
      INTEGER                    :: number_grid_points
      LOGICAL                    :: field_on
      REAL(wp)                   :: threshold_di
      COMPLEX(wp)                :: deriv_psi(x_1st:x_last)

!     CALL get_two_electron_wavefunction_beyond_outer_bndries(psi_double_outer, wf_halo_right, wf_halo_top)
!     call to this routine only needed at this point if first derivative at all grid points is needed,
!     e.g to calculate derivatives for the velocity form of the dipole operator.

      field_on = ANY(field_strength /= 0.0_wp)

      IF (i_am_along_outer_boundary) THEN
          first_deriv_of_psi_at_b = zero
          !$OMP PARALLEL DO PRIVATE(isol,i,channel_id) COLLAPSE(3)
          DO isol = 1, numsols
              DO i = x_1st, x_last
                 DO channel_id = 1, two_electron_nchan
!                           (psi_double_outer(x_1st+1:x_1st + nfdm, i, channel_id, isol), &
                    CALL get_1st_deriv &
                           (psi_double_outer(x_1st:x_1st + nfdm, i, channel_id, isol), &
                            deriv_psi, &
                            wf_halo_left(:, i, channel_id, isol), &
                            wf_halo_left(:, i, channel_id, isol), & !not used: dummy argument
                            one_over_delR)
                    first_deriv_of_psi_at_b(i, channel_id, isol) = deriv_psi(x_1st)
                 END DO
              END DO
          END DO

          first_deriv_of_psi_at_b = first_deriv_of_psi_at_b * t_step2
          CALL send_boundary_derivative_double_outer_to_outer(first_deriv_of_psi_at_b)

      END IF

      z_minus_n_over_r_nplus1 = Z_minus_N * one_over_r_nplus1
      z_minus_n_over_r_nplus2 = Z_minus_N * one_over_r_nplus2
      number_grid_points = x_last - x_1st + 1

!     Get wavefunction data needed to evaluate second radial derivative at boundary between one- and two-electron outer regions
      CALL get_two_electron_wavefunction_beyond_outer_bndries(psi_double_outer, wf_halo_right, wf_halo_top)

      h_psi_double_outer = zero
      !$OMP PARALLEL DO PRIVATE(isol,channel_id,la,lb,threshold_di) COLLAPSE(2)
      DO isol = 1, numsols
         DO channel_id = 1, two_electron_nchan
             la = first_l(channel_id)
             lb = second_l(channel_id)
             threshold_di = ion_e(channel_id)
             ! [-0.5*grad_{N+1}^2 -0.5*grad_{N+2}^2 ] psi
             CALL apply_two_electron_laplacian  &
                  (psi_double_outer(:, :, channel_id, isol), &
                   h_psi_double_outer(:, :, channel_id, isol), &
                   wf_halo_left(:, :, channel_id, isol), &
                   wf_halo_right(:, :, channel_id, isol), &
                   wf_halo_bottom(:, :, channel_id, isol), &
                   wf_halo_top(:, :, channel_id, isol), &
                   la, &
                   lb, &
                   number_grid_points, &
                   delR)

             ! - ( (Z-N)/r_(N+1) + (Z-N)/r_(N+2) ) psi
             CALL apply_two_electron_centrifugal_and_energy_shift &
                  (number_grid_points, &
                   psi_double_outer(:, :, channel_id, isol), &
                   h_psi_double_outer(:, :, channel_id, isol), &
                   z_minus_n_over_r_nplus1, &
                   z_minus_n_over_r_nplus2, &
                   threshold_di)

             CALL apply_two_electron_long_range_potentials &
                  (number_grid_points, &
                   channel_id, &
                   field_strength(:, isol), &
                   field_on, &
                   r_nplus1, &
                   r_nplus2, &
                   WE_store_reg3, &
                   psi_double_outer(:, :, :, isol), &
                   h_psi_double_outer(:, :, channel_id, isol))
         END DO
      END DO

    END SUBROUTINE ham_x_vector_double_outer

!---------------------------------------------------------------------

    !> \brief   build arrays which decide which channels are coupled, and determine
    !>          the maximum bandwidth of these arrays i.e. the maximum number of
    !>          channels to which any channel may be coupled (by either the W_P
    !>          or W_E interactions in the two-electron outer region).
    !> \authors G Armstrong
    !> \date    2022
    !>
    !> Instead of running through every possible channel at run-time and determining whether it is coupled to the current channel,
    !> we calculate ahead of time for each channel, which other channels are its "neighbours" i.e. which have a non-zero coupling
    !> for each of the three potential matrices. The result is two arrays each for WE, WP and WD. There is a neighbour_count of
    !> dimension (number of channels) which for each channel stores the number of neighbours. And there is a neighbour_store of
    !> dimension (number of channels, number of channels) which stores for channel_i, the list of which channel_js are coupled.

    SUBROUTINE setup_region_three_neighbour_counts_and_store

        USE lrpots, ONLY: region_three_we_width, region_three_wp_width

        INTEGER     :: channel_i, channel_j
        INTEGER     :: coupling_index
        REAL(wp)    :: nonzero = 1.0e-14_wp
        REAL(wp)    :: we_ham3
        COMPLEX(wp) :: wp_ham3(3)

        CALL allocate_region_three_neighbour_stores
        ! WE
        DO channel_i = 1, two_electron_nchan

            coupling_index = 0
            DO channel_j = 1, two_electron_nchan

                IF (ABS(channel_i - channel_j) <= region_three_we_width ) THEN
                    CALL makewemat_reg3(r_greater(x_1st, x_1st), r_less_over_r_greater(x_1st, x_1st), &
                                        channel_i, channel_j, we_ham3)
                ELSE
                    we_ham3 = 0.0_wp
                END IF

                IF  (ABS(WE_ham3) > nonzero) THEN
                    IF (region_three_we_coupled(channel_i, channel_j)) THEN
                        coupling_index = coupling_index + 1
                        twoe_we_neighbour_count(channel_i) = coupling_index
                        twoe_we_neighbour_store(channel_i,coupling_index) = channel_j
                    END IF
                END IF
            END DO
        END DO


        ! WP
        DO channel_i = 1, two_electron_nchan

            coupling_index = 0
            DO channel_j = 1, two_electron_nchan

                IF (ABS(channel_i - channel_j) <= region_three_wp_width) THEN
                    CALL makewpmat_reg3(channel_i, channel_j, wp_ham3)
                ELSE
                    wp_ham3 = zero
                END IF

                IF ( any(ABS(wp_ham3) > nonzero) ) THEN
                    IF (region_three_wp_coupled(channel_i, channel_j)) THEN
                        coupling_index = coupling_index + 1
                        twoe_wp_neighbour_count(channel_i) = coupling_index
                        twoe_wp_neighbour_store(channel_i,coupling_index) = channel_j
                    END IF
                END IF
            END DO
        END DO

        region_three_we_width = MAXVAL(twoe_we_neighbour_count)
        region_three_wp_width = MAXVAL(twoe_wp_neighbour_count)

    END SUBROUTINE setup_region_three_neighbour_counts_and_store

!---------------------------------------------------------------------

    !> \brief   apply the two-electron Laplacian for each two-electron channel and each solution
    !> \authors G Armstrong
    !> \date    2022
    !>
    !> \param[in]     psi                 two-electron wavefunction for a single channel and solution on local 2D grid
    !>                                    dimension(number_grid_points, number_grid_points)
    !> \param[inout]  H_psi               Hamiltonian times psi on local 2D grid
    !>                                    dimension(number_grid_points, number_grid_points)
    !> \param[in]     la                  orbital angular momentum of electron N+1
    !> \param[in]     lb                  orbital angular momentum of electron N+2
    !> \param[in]     wf_halo_left        Two-electron wavefunction values at two points in the one-electron outer region
    !> \param[in]     wf_halo_right       Two-electron wavefunction values at two points outside the two-electron outer region
    !> \param[in]     wf_halo_bottom      Two-electron wavefunction values at two points in the one-electron outer region
    !> \param[in]     wf_halo_top         Two-electron wavefunction values at two points outside the two-electron outer region
    !> \param[in]     number_grid_points  number of radial grid points spanned in each dimension on local 2D grid
    !> \param[in]     delR                radial grid spacing

    SUBROUTINE apply_two_electron_laplacian(psi, H_psi, &
               wf_halo_left, wf_halo_right, &
               wf_halo_bottom, wf_halo_top, &
               la, lb, &
               number_grid_points, delR)

        USE local_ham_matrix, ONLY: incr_with_laplacian_in_r_nplus1, &
                                    incr_with_laplacian_in_r_nplus2

        COMPLEX(wp), INTENT(IN)    :: psi(number_grid_points, number_grid_points)
        COMPLEX(wp), INTENT(OUT)   :: H_psi(number_grid_points, number_grid_points)
        COMPLEX(wp), INTENT(IN)    :: wf_halo_left(x_1st - nfdm:x_1st - 1, number_grid_points)
        COMPLEX(wp), INTENT(IN)    :: wf_halo_right(x_last + 1:x_last + nfdm, number_grid_points)
        COMPLEX(wp), INTENT(IN)    :: wf_halo_bottom(number_grid_points, x_1st - nfdm:x_1st - 1)
        COMPLEX(wp), INTENT(IN)    :: wf_halo_top(number_grid_points, x_last + 1:x_last + nfdm)
        INTEGER, INTENT(IN)        :: la, lb
        INTEGER, INTENT(IN)        :: number_grid_points
        REAL(wp), INTENT(IN)       :: delR

        REAL(wp)    :: atomic_ham_factor, one_over_delr_sq
        REAL(wp)    :: second_deriv_coeff

        atomic_ham_factor = -0.5_wp
        one_over_delr_sq = 1.0_wp/(delR*delR)
        second_deriv_coeff = atomic_ham_factor * one_over_delr_sq

        H_psi = zero

!>      Apply Laplacian for electron N+1, using la for the centripetal term, and wavefunctions from the left and right boundaries
        CALL incr_with_laplacian_in_r_nplus1(psi, H_psi, la, second_deriv_coeff, atomic_ham_factor, &
                                    one_over_r_nplus1_sq, wf_halo_left, wf_halo_right)
!>      Apply Laplacian for electron N+2, using lb for the centripetal term, and wavefunctions from the bottom and top boundaries
        CALL incr_with_laplacian_in_r_nplus2(psi, H_psi, lb, second_deriv_coeff, atomic_ham_factor, &
                                    one_over_r_nplus2_sq, wf_halo_bottom, wf_halo_top)

    END SUBROUTINE apply_two_electron_laplacian

!---------------------------------------------------------------------

    !> \brief   apply the centrifugal barrier and energy shift terms of the two-e hamiltonian for a single channel
    !> \authors G Armstrong
    !> \date    2022
    !>
    !> \param[in]    number_grid_points
    !> \param[in]    psi                     wavefunction for a single channel on local 2D grid: dimension (grid points, grid points)
    !> \param[inout] H_psi                   hamiltonian times wavefunction on local 2D grid
    !>                                       dimension (grid points, grid_points)
    !> \param[in]    Z_minus_N_over_R
                                               
    SUBROUTINE apply_two_electron_centrifugal_and_energy_shift (number_grid_points, &
                                                                psi, &
                                                                H_psi, &
                                                                Z_minus_N_over_r_nplus1, &
                                                                Z_minus_N_over_r_nplus2, &
                                                                double_ion_threshold)

        INTEGER, INTENT(IN)        :: number_grid_points
        COMPLEX(wp), INTENT(IN)    :: psi(number_grid_points, number_grid_points)
        COMPLEX(wp), INTENT(INOUT) :: H_psi(number_grid_points, number_grid_points)
        REAL(wp), INTENT(IN)       :: Z_minus_N_over_r_nplus1(number_grid_points)
        REAL(wp), INTENT(IN)       :: Z_minus_N_over_r_nplus2(number_grid_points)
        REAL(wp), INTENT(IN)       :: double_ion_threshold

        INTEGER                    :: i, j

        DO i = 1, number_grid_points
            DO j = 1, number_grid_points
                H_psi(i, j) = H_psi(i, j) - &
                              ( Z_minus_N_over_r_nplus1(i) + Z_minus_N_over_r_nplus2(j) - &
                                double_ion_threshold ) * psi(i, j)
            END DO
        END DO

    END SUBROUTINE apply_two_electron_centrifugal_and_energy_shift

!---------------------------------------------------------------------

    !> \brief   apply the three long range potential matrices (WE, WP and WD) to the wavefunction
    !>          for a single channel in the two-electron outer region.
    !> \authors G Armstrong
    !> \date    2022
    !>
    !> \param[in]    number_grid_points
    !> \param[in]    channel_id          ID for the current channel (needed to determine couplings)
    !> \param[in]    field_strength      3D electric field strength in a.u. (needed for laser coupling terms)
    !> \param[in]    field_on            logical: if false then laser coupling terms can be ignored
    !> \param[in]    r_store             radial coordinates of the local grid points dimension(grid points)
    !> \param[in]    WE_store_reg3       stored values of the WE potential matrix
    !>                                   dimension (grid points, grid points, channels, channels)
    !> \param[in]    psi                 Wavefunction (note: for all channels), dimension (grid points, grid points, channels)
    !> \param[inout] H_psi               Hamiltonian times wavefunction, dimension (grid points, grid points)
    !>
    SUBROUTINE apply_two_electron_long_range_potentials(number_grid_points, &
                                                           channel_id, &
                                                           field_strength, field_on, &
                                                           r_nplus1, r_nplus2, &
                                                           WE_store_reg3, psi, H_psi)

        USE initial_conditions, ONLY: use_two_electron_we_ham, use_two_electron_wp_ham
        USE lrpots,             ONLY: region_three_we_width

        INTEGER, INTENT(IN)        :: number_grid_points
        INTEGER, INTENT(IN)        :: channel_id
        REAL(wp), INTENT(IN)       :: field_strength(3)
        REAL(wp), INTENT(IN)       :: r_nplus1(number_grid_points)
        REAL(wp), INTENT(IN)       :: r_nplus2(number_grid_points)
        REAL(wp), INTENT(IN)       :: WE_store_reg3(number_grid_points, number_grid_points, &
                                                    region_three_we_width, two_electron_nchan)
        COMPLEX(wp), INTENT(IN)    :: psi(number_grid_points, number_grid_points, 1:two_electron_nchan)
        COMPLEX(wp), INTENT(INOUT) :: H_psi(number_grid_points, number_grid_points)
        LOGICAL, INTENT(IN)        :: field_on

        IF (use_two_electron_we_ham) THEN
            CALL incr_w_we_ham_x_vec_reg3(psi, H_psi, channel_id, number_grid_points, WE_store_reg3)
        END IF

        IF (field_on .AND. use_two_electron_wp_ham) THEN
           CALL incr_w_wp_ham_x_vec_len_reg3(psi, H_psi, channel_id, field_strength, number_grid_points, r_nplus1, r_nplus2)
        END IF

    END SUBROUTINE apply_two_electron_long_range_potentials

!---------------------------------------------------------------------

    !> \brief   apply the WE long range potential matrix (coupling between outer electrons and residual doubly-charged ion,
    !>          and between the two outer electrons themselves)
    !> \authors G Armstrong
    !> \date    2022
    !>
    !> \param[in]    psi                Wavefunction on the local 2D grid (note, full wavefunction for all channels)
    !>                                  dimension (grid points, grid points, channels)
    !> \param[inout] H_psi              Hamiltonian times the wavefunction on the local 2D grid for a specific channel
    !>                                  dimension (grid_points, grid_points)
    !> \param[in]    channel_i          ID of the current channel
    !> \param[in]    number_grid_points
    !> \param[in]    WE_store_reg3      Stored values for the WE coupling matrix (grid points, grid points, width, channels)
    !>
    SUBROUTINE incr_w_we_ham_x_vec_reg3(psi, H_psi, channel_i, number_grid_points, WE_store_reg3)

        USE lrpots, ONLY: region_three_we_width

        INTEGER, INTENT(IN)        :: channel_i
        INTEGER, INTENT(IN)        :: number_grid_points
        COMPLEX(wp), INTENT(IN)    :: psi(number_grid_points, number_grid_points, two_electron_nchan)
        COMPLEX(wp), INTENT(INOUT) :: H_psi(number_grid_points, number_grid_points)
        REAL(wp), INTENT(IN)       :: WE_store_reg3(number_grid_points, number_grid_points, &
                                                    region_three_we_width, two_electron_nchan)

        INTEGER                    :: i, ii, j
        INTEGER                    :: channel_j

        DO ii = 1, twoe_we_neighbour_count(channel_i)
            channel_j = twoe_we_neighbour_store(channel_i, ii)
            DO j = 1, number_grid_points
                DO i = 1, number_grid_points
                    H_psi(i, j) = H_psi(i, j) + WE_store_reg3(i, j, ii, channel_i)*psi(i, j, channel_j)
                END DO
            END DO
        END DO       

    END SUBROUTINE incr_w_we_ham_x_vec_reg3

!---------------------------------------------------------------------

    !> \brief   Apply the two-electron WP (laser+outer electron) potential
    !> \authors G Armstrong
    !> \date    2022
    !>
    !> Multiplies the vector \c Psi (corresponding to channel \c i) by the \f$ W_P \f$ potential
    !> in length gauge and adds the result to \c H_Psi (corresponding to channel \c j). Channels that
    !> are coupled by \f$ W_P \f$ are listed in the `twoe_wp_neighbour_store`.
    !>
    !> \param[in]    psi                Full (all channels) wavefunction on the local 2D grid
    !> \param[in]    H_psi              Hamiltonian times the wavefunction for the current channel on the local 2D grid
    !> \param[in]    channel_i          ID for the current channel
    !> \param[in]    field_strength     3D Electric field strength in a.u.
    !> \param[in]    number_grid_points
    !> \param[in]    r_nplus1           radial coordinate for the local grid points in r_N+1 dimension (grid points)
    !> \param[in]    r_nplus2           radial coordinate for the local grid points in r_N+2 dimension (grid points)
    !>
    SUBROUTINE incr_w_wp_ham_x_vec_len_reg3(psi, H_psi, channel_i, field_strength, &
                                            number_grid_points, r_nplus1, r_nplus2)

        USE coupling_rules, ONLY: field_contraction

        INTEGER, INTENT(IN)        :: number_grid_points
        COMPLEX(wp), INTENT(INOUT) :: H_psi(1:number_grid_points, 1:number_grid_points)
        COMPLEX(wp), INTENT(IN)    :: psi(1:number_grid_points, 1:number_grid_points, two_electron_nchan)
        INTEGER, INTENT(IN)        :: channel_i
        REAL(wp), INTENT(IN)       :: field_strength(3)
        REAL(wp), INTENT(IN)       :: r_nplus1(number_grid_points)
        REAL(wp), INTENT(IN)       :: r_nplus2(number_grid_points)

        INTEGER     :: i, j
        INTEGER     :: channel_j, ii
        INTEGER     :: total_Mi, total_Mj
        COMPLEX(wp) :: wp_ham3(3), wp_ham3_coeff

        total_Mi = tot_ml(channel_i)
        DO ii = 1, twoe_wp_neighbour_count(channel_i)
            channel_j = twoe_wp_neighbour_store(channel_i,ii)
            if (first_l(channel_i) /= first_l(channel_j)) then
             total_Mj = tot_ml(channel_j)
             CALL makewpmat_reg3(channel_i, channel_j, wp_ham3)
 
             wp_ham3_coeff = field_contraction(field_strength, wp_ham3, total_Mj, total_Mi)

             IF (wp_ham3_coeff /= zero) THEN
                 DO j = 1, number_grid_points
                    DO i = 1, number_grid_points
                        H_psi(i, j) = H_psi(i, j) + wp_ham3_coeff*r_nplus1(i)*psi(i, j, channel_j)
                    END DO
                 END DO
             END IF

            else

             total_Mj = tot_ml(channel_j)
             CALL makewpmat_reg3(channel_i, channel_j, wp_ham3)
 
             wp_ham3_coeff = field_contraction(field_strength, wp_ham3, total_Mj, total_Mi)

             IF (wp_ham3_coeff /= zero) THEN
                 DO j = 1, number_grid_points
                    DO i = 1, number_grid_points
                        H_psi(i, j) = H_psi(i, j) + wp_ham3_coeff*r_nplus2(j)*psi(i, j, channel_j)
                    END DO
                 END DO
             END IF

            end if
        END DO

    END SUBROUTINE incr_w_wp_ham_x_vec_len_reg3

!---------------------------------------------------------------------

    !> \brief   Setup and store the two-electron (region III) WE array
    !> \authors G Armstrong
    !> \date    2022
    !>
    !> The array storing the WE potential coefficients  (coupling the electron to the residual
    !> ion) is set up outside of the time loop to avoid unnecessary steps in the
    !> time propagation.
    !>
    !> \param[inout] WE_store              Stored array of coefficients (grid points, grid points, width, channels)
    !> \param[in]    r_greater             radial coordinates of outermost electron (grid points, grid points)
    !> \param[in]    r_less_over_r_greater Values of MIN(r_{N+1}, r_{N+2}}/MAX(r_{N+1}, r_{N+2}) needed in multipole expansion
    !>                                     dimension (grid ponts, grid points)
    !> \param[in]    number_grid_points
    !>
    SUBROUTINE store_region_three_we (WE_store, r_greater, r_less_over_r_greater, number_grid_points)

        USE lrpots,     ONLY: region_three_we_width
        USE potentials, ONLY: makewemat_reg3

        INTEGER,  INTENT(IN)    :: number_grid_points
        REAL(wp), INTENT(INOUT) :: WE_store(number_grid_points, number_grid_points, region_three_we_width, two_electron_nchan)
        REAL(wp), INTENT(IN)    :: r_greater(number_grid_points, number_grid_points)
        REAL(wp), INTENT(IN)    :: r_less_over_r_greater(number_grid_points, number_grid_points)
        INTEGER                 :: channel_i, channel_j
        INTEGER                 :: ii, i, j
        REAL(wp)                :: WE_ham

        DO channel_i = 1, two_electron_nchan

            DO ii = 1, twoe_we_neighbour_count(channel_i)
                channel_j = twoe_we_neighbour_store(channel_i,ii)

                DO i = 1, number_grid_points
                    DO j = 1, number_grid_points
                        CALL makewemat_reg3(r_greater(i, j), r_less_over_r_greater(i, j), channel_i, channel_j, WE_ham)
                        WE_store(i, j, ii, channel_i) = WE_ham
                    END DO
                END DO
            END DO
        END DO

    END SUBROUTINE store_region_three_we

!---------------------------------------------------------------------

    !>  \brief  Set up transfer of wavefunction values between neighbouring cores in the two-electron outer region.
    !> \authors G Armstrong
    !> \date    2022

    !> To calculate derivatives at the first and final two points in each
    !> sector, wavefunction values from neighbouring cores are required.
    !> To calculate the derivative at the first two points in each direction,
    !> each core must source wavefunction values from the core to the left and
    !> the core directly below it.
    !> Wavefunction values at the last two points on each core must also be sent
    !> to the cores directly to the right and above it to compute derivatives on
    !> these neighbouring cores.
    !> In setting up the grid of cores for the two-electron outer region, 
    !> r_{N+2} is treated as the horizontal axis, and r_{N+1} as the vertical.
    !> This means that wavefunction values for different values of r_{N+2} are
    !> passed from between cores to the left and right of any give core.
    !> Wavefunction values for different values of r_{N+1} are passed between
    !> cores above and below a given core.

    !>  \param[in]  psi           The two-electron outer region wavefunction
    !>                            dimension(x_1st:xlast, x_1st:x_last, two_electron_nchan, numsols)
    !>  \param[out] r1_bndry_lo   Wavefunction values at the last two points on the below neighbour core
    !>                            dimension(x_1st-2:x_1st-1, x_1st:x_last, two_electron_nchan, numsols)
    !>  \param[out] r1_bndry_hi   Wavefunction values at the first two points on the above neighbour core
    !>                            dimension(x_last+1:x_last+2, x_1st:x_last, two_electron_nchan, numsols)
    !>  \param[out] r2_bndry_lo   Wavefunction values at the last two points on the left neighbour core
    !>                            dimension(x_1st:x_last, x_1st-2:x_1st-1, two_electron_nchan, numsols)
    !>  \param[out] r2_bndry_hi   Wavefunction values at the first two points on the right neighbour core
    !>                            dimension(x_1st:x_last, x_last+1:x_last+2, two_electron_nchan, numsols)

    SUBROUTINE get_two_electron_remote_bndries(psi, r1_bndry_lo, r1_bndry_hi, &
                                               r2_bndry_lo, r2_bndry_hi)

        COMPLEX(wp), INTENT(IN)  :: psi(x_1st:x_last, x_1st:x_last, two_electron_nchan, numsols)
        COMPLEX(wp), INTENT(OUT) :: r1_bndry_lo(x_1st - nfdm:x_1st - 1, x_1st:x_last, two_electron_nchan, numsols)
        COMPLEX(wp), INTENT(OUT) :: r1_bndry_hi(x_last + 1:x_last + nfdm, x_1st:x_last, two_electron_nchan, numsols)
        COMPLEX(wp), INTENT(OUT) :: r2_bndry_lo(x_1st:x_last, x_1st - nfdm:x_1st - 1, two_electron_nchan, numsols)
        COMPLEX(wp), INTENT(OUT) :: r2_bndry_hi(x_1st:x_last, x_last + 1:x_last + nfdm, two_electron_nchan, numsols)

        INTEGER :: my_block_id, my_row_id, my_col_id
        INTEGER :: my_r1_nbr_hi_row_id, my_r1_nbr_lo_row_id
        INTEGER :: my_r2_nbr_hi_col_id, my_r2_nbr_lo_col_id
        INTEGER :: dest_pe, source_pe, ierr, tag
        INTEGER :: x_length, x_size, num_channels
        INTEGER :: status(MPI_STATUS_SIZE)
        INTEGER :: i, j, channel_id
        REAL(wp) :: phase

        INTEGER :: la, lb, l, s, xchan

        CALL get_my_block_id(my_block_id)
        CALL get_region_three_row_and_col_id(my_block_id, my_row_id, my_col_id)

        my_r2_nbr_hi_col_id = my_col_id + 1
        my_r2_nbr_lo_col_id = my_col_id - 1

        my_r1_nbr_hi_row_id = my_row_id + 1
        my_r1_nbr_lo_row_id = my_row_id - 1

        x_length = x_last - x_1st + 1
        num_channels = two_electron_nchan
        x_size = x_length * nfdm* num_channels

        ! outer electron 2 (columns)
        IF (my_r2_nbr_hi_col_id > col_id_last) THEN
           dest_pe = MPI_PROC_NULL
        ELSE
           dest_pe = pe_id_for(nbr_block_id(my_row_id, my_r2_nbr_hi_col_id))
        END IF

        IF (my_r2_nbr_lo_col_id < my_row_id) THEN
           source_pe = MPI_PROC_NULL
        ELSE
           source_pe = pe_id_for(nbr_block_id(my_row_id, my_r2_nbr_lo_col_id))
        END IF

        DO i = x_1st - nfdm, x_1st - 1
           r2_bndry_lo(:, i, :, :) = psi(:, i + x_last, :, :)
        END DO

        tag = 1
        CALL MPI_SENDRECV_REPLACE(r2_bndry_lo, x_size * numsols, MPI_DOUBLE_COMPLEX, &
                                  dest_pe, tag, source_pe, tag, mpi_comm_region, status, ierr)

        IF (my_r2_nbr_lo_col_id < my_row_id) THEN
           dest_pe = MPI_PROC_NULL
        ELSE
           dest_pe = pe_id_for(nbr_block_id(my_row_id, my_r2_nbr_lo_col_id))
        END IF

        IF (my_r2_nbr_hi_col_id > col_id_last) THEN
           source_pe = MPI_PROC_NULL
        ELSE
           source_pe = pe_id_for(nbr_block_id(my_row_id, my_r2_nbr_hi_col_id))
        END IF

        DO i = x_last + 1, x_last + nfdm
           r2_bndry_hi(:, i, :, :) = psi(:, i - x_last, :, :)
        END DO

        tag = 2
        CALL MPI_SENDRECV_REPLACE(r2_bndry_hi, x_size * numsols, MPI_DOUBLE_COMPLEX, &
                                  dest_pe, tag, source_pe, tag, mpi_comm_region, status, ierr)


        ! outer electron 1 (rows)
        IF (my_r1_nbr_hi_row_id > my_col_id) THEN
           dest_pe = MPI_PROC_NULL
        ELSE
           dest_pe = pe_id_for(nbr_block_id(my_r1_nbr_hi_row_id, my_col_id))
        END IF

        IF (my_r1_nbr_lo_row_id < row_id_1st) THEN
           source_pe = MPI_PROC_NULL
        ELSE
           source_pe = pe_id_for(nbr_block_id(my_r1_nbr_lo_row_id, my_col_id))
        END IF

        DO j = x_1st - nfdm, x_1st - 1
           r1_bndry_lo(j, :, :, :) = psi(j + x_last, :, :, :)
        END DO

        tag = 3
        CALL MPI_SENDRECV_REPLACE(r1_bndry_lo, x_size * numsols, MPI_DOUBLE_COMPLEX, &
                                  dest_pe, tag, source_pe, tag, mpi_comm_region, status, ierr)

        IF (my_r1_nbr_lo_row_id < row_id_1st) THEN
           dest_pe = MPI_PROC_NULL
        ELSE
           dest_pe = pe_id_for(nbr_block_id(my_r1_nbr_lo_row_id, my_col_id))
        END IF

        IF (my_r1_nbr_hi_row_id > my_col_id) THEN
           source_pe = MPI_PROC_NULL
        ELSE
           source_pe = pe_id_for(nbr_block_id(my_r1_nbr_hi_row_id, my_col_id))
        END IF

        DO j = x_last + 1, x_last + nfdm
           r1_bndry_hi(j, :, :, :) = psi(j - x_last, :, :, :)
        END DO

        tag = 4
        CALL MPI_SENDRECV_REPLACE(r1_bndry_hi, x_size * numsols, MPI_DOUBLE_COMPLEX, &
                                  dest_pe, tag, source_pe, tag, mpi_comm_region, status, ierr)



        IF (i_am_along_diagonal) THEN
!     check IF tests
            DO channel_id = 1, two_electron_nchan
                la = first_l(channel_id)
                lb = second_l(channel_id)
                l = two_electron_l(channel_id)
                s = two_electron_spin(channel_id)
                xchan = exchange_channel(channel_id)
 ! Hugo - This will later need a spin component as well
                phase = two_electron_xphase(channel_id) !(-1.0_wp)**(la + lb + l + s)
                DO i = 1, nfdm
                    DO j = x_1st, x_last
                        IF (my_r2_nbr_lo_col_id >= col_id_1st) THEN
                            r2_bndry_lo(j, x_1st - i, channel_id, :) = &
                            phase * r1_bndry_lo(x_1st - i, j, xchan, :)
                        END IF
                        IF (my_r1_nbr_hi_row_id <= row_id_last) THEN
                            r1_bndry_hi(x_last + i, j, channel_id, :) = &
                            phase * r2_bndry_hi(j, x_last + i, xchan, :)
                        END IF
                    END DO
                END DO
            END DO

        END IF

    END SUBROUTINE get_two_electron_remote_bndries

!---------------------------------------------------------------------

    !> \brief  Calculate the two electron wavefunction at two points beyond the end of the two-electron outer region.
    !> \authors G Armstrong
    !> \date    2022
    !>
    !> Enforce odd parity at the boundary so that the two-electron wavefunction is zero at the
    !> point after the boundary, and at the second point beyond the boundary the sign of the wavefunction
    !> is changed relative to its value at the last point on the grid.
    !>
    !>  \param[in]    psi_double_outer   Two-electron wavefunction
    !>                                   dimension(x_1st:x_last, x_1st:x_last, two_electron_nchan, numsols)
    !>  \param[inout] r1_bndry_hi        Two-electron wavefunction at two points beyond the outer boundary in
    !>                                   the radial coordinate of electron N+1
    !>                                   dimension(x_last+1:x_last+2, x_1st:x_last, two_electron_nchan, numsols)
    !>  \param[inout] r2_bndry_hi        Two-electron wavefunction at two points beyond the outer boundary in
    !>                                   the radial coordinate of electron N+2
    !>                                   dimension(x_1st:x_last, x_last+1:x_last+2, two_electron_nchan, numsols)

!
! Hugo - principle may need to change here. Better to use wavefunction inside
! only as the result of this is not the highest order result possible.
! Differentiation routines changed, so data here does not really matter - Keep
! as is, but allow for nfdm points.
!

    SUBROUTINE get_two_electron_wavefunction_beyond_outer_bndries(psi_double_outer, r1_bndry_hi, r2_bndry_hi)

!       USE mpi_communications,        ONLY: get_my_group_pe_id
!       USE communications_parameters, ONLY: pe_id_last_double_outer

        COMPLEX(wp), INTENT(IN)    :: psi_double_outer(x_1st:x_last, x_1st:x_last, 1:two_electron_nchan, 1:numsols)
        COMPLEX(wp), INTENT(INOUT) :: r1_bndry_hi(x_last + 1:x_last + nfdm, x_1st:x_last, two_electron_nchan, numsols)
        COMPLEX(wp), INTENT(INOUT) :: r2_bndry_hi(x_1st:x_last, x_last + 1:x_last + nfdm, two_electron_nchan, numsols)

        REAL(wp) :: outer_boundary_ratio
        INTEGER  :: my_block_id, my_row_id, my_col_id
        INTEGER  :: my_r1_nbr_hi_col_id, my_r2_nbr_hi_row_id

        CALL get_my_block_id(my_block_id)
        CALL get_region_three_row_and_col_id(my_block_id, my_row_id, my_col_id)

! Hugo - Swapped if statements below around - but that gave an incorrect match for last row
!      - Undo the swap
!      - row_id and col_id appear mixed up

        my_r1_nbr_hi_col_id = my_col_id + 1
        my_r2_nbr_hi_row_id = my_row_id + 1

        outer_boundary_ratio = -1.0_wp
!        CALL get_my_group_pe_id(my_group_pe_id)
!        IF (my_group_pe_id .EQ. pe_id_last_double_outer) THEN
        IF (my_r2_nbr_hi_row_id > row_id_last) THEN
           r1_bndry_hi(x_last + 1, :, :, :) = zero
           r1_bndry_hi(x_last + 2, :, :, :) = outer_boundary_ratio*psi_double_outer(x_last, :, :, :)
           if (nfdm.eq. 3) r1_bndry_hi(x_last + 3, :,:, :) = zero
        END IF
   
        IF (my_r1_nbr_hi_col_id > col_id_last) THEN
           r2_bndry_hi(:, x_last + 1, :, :) = zero
           r2_bndry_hi(:, x_last + 2, :, :) = outer_boundary_ratio*psi_double_outer(:, x_last, :, :)
           if (nfdm.eq.3) r2_bndry_hi(:,x_last+3,:,:) = zero
        END IF
!       END IF

    END SUBROUTINE get_two_electron_wavefunction_beyond_outer_bndries

!---------------------------------------------------------------------

    !> \brief   Apply exchange symmetry to the two electron wavefunction on all cores along the diagonal
    !>          of the two-electron outer-region grid.
    !> \authors G Armstrong
    !> \date    2023
    !>
    !> Enforce exchange symmetry along the diagonal of the two-electron outer region, so that
    !>
    !> \f[ f{l_b l_a l s}(r_{N+2}, r_{N+1}, t) =
    !>     (-1)^{l_a + l_b + l}f_{l_a l_b ls}(r_{N+1}, r_{N+2}, t)
    !>  \f]
    !>
    !>  \param[inout]    psi   Two-electron wavefunction

    SUBROUTINE symmetrize_psi_about_diagonal(psi)

        COMPLEX(wp), INTENT(INOUT) :: psi(x_1st:x_last, x_1st:x_last, 1:two_electron_nchan, 1:numsols)

        INTEGER  :: i, ii, j, jj
        INTEGER  :: la, lb, l, s
        REAL(wp) :: phase

        IF (i_am_along_diagonal) THEN
            ! This ought to be OK as only one triangle is being overwritten...
            !$OMP PARALLEL DO PRIVATE(j,la,lb,l,s,jj,phase,i,ii)
            DO j = 1, two_electron_nchan
                la = first_l(j); lb = second_l(j); l = two_electron_l(j)
                s = two_electron_spin(j)
                jj = exchange_channel(j)
 ! Hugo - This will later need a spin component as well
                phase = two_electron_xphase(j) ! (-1.0_wp)**(la + lb + l + s)
                DO i = x_1st, x_last
                    DO ii = x_1st, x_last
                        IF (ii > i) THEN
 ! Hugo - The base case is where r_2 > r_1, so phases should change when r1 > r2.
 ! But it might be good to check this anyway - so uncomment the oiginal second line...
                            psi(ii, i, j, :) = phase * psi(i, ii, jj, :)
!                           psi(i, ii, j, :) = phase * psi(ii, i, jj, :)
                        END IF
                    END DO
                END DO
            END DO
        END IF

    END SUBROUTINE symmetrize_psi_about_diagonal

!---------------------------------------------------------------------

    !> \brief    Allocate the arrays for the coupling information
    !> \authors  G Armstrong
    !> \date     2022
    !>
    !> For each two-electron channel i, store the channels j to which i couples via
    !> the \f$ W_D, W_E \f$ and \f$ W_P\f$ potentials.
    !>
    SUBROUTINE allocate_region_three_neighbour_stores

        USE lrpots, ONLY: region_three_we_width, region_three_wp_width
        USE readhd, ONLY: two_electron_nchan

        IMPLICIT NONE
        INTEGER :: ierr

        allocate(twoe_wp_neighbour_store(two_electron_nchan, 2*region_three_wp_width + 1),&
                 twoe_we_neighbour_store(two_electron_nchan, 2*region_three_we_width + 1),&
                 twoe_wp_neighbour_count(two_electron_nchan),&
                 twoe_we_neighbour_count(two_electron_nchan),&
                 stat=ierr)

        CALL assert((ierr == 0), 'allocation error in two_electron_outer_hamiltonian stores')

        ! Initialize
        twoe_wp_neighbour_count = 0  ;  twoe_wp_neighbour_store = (0,0)
        twoe_we_neighbour_count = 0  ;  twoe_we_neighbour_store = (0,0)

    END SUBROUTINE allocate_region_three_neighbour_stores

!---------------------------------------------------------------------

END MODULE two_electron_outer_hamiltonian
