! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Controls the flow of information from the two-electron outer region
!> to the one-electron outer region, mainly the sending of the wavefunction derivatives
!> at the boundary for the calculation of the surface term in the one-electron outer region.
 
MODULE double_outer_to_outer_interface

    USE calculation_parameters, ONLY: nfdm
    USE communications_parameters, ONLY: id_of_1st_pe_outer, &
                                         id_of_1st_pe_double_outer, &
                                         nsectors
    USE global_data,            ONLY: zero
    USE grid_parameters,        ONLY: channel_id_1st, &
                                      channel_id_last, &
                                      x_1st, &
                                      x_last
    USE initial_conditions,     ONLY: numsols => no_of_field_confs
    USE precisn,                ONLY: wp
    USE readhd,                 ONLY: LML_block_tot_nchan, &
                                      two_electron_nchan, &
                                      LML_block_nchan, &
                                      LML_block_chanstart, &
                                      LML_block_chanend, &
                                      chan_2e_start, &
                                      chan_2e_end, &
                                      No_of_LML_Blocks, &
                                      nchmx
    USE rmt_assert,             ONLY: assert
    USE MPI

    IMPLICIT NONE

    COMPLEX(wp), ALLOCATABLE :: first_deriv_at_bndry(:, :, :)

    PUBLIC send_boundary_derivative_double_outer_to_outer
    PUBLIC recv_boundary_derivative_from_double_outer_region
    PUBLIC project_outer_derivs_onto_surf_amps
    PUBLIC allocate_first_deriv_at_bndry
    PUBLIC deallocate_first_deriv_at_bndry

CONTAINS

!---------------------------------------------------------------------------------------------

!> \brief   Send the derivative of the two-electron wavefunction at the boundary with
!>          the one-electron outer region from each core in the lowest row of the
!>          two-electron outer region (i.e closest to the boundary), to the core
!>          directly below it in the one-electron outer region.
!> \authors G Armstrong
!> \date    2022
!>
!> \param[in] first_deriv_at_bndry   The first derivative of the two-electron radial wavefunction
!>                                   at the boundary r_{N+1} = b for all values of r_{N+2}.
!>                                   dimensions (x_1st:x_last, two_electron_nchan, numsols)

    SUBROUTINE send_boundary_derivative_double_outer_to_outer(first_deriv_at_bndry)

        USE communications_parameters, ONLY: no_of_cols
        USE mpi_communications,        ONLY: get_my_pe_id

        COMPLEX(wp), INTENT(IN) :: first_deriv_at_bndry(x_1st:x_last, 1:two_electron_nchan, 1:numsols)
        INTEGER                 :: dest_pe, my_rank
        INTEGER                 :: tag, ierr
        INTEGER                 :: x_size
        COMPLEX(wp) :: derivtmp(x_1st:x_last/nsectors, 1:two_electron_nchan, 1:numsols)
        INTEGER :: i, s_test

        ! Determine the one-electron outer region PE ID to which data should be sent
        CALL get_my_pe_id(my_rank)
!       dest_pe = my_rank - no_of_cols
        dest_pe = (my_rank-id_of_1st_pe_double_outer)*nsectors+id_of_1st_pe_outer
!       print *,'Intraface ', my_rank, dest_pe, s_test

        ! Determine sector size
        x_size = (x_last - x_1st + 1)/nsectors

        ! Send data
        do i = 1, nsectors
          tag = 123
          derivtmp(x_1st:x_last/nsectors, :, :) = first_deriv_at_bndry(x_1st+(i-1)*x_last/nsectors: &
                  x_last*i/nsectors,:,:)
          CALL MPI_SEND(derivtmp, x_size*two_electron_nchan*numsols, MPI_DOUBLE_COMPLEX, &
                      dest_pe, tag, MPI_COMM_WORLD, ierr)
          dest_pe = dest_pe + 1
        end do


    END SUBROUTINE send_boundary_derivative_double_outer_to_outer

!---------------------------------------------------------------------

!> \brief   Each core in the one-electron outer regoin receives the derivative
!>          of the two-electron wavefunction at the boundary with the one-electron
!>          outer region from each core above it in the lowest row of the two-electron
!>          outer region (i.e closest to the boundary).
!> \authors G Armstrong
!> \date    2022
!>
!> \param[out] first_deriv_at_bndry   The first derivative of the two-electron radial wavefunction
!>                                    at the boundary r_{N+1} = b for all values of r_{N+2}.
!>                                    dimensions (x_1st:x_last, two_electron_nchan, numsols)

    SUBROUTINE recv_boundary_derivative_from_double_outer_region(first_deriv_at_bndry)

        USE communications_parameters,       ONLY: no_of_cols
        USE mpi_communications,              ONLY: get_my_pe_id

        COMPLEX(wp), INTENT(OUT) :: first_deriv_at_bndry(x_1st:x_last, two_electron_nchan, numsols)

        INTEGER :: my_rank, dest_pe, source_pe
        INTEGER :: tag, ierr
        INTEGER :: x_size
        INTEGER :: status(MPI_STATUS_SIZE)
        INTEGER :: s_test

        first_deriv_at_bndry = zero

        CALL get_my_pe_id(my_rank)

        ! Set the one-electron outer region processor that receives
        dest_pe = my_rank

        ! Determine which two-electron outer region processor has sent
!       source_pe = my_rank + no_of_cols
        source_pe = int((my_rank - id_of_1st_pe_outer)/nsectors) + id_of_1st_pe_double_outer
!       print *,'Intraface ', my_rank, source_pe, s_test

        ! Determine sector size
        x_size = x_last - x_1st + 1

        ! Receive data from double outer region
        IF (my_rank == dest_pe) THEN
            tag = 123
            CALL MPI_RECV(first_deriv_at_bndry, x_size*two_electron_nchan*numsols, MPI_DOUBLE_COMPLEX, &
                          source_pe, tag, MPI_COMM_WORLD, status, ierr)
        END IF

    END SUBROUTINE recv_boundary_derivative_from_double_outer_region

!---------------------------------------------------------------------

    !> \brief   Project one-electron wavefunction boundary derivatives onto surface amplitudes
    !> \authors G Armstrong
    !> \date    2022

    !>   The first derivative of the one-electron outer region wavefunction is projected
    !>   onto  surface amplitudes to contruct the Bloch operator term in the one-electron
    !>   outer region Hamiltonian. This procedure is similar to the projection carried
    !>   out to calculate the surface term in the inner region Hamiltonian.
    !>
    !> \param[in] vecin    The first derivative of the two-electron radial wavefunction
    !>                     at the boundary r_{N+1} = b for all values of r_{N+2}.
    !>                     dimensions (x_1st:x_last, two_electron_nchan, numsols)
    !> \param[out] vecout  The result of multiplication of the surface amplitudes matrix omega
    !>                     by the first derivative of the two-electron radial wavefunction
    !>                     at the boundary r_{N+1} = b for all values of r_{N+2}.
    !>                     dimensions (x_1st:x_last, LML_block_tot_nchan, numsols)

    SUBROUTINE project_outer_derivs_onto_surf_amps(vecin, vecout)

        USE outer_to_double_outer_interface, ONLY: omega, omega_red

        COMPLEX(wp), INTENT(IN)  :: vecin(x_1st:x_last, two_electron_nchan, numsols)
        COMPLEX(wp), INTENT(OUT) :: vecout(x_1st:x_last, LML_block_tot_nchan, numsols)

        INTEGER :: i
!       COMPLEX(wp), ALLOCATABLE :: work(:)
        COMPLEX(wp) :: tempin(two_electron_nchan)
        COMPLEX(wp) :: work(LML_block_tot_nchan)
        COMPLEX(wp) :: lhs_mat(two_electron_nchan, numsols)
        COMPLEX(wp) :: rhs_mat(LML_block_tot_nchan, numsols)

        COMPLEX(wp), PARAMETER :: zihalf = (0.0_wp, 0.5_wp)

        INTEGER     :: size2e(no_of_LML_blocks)
        INTEGER     :: max2e, kk
        LOGICAL     :: not_reduced_flag

        not_reduced_flag = .false.
        size2e = chan_2e_end - chan_2e_start + 1
        max2e = MAXVAL(size2e)

!       ALLOCATE(work(LML_block_tot_nchan))
            ! project derivatives onto surface amplitudes (omega)
!           vecout = zero; tempin = zero; work = zero
!           lhs_mat = zero; rhs_mat = zero
        IF (not_reduced_flag) THEN

            IF (numsols == 1) THEN
                vecout = zero; tempin = zero
                DO i = x_1st, x_last
                    work = zero
                    tempin(:) = vecin(i, :, 1)
                    CALL ZGEMV('N', LML_block_tot_nchan, two_electron_nchan, zihalf, omega, &
                                LML_block_tot_nchan, tempin, 1, &
                                zero, work, 1)
                    vecout(i, :, 1) = work
                END DO
            ELSE
                lhs_mat = zero; rhs_mat = zero
                DO i = x_1st, x_last
                    lhs_mat(:, :) = vecin(i, :, :)
                    CALL ZGEMM('N', 'N', LML_block_tot_nchan, numsols, two_electron_nchan, zihalf, omega, &
                                LML_block_tot_nchan, lhs_mat, two_electron_nchan, &
                                zero, rhs_mat, LML_block_tot_nchan)
                     vecout(i, :, :) = rhs_mat(:, :)
                END DO
            END IF

         ELSE

            IF (numsols == 1) THEN
              vecout = zero
              DO kk = 1, No_of_LML_Blocks
                !$OMP PARALLEL DO PRIVATE(i, work, tempin)
                DO i = x_1st, x_last
                    work = zero; tempin = zero
                    tempin(1:size2e(kk)) = vecin(i, chan_2e_start(kk):chan_2e_end(kk), 1)
                    CALL ZGEMV('N', LML_block_nchan(kk), size2e(kk), zihalf, &
                                omega_red(1:nchmx, 1:size2e(kk),kk), &
                                nchmx, tempin, 1, &
                                zero, work, 1)
                    vecout(i, LML_block_chanstart(kk):LML_block_chanend(kk), 1) = &
                            work(1:LML_block_nchan(kk))
                END DO
              END DO
            ELSE
              vecout= zero
               DO kk = 1, No_of_LML_Blocks
                lhs_mat = zero; rhs_mat = zero
                DO i = x_1st, x_last
                    lhs_mat(1:size2e(kk), :) = vecin(i, chan_2e_start(kk):chan_2e_end(kk), :)
                    CALL ZGEMM('N', 'N', LML_block_nchan(kk), numsols, size2e(kk), zihalf, &
                                omega_red(1:nchmx,1:size2e(kk),kk), &
                                nchmx, lhs_mat, two_electron_nchan, &
                                zero, rhs_mat, LML_block_tot_nchan)
                     vecout(i, LML_block_chanstart(kk):LML_block_chanend(kk), :) = &
                            rhs_mat(1:LML_block_nchan(kk), :)
                END DO
               END DO
            END IF

          END IF
!        DEALLOCATE(work)

    END SUBROUTINE project_outer_derivs_onto_surf_amps

!---------------------------------------------------------------------

    SUBROUTINE allocate_first_deriv_at_bndry

        IMPLICIT NONE

        INTEGER  :: err

        ALLOCATE (first_deriv_at_bndry(x_1st:x_last, two_electron_nchan, numsols), stat=err)
        CALL assert(err == 0, 'allocation error with first_deriv_at_bndry')

    END SUBROUTINE allocate_first_deriv_at_bndry

!---------------------------------------------------------------------

    SUBROUTINE deallocate_first_deriv_at_bndry

        IMPLICIT NONE

        INTEGER  :: err

        DEALLOCATE (first_deriv_at_bndry, stat=err)
        CALL assert(err == 0, 'deallocation error with first_deriv_at_bndry')

    END SUBROUTINE deallocate_first_deriv_at_bndry

!---------------------------------------------------------------------

END MODULE double_outer_to_outer_interface
