! Copyright 2023
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.
!



!> FIND SYMMETRY INDEX

MODULE dipole_utilities

    IMPLICIT NONE

CONTAINS

    SUBROUTINE finind_L(lgsf, lglf, lgpf, sym_no, no_of_L_blocks, L_block_nspn, L_block_lrgl, L_block_npty)

        INTEGER, INTENT(IN)  :: lgsf, lglf, lgpf
        INTEGER, INTENT(IN)  :: no_of_L_blocks, L_block_nspn(:), L_block_lrgl(:), L_block_npty(:)
        INTEGER, INTENT(OUT) :: sym_no ! previously 'nf'
        INTEGER :: i

        sym_no = -1
        DO i = 1, no_of_L_blocks  
            IF (lgsf == L_block_nspn(i)) THEN
                IF (lglf == L_block_lrgl(i)) THEN
                    IF (lgpf == L_block_npty(i)) THEN
                        sym_no = i
                    END IF
                END IF
            END IF
        END DO

    END SUBROUTINE finind_L

    !---------------------------------------------------------------------------

    !> FIND SYMMETRY INDEX (LML BLOCKS) ADDED BY DDAC
    SUBROUTINE finind_LML(lgs, lgl, lgp, lgml, sym_no, &
                        no_of_LML_blocks, LML_block_nspn, LML_block_lrgl, LML_block_npty, LML_block_ml)

        INTEGER, INTENT(IN)  :: lgs, lgl, lgp, lgml
        INTEGER, INTENT(IN)  :: no_of_LML_blocks, LML_block_nspn(:), LML_block_lrgl(:)
        INTEGER, INTENT(IN)  :: LML_block_npty(:), LML_block_ml(:)
        INTEGER, INTENT(INOUT) :: sym_no
        INTEGER :: i

        sym_no = -1

        DO i = 1, no_of_LML_blocks
            IF (lgs == LML_block_nspn(i)) THEN
                IF (lgl == LML_block_lrgl(i)) THEN
                    IF (lgp == LML_block_npty(i)) THEN
                        IF (lgml == LML_block_ml(i)) THEN
                            sym_no = i
                        END IF
                    END IF
                END IF
            END IF
        END DO
    END SUBROUTINE finind_LML

END MODULE