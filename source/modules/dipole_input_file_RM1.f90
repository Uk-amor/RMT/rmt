! Copyright 2023
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief A piecemeal reimplementation of the code in `readhd`.

MODULE dipole_input_file_RM1
    ! NOTE: We define the d file in terms of explicit kinds to decouple RMT's working precision from the types used in the H file format.
    USE ISO_FORTRAN_ENV, ONLY: INT32, REAL64

    USE initial_conditions, ONLY: jK_coupling_id, LS_coupling_id    !initial_conditions is setup upon build
    !WP allows us to work with at least 14 decimal digits accuracy and at least decimal exponents between - 300 and + 300
    USE precisn, ONLY: wp
    USE rmt_assert, ONLY: assert
    USE angular_momentum,          ONLY: cg, dcg, &
                                        parity_allowed, &
                                        satisfies_triangle_rule
    USE wall_clock, ONLY: time => hel_time
    USE dipole_utilities, ONLY: finind_L, finind_LML

    IMPLICIT NONE

    TYPE D_file
        INTEGER(INT32):: ntarg
        INTEGER(INT32):: fintr
        INTEGER(INT32):: intr
        TYPE(dipole_block), DIMENSION(:), ALLOCATABLE:: blocks
    END TYPE D_file

    TYPE dipole_block
        INTEGER(INT32):: noterm, isi, inch, ILrgL, IPty, ISpn, jsi, jnch, JLrgL
        INTEGER(INT32):: lgsf, lglf, lgpf, lgsi, lgli, lgpi ! quantum numbers initial/final
        REAL(REAL64), ALLOCATABLE :: temp_dipsto(:, :) ! length gauge reduced dipole transition matrix
        REAL(REAL64), ALLOCATABLE :: temp_dipsto_v(:, :) ! velocity gauge reduced dipole transition matrix
        INTEGER(INT32), ALLOCATABLE :: iidip(:)
        INTEGER(INT32), ALLOCATABLE :: ifdip(:)
    END TYPE dipole_block

    INTEGER, PARAMETER           :: nocoupling = INT(b'000')
    INTEGER, PARAMETER           :: downcoupling = INT(b'001')
    INTEGER, PARAMETER           :: samecoupling = INT(b'010')
    INTEGER, PARAMETER           :: upcoupling = INT(b'100')

    PUBLIC :: read_D_file_RM1

    !**************************************************************************************

    CONTAINS
    SUBROUTINE parse_D_file_RM1(&
                                d00_path, &
                                d_path, &
                                D, &
                                lplusp, &
                                crlv, &
                                block_ind, &
                                dipole_coupled, &
                                wp_read_store, &
                                wd_read_store, &
                                wd_read_store_v, &
                                LML_wp_read_store, &
                                LML_wd_read_store, &
                                LML_wd_read_store_v, &
                                L_block_nchan, &
                                L_block_tot_nchan, &
                                LML_block_nchan, &
                                LML_block_tot_nchan, &
                                ntarg, &
                                nstmx, &
                                no_of_L_blocks, &
                                no_of_LML_blocks, &
                                coupling_id, &
                                dipole_velocity_output, &
                                L_block_lrgl, &
                                L_block_nspn, &
                                L_block_npty, &
                                LML_block_nspn, &
                                LML_block_lrgl, &
                                LML_block_l2p, &
                                LML_block_npty, &
                                LML_block_ml, &
                                LML_block_nconat, &
                                ML_Max, &
                                xy_plane_desired, &
                                debug, &
                                mat_size_wd, &
                                mat_size_wp)

        INTEGER, INTENT(IN) :: lplusp
        INTEGER :: lgsf, lgsi, lgli, lglf, lgpi, lgpf, ni, nf, err, intr
        INTEGER :: i, noterm, isi, jsi, inch, jnch, icount
        INTEGER :: blocki, blockf, lgmli, lgmlf
        INTEGER :: lptermi, lptermf, stride
        REAL(wp) :: cgc
        INTEGER :: lptesti, lptestf

        REAL(wp), ALLOCATABLE:: wp_read(:, :)
        REAL(wp), ALLOCATABLE:: wd_read(:, :)
        REAL(wp), ALLOCATABLE:: wd_read_v(:, :)

        INTEGER :: N1
        INTEGER :: N2
        INTEGER :: M1
        INTEGER :: M2
        INTEGER :: M
        INTEGER :: N

        INTEGER :: ILrgL
        INTEGER :: IPty
        INTEGER :: ISpn
        INTEGER :: JLrgL

        INTEGER :: nchanBegin
        INTEGER :: nchanEnd
        INTEGER :: mchanBegin
        INTEGER :: mchanEnd

        LOGICAL :: read_dipsto

        INTEGER :: io_header, io_data, temp_dipsto_txt, temp_dipsto_v_txt

        INTEGER, INTENT(IN)                  :: coupling_id
        INTEGER, INTENT(IN)                  :: no_of_L_blocks
        INTEGER, INTENT(IN)                  :: no_of_LML_blocks
        INTEGER, INTENT(IN)                  :: nstmx
        INTEGER, INTENT(IN)                  :: ntarg
        INTEGER, INTENT(IN)                  :: ML_Max

        LOGICAL, INTENT(IN)                  :: debug
        LOGICAL, INTENT(IN)                  :: dipole_velocity_output 

        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: crlv(:, :)

        INTEGER, ALLOCATABLE, INTENT(INOUT) :: dipole_coupled(:, :)
        INTEGER, ALLOCATABLE, INTENT(INOUT) :: block_ind(:, :, :)

        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: wp_read_store(:, :)
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: wd_read_store(:, :)
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: wd_read_store_v(:, :)

        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: LML_wp_read_store(:, :)
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: LML_wd_read_store(:, :)
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: LML_wd_read_store_v(:, :)
        
        INTEGER, ALLOCATABLE, INTENT(INOUT) :: L_block_nchan(:)
        INTEGER, ALLOCATABLE, INTENT(INOUT) :: LML_block_nchan(:)
        INTEGER, INTENT(IN) :: L_block_tot_nchan
        INTEGER, INTENT(IN) :: LML_block_tot_nchan

        INTEGER, INTENT(IN)                  :: L_block_lrgl(:)
        INTEGER, INTENT(IN)                  :: L_block_nspn(:)
        INTEGER, INTENT(IN)                  :: L_block_npty(:)

        INTEGER, INTENT(IN)                  :: LML_block_lrgl(:)
        INTEGER, INTENT(IN)                  :: LML_block_l2p(:, :)
        INTEGER, INTENT(IN)                  :: LML_block_npty(:)
        INTEGER, INTENT(IN)                  :: LML_block_nspn(:)
        INTEGER, INTENT(IN)                  :: LML_block_ml(:)
        INTEGER, INTENT(IN)                  :: LML_block_nconat(:, :)
        LOGICAL, INTENT(IN)                  :: xy_plane_desired(:)

        CHARACTER(len=*), INTENT(IN)         :: d00_path
        CHARACTER(len=*), INTENT(IN)         :: d_path

        TYPE(D_file), INTENT(INOUT)            :: D
        !*********************************************
        INTEGER :: sym_one
        INTEGER :: sym_two
        INTEGER :: target_one
        INTEGER :: target_two
        INTEGER :: ncounter_one
        INTEGER :: ncounter_two
        INTEGER :: channel_counter_one
        INTEGER :: channel_counter_two
        INTEGER :: channel_insym_counter_one
        INTEGER :: channel_insym_counter_two
        INTEGER, INTENT(INOUT) :: mat_size_wd
        INTEGER, INTENT(INOUT) :: mat_size_wp
        !*********************************************
        
        INTEGER :: ML_Dipole_Coupling_Diff

        CHARACTER(LEN = 3) :: transition_number
        
        CALL channel_data(wp_read_store, &
                            wd_read_store, &
                            wd_read_store_v, &
                            LML_wp_read_store, &
                            LML_wd_read_store, &
                            LML_wd_read_store_v, &
                            wp_read, &
                            wd_read, &
                            wd_read_v, &
                            L_block_nchan, &
                            L_block_tot_nchan, &
                            LML_block_nchan, &
                            LML_block_tot_nchan, &
                            ntarg, &
                            debug, &
                            coupling_id, &
                            no_of_L_blocks, &
                            no_of_LML_blocks, &
                            LML_block_lrgl, &
                            LML_block_l2p, &
                            LML_block_npty, &
                            LML_block_ml, &
                            LML_block_nconat, &
                            mat_size_wd, &
                            mat_size_wp, &
                            channel_counter_one, &
                            channel_counter_two, &
                            ML_Dipole_Coupling_Diff)

        !*****************************************************************************************************

        OPEN (NEWUNIT = io_header, FILE = d00_path, STATUS='old', FORM='unformatted')

        ! Allocate target dipoles-these aren't used, but need to be there for bcasts
        D%ntarg = ntarg
        ALLOCATE (crlv(ntarg, ntarg), stat = err)
        CALL assert(err .EQ. 0, 'Allocation error in crlv')
        ! Note form of d00 is Li-L(i-1) - Not necessarily true for RMatrixI
        READ (io_header) intr  ! number of block transitions

        IF (debug)  PRINT *, 'intr=', intr
        D%intr = intr

        ALLOCATE (dipole_coupled(no_of_LML_blocks, no_of_LML_blocks), stat = err)
        CALL Assert(err .eq. 0, 'Allocation error in dipole_coupled')
        
        IF (debug) THEN
            write(6, *) 'dipole_coupling allocated with size',No_Of_LML_blocks*no_of_LML_Blocks
        END IF 

        ALLOCATE (block_ind(No_Of_L_Blocks, No_Of_L_Blocks, 3), stat = err)
        CALL Assert(err .eq. 0, 'Allocation error in dipole_coupled')

        ALLOCATE (D%blocks(intr))
        
        crlv = 0.0_wp

        icount = 0
        dipole_coupled = NoCoupling

        DO i = 1, intr

            ! Open Dipole file
            WRITE (transition_number, FMT='(I3.3)') i
            IF (debug) THEN
                PRINT *, 'Loading Transition Number: '//trim(d_path)//transition_number
            END IF

            OPEN (NEWUNIT = io_data, FILE = trim(d_path)//transition_number, STATUS='old', FORM='unformatted')

            ! i refers now to the LEFT-HAND SIDE
            ! f to the RIGHT-HAND SIDE

            !Read line from D00
            READ (io_header, IOSTAT = err) lgsf, lglf, lgpf, lgsi, lgli, lgpi
            IF (debug) WRITE (*, *) 'd00 Line:', lgsf, lglf, lgpf, lgsi, lgli, lgpi

            IF (err < 0) THEN
                PRINT *, 'End of file reached'
                EXIT
            END IF
            CALL assert(err .LE. 0, 'Problems my dear')

            !Check that this dipole transition is needed
            if (coupling_id .eq. LS_coupling_id) then
                lptesti = MOD(lgli+lgpi, 2)
                lptestf = MOD(lglf+lgpf, 2)
            ELSE
                lptesti = MOD(lgli/2 + lgpi, 2)
                lptestf = MOD(lglf/2 + lgpf, 2)
            END IF
            read_dipsto=((ML_Max .eq. 0).AND.(lplusp == lptesti).AND.(lplusp == lptestf)).OR.(ML_Max > 0)
            
            IF ((lgpf <= 1) .AND. (lgpi <= 1)) THEN 
                IF (read_dipsto) THEN
                    !Find which symettries this transition is between
                    CALL finind_L(lgsf, lglf, lgpf, nf, no_of_L_blocks, L_block_nspn, L_block_lrgl, L_block_npty)
                    CALL finind_L(lgsi, lgli, lgpi, ni, no_of_L_blocks, L_block_nspn, L_block_lrgl, L_block_npty)

                    if ((ni == -1) .or. (nf == -1)) then
                        PRINT *, "cycle"
                        cycle
                    end if
                    icount = icount+1
                    
                    ASSOCIATE (bloc => D%blocks(icount))
                        bloc%lgsf = lgsf
                        bloc%lglf = lglf
                        bloc%lgpf = lgpf
                        bloc%lgsi = lgsi
                        bloc%lgli = lgli
                        bloc%lgpi = lgpi
                        block_ind(ni, nf, :) = icount
                        block_ind(nf, ni, :) = 0 !icount

                        ALLOCATE(bloc%iidip(intr))
                        ALLOCATE(bloc%ifdip(intr))

                        D%blocks(icount)%iidip(icount) = ni
                        D%blocks(icount)%ifdip(icount) = nf

                        IF (debug) WRITE (*, *) 'Dipoles between symmetry: ', nf, ni
                        READ (io_data) bloc%noterm, &
                                        bloc%isi, &
                                        bloc%inch, &
                                        bloc%ILrgL, &
                                        bloc%IPty, &
                                        bloc%ISpn, &
                                        bloc%jsi, &
                                        bloc%jnch, &
                                        bloc%JLrgL  ! Line 1 of DNN file, inch = no. channels in i, jnch = no. of channels in j
                                
                        IF (debug) PRINT *, 'ParityCheck: ', bloc%lgpf, bloc%lgpi, bloc%IPty

                        ALLOCATE (bloc%temp_dipsto(bloc%isi, bloc%jsi))
                        ALLOCATE (bloc%temp_dipsto_v(bloc%isi, bloc%jsi))
                        bloc%temp_dipsto = 0.0_wp
                        bloc%temp_dipsto_v = 0.0_wp

                        N2 = 0
                        DO WHILE (N2 .LT. bloc%jsi)
                            N1 = N2+1
                            N2 = MIN(N2+bloc%noterm, bloc%jsi)
                            M2 = 0
                            DO WHILE (M2 .LT. bloc%isi)
                                M1 = M2+1
                                M2 = MIN(M2+bloc%noterm, bloc%isi)
                                IF (dipole_velocity_output) THEN
                                    READ (io_data) ((bloc%temp_dipsto(M, N), M = M1, M2), N = N1, N2), &
                                                    ((bloc%temp_dipsto_v(M, N), M = M1, M2), N = N1, N2)
                                ELSE
                                    READ (io_data) ((bloc%temp_dipsto(M, N), M = M1, M2), N = N1, N2)
                                END IF
                            END DO
                        END DO       
                        
                        ! skip through RMatrixI buttle correction data
                        READ (io_data)
                        READ (io_data)
                        READ (io_data)
                        READ (io_data)

                        ! Read outer region dipole-we read velocity gauge whether or not
                        ! required. These matrices can be neglected later.
                        wp_read = 0.0_wp
                        wd_read = 0.0_wp
                        wd_read_v = 0.0_wp
                        Read (io_data) ((wp_read(M, N), N = 1, bloc%jnch), M = 1, bloc%inch), &
                            ((wd_read(M, N), N = 1, bloc%jnch), M = 1, bloc%inch), &
                            ((wd_read_v(M, N), N = 1, bloc%jnch), M = 1, bloc%inch)

                        ! Multiply all reduced dipoles by clebsch-gordan coefficient here
                        ! Multiplication by cgc moved to live communications

                        ! Set up for putting individual transition outer region dipole matrices
                        ! together in matrix for entire system
                        CALL assert((L_block_nchan(ni) .EQ. bloc%inch), 'error in inchan(ni) and bloc%inch')
                        CALL assert((L_block_nchan(nf) .EQ. bloc%jnch), 'error in inchan(nf) and bloc%jnch')
                        IF (ni .EQ. 1) THEN
                            nchanBegin = 1
                        ELSE
                            nchanBegin = SUM(L_block_nchan(1:(ni-1))) + 1
                        END IF
                        IF (nf .EQ. 1) THEN
                            mchanBegin = 1
                        ELSE
                            mchanBegin = SUM(L_block_nchan(1:(nf-1))) + 1
                        END IF
                        nchanEnd = SUM(L_block_nchan(1:ni))
                        mchanEnd = SUM(L_block_nchan(1:nf))

                        CALL assert(((nchanEnd-nchanBegin+1) .EQ. bloc%inch), 'error in nchans and bloc%inch')
                        CALL assert(((mchanEnd-mchanBegin+1) .EQ. bloc%jnch), 'error in mchans and bloc%jnch')
    
                        ! Write outer region transition matrix to big outer region matrix
                        wp_read_store(nchanBegin:nchanEnd, mchanBegin:mchanEnd) = &
                                            (wp_read(1:bloc%inch, 1:bloc%jnch))
                        wp_read_store(mchanBegin:mchanEnd, nchanBegin:nchanEnd) =  &
                                            1.0_wp*transpose(wp_read(1:bloc%inch, 1:bloc%jnch))
    
                        wd_read_store(nchanBegin:nchanEnd, mchanBegin:mchanEnd) = &
                                            (wd_read(1:bloc%inch, 1:bloc%jnch))
                        wd_read_store(mchanBegin:mchanEnd, nchanBegin:nchanEnd) =  &
                                            1.0_wp*transpose(wd_read(1:bloc%inch, 1:bloc%jnch))
    
                        wd_read_store_v(nchanBegin:nchanEnd, mchanBegin:mchanEnd) = &
                                            (wd_read_v(1:bloc%inch, 1:bloc%jnch))
                        wd_read_store_v(mchanBegin:mchanEnd, nchanBegin:nchanEnd) =  &
                                            1.0_wp*transpose(wd_read_v(1:bloc%inch, 1:bloc%jnch))

                        IF (coupling_id .eq. LS_coupling_id) then
                            IF (xy_plane_desired(1)) THEN
                                stride = 2
                                lptermi = MOD(bloc%lgli+bloc%lgpi, 2)
                                lptermf = MOD(bloc%lglf+bloc%lgpf, 2)
                            ELSE
                                stride = 1
                                lptermi = 0
                                lptermf = 0
                            END IF
                        ELSE
                            IF (xy_plane_desired(1)) THEN
                                stride = 4
                                IF (mod(ML_Max, 2).eq.0) then
                                    lptermi = 2*MOD(bloc%lgli/2 + bloc%lgpi, 2)
                                    lptermf = 2*MOD(bloc%lglf/2 + bloc%lgpf, 2)
                                ELSE
                                    lptermi = 2*MOD(bloc%lgpi, 2)
                                    lptermf = 2*MOD(bloc%lgpf, 2)
                                END IF
                            ELSE
                                stride = 2
                                lptermi = 0
                                lptermf = 0
                            END IF
                        END IF

                        DO lgmli = -min(bloc%lgli, ML_Max)+lptermi, min(bloc%lgli, ML_Max)-lptermi, stride
                            DO lgmlf = -min(bloc%lglf, ML_Max)+lptermf, min(bloc%lglf, ML_Max)-lptermf, stride
                                IF (lgmli == lgmlf .or. abs(lgmli-lgmlf)==ML_Dipole_Coupling_Diff) then
                                CALL finind_LML(bloc%lgsi, bloc%lgli, bloc%lgpi, lgmli, blocki, &
                                                no_of_LML_blocks, LML_block_nspn, LML_block_lrgl, LML_block_npty, LML_block_ml)
                                CALL finind_LML(bloc%lgsf, bloc%lglf, bloc%lgpf, lgmlf, blockf, &
                                                no_of_LML_blocks, LML_block_nspn, LML_block_lrgl, LML_block_npty, LML_block_ml)
                                IF (lglf < lgli) THEN
                                   dipole_coupled(blockf, blocki) = DownCoupling
                                   dipole_coupled(blocki, blockf) = UpCoupling
                                END IF
                                IF (bloc%lglf == bloc%lgli) THEN
                                   dipole_coupled(blockf, blocki) = SameCoupling
                                   dipole_coupled(blocki, blockf) = SameCoupling
                                END IF
                                IF (bloc%lglf > bloc%lgli) THEN
                                   dipole_coupled(blockf, blocki) = UpCoupling
                                   dipole_coupled(blocki, blockf) = DownCoupling
                                END IF
                                !JW Additions for Arbitrary polarisation
                                IF (coupling_id .eq. LS_coupling_id) then
                                !    cgc = -cg(lgli, 1, lglf, lgmli, lgmli-lgmlf, lgmlf) &
                                    cgc = cg(bloc%lgli, 1, bloc%lglf, lgmli, lgmlf-lgmli, lgmlf) &
                                        /SQRT(REAL(bloc%lglf, wp) + REAL(bloc%lglf, wp) + 1._wp)
                                ELSE
                                    cgc = dcg(bloc%lgli, 2, bloc%lglf, lgmli, lgmlf-lgmli, lgmlf) &
                                        /SQRT(REAL(bloc%lglf, wp) + 1._wp)
                                END IF
    
                                IF (debug) THEN
                                    print *,''
                                    print *,'----------------------------------------------'
                                    print *,lgli, 1, lglf, lgmli, lgmlf-lgmli, lgmlf
                                    print *,cgc
                                    print *,LML_block_npty(blocki), LML_block_npty(blockf)
                                    print *,'----------------------------------------------'
                                END IF
                                ! Set up for putting individual transition outer region dipole matricies
                                ! together in matrix for entire system
                                CALL assert((LML_block_nchan(blocki) .EQ. bloc%inch), 'error in inchan(ni) and bloc%inch')
                                CALL assert((LML_block_nchan(blockf) .EQ. bloc%jnch), 'error in inchan(nf) and bloc%jnch')
                                IF (blocki .EQ. 1) THEN
                                    nchanBegin = 1
                                ELSE
                                    nchanBegin = SUM(LML_block_nchan(1:(blocki-1))) + 1
                                END IF
                                IF (blockf .EQ. 1) THEN
                                    mchanBegin = 1
                                ELSE
                                    mchanBegin = SUM(LML_block_nchan(1:(blockf-1))) + 1
                                END IF
                                nchanEnd = SUM(LML_block_nchan(1:blocki))
                                mchanEnd = SUM(LML_block_nchan(1:blockf))
    
                                CALL assert(((nchanEnd-nchanBegin+1) .EQ. bloc%inch), 'error in nchans and bloc%inch')
                                CALL assert(((mchanEnd-mchanBegin+1) .EQ. bloc%jnch), 'error in mchans and bloc%jnch')
    
                                ! Write outer region transition matrix to big outer region matrix
    
                                DO channel_counter_one = nchanBegin, nchanEnd
                                DO channel_counter_two = mchanBegin, mchanEnd
    
                                IF (abs(wp_read(channel_counter_one-nchanBegin+1,       &
                                    channel_counter_two-mchanBegin+1)).GT.0.000000000001_wp) then
                                   if (abs(channel_counter_one-channel_counter_two).gt.mat_size_wp) THEN
                                      print *,'error in wp size!',channel_counter_one, channel_counter_two,      &
                                      wp_read(channel_counter_one-nchanBegin+1, channel_counter_two-mchanBegin+1)
                                   END IF
                                    IF (coupling_id .EQ. LS_coupling_id) THEN
                                        LML_wp_read_store(channel_counter_one-channel_counter_two, channel_counter_two) =    &
                                            (-1._wp)**(lgmlf-lgmli+1)*cgc*          &
                                            (wp_read(channel_counter_one-nchanBegin+1, channel_counter_two-mchanBegin+1))
                                    ELSE
                                        LML_wp_read_store(channel_counter_one-channel_counter_two, channel_counter_two) =    &
                                            (-1._wp)**((lgmlf-lgmli)/2+1)*cgc*      &
                                            (wp_read(channel_counter_one-nchanBegin+1, channel_counter_two-mchanBegin+1))
                                    END IF
    
                                    !and do the transpose
                                    LML_wp_read_store(channel_counter_two-channel_counter_one, channel_counter_one) =&
                                        cgc*(wp_read(channel_counter_one-nchanBegin+1, channel_counter_two-mchanBegin+1))
    
                                END IF
    
                                IF (abs(wd_read(channel_counter_one-nchanBegin+1,       &
                                    channel_counter_two-mchanBegin+1)).GT.0.000000000001_wp) then
    
                                    IF (coupling_id .EQ. LS_coupling_id) THEN
                                        LML_wd_read_store(channel_counter_one-channel_counter_two, channel_counter_two) =    &
                                            (-1._wp)**(lgmlf-lgmli+1)*cgc*      &
                                            (wd_read(channel_counter_one-nchanBegin+1, channel_counter_two-mchanBegin+1))
                                    ELSE
                                        LML_wd_read_store(channel_counter_one-channel_counter_two, channel_counter_two) =    &
                                            (-1._wp)**((lgmlf-lgmli)/2+1)*cgc*      &
                                            (wd_read(channel_counter_one-nchanBegin+1, channel_counter_two-mchanBegin+1))
                                    END IF
    
                                    LML_wd_read_store(channel_counter_two-channel_counter_one, channel_counter_one) =&
                                        cgc*(wd_read(channel_counter_one-nchanBegin+1, channel_counter_two-mchanBegin+1))
    
                                END IF
    
                                IF (abs(wd_read_v(channel_counter_one-nchanBegin+1,         &
                                    channel_counter_two-mchanBegin+1)).GT.0.000000000001_wp) then
                                    IF (coupling_id .EQ. LS_coupling_id) THEN
                                        LML_wd_read_store_v(channel_counter_one-channel_counter_two, channel_counter_two) =  &
                                            (-1._wp)**(lgmlf-lgmli+1)*cgc*      &
                                            (wd_read_v(channel_counter_one-nchanBegin+1, channel_counter_two-mchanBegin+1))
                                    ELSE
                                        LML_wd_read_store_v(channel_counter_one-channel_counter_two, channel_counter_two) =  &
                                            (-1._wp)**((lgmlf-lgmli)/2+1)*cgc*      &
                                            (wd_read_v(channel_counter_one-nchanBegin+1, channel_counter_two-mchanBegin+1))
                                    END IF
                                    LML_wd_read_store_v(channel_counter_two-channel_counter_one, channel_counter_one) =&
                                        cgc*(wd_read_v(channel_counter_one-nchanBegin+1, channel_counter_two-mchanBegin+1))
                                END IF
    
                                END DO
                                END DO
    
    
                                CALL assert(LML_block_npty(blocki).ne.LML_block_npty(blockf),       &
                                'Error in DBlock reading  ! Transition with no parity change!')
    
                            END IF
                        END DO
                    END DO


                    END ASSOCIATE
                END IF
            END IF
            ! Close DNN file for individual transition
            CLOSE (io_data)
        END DO
        CLOSE (temp_dipsto_txt)
        CLOSE (temp_dipsto_v_txt)
        ! Close D00 file
        CLOSE (io_header)
        D%fintr = icount

        IF (debug) THEN
            WRITE (*, *)  'fintr in Read_D_File = ', D%fintr
            WRITE (*, *)  'intr in Read_D_File = ', intr
        END IF

        ! We can now deallocate outer region matricies used for individual transitions
        DEALLOCATE (wp_read, stat = err)
        CALL assert(err .EQ. 0, 'deAllocation error in wp_read_store')
        DEALLOCATE (wd_read, stat = err)
        CALL assert(err .EQ. 0, 'deAllocation error in wd_read_store')
        DEALLOCATE (wd_read_v, stat = err)
        CALL assert(err .EQ. 0, 'deAllocation error in wd_read_store_v')

    END SUBROUTINE parse_D_file_RM1

    SUBROUTINE read_D_file_RM1( &
                                d00_path, &
                                d_path, &
                                lplusp, &
                                crlv, &
                                iidip, &
                                ifdip, &
                                block_ind, &
                                dipole_coupled, &
                                wp_read_store, &
                                wd_read_store, &
                                wd_read_store_v, &
                                LML_wp_read_store, &
                                LML_wd_read_store, &
                                LML_wd_read_store_v, &
                                L_block_nchan, &
                                L_block_tot_nchan, &
                                LML_block_nchan, &
                                LML_block_tot_nchan, &
                                ntarg, &
                                nstmx, &
                                no_of_L_blocks, &
                                no_of_LML_blocks, &
                                coupling_id, &
                                dipole_velocity_output, &
                                L_block_lrgl, &
                                L_block_nspn, &
                                L_block_npty, &
                                LML_block_nspn, &
                                LML_block_lrgl, &
                                LML_block_l2p, &
                                LML_block_npty, &
                                LML_block_ml, &
                                LML_block_nconat, &
                                ML_Max, &
                                xy_plane_desired, &
                                fintr, &
                                dipsto, &
                                dipsto_v, &
                                debug, &
                                mat_size_wd, &
                                mat_size_wp)
        
        CHARACTER(len=*), INTENT(IN)         :: d00_path
        CHARACTER(len=*), INTENT(IN)         :: d_path

        INTEGER, INTENT(IN) :: lplusp

        INTEGER, INTENT(IN)                  :: coupling_id
        INTEGER, INTENT(IN)                  :: no_of_L_blocks
        INTEGER, INTENT(IN)                  :: no_of_LML_blocks
        INTEGER, INTENT(IN)                  :: nstmx
        INTEGER, INTENT(IN)                  :: ntarg
        INTEGER, INTENT(IN)                  :: ML_Max

        LOGICAL, INTENT(IN)                  :: debug
        LOGICAL, INTENT(IN)                  :: dipole_velocity_output 

        INTEGER, ALLOCATABLE, INTENT(INOUT)  :: iidip(:)
        INTEGER, ALLOCATABLE, INTENT(INOUT)  :: ifdip(:)

        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: crlv(:, :)
        INTEGER, ALLOCATABLE, INTENT(INOUT)  :: dipole_coupled(:, :)
        INTEGER, ALLOCATABLE, INTENT(INOUT)  :: block_ind(:, :, :)
        REAL(WP), ALLOCATABLE, INTENT(INOUT) :: wp_read_store(:, :)
        REAL(WP), ALLOCATABLE, INTENT(INOUT) :: wd_read_store(:, :)
        REAL(WP), ALLOCATABLE, INTENT(INOUT) :: wd_read_store_v(:, :)

        REAL(WP), ALLOCATABLE, INTENT(INOUT) :: LML_wp_read_store(:, :)
        REAL(WP), ALLOCATABLE, INTENT(INOUT) :: LML_wd_read_store(:, :)
        REAL(WP), ALLOCATABLE, INTENT(INOUT) :: LML_wd_read_store_v(:, :)

        INTEGER, INTENT(IN)                  :: L_block_lrgl(:)
        INTEGER, INTENT(IN)                  :: L_block_nspn(:)
        INTEGER, INTENT(IN)                  :: L_block_npty(:)

        INTEGER, ALLOCATABLE, INTENT(INOUT) :: L_block_nchan(:)
        INTEGER, ALLOCATABLE, INTENT(INOUT) :: LML_block_nchan(:)
        INTEGER, INTENT(IN)                 :: L_block_tot_nchan
        INTEGER, INTENT(IN)                 :: LML_block_tot_nchan

        INTEGER, INTENT(IN)                  :: LML_block_lrgl(:)
        INTEGER, INTENT(IN)                  :: LML_block_l2p(:, :)
        INTEGER, INTENT(IN)                  :: LML_block_npty(:)
        INTEGER, INTENT(IN)                  :: LML_block_nspn(:)
        INTEGER, INTENT(IN)                  :: LML_block_ml(:)
        INTEGER, INTENT(IN)                  :: LML_block_nconat(:, :)

        INTEGER, INTENT(INOUT) :: mat_size_wd
        INTEGER, INTENT(INOUT) :: mat_size_wp

        LOGICAL, INTENT(IN)                  :: xy_plane_desired(:)
        INTEGER, INTENT(INOUT)               :: fintr

        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: dipsto(:, :, :)
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: dipsto_v(:, :, :)

        INTEGER :: ii
        INTEGER :: M
        INTEGER :: N
        INTEGER :: output_data
        
        TYPE(D_file) :: D

        CALL parse_D_file_RM1(&
                                d00_path, &
                                d_path, &
                                D, &
                                lplusp, &
                                crlv, &
                                block_ind, &
                                dipole_coupled, &
                                wp_read_store, &
                                wd_read_store, &
                                wd_read_store_v, &
                                LML_wp_read_store, &
                                LML_wd_read_store, &
                                LML_wd_read_store_v, &
                                L_block_nchan, &
                                L_block_tot_nchan, &
                                LML_block_nchan, &
                                LML_block_tot_nchan, &
                                ntarg, &
                                nstmx, &
                                no_of_L_blocks, &
                                no_of_LML_blocks, &
                                coupling_id, &
                                dipole_velocity_output, &
                                L_block_lrgl, &
                                L_block_nspn, &
                                L_block_npty, &
                                LML_block_nspn, &
                                LML_block_lrgl, &
                                LML_block_l2p, &
                                LML_block_npty, &
                                LML_block_ml, &
                                LML_block_nconat, &
                                ML_Max, &
                                xy_plane_desired, &
                                debug, &
                                mat_size_wd, &
                                mat_size_wp)

        
        fintr = D%fintr
        ALLOCATE (dipsto(nstmx, nstmx, fintr))
        IF(dipole_velocity_output) THEN
            ALLOCATE (dipsto_v(nstmx, nstmx, fintr))
        END IF

        ALLOCATE (iidip(fintr))
        ALLOCATE (ifdip(fintr))

        DO ii=1, fintr

            iidip(ii) = D%blocks(ii)%iidip(ii)
            ifdip(ii) = D%blocks(ii)%ifdip(ii)

            ! Reorder dipole matrix, (RMatrixI provides states in order from
            ! highest to lowest eigenvalue. RMT expects these in order from
            ! lowest to highest. The H file has already been reordered in
            ! MakeSplineWaves. A corresponding reordering needs to happen here
            ! for the dipoles
            DO M = 1, D%blocks(ii)%isi
                DO N = 1, D%blocks(ii)%jsi
                    !PRINT *, "M2", M
                    dipsto(M, N, ii) = D%blocks(ii)%temp_dipsto(D%blocks(ii)%isi+1 - M, D%blocks(ii)%jsi+1 -N)
                    
                    IF (dipole_velocity_output) THEN
                        dipsto_v(M, N, ii) = D%blocks(ii)%temp_dipsto_v(D%blocks(ii)%isi+1 - M, D%blocks(ii)%jsi+1 -N)
                    END IF
                END DO
            END DO


            IF (coupling_id .EQ. LS_coupling_id) THEN
                IF (D%blocks(ii)%JLrgL .EQ. D%blocks(ii)%ILrgL) THEN
                   dipsto(:,:,ii) = -1.0_wp*dipsto(:,:,ii)
                END IF
            END IF

        END DO
        
        CLOSE (output_data)
        CLOSE (81)
        CLOSE (82)

    END SUBROUTINE read_D_file_RM1

    !**************************************************************************************

    !> CHANNELS
    SUBROUTINE channel_data(wp_read_store, &
                            wd_read_store, &
                            wd_read_store_v, &
                            LML_wp_read_store, &
                            LML_wd_read_store, &
                            LML_wd_read_store_v, &
                            wp_read, &
                            wd_read, &
                            wd_read_v, &
                            L_block_nchan, &
                            L_block_tot_nchan, &
                            LML_block_nchan, &
                            LML_block_tot_nchan, &
                            ntarg, &
                            debug, &
                            coupling_id, &
                            no_of_L_blocks, &
                            no_of_LML_blocks, &
                            LML_block_lrgl, &
                            LML_block_l2p, &
                            LML_block_npty, &
                            LML_block_ml, &
                            LML_block_nconat, &
                            mat_size_wd, &
                            mat_size_wp, &
                            channel_counter_one, &
                            channel_counter_two, &
                            ML_Dipole_Coupling_Diff)

        REAL(wp), ALLOCATABLE, INTENT(INOUT)    :: wp_read_store(:, :)
        REAL(wp), ALLOCATABLE, INTENT(INOUT)    :: wd_read_store(:, :)
        REAL(wp), ALLOCATABLE, INTENT(INOUT)    :: wd_read_store_v(:, :)

        REAL(wp), ALLOCATABLE, INTENT(INOUT)    :: LML_wp_read_store(:, :)
        REAL(wp), ALLOCATABLE, INTENT(INOUT)    :: LML_wd_read_store(:, :)
        REAL(wp), ALLOCATABLE, INTENT(INOUT)    :: LML_wd_read_store_v(:, :)
        INTEGER, INTENT(IN)                     :: ntarg

        INTEGER, ALLOCATABLE, INTENT(INOUT)     :: L_block_nchan(:)
        INTEGER, ALLOCATABLE, INTENT(INOUT)     :: LML_block_nchan(:)
        INTEGER, INTENT(IN)                     :: L_block_tot_nchan
        INTEGER, INTENT(IN)                     :: LML_block_tot_nchan
        INTEGER, INTENT(IN)                     :: coupling_id
        INTEGER, INTENT(IN)                     :: LML_block_lrgl(:)
        INTEGER, INTENT(IN)                     :: LML_block_l2p(:, :)
        INTEGER, INTENT(IN)                     :: LML_block_npty(:)
        INTEGER, INTENT(IN)                     :: LML_block_ml(:)
        INTEGER, INTENT(IN)                     :: LML_block_nconat(:, :)
        
        LOGICAL, INTENT(IN)                     :: debug

        INTEGER, INTENT(IN)                     :: no_of_L_blocks
        INTEGER, INTENT(IN)                     :: no_of_LML_blocks

        INTEGER                                 :: err

        INTEGER                                 :: sym_one
        INTEGER                                 :: sym_two
        INTEGER                                 :: target_one
        INTEGER                                 :: target_two
        INTEGER                                 :: ncounter_one
        INTEGER                                 :: ncounter_two
        INTEGER, INTENT(OUT)                    :: channel_counter_one
        INTEGER, INTENT(OUT)                    :: channel_counter_two
        INTEGER                                 :: channel_insym_counter_one
        INTEGER                                 :: channel_insym_counter_two

        REAL(wp), ALLOCATABLE, INTENT(INOUT)    :: wp_read(:, :)
        REAL(wp), ALLOCATABLE, INTENT(INOUT)    :: wd_read(:, :)
        REAL(wp), ALLOCATABLE, INTENT(INOUT)    :: wd_read_v(:, :)

        INTEGER, INTENT(INOUT)                  :: mat_size_wd
        INTEGER, INTENT(INOUT)                  :: mat_size_wp
        
        INTEGER, INTENT(INOUT)                    :: ML_Dipole_Coupling_Diff

        If (coupling_id .eq. LS_coupling_id) THEN
            ML_Dipole_Coupling_Diff = 1
        ELSE
            ML_Dipole_Coupling_Diff = 2  ! because J is doubled in JK coupling in RMT for half integers
        END IF

        channel_counter_one = 0
        channel_counter_two = 0
        channel_insym_counter_one = 0
        channel_insym_counter_two = 0
        mat_size_wd = 0
        mat_size_wp = 0
        
        DO sym_one = 1, no_of_LML_blocks
            channel_insym_counter_one = 0
            DO target_one = 1, ntarg
                DO ncounter_one = 1, LML_block_nconat(target_one, sym_one)
                    channel_counter_one = channel_counter_one+1
                    channel_insym_counter_one = channel_insym_counter_one+1

                    channel_counter_two = 0
                    DO sym_two = 1, no_of_LML_blocks
                        channel_insym_counter_two = 0
                        DO target_two = 1, ntarg
                            DO ncounter_two = 1, LML_block_nconat(target_two, sym_two)
                                channel_counter_two = channel_counter_two+1
                                channel_insym_counter_two = channel_insym_counter_two+1

                                IF (abs(LML_block_lrgl(sym_one)-LML_block_lrgl(sym_two)).le.ML_Dipole_Coupling_Diff) THEN
                                    IF (LML_block_npty(sym_one).NE.LML_block_npty(sym_two)) THEN
                                    IF (abs(LML_block_ml(sym_one)-LML_block_ml(sym_two)).le.ML_Dipole_Coupling_Diff) THEN

                                    !Do wp Test
                                    IF (abs(LML_block_l2p(channel_insym_counter_one, sym_one)        &
                                        -LML_block_l2p(channel_insym_counter_two, sym_two)).le.1) THEN
                                        mat_size_wp = max(mat_size_wp, abs(channel_counter_one-channel_counter_two))
                                    END IF

                                    !Do wd Test
                                    mat_size_wd = max(mat_size_wd, abs(channel_counter_one-channel_counter_two))
                                    END IF
                                    END IF
                                    END IF

                                !End loop
                                !End loop
                            END DO
                        END DO

                        !End loop
                        !End loop
                    END DO
                END DO

                !End loop
                !End loop
            END DO
        END DO

        IF (debug) THEN
            print *,'mat_size_wp = ',mat_size_wp
            print *,'mat_size_wd = ',mat_size_wd
        END IF

        ! At this point mat_size_wp = 119

        ALLOCATE (wp_read_store(L_block_tot_nchan, L_block_tot_nchan), stat = err)
        CALL assert(err .EQ. 0, 'Allocation error in wp_read_store')
        ALLOCATE (wd_read_store(L_block_tot_nchan, L_block_tot_nchan), stat = err)
        CALL assert(err .EQ. 0, 'Allocation error in wd_read_store')
        ALLOCATE (wd_read_store_v(L_block_tot_nchan, L_block_tot_nchan), stat = err)
        CALL assert(err .EQ. 0, 'Allocation error in wd_read_store_v')

        ALLOCATE (LML_wp_read_store(-mat_size_wp:mat_size_wp, LML_block_tot_nchan), stat = err)
        CALL assert(err .EQ. 0, 'Allocation error in wp_read_store')
        ALLOCATE (LML_wd_read_store(-mat_size_wd:mat_size_wd, LML_block_tot_nchan), stat = err)
        CALL assert(err .EQ. 0, 'Allocation error in wd_read_store')
        ALLOCATE (LML_wd_read_store_v(-mat_size_wd:mat_size_wd, LML_block_tot_nchan), stat = err)
        CALL assert(err .EQ. 0, 'Allocation error in wd_read_store_v')

        ALLOCATE (wp_read(maxval(L_block_nchan), maxval(L_block_nchan)), stat = err)
        CALL assert(err .EQ. 0, 'Allocation error in wp_read_store')
        ALLOCATE (wd_read(maxval(L_block_nchan), maxval(L_block_nchan)), stat = err)
        CALL assert(err .EQ. 0, 'Allocation error in wd_read_store')
        ALLOCATE (wd_read_v(maxval(L_block_nchan), maxval(L_block_nchan)), stat = err)
        CALL assert(err .EQ. 0, 'Allocation error in wd_read_store_v')

        wp_read_store = 0.0_wp
        wd_read_store = 0.0_wp
        wd_read_store_v = 0.0_wp
        LML_wp_read_store = 0.0_wp
        LML_wd_read_store = 0.0_wp
        LML_wd_read_store_v = 0.0_wp
        wp_read = 0.0_wp
        wd_read = 0.0_wp
        wd_read_v = 0.0_wp

    END SUBROUTINE channel_data

END MODULE dipole_input_file_RM1