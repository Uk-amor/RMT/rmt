! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Routines for arbitrary solid rotation of the coordinate system.
!> Used to change orientation of the polarization plane.

MODULE coordinate_system

    USE precisn, ONLY: wp

    IMPLICIT NONE

    PRIVATE

    PUBLIC print_coordinate_system
    PUBLIC coordinate_transform,rot_3_vecs

CONTAINS

    !> @brief Calculate rotation matrix from axis and angle.
    !>
    !> This matrix can be then used to multiply vectors, producing components
    !> of appropriately rotated vectors in the same coordinate system. The formula
    !> has been shamelessly copied from Wikipedia.
    !>
    !> @param ax     Rotation axis.
    !> @param theta  Rotation angle.
    !> @param mat    The rotation matrix populated by this routine.
    !>
    SUBROUTINE rot_mat_axis_angle(ax, theta, mat)

        IMPLICIT NONE

        REAL(wp), DIMENSION(3), INTENT(IN)     :: ax
        REAL(wp), INTENT(IN)                   :: theta
        REAL(wp), DIMENSION(3, 3), INTENT(OUT) :: mat

        REAL(wp) :: c, s

        c = COS(theta)
        s = SIN(theta)

        mat(1, 1) = c + ax(1)**2*(1 - c)
        mat(2, 2) = c + ax(2)**2*(1 - c)
        mat(3, 3) = c + ax(3)**2*(1 - c)

        mat(1, 2) = ax(1)*ax(2)*(1 - c) - ax(3)*s
        mat(1, 3) = ax(1)*ax(3)*(1 - c) + ax(2)*s
        mat(2, 3) = ax(2)*ax(3)*(1 - c) - ax(1)*s

        mat(2, 1) = ax(2)*ax(1)*(1 - c) + ax(3)*s
        mat(3, 1) = ax(3)*ax(1)*(1 - c) - ax(2)*s
        mat(3, 2) = ax(3)*ax(2)*(1 - c) + ax(1)*s

    END SUBROUTINE rot_mat_axis_angle

!---------------------------------------------------------------------------

    !> @brief Rotate three vectors around an axis.
    !>
    !> Given an axis and an angle, construct a rotation matrix
    !> and apply it on three vectors.
    !>
    !> @param axis   Rotation axis.
    !> @param angle  Rotation angle.
    !> @param v1     First vector to transform.
    !> @param v2     Second vector to transform.
    !> @param v3     Third vector to transform.
    !>
    SUBROUTINE rot_3_vecs(axis, angle, v1, v2, v3)

        IMPLICIT NONE

        REAL(wp), DIMENSION(3), INTENT(IN)    :: axis
        REAL(wp), INTENT(IN)                  :: angle
        REAL(wp), DIMENSION(3), INTENT(INOUT) :: v1, v2, v3

        REAL(wp), DIMENSION(3, 3) :: m

        ! Calculate the matrix of rotation
        CALL rot_mat_axis_angle(axis, angle, m)

        ! Apply the matrix on all three vectors
        v1 = MATMUL(m, v1)
        v2 = MATMUL(m, v2)
        v3 = MATMUL(m, v3)

    END SUBROUTINE rot_3_vecs

!---------------------------------------------------------------------------

    !> @brief Transform a vector using Euler angles.
    !>
    !> Given three Euler angles @f$ alpha @f$, @f$ beta @f$ and @f$ gamma @f$,
    !> this subroutine calculates coordinates of a vector that is supplied via
    !> coordinates in the rotated frame. The particular variant of the Euler
    !> transformation used is x-z'-x'', i.e.
    !> - rotate coordinate system around 'x' by 'alpha'
    !> - rotate coordinate system around the new 'z' by 'beta'
    !> - rotate coordinate system around the new-new 'x' by 'gamma'
    !>
    !> @param v  Vector to transform
    !>
    SUBROUTINE coordinate_transform(v, i)

        USE global_data, ONLY: pi
        USE initial_conditions, ONLY: euler_alpha, &
                                      euler_beta, &
                                      euler_gamma

        IMPLICIT NONE

        REAL(wp), DIMENSION(3), INTENT(INOUT) :: v
        INTEGER,                INTENT(IN)    :: i

        REAL(wp), DIMENSION(3) :: ex, ey, ez
        REAL(wp)               :: alpha, beta, gamma

        ! initial canonical basis
        ex = [1, 0, 0]
        ey = [0, 1, 0]
        ez = [0, 0, 1]

        ! convert angles to radians
        alpha = euler_alpha(i)*pi/180
        beta = euler_beta(i)*pi/180
        gamma = euler_gamma(i)*pi/180

        ! carry out the transformation
        CALL rot_3_vecs(ex, alpha, ey, ez, v) ! rotate by 'alpha' around old 'x' (extrinsic)
        CALL rot_3_vecs(ez, beta, ex, ey, v) ! rotate by 'beta'  around new 'z' (node line)
        CALL rot_3_vecs(ex, gamma, ey, ez, v) ! rotate by 'gamma' around new 'x' (intrinsic)

    END SUBROUTINE coordinate_transform

!---------------------------------------------------------------------------

    !> @brief Current coordinate system information.
    !>
    !> Writes current orientation of the coordinate axes for the available
    !> Euler angles (these are read from the namelist input file). In case of multiple
    !> field configurations only the orientation of the first polarization plane is printed.
    !>
    SUBROUTINE print_coordinate_system

        IMPLICIT NONE

        REAL(wp), DIMENSION(3) :: ex, ey, ez

        ex = [1, 0, 0]; CALL coordinate_transform(ex, 1)
        ey = [0, 1, 0]; CALL coordinate_transform(ey, 1)
        ez = [0, 0, 1]; CALL coordinate_transform(ez, 1)

        WRITE (*, "(' Coordinate system')")
        WRITE (*, "('  x -> (',3f7.3,' )  /plane normal direction/')") ex
        WRITE (*, "('  y -> (',3f7.3,' )  /reverse minor semi-axis/')") ey
        WRITE (*, "('  z -> (',3f7.3,' )  /major semi-axis/')") ez
        WRITE (*, *)

    END SUBROUTINE print_coordinate_system

END MODULE coordinate_system
