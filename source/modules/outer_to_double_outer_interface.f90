! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Controls the flow of information from the one-electron outer region
!> to the two-electron outer region, including the matching of the wavefunction
!> at the boundary, and the calculation of the two-electron wavefunction continuation
!> in the single-electron outer region, for construction of the finite-difference
!> two-electron Laplacian terms.
 
MODULE outer_to_double_outer_interface

    USE angular_momentum,       ONLY: dracah
    USE calculation_parameters, ONLY: nfdm
    USE communications_parameters, ONLY: id_of_1st_pe_outer, &
                                         id_of_1st_pe_double_outer, &
                                         nsectors
    USE global_data,            ONLY: one, &
                                      zero
    USE grid_parameters,        ONLY: channel_id_1st, &
                                      channel_id_last, &
                                      x_1st, &
                                      x_last
    USE initial_conditions,     ONLY: ML_max, &
                                      numsols => no_of_field_confs
    USE precisn,                ONLY: wp
    USE readhd,                 ONLY: bspl_ndim, &
                                      first_l, &
                                      second_l, &
                                      ion_l, &
                                      ion_s, &
                                      ion_p, &
                                      lks, &
                                      ltarg, &
                                      ltarg_dication, &
                                      LML_block_l2p, &
                                      LML_block_lrgl, &
                                      LML_block_ml, &
                                      LML_block_nchan, &
                                      LML_block_nconat, &
                                      LML_block_npty, &
                                      LML_block_nspn, &
                                      LML_block_tot_nchan, &
                                      no_of_LML_blocks, &
                                      nser, &
                                      ntarg, &
                                      nks, &
                                      n2p, &
                                      one_elec_basis_coeffs, &
                                      outer_surf_amp, &
                                      starg, &
                                      starg_dication, &
                                      tot_l, &
                                      tot_ml, &
                                      tot_p, &
                                      tot_s, &
                                      two_electron_l, &
                                      two_electron_nchan, &
                                      two_electron_spin, &
                                      LML_block_chanstart, &
                                      LML_block_chanend, &
                                      chan_2e_start, &
                                      chan_2e_end, &
                                      nchmx
    USE rmt_assert,             ONLY: assert
    USE MPI

    IMPLICIT NONE

    COMPLEX(wp), ALLOCATABLE :: two_electron_psi_inside_outer_bndry(:, :, :, :)
    COMPLEX(wp), ALLOCATABLE :: overlap(:, :, :)
    COMPLEX(wp), ALLOCATABLE :: overlap_red(:, :, :, :)
    COMPLEX(wp), ALLOCATABLE :: omega(:, :), omega_red(:, :, :)
    REAL(wp), ALLOCATABLE    :: one_elec_basis_near_bndry(:, :, :)
    REAL(wp), ALLOCATABLE    :: bsplines(:, :)

    PUBLIC compute_outer_boundary_amplitudes
    PUBLIC allocate_overlap_and_surf_amps
    PUBLIC setup_overlap_and_outer_surf_amp_matrices
    PUBLIC get_two_electron_psi_in_one_electron_outer_region
    PUBLIC send_two_electron_psi_in_one_electron_outer_region
    PUBLIC recv_two_electron_psi_in_one_electron_outer_region
    PUBLIC allocate_two_electron_psi_near_bndry
    PUBLIC deallocate_two_electron_psi_near_bndry
    PUBLIC allocate_one_elec_basis_near_bndry
    PUBLIC deallocate_one_elec_basis_near_bndry

CONTAINS

!---------------------------------------------------------------------------

    !> \brief   Calculate the wavefunction amplitudes near the boundary between
    !>          the one- and two-electron outer regions.
    !> \authors G Armstrong
    !> \date    2022

    SUBROUTINE compute_outer_boundary_amplitudes

        USE calculation_parameters, ONLY: nfdm
        USE precisn,                ONLY: wp
        USE readhd,                 ONLY: bspl_ndim, &
                                          nser, &
                                          one_elec_basis_coeffs
        USE rmt_assert,             ONLY: assert
        USE setup_bspline_basis,    ONLY: bsplines

        INTEGER  :: i, ii, il, jj
        REAL(wp) :: total

        one_elec_basis_near_bndry = 0.0_wp
        DO i = 1, nfdm
            DO il = 0, nser - 1
                DO ii = 1, bspl_ndim
                    total = 0.0_wp
                    DO jj = 1, bspl_ndim
                        total = total + one_elec_basis_coeffs(jj, ii, il)*bsplines(i, jj+1)
                    END DO
                    one_elec_basis_near_bndry(ii, il, i) = total
                END DO
            END DO
        END DO

    END SUBROUTINE compute_outer_boundary_amplitudes

!---------------------------------------------------------------------

    !> \brief   Extend the wavefunction in the two-electron outer region into
    !>          the one-electron outer region.
    !> \authors G Armstrong
    !> \date    2022
    !>          The procedure is similar to the one used to extend the one-electron
    !>          outer region wavefunction into the inner region. Here we loop over channels
    !>          in both one- and two-electron outer regions, multiply the one-electron
    !>          outer region wavefunction by appropriate basis and recoupling coefficients,
    !>          and sum over all channels in the one-electron outer region.

    !>          Currently, the recoupling coefficients used below are general, and the radial basis
    !>          function is specific to helium.

    SUBROUTINE setup_overlap_and_outer_surf_amp_matrices

        INTEGER  :: chcount, i, ncti
        INTEGER  :: ii, itgt, j, jj
        INTEGER  :: cation_l, cation_s, dication_l, dication_s
        INTEGER  :: la, lb
        INTEGER  :: l, s, total_l, total_ml, total_s, total_p
        REAL(wp) :: factor, recoupling, recoupling_l, recoupling_s, wl, ws
        LOGICAL  :: total_l_selection, total_ml_selection, total_s_selection
        LOGICAL  :: total_p_selection
        LOGICAL  :: one_electron_l_selection
        LOGICAL  :: coupling_allowed

        print *,'Chan2estart ',chan_2e_start
        print *,'LML_chanstart ',LML_block_chanstart

        omega = zero; omega_red = zero; overlap = zero; overlap_red = zero
        wl = 0.0_wp; ws = 0.0_wp
!       total_s = (LML_block_nspn(1) - 1) / 2
        DO jj = 1, two_electron_nchan
            dication_l = ion_l(jj)
            dication_s = ion_s(jj)
            la = first_l(jj)
            lb = second_l(jj)
            l = two_electron_l(jj)
            s = two_electron_spin(jj)
            total_l = tot_l(jj)
            total_ml = tot_ml(jj)
            total_s = tot_s(jj)
            total_p = tot_p(jj)

            j = 0
            DO i = 1, no_of_LML_blocks
                chcount = 0
                DO itgt = 1, ntarg
                    DO ncti = 1, LML_block_nconat(itgt, i)
                       chcount = chcount + 1
                       j = j + 1
                       cation_l = ltarg(itgt); cation_s = starg(itgt)
                       ! Conserve total L,  total M_L, and total S
                       total_l_selection = total_l == LML_block_lrgl(i)
                       total_ml_selection = total_ml == LML_block_ml(i)
                       total_s_selection = total_s == (LML_block_nspn(i)-1)/2    ! check this
                       total_p_selection = total_p ==LML_block_npty(i)
                       ! la, lb are the inner and outer electron angular momenta
                       !one_electron_l_selection = (la == ltarg(itgt)) .AND. (lb == lks(j))
                       one_electron_l_selection = (la == ltarg(itgt)) .AND. (lb == LML_block_l2p(chcount, i))
                       ! Channels are coupled only if all the above criteria are true
                       coupling_allowed = total_l_selection &
                                          .AND. total_ml_selection &
                                          .AND. total_s_selection &
                                          .AND. total_p_selection &
                                          .AND. one_electron_l_selection

                       IF (coupling_allowed) THEN !CHECK SELECTION
                           CALL dracah(2*dication_l, 2*la, 2*total_l, 2*lb, 2*cation_l, 2*l, wl)
                           CALL dracah(2*dication_s, 1, 2*total_s, 1, cation_s - 1, 2*s, ws)  !cation_s = 2S' + 1
                           recoupling_l = SQRT((2._wp * REAL(l, wp) + 1._wp)*(2._wp * REAL(cation_l, wp) + 1._wp)) * wl
                           recoupling_s = SQRT((2._wp * REAL(s, wp) + 1._wp)*REAL(cation_s, wp)) * ws
                           factor = 1.0_wp !MERGE(1.0_wp, SQRT(0.5_wp),  la == lb)
                           recoupling =  factor * recoupling_l * recoupling_s

                           DO ii = 1, nfdm
                               overlap(jj, j, ii) = one * recoupling * one_elec_basis_near_bndry(nks(j), la, ii)
                               overlap_red(jj-chan_2e_start(i)+1, j-LML_block_chanstart(i)+1, &
                                       ii, i) = overlap(jj, j, ii) 
                           END DO
! Hugo - The omega's need to be determined at the boundary - not at the last point inside
!                          omega(j, jj) = overlap(jj, j, nfdm)
                           omega(j, jj) = one * recoupling * one_elec_basis_coeffs(bspl_ndim, nks(j), la)
                           omega_red(j-LML_block_chanstart(i)+1, jj-chan_2e_start(i)+1, i) =omega(j, jj)
                       END IF
                    END DO
                END DO
            END DO
        END DO

    END SUBROUTINE setup_overlap_and_outer_surf_amp_matrices

!---------------------------------------------------------------------

    !> \brief   Calculate the two-electron wavefunction at two points inside the
    !>          one-electron outer region.
    !> \authors G Armstrong
    !> \date    2022
    !>
    !>          Project the two-electron wavefunction onto the 'overlap' matrix
    !>          that contains the couplings between one- and two-electron
    !>          wavefunctions.
    !>
    !> \param[in]  vecin    The one-electron radial wavefunction
    !>                      dimensions (x_1st:x_last, LML_block_tot_nchan, numsols)
    !> \param[out] vecout   The two-electron radial wavefunction at r_{N+1} = b - dr, b - 2dr
    !>                      for all r_{N+2} values.
    !>                      dimensions (nfdm, x_1st:x_last, two_electron_nchan, numsols)

    SUBROUTINE get_two_electron_psi_in_one_electron_outer_region(vecin, vecout)

        USE readhd, ONLY: LML_block_tot_nchan, &
                          LML_block_nconat

        COMPLEX(wp), INTENT(IN)  :: vecin(x_1st:x_last, LML_block_tot_nchan, numsols)
        COMPLEX(wp), INTENT(OUT) :: vecout(nfdm, x_1st:x_last, two_electron_nchan, numsols)

        LOGICAL     :: coupling_allowed, not_reduced_flag
        INTEGER     :: i, iblk, ii, itgt, j, jj, ncti
        REAL(wp)    :: term
        COMPLEX(wp) :: mat_tmp(two_electron_nchan, nchmx)
        COMPLEX(wp) :: vec_tmp(LML_block_tot_nchan)
        COMPLEX(wp) :: tmp_out(two_electron_nchan)
        COMPLEX(wp) :: lhs_mat(LML_block_tot_nchan, numsols)
        COMPLEX(wp) :: rhs_mat(two_electron_nchan, numsols)
        INTEGER     :: size2e(no_of_LML_blocks)
        INTEGER     :: max2e, kk

        not_reduced_flag = .false.
        size2e = chan_2e_end - chan_2e_start + 1
        max2e = MAXVAL(size2e)
!       max1e = MAXVAL(size1e)

        IF (not_reduced_flag) THEN
           vecout = zero
           IF (numsols == 1) THEN
               DO i = 1, nfdm
                   mat_tmp(:, :) = overlap(:, :, i)
                   DO ii = x_1st, x_last
                       vec_tmp(:) = vecin(ii, :, 1)
                       tmp_out = zero
                       CALL ZGEMV('N', two_electron_nchan, LML_block_tot_nchan, &
                                   one, mat_tmp, two_electron_nchan, &
                                   vec_tmp, 1, zero, tmp_out, 1)
                       vecout(i, ii, :, 1) = tmp_out(:)
                   END DO
               END DO
           ELSE
               DO i = 1, nfdm
                   mat_tmp(:, :) = overlap(:, :, i)
                   DO ii = x_1st, x_last
                       lhs_mat(:, :) = vecin(ii, :, :)
                       CALL ZGEMM('N', 'N', two_electron_nchan, numsols, LML_block_tot_nchan, &
                                   one, mat_tmp, two_electron_nchan, &
                                   lhs_mat, LML_block_tot_nchan, &
                                   zero, rhs_mat, two_electron_nchan)
                       vecout(i, ii, :, :) = rhs_mat(:, :)
                   END DO
               END DO
           END IF
        ELSE
         vecout = zero
         DO kk = 1, no_of_LML_blocks
           IF (numsols == 1) THEN
               DO i = 1, nfdm
                   mat_tmp(1:size2e(kk),1:LML_block_nchan(kk)) = overlap_red(1:size2e(kk), 1:LML_block_nchan(kk), i, kk)
                   !$OMP PARALLEL DO PRIVATE(ii,vec_tmp,tmp_out)
                   DO ii = x_1st, x_last
                       vec_tmp(1:LML_block_nchan(kk)) = &
                               vecin(ii, LML_block_chanstart(kk):LML_block_chanend(kk), 1)
                       tmp_out = zero
                       CALL ZGEMV('N', size2e(kk), LML_block_nchan(kk), &
                                   one, mat_tmp, two_electron_nchan, &
                                   vec_tmp, 1, zero, tmp_out, 1)
                       vecout(i, ii, chan_2e_start(kk):chan_2e_end(kk), 1) = tmp_out(1:size2e(kk))
                   END DO
               END DO
           ELSE
               DO i = 1, nfdm
                   mat_tmp(1:size2e(kk), 1:LML_block_nchan(kk)) = &
                           overlap_red(1:size2e(kk), 1:LML_block_nchan(kk), i, kk)
                   DO ii = x_1st, x_last
                       lhs_mat(1:LML_block_nchan(kk), :) = &
                               vecin(ii, LML_block_chanstart(kk):LML_block_chanend(kk), :)
                       CALL ZGEMM('N', 'N', size2e(kk), numsols, LML_block_nchan(kk), &
                                   one, mat_tmp, two_electron_nchan, &
                                   lhs_mat, LML_block_tot_nchan, &
                                   zero, rhs_mat, two_electron_nchan)
                       vecout(i, ii, chan_2e_start(kk):chan_2e_end(kk), :) = &
                               rhs_mat(1:size2e(kk), :)
                   END DO
               END DO
           END IF
         END DO
        END IF

!        vecout = zero
!        DO i = 1, nfdm
!            DO jj = 1, two_electron_nchan
!                j = 0
!                DO iblk = 1, no_of_LML_blocks
!                    DO itgt = 1, ntarg
!                        DO ncti = 1, LML_block_nconat(itgt, iblk)
!                            j = j + 1
!                            term = one_elec_basis_near_bndry(nks(j), lks(j), i)
!                            coupling_allowed = first_l(jj) == ltarg(itgt) &
!                                         .AND. second_l(jj) == lks(j) &
!                                         .AND. tot_l(jj) == LML_block_lrgl(i) &
!                                         .AND. tot_ml(jj) == LML_block_ml(i)
!                            IF (coupling_allowed) THEN
!                                vecout(i, :, jj, 1) = vecout(i, :, jj, 1) + term * vecin(:, j, 1)
!                            END IF
!                        END DO
!                    END DO
!                END DO
!            END DO
!        END DO

    END SUBROUTINE get_two_electron_psi_in_one_electron_outer_region

!---------------------------------------------------------------------------------------------

    !> \brief   Send the two-electron wavefunction in the one-electron outer region from
    !>          each core in the one-electron outer region to the core directly above it
    !>          in the two-electron outer region.
    !> \authors G Armstrong
    !> \date    2022
    !>
    !> \param[out] two_electron_psi_inside_outer_bndry   The two-electron radial wavefunction at
    !>                                                   r_{N+1} = b + o*dr - dr, b + o*dr - 2dr for all r_{N+2} values
    !>                                                   where o is the value of the grid offset.
    !>                                                   dimensions (nfdm, x_1st:x_last, two_electron_nchan, numsols)

    SUBROUTINE send_two_electron_psi_in_one_electron_outer_region(two_electron_psi_inside_outer_bndry)

        USE communications_parameters, ONLY: no_of_cols
        USE mpi_communications,        ONLY: get_my_pe_id

        COMPLEX(wp), INTENT(IN) :: two_electron_psi_inside_outer_bndry(nfdm, x_1st:x_last, two_electron_nchan, numsols)

        INTEGER :: dest_pe, my_rank
        INTEGER :: tag, ierr
        INTEGER :: x_size
        INTEGER :: s_test

        ! Determine the two_electron outer region PE ID to which data should be sent
        CALL get_my_pe_id(my_rank)
        dest_pe = int((my_rank - id_of_1st_pe_outer)/nsectors) + id_of_1st_pe_double_outer
!       dest_pe = my_rank + no_of_cols
!       print *,' Interface ', s_test, dest_pe

        ! Determine sector size
        x_size = x_last - x_1st + 1

        ! Send data
        tag = 11
        CALL MPI_SEND(two_electron_psi_inside_outer_bndry, nfdm*x_size*two_electron_nchan*numsols, MPI_DOUBLE_COMPLEX, &
                      dest_pe, tag, MPI_COMM_WORLD, ierr)

    END SUBROUTINE send_two_electron_psi_in_one_electron_outer_region

!---------------------------------------------------------------------

    !> \brief   Each core in the lowest row of two-electron outer region
    !>          (i.e. those closest to the boundary between the one- and two-electron outer regions)
    !>          receives the two-electron wavefunction in the one-electron outer region from cores
    !>          directly below them in the one-electron outer region.
    !> \authors G Armstrong
    !> \date    2022
    !>
    !> \param[out] vecout   The two-electron radial wavefunction at
    !>                      r_{N+1} = b + o*dr - dr, b + o*dr - 2dr for all r_{N+2} values
    !>                      where o is the value of the grid offset.
    !>                      dimensions (nfdm, x_1st:x_last, two_electron_nchan, numsols)

    SUBROUTINE recv_two_electron_psi_in_one_electron_outer_region(vecout)

        USE communications_parameters, ONLY: no_of_cols
        USE mpi_communications,        ONLY: get_my_pe_id

        COMPLEX(wp), INTENT(OUT) :: vecout(nfdm, x_1st:x_last, two_electron_nchan, 1:numsols)
        INTEGER                  :: my_rank, dest_pe, source_pe
        INTEGER                  :: tag, ierr
        INTEGER                  :: x_size
        INTEGER                  :: status(MPI_STATUS_SIZE)
        COMPLEX(wp) :: vectmp(nfdm, x_1st:x_last/nsectors, two_electron_nchan, 1:numsols)
        INTEGER :: j, s_test

        CALL get_my_pe_id(my_rank)

        ! Set the two-electron outer region processor that receives
        dest_pe = my_rank

        ! Determine which one-electron outer region processor has sent
!       source_pe = my_rank - no_of_cols
        source_pe = (my_rank-id_of_1st_pe_double_outer)*nsectors+id_of_1st_pe_outer
!       print *, 'Interface ', source_pe, s_test

        ! Determine sector size
        x_size = (x_last - x_1st + 1)/nsectors

        ! Receive data from one-electron outer region
        IF (my_rank == dest_pe) THEN
            DO j = 1, nsectors
               tag = 11
               CALL MPI_RECV(vectmp, nfdm*x_size*two_electron_nchan*numsols, MPI_DOUBLE_COMPLEX, &
                          source_pe, tag, MPI_COMM_WORLD, status, ierr)
               vecout(:,x_1st+(j-1)*x_last/nsectors:x_last*j/nsectors,:,:) = vectmp(:,:,:,:)
               source_pe = source_pe + 1
            END DO
        END IF

    END SUBROUTINE recv_two_electron_psi_in_one_electron_outer_region

!-----------------------------------------------------------------------

    !> \brief   Calculate the two-electron wavefunction at the boundary r_{N+1} = b.
    !> \authors G Armstrong
    !> \date    2022
    !>          On the boundary between the one- and two-electron outer regions,
    !>          F_q(r_{N+1}=b, r_{N+2}, t) = sum_p omega_qp * f_p(r_{N+2}, t),
    !>          where p labels one-electron outer region channels,
    !>                q labels two-electron outer region channels,
    !>                F_q are two-electron outer region channel wavefunctions,
    !>                omega_qp are outer-region surface amplitudes,
    !>                f_p are one-electron outer region channel wavefunctions
    !>
    !> \param[inout] psi_double_outer_at_b   The initial two-electron radial wavefunction at the boundary
    !>                                       dimensions (two_electron_nchan, numsols)
    !> \param[in] psi_outer                  The initial one-electron radial wavefunction at the boundary
    !>                                       dimensions (LML_block_tot_nchan, numsols)

    SUBROUTINE get_two_electron_initial_state_at_b(psi_double_outer_at_b, psi_outer)

        COMPLEX(wp), INTENT(INOUT) :: psi_double_outer_at_b(1:two_electron_nchan, 1:numsols)
        COMPLEX(wp), INTENT(IN)    :: psi_outer(1:LML_block_tot_nchan, 1:numsols)
        INTEGER                    :: isol, jj

        DO isol = 1, numsols
            DO jj = 1, two_electron_nchan
                psi_double_outer_at_b(jj, isol) = zero !* DOT_PRODUCT(psi_outer(:, isol), omega(:, jj))
            END DO
        END DO

    END SUBROUTINE get_two_electron_initial_state_at_b

!---------------------------------------------------------------------

    SUBROUTINE allocate_two_electron_psi_near_bndry

        IMPLICIT NONE

        INTEGER  :: err

        ALLOCATE (two_electron_psi_inside_outer_bndry(x_1st:x_last, nfdm, two_electron_nchan, numsols), &
                  stat=err)
        CALL assert(err == 0, 'allocation error with two_electron_psi_inside_outer_bndry')

    END SUBROUTINE allocate_two_electron_psi_near_bndry

!---------------------------------------------------------------------

    SUBROUTINE allocate_one_elec_basis_near_bndry

        IMPLICIT NONE

        INTEGER :: err

        ALLOCATE(one_elec_basis_near_bndry(bspl_ndim, 0:nser-1, nfdm), stat=err)
        CALL assert(err == 0, 'Allocation of one_elec_basis_near_bndry failed')

    END SUBROUTINE allocate_one_elec_basis_near_bndry

!---------------------------------------------------------------------

    SUBROUTINE allocate_overlap_and_surf_amps

        IMPLICIT NONE

        INTEGER  :: err
        INTEGER     :: size2e(no_of_LML_blocks)
        INTEGER     :: max2e

!       size1e = LML_block_chanend - LML_block_chanstart + 1
        size2e = chan_2e_end - chan_2e_start + 1
        max2e = MAXVAL(size2e)
!       max1e = MAXVAL(size1e)

        ALLOCATE (overlap(two_electron_nchan, channel_id_1st:channel_id_last, nfdm), &
                  overlap_red(max2e, nchmx, nfdm, no_of_LML_blocks), &
                  omega(channel_id_1st:channel_id_last, two_electron_nchan), &
                  omega_red(nchmx, max2e, no_of_LML_blocks), &
                  stat=err)

        CALL assert(err == 0, 'allocation error with overlap and/or outer surface amps')

    END SUBROUTINE allocate_overlap_and_surf_amps

!---------------------------------------------------------------------

    SUBROUTINE deallocate_two_electron_psi_near_bndry

        IMPLICIT NONE

        INTEGER  :: err

        DEALLOCATE (two_electron_psi_inside_outer_bndry, stat=err)
        CALL assert(err == 0, 'deallocation error with two_electron_psi_inside_outer_bndry')

    END SUBROUTINE deallocate_two_electron_psi_near_bndry

!---------------------------------------------------------------------

    SUBROUTINE deallocate_one_elec_basis_near_bndry

        IMPLICIT NONE

        INTEGER :: err

        DEALLOCATE (one_elec_basis_near_bndry, stat=err)
        CALL assert(err == 0, 'deallocation error with one_elec_basis_near_bndry')

    END SUBROUTINE deallocate_one_elec_basis_near_bndry

!---------------------------------------------------------------------

    SUBROUTINE deallocate_overlap_and_surf_amps

        IMPLICIT NONE

        INTEGER  :: err

        DEALLOCATE (overlap, overlap_red, omega, omega_red, stat=err)
        CALL assert(err == 0, 'deallocation error with overlap and/or outer surface amps')

    END SUBROUTINE deallocate_overlap_and_surf_amps

!---------------------------------------------------------------------

END MODULE outer_to_double_outer_interface
