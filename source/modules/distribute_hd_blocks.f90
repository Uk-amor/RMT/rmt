! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Handles the distribution of the H file data and D file data across processors. 

!> MPI communication is carried out in this module, but only
!> at start-up. Each Lblock communicator has a master processor which receives a
!> relevant dipole block sent by the overall Inner Region master processor.  In
!> practice each Lblock master receives two separate dipole blocks corresponding
!> to the SP and PS like structure of the TD Hamiltonian Upon receiving these
!> my_dblock_u and my_dblock_d matrices the Lblock master subsequently
!> distributes these matrices among the rest of the processors within the
!> communicator of which it is the master (This further distribution is carried
!> out in a separate module distribute_hd_blocks2).  Once the dipole blocks have
!> been fully distributed (awp with diagonal eigenvalue arrays), the
!> matrix-vector kernel of the Lanczos method is ready to be called (in a
!> separate module).
!>
!> Molecular case: Each inner region block master with rank Lb_m_rank handles the
!> symmetry component of the full wavefunction with index Lb_m_rank+1. This
!> symmetry is dipole-coupled to at most three other symmetries. The corresponding
!> dipole blocks of the type [Lb_m_rank+1,I],[Lb_m_rank+1,J],[Lb_m_rank+1,K] are
!> stored in the arrays my_dblock_d,my_dblock_s,my_dblock_u.

MODULE distribute_hd_blocks

    USE precisn
    USE rmt_assert,         ONLY: assert
    USE initial_conditions, ONLY: dipole_velocity_output, &
                                  molecular_target,&
                                  debug
    USE mpi_layer_lblocks,  ONLY: Lb_m_comm, &
                                  Lb_m_rank, &
                                  Lb_m_size, &
                                  i_am_block_master
    USE readhd,             ONLY: max_L_block_size, &
                                  L_block_lrgl,     L_block_nspn,     L_block_npty, &
                                  LML_block_lrgl, LML_block_nspn, LML_block_npty, &
                                  LML_block_ml, & 
                                  LML_block_tot_nchan, &
                                  no_of_L_blocks, &
                                  no_of_LML_blocks, &
                                  finind_L, &         
                                  dipole_coupled, &
                                  cg_store, &
                                  block_ind
    USE MPI
 
    IMPLICIT NONE

    REAL(wp), ALLOCATABLE, SAVE :: dblock_12(:, :)
    REAL(wp), ALLOCATABLE, SAVE :: vblock_12(:, :)
    REAL(wp), ALLOCATABLE, SAVE :: my_dblock_u(:, :, :), my_dblock_d(:, :, :), my_dblock_s(:, :, :)
    REAL(wp), ALLOCATABLE, SAVE :: my_vblock_u(:, :, :), my_vblock_d(:, :, :), my_vblock_s(:, :, :)
    REAL(wp), ALLOCATABLE, SAVE :: hblock_11(:), surf_amps_11(:, :)
    REAL(wp), ALLOCATABLE, SAVE :: my_diag_els(:), my_surf_amps(:, :, :)
    INTEGER, allocatable, save  :: num_hblock_11_store(:,:)

    PRIVATE allocate_my_surf_amps, allocate_my_diag_els, allocate_my_dblocks
    PRIVATE allocate_my_couplings, allocate_my_surf_amps_not_block_master
    PRIVATE allocate_my_diag_els_not_block_master, allocate_my_dblocks_not_block_master

    PRIVATE setup_and_distribute_dblocks
    PRIVATE setup_and_send_hblocks
    PRIVATE setup_and_send_surf_amps
    PRIVATE recv_surf_amps_from_master
    PRIVATE recv_hblock_from_master
    PUBLIC setup_mpi_layer_1
    PUBLIC dealloc_my_surf_amps, deallocate_my_diag_els, deallocate_my_dblocks
    PUBLIC my_dblock_d, my_dblock_u, my_dblock_s, my_surf_amps, my_diag_els
    PUBLIC my_vblock_d, my_vblock_u, my_vblock_s
    PUBLIC deallocate_my_couplings

CONTAINS

    SUBROUTINE setup_mpi_layer_1(i_am_inner_master, i_am_in_outer_region)

        USE mpi_communications, ONLY: get_my_pe_id, all_processor_barrier_region
        USE readhd,             ONLY: deallocate_rank0_read_hd_files1, &
                                      deallocate_rank0_read_hd_files

        LOGICAL, INTENT(IN) :: i_am_inner_master
        LOGICAL, INTENT(IN) :: i_am_in_outer_region
        INTEGER             :: my_rank

        CALL get_my_pe_id(my_rank)

        IF (my_rank > 0) THEN
            CALL allocate_my_dips
            CALL allocate_my_couplings
        END IF

        CALL bcast_dips

        CALL all_processor_barrier_region

        IF (i_am_block_master) THEN
           CALL setup_local_couplings
           CALL allocate_my_dblocks
           CALL allocate_my_diag_els
           CALL allocate_my_surf_amps
        ELSE
           CALL allocate_my_dblocks_not_block_master
           CALL allocate_my_surf_amps_not_block_master
           CALL allocate_my_diag_els_not_block_master
        END IF

        ! Inner Region Master sets up and distributes to masters of each L Block that
        !    handle propagation order 0

        IF (i_am_inner_master) THEN
            CALL setup_and_send_hblocks
            CALL setup_and_send_surf_amps
            CALL deallocate_rank0_read_hd_files1(i_am_inner_master, i_am_in_outer_region)
        ELSE
            IF (i_am_block_master) THEN
                CALL recv_hblock_from_master
                CALL recv_surf_amps_from_master
            END IF
        END IF

        ! Inner Region Master distributes all required dipole blocks to the block
        ! masters
        IF (i_am_block_master) THEN
            CALL setup_and_distribute_dblocks(i_am_inner_master)
        END IF

        IF (i_am_inner_master) THEN
            ! Deallocate arrays that will not be used by PE 0 but that have been set up:
            ! Must be after Hblocks and Dblocks have been set up
            CALL deallocate_rank0_read_hd_files(i_am_inner_master, i_am_in_outer_region)
        END IF

    END SUBROUTINE setup_mpi_layer_1

!-----------------------------------------------------------------------------

    !> \brief Send block indices to subordinate processes
    !>
    !> Sends per-block global quantum numbers, coupling rules and dipole storage
    !> indices from the rank-0 MPI process to all other block masters.
    !>
    SUBROUTINE bcast_dips

        USE mpi_communications, ONLY: mpi_comm_region
!        use mpi_layer_lblocks, only: inner_region_rank 

        INTEGER :: ierr

        CALL MPI_BCAST(L_block_lrgl, no_of_L_blocks, MPI_INTEGER, 0, Lb_m_comm, ierr)
        CALL MPI_BCAST(L_block_nspn, no_of_L_blocks, MPI_INTEGER, 0, Lb_m_comm, ierr)
        CALL MPI_BCAST(L_block_npty, no_of_L_blocks, MPI_INTEGER, 0, Lb_m_comm, ierr)

        CALL MPI_BCAST(LML_block_lrgl, no_of_LML_blocks, MPI_INTEGER, 0, Lb_m_comm, ierr)
        CALL MPI_BCAST(LML_block_nspn, no_of_LML_blocks, MPI_INTEGER, 0, Lb_m_comm, ierr)
        CALL MPI_BCAST(LML_block_npty, no_of_LML_blocks, MPI_INTEGER, 0, Lb_m_comm, ierr)
        CALL MPI_BCAST(LML_block_ml, no_of_LML_blocks, MPI_INTEGER, 0, Lb_m_comm, ierr)
        IF (.NOT. molecular_target) THEN
           CALL MPI_BCAST (cg_store, no_of_LML_blocks*no_of_LML_blocks, MPI_DOUBLE_PRECISION, 0, Lb_m_comm, ierr)
        END IF

        CALL MPI_BCAST(dipole_coupled, no_of_LML_blocks*no_of_LML_blocks, MPI_INTEGER, 0, mpi_comm_region, ierr)
        CALL MPI_BCAST(block_ind, no_Of_L_blocks*no_of_L_blocks*3, MPI_INTEGER, 0, Lb_m_comm, ierr)

    END SUBROUTINE bcast_dips

!-----------------------------------------------------------------------------

    SUBROUTINE setup_and_send_hblocks
        USE mpi_layer_lblocks, ONLY: num_blocks_pe, block_start_pe, pes_per_lblock, max_num_LML_blocks

        IMPLICIT NONE

        INTEGER   :: ist           ! label for LML block
        INTEGER   :: i_pe, all_pe  ! label for lb_master, PE   
        INTEGER   :: i_previous    ! label for final LML block of previous PE   
        INTEGER   :: i_start, num, err    
        INTEGER   :: num_hblock_11 ! number of rows/columns in current hblock
        INTEGER   :: sum_num_hblock_11  ! cumulative sum for previous hblocks for this pe

        CALL allocate_hblock_11

        allocate (num_hblock_11_store(max_num_LML_blocks,lb_m_size), stat=err)
        all_pe = 1
        DO i_pe = 1, lb_m_size          
           num = num_blocks_pe(all_pe) - 1
           i_start = block_start_pe(all_pe)
           i_previous = i_start - 1
           sum_num_hblock_11 = 0
           do ist = i_start, i_start + num               
               CALL setup_hblock(ist, num_hblock_11)
               num_hblock_11_store(ist-i_previous,i_pe) = num_hblock_11 
               CALL send_hblock_to_lb_masters(i_pe, num_hblock_11, sum_num_hblock_11)
               sum_num_hblock_11 = sum_num_hblock_11 + num_hblock_11
           End Do
           all_pe = all_pe + pes_per_Lblock(i_pe)
        END DO
        call assert (err == 0 .and. (i_start+num) == no_of_LML_blocks, 'problem in setup_and_send_hblocks')

        CALL deallocate_hblock_11

    END SUBROUTINE setup_and_send_hblocks

!-----------------------------------------------------------------------------

    SUBROUTINE setup_hblock(ist, num_hblock_11)

        USE readhd, ONLY: eig, &
                          mnp1, &
                          etarg, &
                          neigsrem

        IMPLICIT NONE

        INTEGER, INTENT(IN)         :: ist
        INTEGER, INTENT(OUT)        :: num_hblock_11
        INTEGER                     :: i, L_block_symind

        hblock_11 = 0.0_wp

        ! DDAC: FIND INDEX FOR CORRESPONDING LSPi SYMMETRY BLOCK

        CALL finind_L(LML_block_nspn(ist), LML_block_lrgl(ist), LML_block_npty(ist), L_block_symind)

!       PRINT*, 'ist = ', ist

!       PRINT*, 'symind = ', L_block_symind

        num_hblock_11 = mnp1(L_block_symind) - neigsrem(L_block_symind)
        DO i = 1, num_hblock_11

            hblock_11(i) = eig(i, L_block_symind) - etarg(1)

        END DO

    END SUBROUTINE setup_hblock

!-----------------------------------------------------------------------------

    SUBROUTINE send_hblock_to_lb_masters(i_pe, num_hblock_11, sum_num_hblock_11)

        IMPLICIT NONE

        INTEGER, INTENT(IN)         :: i_pe          ! label for PE   
        INTEGER, INTENT(IN)         :: num_hblock_11 ! number of rows/columns in current hblock
        INTEGER, INTENT(IN)         :: sum_num_hblock_11  ! cumulative sum for previous hblocks for this pe
        INTEGER                     :: ierr, i_array(3)

        ! The master inner region processor sends diagonal elements of H to the master of each
        ! symmetry block handling the same propagation orders as the master inner region processor

        IF (i_pe == 1) THEN
            my_diag_els(sum_num_hblock_11+1:sum_num_hblock_11+num_hblock_11) = hblock_11(1:num_hblock_11)
        ELSE IF (i_pe > 1) THEN
           i_array(1) = i_pe
           i_array(2) = sum_num_hblock_11
           i_array(3) = num_hblock_11
           CALL MPI_SSEND(i_array, 3, MPI_INTEGER, i_pe - 1, 0, Lb_m_comm, ierr)
!             print *, 'iarray sent to i_pe - 1, lb_m_rank', i_pe - 1, lb_m_rank
           CALL MPI_SSEND(hblock_11, num_hblock_11, MPI_DOUBLE_PRECISION, i_pe - 1, 0, &
                           Lb_m_comm, ierr)
        END IF

        IF (debug) PRINT *, 'Inner master has completed Hblock sends to lb_m_comm rank', i_pe - 1

    END SUBROUTINE send_hblock_to_lb_masters

!-----------------------------------------------------------------------------

    SUBROUTINE recv_hblock_from_master

        USE mpi_communications, ONLY: get_my_pe_id
        USE mpi_layer_lblocks,  ONLY: my_num_LML_blocks

        IMPLICIT NONE

        INTEGER         :: i_pe          ! label for PE in lb_m_comm (block masters)   
        INTEGER         :: num_hblock_11 ! number of rows/columns in current hblock
        INTEGER         :: sum_num_hblock_11  ! cumulative sum for previous hblocks for this pe

        INTEGER  :: my_rank
        INTEGER  :: ierr, i_array(3), i_block, status1a(MPI_STATUS_SIZE), status1b(MPI_STATUS_SIZE)

        ! The master of each symmetry block handling the same propagation orders as the master
        ! inner region processor receives diagonal elements of H from the master inner region processor

        IF (i_am_block_master) THEN
           DO i_block = 1, my_num_LML_blocks
              IF (Lb_m_rank > 0) THEN
                  CALL MPI_RECV(i_array, 3, MPI_INTEGER, 0, 0, Lb_m_comm, status1a, ierr)
                  i_pe = i_array(1)
                  sum_num_hblock_11 = i_array(2)
                  num_hblock_11 = i_array(3)
                  CALL MPI_RECV(my_diag_els(sum_num_hblock_11+1:sum_num_hblock_11+num_hblock_11), num_hblock_11, &
                                MPI_DOUBLE_PRECISION, 0, 0, Lb_m_comm, status1b, ierr)
                  Call assert (Lb_m_rank == (i_pe - 1), 'problem in recv_hblock_from_master')
              END IF
           END DO
        END IF

        ! Get my rank
        CALL get_my_pe_id(my_rank)

        IF (debug) PRINT *, 'rank', my_rank, 'has completed all Hblock recvs'

    END SUBROUTINE recv_hblock_from_master

!-----------------------------------------------------------------------------

    SUBROUTINE setup_and_send_surf_amps
        USE mpi_layer_lblocks, only: num_blocks_pe, block_start_pe, pes_per_lblock

        IMPLICIT NONE

        INTEGER   :: ist, ichco
        INTEGER   :: i_pe, all_pe  ! label for lb_master, PE   
        INTEGER   :: i_previous    ! label for final LML block of previous PE   
        INTEGER   :: i_start, num    

        ichco = 0

        CALL allocate_surface_amplitudes

        all_pe = 1
        DO i_pe = 1, lb_m_size
           num = num_blocks_pe(all_pe) - 1
           i_start = block_start_pe(all_pe)
           i_previous = i_start - 1
           do ist = i_start, i_start + num
              CALL setup_surface_amplitudes(ist, ichco)

              CALL send_surf_amps_to_lb_masters(ist, i_pe, i_previous)
           end do 
           all_pe = all_pe + pes_per_Lblock(i_pe)
        END DO
        call assert ((i_start+num) == no_of_LML_blocks, 'problem in setup_and_send_surf_amps')

        CALL deallocate_surface_amplitudes

    END SUBROUTINE setup_and_send_surf_amps

!-----------------------------------------------------------------------------

    SUBROUTINE setup_surface_amplitudes(ist, ichco)

        USE readhd, ONLY: wmat, &
                          mnp1, &
                          neigsrem, &
                          L_block_nchan

        IMPLICIT NONE

        INTEGER, INTENT(IN)         :: ist
        INTEGER, INTENT(INOUT)      :: ichco
        INTEGER                     :: ich, i, L_block_symind

        surf_amps_11 = 0.0_wp

        ! DDAC: FIND INDEX FOR CORRESPONDING LSPi SYMMETRY BLOCK

!       CALL finind_L(2,1,1,L_block_symind)

!       PRINT*, 'symind_test = ', L_block_symind

        CALL finind_L(LML_block_nspn(ist), LML_block_lrgl(ist), LML_block_npty(ist), L_block_symind)

!       PRINT*, 'ist =', ist

!       PRINT*, 'array elements:', LML_block_nspn(ist),LML_block_lrgl(ist),LML_block_npty(ist)

!       PRINT*, 'symind =', L_block_symind

        DO ich = 1, L_block_nchan(L_block_symind)

            ichco = ichco + 1

            DO i = 1, mnp1(L_block_symind) - neigsrem(L_block_symind)
                surf_amps_11(i, ichco) = wmat(ich, i, L_block_symind)
            END DO

        END DO

    END SUBROUTINE setup_surface_amplitudes

!-----------------------------------------------------------------------------

    SUBROUTINE send_surf_amps_to_lb_masters(ist, i_pe, i_previous)

        IMPLICIT NONE

        INTEGER, INTENT(IN)         :: ist           ! label for LML block
        INTEGER, INTENT(IN)         :: i_pe          ! label for PE   
        INTEGER, INTENT(IN)         :: i_previous    ! label for final LML block of previous PE   
        INTEGER                     :: ierr, nc, i_array(3)

        ! The master inner region processor sends surface amplitudes to the master of each symmetry
        ! block handling the same propagation orders as the master inner region processor

        nc = max_L_block_size

        IF (i_pe == 1) THEN
           my_surf_amps(:,:,ist-i_previous) = surf_amps_11
        ELSE IF (i_pe > 1) THEN
           i_array(1) = i_pe
           i_array(2) = ist
           i_array(3) = i_previous
           CALL MPI_SSEND(i_array, 3, MPI_INTEGER, i_pe - 1, 0, Lb_m_comm, ierr)
           CALL MPI_SSEND(surf_amps_11, nc*LML_block_tot_nchan, MPI_DOUBLE_PRECISION, &
                           i_pe - 1, 0, Lb_m_comm, ierr)
        END IF

        IF (debug) PRINT *, 'Inner master has completed all surf_amp sends to rank' , ist -1

    END SUBROUTINE send_surf_amps_to_lb_masters

!-----------------------------------------------------------------------------

    SUBROUTINE recv_surf_amps_from_master

        USE mpi_communications, ONLY: get_my_pe_id
        USE mpi_layer_lblocks,  ONLY: my_LML_block_id, my_num_LML_blocks

        IMPLICIT NONE

        INTEGER         :: ist           ! label for LML block
        INTEGER         :: i_pe          ! label for PE in lb_m_comm (block masters)   
        INTEGER         :: i_previous    ! label for final LML block of previous PE   

        INTEGER :: my_rank
        INTEGER :: status1a(MPI_STATUS_SIZE), status1b(MPI_STATUS_SIZE), ierr, nc, i_array(3), i_block

        ! The master of each symmetry block handling the same propagation orders as the master
        ! inner region processor receives surface amplitudes from the master inner region processor

        nc = max_L_block_size

        ! Get my rank
        CALL get_my_pe_id(my_rank)

!       PRINT*, 'LML_block_tot_nchan =', LML_block_tot_nchan
!       PRINT*, 'rank',my_rank,'nc*LML_block_tot_nchan = ', nc*LML_block_tot_nchan

        IF (i_am_block_master) THEN
!           PRINT *,'My_rank, lb_m_rank =', my_rank, lb_m_rank, ', Check 1'
            IF (Lb_m_rank > 0) THEN
!           PRINT *,'My_rank, lb_m_rank =', my_rank, lb_m_rank, ', Check 2'
               DO i_block = 1, my_num_LML_blocks
                  CALL MPI_RECV(i_array, 3, MPI_INTEGER, 0, 0, Lb_m_comm, status1a, ierr)
                  i_pe = i_array(1)
                  ist  = i_array(2)
                  i_previous = i_array(3)
                  CALL MPI_RECV(my_surf_amps(:,:,ist-i_previous), nc*LML_block_tot_nchan, MPI_DOUBLE_PRECISION, &
                                0, 0, Lb_m_comm, status1b, ierr)
!           PRINT *,'My_rank, lb_m_rank =', my_rank, lb_m_rank, ', Check 3, i_block=', i_block, 'of ', my_num_LML_blocks
                  CALL assert (Lb_m_rank == (i_pe - 1), 'problem in recv_surf_amps_from_master')
               END DO   
            END IF
        END IF

        IF (debug) PRINT *, 'rank', my_rank, 'has completed all surf_amp recvs'

    END SUBROUTINE recv_surf_amps_from_master

!-----------------------------------------------------------------------------

    !> \brief   Send dipole blocks from inner master to block masters
    !> \authors Z Masin, G Armstrong, J Benda
    !> \date    2017 - 2018
    !>
    !> The dipole blocks are read by master during the start-up of the program. And at this point
    !> they are about to be distributed among individual block masters. This subroutine makes use
    !> of the \ref dipole_coupled matrix. It loops over all its elements and sends the appropriate
    !> (d/s/u) dipole blocks to the block masters. The atomic and molecular approaches are slightly
    !> different, as the atomic variant labels d/s/u blocks based on the relation of the angular
    !> monenta \f$ L_i \f$ and \f$ L_f \f$ (and store only one dipole block per all blocks that
    !> differ just by \f$ M_L \f$), whereas the molecular variant stores three dipole blocks (whose
    !> d/s/u labels are base on the dipole operator Cartesian components) for every pair of coupled
    !> \f$ L, M_L \f$ states.
    !>
    !> The body of the routines is guarded by MPI_Barrier to achieve a nice combined log.
    !>
    !> \warning Uses MPI_DOUBLE_COMPLEX type in the parallel communication routines, which means
    !>          that it is not compatible with compile-time selection of real type accuracy!
    !>
    SUBROUTINE setup_and_distribute_dblocks(i_am_inner_master)

        USE global_data, ONLY: im
        USE wall_clock, ONLY: hel_time
        USE readhd, ONLY: L_block_post, &
                          nocoupling, &
                          downcoupling, &
                          samecoupling, &
                          upcoupling, &
                          dipsto, &
                          dipsto_v
        USE mpi_layer_lblocks, ONLY: my_LML_block_id, my_num_LML_blocks
        USE mpi_layer_lblocks, ONLY: num_blocks_pe, block_start_pe, pes_per_Lblock

        IMPLICIT NONE

        LOGICAL, INTENT(IN) :: i_am_inner_master

        INTEGER :: ist, ni, pnf1, pnf, pni1, pni, fdb, idb
        INTEGER :: diff_pnf, diff_pni, err, cmpt, cnt, nc, tag, req, nBlkRequired, nBlkReceived
        LOGICAL :: trpse, blocks_recv(no_of_LML_blocks, no_of_LML_blocks)
        REAL(wp):: tic, toc, timing

! multiple lml_blocks per task labels
        INTEGER   :: nf            ! label for LML block (rows, ni above used for columns)
        INTEGER   :: i_pe, all_pe  ! label for lb_master, PE   
        INTEGER   :: i_start       ! label for first LML block of current PE   
        INTEGER   :: i_previous    ! label for final LML block of previous PE   
        integer   :: i_array(3), i_temp, num

        CALL MPI_BARRIER(Lb_m_comm, err)

        IF (i_am_inner_master) THEN
            tic = hel_time()
            WRITE (*, *) "Setup_And_Distribute_Dblocks"
            WRITE (*, *) "============================"
            WRITE (*, *) "Total number of blocks to distribute: ", COUNT(dipole_coupled /= nocoupling)
        END IF

        pnf1 = 0; pnf = 0
        pni1 = 0; pni = 0

        nc = max_L_block_size

        IF (i_am_inner_master) THEN
            ALLOCATE (dblock_12(nc, nc), stat=err)
            CALL assert(err == 0, 'allocation error with dblock_12')

            IF (dipole_velocity_output) THEN
                ALLOCATE (vblock_12(nc, nc), stat=err)
                CALL assert(err == 0, 'allocation error with vblock_12')
            END IF
        END IF

        ! Loop through the whole matrix of couplings between the blocks and send and recieve the dipole blocks one-by-one.
        blocks_recv = .false.
        all_pe = 1 
        DO i_pe = 1, lb_m_size
           num = num_blocks_pe(all_pe) - 1
           i_start = block_start_pe(all_pe)
           i_previous = i_start - 1
           DO nf = i_start, i_start + num

              DO ni = 1, no_of_LML_blocks ! ... columns

                ! get dipole block indices usable in 'block_ind'
                CALL finind_L(LML_block_nspn(nf), LML_block_lrgl(nf), LML_block_npty(nf), fdb)
                CALL finind_L(LML_block_nspn(ni), LML_block_lrgl(ni), LML_block_npty(ni), idb)

                ! loop over all the three possible coupling modes
                DO cmpt = 1, 3

                    ! coupling mode identifier
                    SELECT CASE (cmpt)
                    CASE (1); cnt = upcoupling   ! at: Lf > Li,  mol: Dx
                    CASE (2); cnt = downcoupling ! at: Lf < Li,  mol: Dy
                    CASE (3); cnt = samecoupling ! at: Lf = Li,  mol: Dz
                    END SELECT

                    ! skip couplings that are not needed
                    IF (IAND(dipole_coupled(nf, ni), cnt) == 0) CYCLE

                    ! identification tag for the MPI message transferring the dipole block
                    tag = 3*(ni - 1) + cmpt

                    ! inner master will queue a synchronous send of the relevant dipole block
                    IF (i_am_inner_master) THEN

                        ! set-up block [fdb,idb], possibly transposing the corresponding unique block
                        trpse = block_ind(fdb, idb, cmpt) == 0
                        ist = MERGE(block_ind(idb, fdb, cmpt), block_ind(fdb, idb, cmpt), trpse)
                        IF (debug) WRITE (*, '(/,5X,"Dipole block [",i1,",",i1,"] with index ",i2)') fdb, idb, ist

                        ! calculate size of the dipole block matrix
                        pnf1 = L_block_post(fdb - 1) + 1; pnf = L_block_post(fdb); diff_pnf = pnf - pnf1 + 1
                        pni1 = L_block_post(idb - 1) + 1; pni = L_block_post(idb); diff_pni = pni - pni1 + 1

                        ! assemble the <nf|d|ni> matrix for the unique dipole blocks
                        IF (trpse) THEN
                            dblock_12(1:diff_pnf, 1:diff_pni) = TRANSPOSE(dipsto(1:diff_pni, 1:diff_pnf, ist))
                            IF (dipole_velocity_output) THEN
                            ! Factor i required due to complex conjugation (see below for inclusion in real arays), 
                                vblock_12(1:diff_pnf, 1:diff_pni) = TRANSPOSE(dipsto_v(1:diff_pni,1:diff_pnf, ist))
                            END IF
                            IF (debug) WRITE (*, '(5X,"...obtained transposing the corresponding unique dipole block")')
                        ELSE
                            dblock_12(1:diff_pnf, 1:diff_pni) = dipsto(1:diff_pnf, 1:diff_pni, ist)
                            IF (dipole_velocity_output) THEN
                            ! Factor -i required since p_{z} = -id/dz  (see below for inclusion in real arrays  
                                vblock_12(1:diff_pnf, 1:diff_pni) = -dipsto_v(1:diff_pnf, 1:diff_pni, ist)
                            END IF
                        END IF

                        ! inner master sends the block to the appropriate block master (possibly itself)
                        IF (debug) WRITE (*, '(5X,"...will be sent to block master ",i3)') nf - 1
                        CALL MPI_ISSEND(dblock_12, nc*nc, MPI_DOUBLE_PRECISION, i_pe - 1, tag, Lb_m_comm, &
                                        req, err)

                        IF (dipole_velocity_output) THEN
                           CALL MPI_ISSEND(vblock_12, nc*nc, MPI_DOUBLE_PRECISION, i_pe - 1, tag, Lb_m_comm, &
                                          req, err)
                        END IF

                    END IF

                    ! one of the block masters will now receive the dipole block sent into the correct buffer
                    IF (Lb_m_rank == i_pe - 1) THEN
                        blocks_recv(nf, ni) = .true.
                        SELECT CASE (cnt)
                        CASE (downcoupling)
                            CALL MPI_RECV(my_dblock_d(:,:,nf-i_previous), nc*nc, MPI_DOUBLE_PRECISION, 0, &
                                         tag, Lb_m_comm, MPI_STATUS_IGNORE, err)
                            IF (dipole_velocity_output) THEN
                               CALL MPI_RECV(my_vblock_d(:,:,nf-i_previous), nc*nc, MPI_DOUBLE_PRECISION, 0, &
                                             tag, Lb_m_comm, MPI_STATUS_IGNORE, err)
                            END IF
                        CASE (samecoupling)
                            CALL MPI_RECV(my_dblock_s(:,:,nf-i_previous), nc*nc, MPI_DOUBLE_PRECISION, 0, &
                                         tag, Lb_m_comm, MPI_STATUS_IGNORE, err)
                            IF (dipole_velocity_output) THEN
                               CALL MPI_RECV(my_vblock_s(:,:,nf-i_previous), nc*nc, MPI_DOUBLE_PRECISION, 0, &
                                             tag, Lb_m_comm, MPI_STATUS_IGNORE, err)
                            END IF
                        CASE (upcoupling)
                            CALL MPI_RECV(my_dblock_u(:,:,nf-i_previous), nc*nc, MPI_DOUBLE_PRECISION, 0, &
                                          tag, Lb_m_comm, MPI_STATUS_IGNORE, err)
                            IF (dipole_velocity_output) THEN
                               CALL MPI_RECV(my_vblock_u(:,:,nf-i_previous), nc*nc, MPI_DOUBLE_PRECISION, 0, &
                                             tag, Lb_m_comm, MPI_STATUS_IGNORE, err)
                            END IF
                        END SELECT
                    END IF

                    ! inner master waits for finish of its synchronous send
                    IF (i_am_inner_master) THEN
                        CALL MPI_WAIT(req, MPI_STATUS_IGNORE, err)
                    END IF

                END DO ! cmpt
              END DO  !ni  
            END DO ! nf
            all_pe = all_pe + pes_per_Lblock(i_pe)
         END DO ! i_pe

        i_temp = my_num_LML_blocks - 1 
        nBlkRequired = COUNT(dipole_coupled(my_LML_block_id:my_LML_block_id+i_temp, :) /= nocoupling)
        nBlkReceived = COUNT(blocks_recv(my_LML_block_id:my_LML_block_id+i_temp, :))
        IF (debug) THEN
            WRITE (*, '(/,5X,"Block master ",i3," needs ",i2," / got ",i2," dipole block(s).")')&
                        Lb_m_rank, nBlkRequired, nBlkReceived
        END IF
        CALL assert(nBlkReceived == nBlkRequired, 'Incorrect distribution of dipole blocks.')

        IF (i_am_inner_master) THEN
            DEALLOCATE (dblock_12, dipsto, stat=err)
            CALL assert(err == 0, 'deallocation error with dblock_12')

            IF (dipole_velocity_output) THEN
                DEALLOCATE (vblock_12, dipsto_v, stat=err)
                CALL assert(err == 0, 'deallocation error with vblock_12')
            END IF
        END IF

        CALL MPI_BARRIER(Lb_m_comm, err)

        IF (i_am_inner_master) THEN
            toc = hel_time()
            timing = toc - tic
            WRITE (*, *) "Finished Setup_And_Distribute_Dblocks: (secs)", timing
            WRITE (*, *) "======================================================"
            WRITE (*, *) ""
        END IF

    END SUBROUTINE setup_and_distribute_dblocks

!-----------------------------------------------------------------------------

    SUBROUTINE allocate_my_dips

        USE readhd, ONLY: fintr, &
                          ifdip, &
                          iidip

        IMPLICIT NONE

        INTEGER :: err

        ALLOCATE (iidip(fintr), ifdip(fintr), L_block_lrgl(no_of_L_blocks), &
                  L_block_nspn(no_of_L_blocks), L_block_npty(no_of_L_blocks), LML_block_ml(no_of_LML_blocks), &
                  LML_block_lrgl(no_of_LML_blocks), LML_block_nspn(no_of_LML_blocks), LML_block_npty(no_of_LML_blocks), &
                  cg_store(no_of_LML_blocks, no_of_LML_blocks), stat=err)

        CALL assert(err .EQ. 0, 'allocation error with My_dips')

    END SUBROUTINE allocate_my_dips

!-----------------------------------------------------------------------------

    SUBROUTINE allocate_my_couplings

        IMPLICIT NONE

        INTEGER :: err

        ALLOCATE (dipole_coupled(no_of_LML_blocks, no_of_LML_blocks), &
                  block_ind(no_of_L_blocks, no_of_L_blocks, 3), stat=err)

        CALL assert(err .EQ. 0, 'allocation error with My_couplings')

    END SUBROUTINE allocate_my_couplings

!-----------------------------------------------------------------------------

    SUBROUTINE allocate_my_dblocks

        USE mpi_layer_lblocks, ONLY: my_LML_block_id, my_num_LML_blocks
        USE readhd,            ONLY: downcoupling, samecoupling, upcoupling

        INTEGER :: err, i_temp

        i_temp = my_num_LML_blocks - 1 
        IF (ANY(IAND(dipole_coupled(my_LML_block_id:my_LML_block_id+i_temp,:), downcoupling) /= 0)) THEN
            ALLOCATE (my_dblock_d(max_L_block_size, max_L_block_size, my_num_LML_blocks), stat = err)
            CALL assert(err == 0, 'Allocation error with my_dblock_d')
            my_dblock_d = (0.0_wp, 0.0_wp)
            IF (dipole_velocity_output) THEN
                ALLOCATE (my_vblock_d(max_L_block_size, max_L_block_size, my_num_LML_blocks), stat = err)
                CALL assert(err == 0, 'Allocation error with my_vblock_d')
                my_vblock_d = (0.0_wp, 0.0_wp)
            END IF
        END IF

        IF (ANY(IAND(dipole_coupled(my_LML_block_id:my_LML_block_id+i_temp,:), samecoupling) /= 0)) THEN
            ALLOCATE (my_dblock_s(max_L_block_size, max_L_block_size, my_num_LML_blocks), stat = err)
            CALL assert(err == 0, 'Allocation error with my_dblock_s')
            my_dblock_s = (0.0_wp, 0.0_wp)
            IF (dipole_velocity_output) THEN
                ALLOCATE (my_vblock_s(max_L_block_size, max_L_block_size, my_num_LML_blocks), stat = err)
                CALL assert(err == 0, 'Allocation error with my_vblock_s')
                my_vblock_s = (0.0_wp, 0.0_wp)
            END IF
        END IF

        IF (ANY(IAND(dipole_coupled(my_LML_block_id:my_LML_block_id+i_temp,:), upcoupling) /= 0)) THEN
            ALLOCATE (my_dblock_u(max_L_block_size, max_L_block_size,my_num_LML_blocks), stat = err)
            CALL assert(err .EQ. 0, 'Allocation error with my_dblock_u')
            my_dblock_u = (0.0_wp, 0.0_wp)
            IF (dipole_velocity_output) THEN
                ALLOCATE (my_vblock_u(max_L_block_size, max_L_block_size,my_num_LML_blocks), stat = err)
                CALL assert(err == 0, 'Allocation error with my_vblock_u')
                my_vblock_u = (0.0_wp, 0.0_wp)
            END IF
        END IF

    END SUBROUTINE allocate_my_dblocks

!-----------------------------------------------------------------------------

    SUBROUTINE allocate_my_dblocks_not_block_master

        USE mpi_layer_lblocks, ONLY: my_LML_block_id, my_num_LML_blocks
        USE readhd,            ONLY: downcoupling, samecoupling, upcoupling

        INTEGER :: err, i_temp

        i_temp = my_num_LML_blocks - 1 ! i_temp should/will always be zero for not_block_master
        IF (ANY(IAND(dipole_coupled(my_LML_block_id:my_LML_block_id+i_temp,:), downcoupling) /= 0)) THEN
            ALLOCATE (my_dblock_d(0, 0, 0), stat = err)
            CALL assert(err == 0, 'Allocation error with my_dblock_d')
            my_dblock_d = (0.0_wp, 0.0_wp)
            IF (dipole_velocity_output) THEN
                ALLOCATE (my_vblock_d(0, 0,0), stat = err)
                CALL assert(err == 0, 'Allocation error with my_vblock_d')
                my_vblock_d = (0.0_wp, 0.0_wp)
            END IF
        END IF

        IF (ANY(IAND(dipole_coupled(my_LML_block_id:my_LML_block_id+i_temp,:), samecoupling) /= 0)) THEN
            ALLOCATE (my_dblock_s(0, 0,0), stat = err)
            CALL assert(err == 0, 'Allocation error with my_dblock_s')
            my_dblock_s = (0.0_wp, 0.0_wp)
            IF (dipole_velocity_output) THEN
                ALLOCATE (my_vblock_s(0, 0, 0), stat = err)
                CALL assert(err == 0, 'Allocation error with my_vblock_s')
                my_vblock_s = (0.0_wp, 0.0_wp)
            END IF
        END IF

        IF (ANY(IAND(dipole_coupled(my_LML_block_id:my_LML_block_id+i_temp,:), upcoupling) /= 0)) THEN
            ALLOCATE (my_dblock_u(0, 0,0), stat = err)
            CALL assert(err .EQ. 0, 'Allocation error with my_dblock_u')
            my_dblock_u = (0.0_wp, 0.0_wp)
            IF (dipole_velocity_output) THEN
                ALLOCATE (my_vblock_u(0, 0, 0), stat = err)
                CALL assert(err == 0, 'Allocation error with my_vblock_u')
                my_vblock_u = (0.0_wp, 0.0_wp)
            END IF
        END IF

    END SUBROUTINE allocate_my_dblocks_not_block_master

!-----------------------------------------------------------------------------

    SUBROUTINE allocate_my_diag_els

        USE mpi_layer_lblocks, ONLY: my_num_LML_blocks
        IMPLICIT NONE

        INTEGER :: err

        ALLOCATE (my_diag_els(max_L_block_size * my_num_LML_blocks), stat=err)
        CALL assert(err .EQ. 0, 'deallocation error with my_diag_els')

        my_diag_els = 0.0_wp

    END SUBROUTINE allocate_my_diag_els

!-----------------------------------------------------------------------------

    SUBROUTINE allocate_my_diag_els_not_block_master

        IMPLICIT NONE

        INTEGER :: err

        ALLOCATE (my_diag_els(0), stat=err)
        CALL assert(err .EQ. 0, 'deallocation error with my_diag_els')

        my_diag_els = 0.0_wp

    END SUBROUTINE allocate_my_diag_els_not_block_master

!-----------------------------------------------------------------------------

    SUBROUTINE allocate_my_surf_amps

        USE mpi_layer_lblocks, ONLY: my_num_LML_blocks
        IMPLICIT NONE

        INTEGER :: err

        ALLOCATE (my_surf_amps(max_L_block_size, LML_block_tot_nchan, my_num_LML_blocks), stat=err)
        CALL assert(err .EQ. 0, 'allocation error with my_surf_amps')

        my_surf_amps = 0.0_wp

    END SUBROUTINE allocate_my_surf_amps

!-----------------------------------------------------------------------------

    SUBROUTINE allocate_my_surf_amps_not_block_master

        IMPLICIT NONE

        INTEGER :: err

        ALLOCATE (my_surf_amps(0, 0, 0), stat=err)
        CALL assert(err .EQ. 0, 'allocation error with my_surf_amps')

        my_surf_amps = 0.0_wp

    END SUBROUTINE allocate_my_surf_amps_not_block_master

!-----------------------------------------------------------------------------

    SUBROUTINE dealloc_my_surf_amps

        IMPLICIT NONE

        INTEGER :: err

        IF (ALLOCATED(my_surf_amps)) THEN
            DEALLOCATE (my_surf_amps, stat = err)
            CALL assert(err == 0, 'Deallocation error with my_surf_amps')
        END IF

    END SUBROUTINE dealloc_my_surf_amps

!-----------------------------------------------------------------------------

    SUBROUTINE deallocate_my_diag_els

        IMPLICIT NONE

        INTEGER  :: err

        IF (ALLOCATED(my_diag_els)) THEN
            DEALLOCATE (my_diag_els, stat = err)
            CALL assert(err == 0, 'Deallocation error with my_diag_els')
        END IF

    END SUBROUTINE deallocate_my_diag_els

!-----------------------------------------------------------------------------

    SUBROUTINE deallocate_my_dblocks

        IMPLICIT NONE

        INTEGER :: err

        IF (ALLOCATED(my_dblock_s)) THEN
            DEALLOCATE (my_dblock_s, stat=err)
            CALL assert(err .EQ. 0, 'deallocation error with my_dblock_s')
        END IF

        IF (ALLOCATED(my_dblock_d)) THEN
            DEALLOCATE (my_dblock_d, stat=err)
            CALL assert(err .EQ. 0, 'deallocation error with my_dblock_d')
        END IF

        IF (ALLOCATED(my_dblock_u)) THEN
            DEALLOCATE (my_dblock_u, stat=err)
            CALL assert(err .EQ. 0, 'deallocation error with my_dblock_u')
        END IF

        IF (ALLOCATED(my_vblock_u)) THEN
            DEALLOCATE (my_vblock_u, stat = err)
            CALL assert(err == 0, 'Deallocation error with my_vblock_u')
        END IF

        IF (ALLOCATED(my_vblock_s)) THEN
            DEALLOCATE (my_vblock_s, stat = err)
            CALL assert(err == 0, 'Deallocation error with my_vblock_s')
        END IF

        IF (ALLOCATED(my_vblock_d)) THEN
            DEALLOCATE (my_vblock_d, stat = err)
            CALL assert(err == 0, 'Deallocation error with my_vblock_d')
        END IF

    END SUBROUTINE deallocate_my_dblocks

!-----------------------------------------------------------------------------

    SUBROUTINE allocate_hblock_11

        IMPLICIT NONE

        INTEGER  :: err

        ALLOCATE (hblock_11(max_L_block_size), stat=err)
        CALL assert(err .EQ. 0, 'allocation error with hblock_11')

    END SUBROUTINE allocate_hblock_11

!-----------------------------------------------------------------------------

    SUBROUTINE deallocate_hblock_11

        IMPLICIT NONE

        INTEGER  :: err

        DEALLOCATE (hblock_11, stat=err)
        CALL assert(err .EQ. 0, 'deallocation error with hblock_11')

    END SUBROUTINE deallocate_hblock_11

!-----------------------------------------------------------------------------

    SUBROUTINE allocate_surface_amplitudes

        IMPLICIT NONE

        INTEGER :: err

        ALLOCATE (surf_amps_11(max_L_block_size, LML_block_tot_nchan), stat=err)
        CALL assert(err .EQ. 0, 'allocation error with surf_amps')

    END SUBROUTINE allocate_surface_amplitudes

!-----------------------------------------------------------------------------

    SUBROUTINE deallocate_surface_amplitudes

        IMPLICIT NONE

        INTEGER :: err

        DEALLOCATE (surf_amps_11, stat=err)
        CALL assert(err .EQ. 0, 'deallocation error with surf_amps')

    END SUBROUTINE deallocate_surface_amplitudes

!-----------------------------------------------------------------------------

    SUBROUTINE deallocate_my_couplings

        IMPLICIT NONE

        INTEGER :: err

        DEALLOCATE (dipole_coupled, &
                    block_ind, cg_store, stat=err)

        CALL assert(err .EQ. 0, 'deallocation error with My_couplings')

    END SUBROUTINE deallocate_my_couplings

!-----------------------------------------------------------------------------

    !> \brief  Each block master stores its coupled blocks 
    !> \authors Andrew Brown
    !> \date    2018
    !>
    !> Each block master interrogates `dipole_coupled` and stores the indices of
    !> those blocks to which it is coupled (including itself). 
    SUBROUTINE setup_local_couplings

        USE initial_conditions, only: i_couple_to_dim
        USE readhd, ONLY: my_no_of_couplings, &
                          i_couple_to, &
                          downcoupling, &
                          samecoupling, &
                          upcoupling

        use mpi_layer_lblocks, only: my_LML_block_id, my_num_LML_blocks 
        IMPLICIT NONE
        
        INTEGER :: cnt, ii, err, my_block_st, i_temp, i_block

        allocate(my_no_of_couplings(my_num_LML_blocks), i_couple_to(i_couple_to_dim,my_num_LML_blocks), stat=err)
        CALL assert(err == 0, 'allocation problem, my_nO-of_couplings etc in setup_local_couplings')
! **************************  CHECK cnt > 10 *****************************
        i_couple_to = 0
        my_block_st = my_LML_block_id
        err = 0
        
        i_temp = my_num_LML_blocks - 1
        do i_block = 0, my_num_LML_blocks - 1
           cnt = 0
           DO ii = 1, no_of_LML_blocks
               IF (IAND(dipole_coupled(my_LML_block_id + i_block, ii), upcoupling) /= 0 .OR. &
                   IAND(dipole_coupled(my_LML_block_id + i_block, ii), samecoupling) /= 0 .OR. &
                   IAND(dipole_coupled(my_LML_block_id + i_block, ii), downcoupling) /= 0 .OR. &
                   (ii == my_block_st+i_block) ) THEN
                  cnt = cnt + 1
                  IF (cnt > i_couple_to_dim) &
                     call assert (.false., 'i_couple_to_dim is too small. Please reset and begin again') 
                  i_couple_to(cnt, i_block+1) = ii
               END IF
            END DO
            my_no_of_couplings(i_block+1) = cnt
        END DO


    END SUBROUTINE setup_local_couplings

END MODULE distribute_hd_blocks

!> @page compinner RMT Code: Computational considerations
!! @brief Parallelisation Strategy
!! @tableofcontents
!!
!!@section precision A note on precision
!!
!!The code is designed to work with a flexible floating point precision.
!!You can set the number of decimal places in the module precisn.f90 by
!!adjusting the parameter decimal\_precision\_long. The code has been
!!tested for decimal\_precision\_long=15.
!!
!!@section para_strategy Parallelisation Strategy
!!
!!The code employs the standard R-matrix paradigm of dividing
!!configuration space into two regions. Within each region a different
!!parallelisation strategy is employed.
!!
!!* @subpage inner_para \n
!!* @subpage outer_para \n
!!
!!
!!In any given calculation, the
!!bottleneck will exist in one of these two regions. For calculations
!!comprising a high degree of atomic structure, the inner region tends to
!!dominate. For those comprising many channel functions (large angular
!!momentum expansion) the outer region can dominate. The skill in
!!optimising the calculation is to balance the workload in each region by
!!a judicious allocation of cores.
!!
!!Communication between the two regions is handled by the ‘region-master’
!!cores (essentially the first core in each region). Rather than having a
!!separate communicator for this, every core has a logical flag set at the
!!start of the calculation: `I_Am_Inner_Master` is set on all inner region
!!cores and is true only for the inner-region master. Similarly,
!!`I_Am_Outer_Master` is set for the outer region cores. Calls to
!!subroutines named `First_PEs_share_<something>` or
!!`First_PE_receives_<something>` and so on then are called from all cores
!!in a given region, and the logical flags are used to determine which
!!cores are involved in the communication using `MPI_Comm_World`.
!!
!! @image html region_masters.png
!! @image latex region_masters.png


!> @page inner_para Inner Region Parallelisation
!!
!! @brief Inner Region Parallelisation Strategy
!!
!!### Inner region parallelisation
!!
!!The calculation in the inner region involves repeated application of
!!matrix vector multiplications. This calculation is parallelised in three
!!layers using both distributed (MPI) and shared (OpenMP) paradigms.
!!
!!* @subpage inner_layer_1 \n
!!* @subpage inner_layer_2 \n
!!* @subpage inner_layer_3



!> @page inner_layer_1 Inner Region: Layer One
!! 
!! @brief First layer of parallelisation in the Inner Region
!!
!!
!!#### Layer 1
!!
!!Both the Hamiltonian matrix, and the wavefunction vector are divided
!!into symmetry blocks containing states of a given angular momentum, as
!!shown in the diagram below. The first layer of parallelism is to assign
!!each symmetry block to an MPI task. In the simplest arrangement, one MPI
!!task is assigned to each block, so (refering to the diagram below)
!!\f$H_{00}\f$, \f$D_{10}\f$ and \f$\psi_{0}\f$ are local to MPI task 0, \f$H_{11}\f$,
!!\f$D_{01}\f$, \f$D_{12}\f$ and \f$\psi_{1}\f$ on local to MPI task 1 etc. Here,
!!\f$H_{ii}\f$ refers to the energies of the states which have total angular
!!momentum \f$L=i\f$, while \f$D_{ij}\f$ refers to the dipole matrix elements
!!coupling a state with \f$L=i\f$ to a state with \f$L=j\f$.
!!
!! @image html block_structure.png
!! @image latex block_structure.png
!!
!!There will be multiple states within each symmetry block- these may be handled
!!in parallel in layer 2. In layer 1, the code will automatically divide the
!!total number of PEs available, `No_of_PEs_to_use_inner`, amongst the blocks.
!!This may mean that a single block is handled by multiple MPI tasks, or a single
!!MPI task may handle multiple symmetry blocks.
!!
!!The communication for this layer is handled between nearest neighbours.
!!Each MPI task needs only the wavefunction data from the tasks above and
!!below. At the beginning of each iteration, the data is transferred
!!between the tasks using the communicator `Lb_m_comm` (Lb for L block, m
!!for master, see below). This occurs in the subroutine
!!`parallel_matrix_vector_multiply_zm()` in the module `live_communications`.
!!
!!We note that there are other possible couplings when the electric field is
!!arbitrarily polarised (for example)
!!but the parallel scheme remains the same, albeit with
!!additional dipole blocks for each symmetry.



