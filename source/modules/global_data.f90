! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Standard RMatrix module handling frequently used constants.
MODULE global_data

    USE precisn

    IMPLICIT NONE

    PUBLIC

    COMPLEX(wp), PARAMETER :: zero = (0.0_wp, 0.0_wp)
    COMPLEX(wp), PARAMETER :: im = (0.0_wp, 1.0_wp)
    COMPLEX(wp), PARAMETER :: one = (1.0_wp, 0.0_wp)
    REAL(wp), PARAMETER    :: rzero = 0.0_wp
    REAL(wp), PARAMETER    :: rone = 1.0_wp
    REAL(wp), PARAMETER    :: rtwo = 2.0_wp

    REAL(ep), PARAMETER    :: pi = 3.14159265358979323846264338327950288_ep
    REAL(ep), PARAMETER    :: ee = 2.71828182845904523536028747135266249_ep

    !> This (molecular only) parameter controls which coordinate axes, and hence corresponding
    !> inner-region down/same/up dipole couplings, will be included in the calculation.
    !> If the projections of directions of the semi-axes of the polarization ellipse onto a given
    !> coordinate axis are not greater than this tolerance, the corresponding dipole coupling block
    !> won't be considered at all. (See \ref readhd::read_molecular_data for implementation.)
    REAL(wp), PARAMETER :: axis_drop_tol = 1e-10_wp

END MODULE global_data
