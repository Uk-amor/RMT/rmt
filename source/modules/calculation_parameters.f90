! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Contains parameters primarily for varying aspects of the
!> calculations, i.e. parameters which are not dependent on the system, the model
!> of the system or the laser field.
MODULE calculation_parameters

    USE precisn,            ONLY: wp
    USE initial_conditions, ONLY: propagation_order, &
                                  steps_per_run_approx, &
                                  final_t, &
                                  timesteps_per_output, &
                                  checkpts_per_run, &
                                  wd_ham_interactions, &
                                  wp_ham_interactions, &
                                  we_ham_interactions, &
                                  use_two_inner_grid_pts, &
                                  delta_t
    IMPLICIT NONE

    !> Timing information
    LOGICAL, PARAMETER :: krylov_hdt_desired = .true.

    ! Use the flags below to turn on or off the WD and WP Hamiltonian
    ! interactions in the outer region - use for testing
    ! WD = laser coupling with residual ion
    ! WP = laser coupling with outer electron
    ! WE = Coulomb interaction
    ! NOTE: WE also contains the differences in the target state
    !       energies. Do NOT set parameter to false unless
    !       you are using a single target state

    LOGICAL :: use_wd_ham_interactions
    LOGICAL :: use_wp_ham_interactions
    LOGICAL :: use_we_ham_interactions

    !> Parameter to indicate the order of the finite difference method
    !> no. of points used = 2*half_fd_order + 1
    INTEGER, PARAMETER :: half_fd_order = 2

    !> Maximum order of B-splines
    INTEGER, PARAMETER :: jmax = 20

    ! Parameters for cut-off at small radii
    REAL(wp), PARAMETER :: R_cutoff = 2.5_wp
    INTEGER, PARAMETER  :: Lmax_at_bndry = 10

!-----------------------------------------------------------------------------
! CHOICE OF COMMUNICATIONS IN OUTER REGION
!
! Select method for broadcasting data from PE 0, and for summing data on PE 0:
! Used in communications involving the outer region
! Do not change the following 3 parameters:

    INTEGER, PARAMETER :: use_sends_recvs = 1
    ! Individual MPI_Send and MPI_Recv commands
    ! May not work in limit of large processor counts
    ! In particular core counts > 1000 to 4000
    INTEGER, PARAMETER :: use_sends_recvs_synch = 2
    ! Use individual MPI_Send and MPI_Recv but
    ! call a barrier synchronization every x calls
    INTEGER, PARAMETER :: use_mpi_collective = 3
    ! Use the collective MPI broadcast and reduce commands

    ! This can be changed:
    INTEGER, PARAMETER :: no_comms_per_synch = 1000
    ! The number of MPI_Sends or MPI_Recvs between synchronization
    ! -- only used if we use method 2: use_sends_recvs_synch

    ! Select one of the choices:
!    INTEGER, PARAMETER :: mpi_comms_method_desired = use_sends_recvs
!   INTEGER, PARAMETER :: mpi_comms_method_desired = use_sends_recvs_synch
   INTEGER, PARAMETER :: mpi_comms_method_desired = use_mpi_collective

!-----------------------------------------------------------------------------
! PARAMETERS FOR THE INNER REGION PROPAGATION

    ! Choose either Arnoldi or Lanczos for inner region propagation:
    ! MUST CHOOSE ONLY ONE OF THE FOLLOWING:
!   CHARACTER(LEN=3), PARAMETER :: krytech = 'arn'
    CHARACTER(LEN=3), PARAMETER :: krytech = 'lan'

    ! Choose either diag or pade method for reduced h
    ! NB Not currently used
    CHARACTER(LEN=4), PARAMETER :: exptech = 'diag'

    ! Split into communicators to handle different propagation orders  in the inner region
    ! MUST CHOOSE ONLY ONE OF THE FOLLOWING:
    ! DO NOT CHANGE
    INTEGER, PARAMETER :: spltype = 0  ! All propagation orders handled on the one communicator
!   INTEGER, PARAMETER :: spltype = 1  ! Split into even and odd propagation order communicators

!-----------------------------------------------------------------------------
! PARAMETERS TO APPROXIMATE SINGLE IONIZATION

    REAL(wp), PARAMETER :: single_ioniz_bndry_in_au = 50.0_wp

    INTEGER, SAVE :: single_ioniz_bndry_in_grid_pts

!-----------------------------------------------------------------------------
! GENERAL PROPAGATION PARAMETERS

    INTEGER, PARAMETER :: pure_taylor_series = 1  !! don't touch
    INTEGER, PARAMETER :: arnoldi_series = 2  !! don't touch

    !! Choose only 1:
!   INTEGER, PARAMETER :: choice_of_wave_propagator = pure_taylor_series  ! NOT CODED HERE YET!
    INTEGER, PARAMETER :: choice_of_wave_propagator = arnoldi_series

    !! If you don't use pure Taylor_Series (see above) then this gives
    !! you the option of propagating Psi by diagonalizing the
    !! the Ham in the Arnoldi subspace (ie by diagonalising h, which is
    !! is a very small matrix: (propagation_order+1) x (propagation_order+1)).

    !! Gram-Schmidt: Iterations improve orthogonality of Arnoldi process-
    !! Diffusion + wave equation:
    INTEGER, PARAMETER :: gs_refinement_iterations = 1
    !! Gram-Schmidt Iterations improve orthogonality of Arnoldi
    !! propagation of wavefunction.
    !! Never seen a case where 2 helps at all, so leave at one.

    INTEGER, PARAMETER :: no_of_kry_sum_segments = 8
    !  Sum Taylor series in kry subspace to Kry_order/no_of_kry_sum_segments:

    INTEGER :: kry_order != 3*(propagation_order+1)*no_of_kry_sum_segments

    LOGICAL, PARAMETER :: eigen_propagator_desired = .false.   !! .false. is best
    !  If this is true then we diagonalise the Hamiltonian in the outer region
    !  in order to propagate wave equation.
    REAL(wp), PARAMETER :: propagator_switch_limit = 0.1_wp 
    ! This is the largest size of the elements in the outer region hamiltonian allowed before
    ! we force the propagation to use the Hamiltonian Diagonalisation

!-----------------------------------------------------------------------------
! PARAMETERS ASSOCIATED WITH THE TIME STEPS OUTPUT/CHECKPOINT

!   INTEGER, PARAMETER :: timesteps_per_output = 20
!   INTEGER            :: timesteps_per_output != 20
    INTEGER            :: steps_per_run_is_divisable_by !=      &
!                         timesteps_per_output * checkpts_per_run

    INTEGER :: steps_per_run  != (steps_per_run_approx     &
!      / steps_per_run_is_divisable_by) * steps_per_run_is_divisable_by

    INTEGER :: steps_per_checkpt != steps_per_run / checkpts_per_run

!-----------------------------------------------------------------------------
! Writing Ground state to disk / reading it in from disk

    !  Parameters for reading ground state from disk
    !  To make ground state and save to disk have:
    !          Read... = FALSE and This... = TRUE
    !
    !  To read ground state from disk at start of run, have:
    !          Read... = TRUE and This... = FALSE
    !
    !  Both flags must be correctly set.

    LOGICAL, PARAMETER :: this_version_only_makes_ground = .false. ! Only works for initial L=0?

!-----------------------------------------------------------------------------
! PARAMETERS TO ASSIST WITH OUTPUT DATA

    !CHARACTER(LEN=2), PARAMETER   :: disk_path = './'
    !CHARACTER(LEN=:), ALLOCATABLE   :: disk_path

    INTEGER, PARAMETER :: scaling_factor_for_i_image = 1000
    LOGICAL, PARAMETER :: make_status_file_if_missing = .true.
    ! For outputting wavefunction info
    LOGICAL, PARAMETER :: wavefunction_output_desired = .false.

!-----------------------------------------------------------------------------
! PROPAGATION CHOICE AT BOUNDARY

    ! Choose which method to use to calculate the time derivatives of the
    ! wavefunction at the fictitious grid points just inside the outer
    ! boundary of the inner region.  The wavefunction and its time derivatives
    ! are needed so as to allow the outer region to apply finite difference
    ! formulae to calculate derivatives.
    !
    ! Method 1: H_Outer is used: the inner region passes psi at an increased
    !           number of grid points, and the outer region finite difference
    !           Hamiltonian is repeatedly applied to obtain the time derivatives.
    !           If this option is chosen, the inner region passes data to the
    !           outer region at the start of every timestep, and the outer
    !           region passes data to the inner region at every propagation
    !           order during the timestep.
    INTEGER, PARAMETER :: using_H_outer = 1

    ! Method 2: H_Inner is used: the inner region calculates all time derivatives
    !           of psi at the points required.  If this option is chosen, the
    !           inner and outer region pass data to each other at every propagation
    !           order in each timestep.  Spltype must be set to 0 below (see
    !           module Inner_Propagation_Parameters below).
    INTEGER, PARAMETER :: using_H_inner = 2

    ! CHOOSE ONE OF THE FOLLOWING:
!   INTEGER, PARAMETER :: calc_psi_derivs_at_bndry_by = using_H_inner
    INTEGER, PARAMETER :: calc_psi_derivs_at_bndry_by = using_H_outer

    INTEGER, SAVE :: nfdm

CONTAINS

    SUBROUTINE init_nfdm

        SELECT CASE ( use_two_inner_grid_pts )
        CASE (.false.)
            ! 2propagation_order+2 points needed if using a 5 point FD rule (half_fd_order=2)
            ! 3propagation_order+3 points needed if using a 7 point FD rule (half_fd_order=3)
            nfdm = half_fd_order*propagation_order + half_fd_order
        CASE (.true.)
            ! 2 points only
            nfdm = half_fd_order
        END SELECT

    END SUBROUTINE init_nfdm

!---------------------------------------------------------------------------

    SUBROUTINE derive_calculation_parameters

        IMPLICIT NONE

!       INTEGER            :: checkpts_per_run != 1
!       INTEGER            :: timesteps_per_output != 20
        LOGICAL            :: user_set_delta_t 

        user_set_delta_t = (steps_per_run_approx == -1)

        kry_order = 3*(propagation_order + 1)*no_of_kry_sum_segments

        ! PARAMETERS ASSOCIATED WITH THE TIME STEPS OUTPUT/CHECKPOINT
!       checkpts_per_run = read_checkpts
!       timesteps_per_output = read_timesteps

        steps_per_run_is_divisable_by = &
            timesteps_per_output*checkpts_per_run

        IF (user_set_delta_t) THEN
            steps_per_run_approx = final_t/delta_t
        END IF
        steps_per_run = (steps_per_run_approx &
                      /steps_per_run_is_divisable_by)*steps_per_run_is_divisable_by
        steps_per_checkpt = steps_per_run/checkpts_per_run
        IF (.not.user_set_delta_t) THEN
           delta_t = final_t/steps_per_run
        END IF

        IF (ABS(delta_t) .GT. 0.5) THEN
            PRINT *, 'Error in Calculation Parameters: Check steps_per_run and final_t: delta_t is too large'
            STOP
        END IF

        use_wd_ham_interactions = wd_ham_interactions
        use_wp_ham_interactions = wp_ham_interactions
        use_we_ham_interactions = we_ham_interactions

    END SUBROUTINE derive_calculation_parameters

END MODULE calculation_parameters
