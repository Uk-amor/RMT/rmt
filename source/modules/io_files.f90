! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Handles opening/closing of output files, and writing of files during the
!> execution of the calculation. (Data files written at the end of the
!> calculation are handled in io_routines.)
MODULE io_files

    USE precisn,            ONLY: wp, decimal_precision_wp
    USE initial_conditions, ONLY: dipole_velocity_output,   &
                                  dipole_output_desired,    &
                                  double_ionization,        &
                                  no_of_pes_to_use_outer,   &
                                  propagation_order,        &
                                  disk_path,                &
                                  dipole_dimensions_desired,&
                                  length_gauge_id,          &
                                  velocity_gauge_id

    IMPLICIT NONE

CONTAINS

    SUBROUTINE open_output_files(stage, my_block_id, my_num_LML_blocks, i_am_inner_master, &
                                 i_am_outer_master, i_am_double_outer_master, i_am_outer_rank1)

        USE initial_conditions,     ONLY: timings_desired, &
                                          numsols => no_of_field_confs
        USE io_parameters,          ONLY: version
        USE rmt_assert,             ONLY: assert
        USE stages,                 ONLY: stage_first
        USE wall_clock,             ONLY: open_inner_timings_file, &
                                          open_outer0_timings_file, &
                                          open_outer1_timings_file

        INTEGER, INTENT(IN) :: stage
        INTEGER, INTENT(IN) :: my_block_id, my_num_LML_blocks
        ! lowest LML associated with pe, number of LMLS associated with pe
        LOGICAL, INTENT(IN) :: i_am_inner_master
        LOGICAL, INTENT(IN) :: i_am_outer_master
        LOGICAL, INTENT(IN) :: i_am_double_outer_master
        LOGICAL, INTENT(IN) :: i_am_outer_rank1
        INTEGER             :: i,j
        INTEGER             :: gap_size_EField = 2*decimal_precision_wp - 13
        INTEGER             :: gap_size_expec = 2*decimal_precision_wp - 13
        INTEGER             :: gap_size_pop = 2*decimal_precision_wp - 11
        LOGICAL             :: is_first_loop = .TRUE.

        CALL assert(((my_block_id .GE. 0) .AND. ((my_block_id - 1 + my_num_LML_blocks) .LE. 99999)), &
                     'ERROR: My_Block_ID - My_Block_id -1 + my_num_LML_blocks not in Range 0-99999')
        IF (i_am_inner_master) THEN
            IF (stage == stage_first) THEN
                OPEN (UNIT=37, FILE=disk_path//'pop_inn.'//version)
                OPEN (UNIT=38, FILE=disk_path//'pop_all.'//version)
!---Headers for pop_inn*/pop_all*-
                      write(37,'(7A)', advance='NO') '   Time'
                      write(38,'(7A)', advance='NO') '   Time'
                      do i=1,numsols
                              do j=1,gap_size_pop
                                    write(37,'(A)', advance='NO') ' '
                                    write(38,'(A)', advance='NO') ' '
                              end do
                              write(37,'(I4.4)', advance='NO') i
                              write(38,'(I4.4)', advance='NO') i
                      end do
                      write(37,'(A)', advance='YES') ' '
                      write(38,'(A)', advance='YES') ' '
!---------------      ------------
                OPEN (UNIT=46, FILE=disk_path//'EField.'//version)

!---Headers for EField*------
                write(46,'(6A)', advance='NO') '   Time'
                do i=1,numsols
                        IF (is_first_loop) THEN
                              do j=1,gap_size_EField+2 
                                    write(46,'(A)', advance='NO') ' '
                              end do
                              is_first_loop = .FALSE.
                        ELSE
                              do j=1,gap_size_EField
                                    write(46,'(A)', advance='NO') ' '
                              end do
                        END IF

                        write(46,'(I4.4,2A)', advance='NO') i,'_x'
                        do j=1,gap_size_EField
                              write(46,'(A)', advance='NO') ' '
                        end do
                        write(46,'(I4.4,2A)', advance='NO') i,'_y'
                        do j=1,gap_size_EField
                              write(46,'(A)', advance='NO') ' '
                        end do
                        write(46,'(I4.4,2A)', advance='NO') i,'_z'
                end do
                write(46,'(A)', advance='YES') ' '
!----------------------------
                IF (dipole_output_desired) THEN

                    OPEN (UNIT=58, FILE=disk_path//'expec_z_all.'//version)
                    IF (dipole_velocity_output) THEN
                        OPEN (UNIT=63, FILE=disk_path//'expec_v_all.'//version)
                    END IF
!---Headers for expec_z*----
                      write(58,'(8A)', advance='NO') '   Time  '
                      do i=1,numsols
                              IF ( dipole_dimensions_desired(1) ) THEN
                                    do j=1,gap_size_expec
                                          write(58,'(A)', advance='NO') ' '
                                    end do
                              write(58,'(I4.4,2A)', advance='NO') i,'_x'
                              END IF
                              IF ( dipole_dimensions_desired(2) ) THEN
                                    do j=1,gap_size_expec
                                          write(58,'(A)', advance='NO') ' '
                                    end do
                                    write(58,'(I4.4,2A)', advance='NO') i,'_y'
                              END IF
                              IF ( dipole_dimensions_desired(3) ) THEN
                                    do j=1,gap_size_expec
                                          write(58,'(A)', advance='NO') ' '
                                    end do
                                    write(58,'(I4.4,2A)', advance='NO') i,'_z'
                              END IF
                      end do
                      write(58,'(A)', advance='YES') ' '
!---------------      ------------
!---Headers for       expec_v*----
                      IF (dipole_velocity_output) THEN
                            write(63,'(8A)', advance='NO') '   Time  '
                            do i=1,numsols
                                    IF ( dipole_dimensions_desired(1) ) THEN
                                          do j=1,gap_size_expec
                                                write(63,'(A)', advance='NO') ' '
                                          end do
                                    write(63,'(I4.4,2A)', advance='NO') i,'_x'
                                    END IF
                                    IF ( dipole_dimensions_desired(2) ) THEN
                                          do j=1,gap_size_expec
                                                write(63,'(A)', advance='NO') ' '
                                          end do
                                          write(63,'(I4.4,2A)', advance='NO') i,'_y'
                                    END IF
                                    IF ( dipole_dimensions_desired(3) ) THEN
                                          do j=1,gap_size_expec
                                                write(63,'(A)', advance='NO') ' '
                                          end do
                                          write(63,'(I4.4,2A)', advance='NO') i,'_z'
                                    END IF
                            end do
                            write(63,'(A)', advance='YES') ' '
                      END IF
!---------------------------
                END IF
            ELSE
                OPEN (UNIT=37, FILE=disk_path//'pop_inn.'//version, STATUS='old', POSITION='append')
                OPEN (UNIT=38, FILE=disk_path//'pop_all.'//version, STATUS='old', POSITION='append')
                OPEN (UNIT=46, FILE=disk_path//'EField.'//version, STATUS='old', POSITION='append')
                IF (dipole_output_desired) THEN

                    OPEN (UNIT=58, FILE=disk_path//'expec_z_all.'//version, STATUS='old', POSITION='append')
                    IF (dipole_velocity_output) THEN
                        OPEN (UNIT=63, FILE=disk_path//'expec_v_all.'//version, STATUS='old', POSITION='append')
                    END IF

                END IF
            END IF
        END IF

        IF (i_am_outer_master) THEN
            IF (stage == stage_first) THEN
                  OPEN (UNIT=36, FILE=disk_path//'pop_out.'//version)
!---Headers for pop_inn*/pop_all*-
                  write(36,'(7A)', advance='NO') '   Time'
                  do i=1,numsols
                        do j=1,gap_size_pop
                              write(36,'(A)', advance='NO') ' '
                        end do
                       write(36,'(I4.4)', advance='NO') i
                  end do
            write(36,'(A)', advance='YES') ' '
            ELSE
                OPEN (UNIT=36, FILE=disk_path//'pop_out.'//version, STATUS='old', POSITION='append')
            END IF
        END IF

        IF (i_am_double_outer_master) THEN
            IF (stage == stage_first) THEN
                  OPEN (UNIT=39, FILE=disk_path//'pop_DI.'//version)
!---Headers for pop_inn*/pop_all*-
                  write(39,'(7A)', advance='NO') '   Time'
                  do i=1,numsols
                        do j=1,gap_size_pop
                              write(39,'(A)', advance='NO') ' '
                        end do
                       write(39,'(I4.4)', advance='NO') i
                  end do
            write(39,'(A)', advance='YES') ' '
            ELSE
                OPEN (UNIT=39, FILE=disk_path//'pop_DI.'//version, STATUS='old', POSITION='append')
            END IF
        END IF

        ! Open timing files
        IF (timings_desired) THEN
            IF (i_am_inner_master) THEN
                CALL open_inner_timings_file
            END IF
            IF (i_am_outer_master) THEN
                CALL open_outer0_timings_file
            END IF
            IF (i_am_outer_rank1) THEN
                CALL open_outer1_timings_file
            END IF
        END IF

    END SUBROUTINE open_output_files

!---------------------------------------------------------------------------

    SUBROUTINE write_output_files(iteration_start_time, iteration_finish_time, current_E_field_strength, expec_all)

        USE initial_conditions, ONLY: numsols => no_of_field_confs

        REAL(wp),    INTENT(IN)           :: iteration_start_time, iteration_finish_time
        REAL(wp),    INTENT(IN)           :: current_E_field_strength(3, numsols)
        COMPLEX(wp), INTENT(IN), OPTIONAL :: expec_all(numsols, 3, 2)
        CHARACTER(LEN=100)                :: writefmt
        CHARACTER(LEN=20)                 :: ddp, eep, num
        LOGICAL                           :: mask(3, numsols)

        WRITE (num, '(I0)') 1 + 3 * numsols
        WRITE (ddp, '(I0)') decimal_precision_wp + 8
        WRITE (eep, '(I0)') decimal_precision_wp - 1

        writefmt = "(" // TRIM(num) // "E"// TRIM(ddp) //"."// TRIM(eep) // "E3)"
        WRITE (46, writefmt) (iteration_start_time + iteration_finish_time) / 2, current_E_field_strength

        IF (dipole_output_desired.AND.PRESENT(expec_all)) THEN

            mask = SPREAD(dipole_dimensions_desired, 2, numsols)

            WRITE (num, '(I0)') 1 + COUNT(dipole_dimensions_desired) * numsols
            writefmt = "(" // TRIM(num) // "E"// TRIM(ddp) //"."// TRIM(eep) // "E3)"

            WRITE (58, writefmt) iteration_start_time, PACK(TRANSPOSE(REAL(expec_all(:, :, length_gauge_id), wp)), mask)

            IF (dipole_velocity_output) THEN
                WRITE (63, writefmt) iteration_start_time, PACK(TRANSPOSE(REAL(expec_all(:, :, velocity_gauge_id), wp)), mask)
            END IF

        END IF

    END SUBROUTINE write_output_files

!---------------------------------------------------------------------------

    SUBROUTINE close_output_files(i_am_double_outer_master, i_am_outer_master, i_am_inner_master, i_am_outer_rank1)

        USE initial_conditions,     ONLY: timings_desired
        USE wall_clock,             ONLY: close_inner_timings_file, &
                                          close_outer0_timings_file, &
                                          close_outer1_timings_file

        LOGICAL, INTENT(IN) :: i_am_double_outer_master
        LOGICAL, INTENT(IN) :: i_am_outer_master
        LOGICAL, INTENT(IN) :: i_am_inner_master
        LOGICAL, INTENT(IN) :: i_am_outer_rank1

        IF (i_am_double_outer_master) THEN
            CLOSE(39)
        END IF

        IF (i_am_outer_master) THEN
            CLOSE (36)
        END IF

        IF (i_am_inner_master) THEN
            CLOSE (37)
            CLOSE (38)
            CLOSE (46)

            
            IF (dipole_output_desired) THEN
                CLOSE (58)
                IF (dipole_velocity_output) CLOSE (63)
            END IF
        END IF

        ! Close timing files
        IF (timings_desired) THEN
            IF (i_am_inner_master) THEN
                CALL close_inner_timings_file
            END IF
            IF (i_am_outer_master) THEN
                CALL close_outer0_timings_file
            END IF
            IF (i_am_outer_rank1) THEN
                CALL close_outer1_timings_file
            END IF
        END IF

    END SUBROUTINE close_output_files

!---------------------------------------------------------------------------

    SUBROUTINE write_pop_files(iteration_start_time, &
                               iteration_finish_time, &
                               pop_all, &
                               pop_all_inner, &
                               pop_gs, &
                               pop_all_outer, &
                               pop_all_outer_si, &
                               pop_all_outer_ryd, &
                               pop_all_double_outer, &
                               current_E_field_strength, &
                               propagation_order, &
                               timeindex, &
                               i_am_double_outer_master, &
                               i_am_outer_master, &
                               i_am_inner_master)

        USE initial_conditions, ONLY: field_period, &
                                      numsols => no_of_field_confs
        USE wall_clock,         ONLY: program_start_time, &
                                      output_time, &
                                      hel_time

        LOGICAL,  INTENT(IN)    :: i_am_double_outer_master
        LOGICAL,  INTENT(IN)    :: i_am_outer_master
        LOGICAL,  INTENT(IN)    :: i_am_inner_master
        REAL(wp), INTENT(IN)    :: iteration_start_time
        REAL(wp), INTENT(IN)    :: iteration_finish_time
        REAL(wp), INTENT(IN)    :: pop_all(numsols)
        REAL(wp), INTENT(IN)    :: pop_all_inner(numsols)
        REAL(wp), INTENT(IN)    :: pop_gs(numsols)
        REAL(wp), INTENT(IN)    :: pop_all_outer(numsols)
        REAL(wp), INTENT(IN)    :: pop_all_outer_si(numsols)
        REAL(wp), INTENT(IN)    :: pop_all_outer_ryd(numsols)
        REAL(wp), INTENT(IN)    :: pop_all_double_outer(numsols)
        REAL(wp), INTENT(IN)    :: current_E_field_strength(3, numsols)
        INTEGER, INTENT(IN)     :: timeindex
        INTEGER, INTENT(IN)     :: propagation_order
        REAL(wp)                :: iteration_time
        CHARACTER(LEN=13) :: writefmt
        CHARACTER(LEN=2)  :: ddp, eep
        CHARACTER(LEN=20) :: f, g
        INTEGER           :: i

        WRITE (ddp,'(I2)') decimal_precision_wp+8
        WRITE (eep,'(I2)') decimal_precision_wp-1

        writefmt = '(*(E' // ddp // '.' // eep // 'E3))'
        f = '(x,A,3(E25.15E3))'  ! format needed for ifort not to break line
        g = '(30x,3(E25.15E3))'

        IF (i_am_outer_master) THEN
            WRITE (36, writefmt) iteration_finish_time, pop_all_outer
        END IF

        IF (i_am_double_outer_master) THEN
            WRITE (39, writefmt) iteration_finish_time, pop_all_double_outer
        END IF

        ! 1st PE in inner region writes pop_all_inner and pop_all as we go
        IF (i_am_inner_master) THEN
            WRITE (37, writefmt) iteration_finish_time, pop_all_inner
            WRITE (38, writefmt) iteration_finish_time, pop_all
            IF (.NOT. dipole_output_desired) THEN
                WRITE (46, writefmt) (iteration_start_time + iteration_finish_time)/2, &
                                     (Current_E_Field_Strength(1:3, i), i = 1, numsols)
            END IF

            PRINT *, '===================================================='
            PRINT *, ''
            PRINT *, 'Time Index                 = ', timeindex
            PRINT *, 'Time (au)                  = ', iteration_finish_time
            PRINT *, 'Time (units of fp)         = ', iteration_finish_time/field_period(1)

            PRINT *, 'Propagation Order          = ', propagation_order
            PRINT f, 'EField                     = ', current_E_field_strength(1, 1), &
                                                      current_E_field_strength(2, 1), &
                                                      current_E_field_strength(3, 1)
            DO i = 2, numsols
                PRINT g, current_E_field_strength(1:3, i)
            END DO

            PRINT *, 'WallClock Time (sec)       = ', hel_time() - program_start_time
            CALL output_time(iteration_time)
            PRINT *, 'Timing                     = ', iteration_time

            PRINT *, 'Total Population Inner     = ', pop_all_inner
            PRINT *, 'Total Population Outer     = ', pop_all_outer
            PRINT *, 'Total Population           = ', pop_all
            PRINT *, 'Total Population Outer_SI  = ', pop_all_outer_si
            PRINT *, 'Total Population Outer_Ryd = ', pop_all_outer_ryd
            PRINT *, 'Population_GS_Inner        = ', pop_gs
            IF (double_ionization) THEN
                PRINT *, 'Population Double Outer    = ', pop_all_double_outer
            END IF
            PRINT *, ' '

            OPEN (UNIT=55, FILE=disk_path//'CurrentPosition')
            WRITE (55, *) 'Time Index                 = ', timeindex
            WRITE (55, *) 'Time (au)                  = ', iteration_finish_time
            WRITE (55, *) 'Time (units of fp)         = ', iteration_finish_time/field_period(1)

            WRITE (55, *) 'Propagation Order          = ', propagation_order
            WRITE (55, f) 'EField                     = ', current_E_field_strength(1, 1), &
                                                           current_E_field_strength(2, 1), &
                                                           current_E_field_strength(3, 1)
            DO i = 2, numsols
                WRITE (55, g) current_E_field_strength(1:3, i)
            END DO

            WRITE (55, *) 'WallClock Time (sec)       = ', hel_time() - program_start_time
            WRITE (55, *) 'Timing                     = ', iteration_time

            WRITE (55, *) 'Total Population Inner     = ', pop_all_inner
            WRITE (55, *) 'Total Population Outer     = ', pop_all_outer
            IF (double_ionization) THEN 
                WRITE (55, *) 'Population Double Outer    = ', pop_all_double_outer
            END IF
            WRITE (55, *) 'Total Population           = ', pop_all
            WRITE (55, *) 'Total Population Outer_SI  = ', pop_all_outer_si
            WRITE (55, *) 'Total Population Outer_Ryd = ', pop_all_outer_ryd
            WRITE (55, *) 'Population_GS_Inner        = ', pop_gs
            WRITE (55, *) ' '
            CLOSE (55)
        END IF

    END SUBROUTINE write_pop_files

!---------------------------------------------------------------------------

    SUBROUTINE write_reform_file(r_at_region_bndry, channel_id_last)

        USE initial_conditions, ONLY: x_last_others, &
                                      x_last_master, &
                                      deltaR
        USE io_parameters,      ONLY: version

        INTEGER, INTENT(IN)          :: channel_id_last
        REAL(wp), INTENT(IN)         :: r_at_region_bndry

        OPEN (UNIT=777, FILE=disk_path//'state/reform_input', STATUS='replace', POSITION='rewind')
        WRITE (777, *) no_of_pes_to_use_outer
        WRITE (777, *) x_last_others
        WRITE (777, *) deltar
        WRITE (777, *) x_last_master
        WRITE (777, *) version
        WRITE (777, *) r_at_region_bndry
        WRITE (777, *) channel_id_last
        CLOSE (777)

    END SUBROUTINE write_reform_file

END MODULE io_files
