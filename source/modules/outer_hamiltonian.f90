! Copyright 2018
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Manages operations of the outer region Hamiltonian (i.e. performing
!> the Hamiltonian-wavefunction multiplication, incrementing with the long range
!> potential matrices).

!> Original version for arbitrarily polarised fields by DDAC
!> Velocity gauge not yet implemented!

MODULE outer_hamiltonian

    USE calculation_parameters, ONLY: nfdm, half_fd_order
    USE rmt_assert,             ONLY: assert
    USE precisn,                ONLY: wp
    USE global_data,            ONLY: zero
    USE grid_parameters,        ONLY: channel_id_1st, &
                                      channel_id_last, &
                                      x_1st, &
                                      x_last, &
                                      my_channel_id_1st, my_channel_id_last, my_num_channels, pe_for_channel
    USE coupling_rules,         ONLY: target_coupled,&
                                      projectile_lcoupled,&
                                      projectile_vcoupled,&
                                      field_contraction
    USE initial_conditions,     ONLY: numsols => no_of_field_confs
    USE initial_conditions,     ONLY: no_of_pes_to_use_outer, no_of_pes_per_sector   
    USE local_ham_matrix,       ONLY: one_over_delR, &
                                      Rinverse, &
                                      Rinverse2, &
                                      RR, &
                                      get_1st_deriv
    USE lrpots,                 ONLY: liuu, &
                                      miuu, &
                                      lpuu, &
                                      ltuu, &
                                      lmuu, &
                                      etuu, &
                                      lsuu, &
                                      icuu, &
                                      ksq
    USE potentials,             ONLY: makewemat, &
                                      makewpmat, &
                                      makewpmat2, &
                                      makewdpmat, &
                                      makewdpmat_v
    USE readhd,                 ONLY: lmaxp1, &
                                      lstartm1, &
                                      no_of_LML_blocks
    USE outer_to_inner_interface, ONLY: send_fderivs_outer_to_inner 
    USE mpi_communications,     ONLY: get_my_group_pe_id, mpi_comm_region, mpi_comm_block, &
                                      get_my_block_group_pe_id, i_am_in_first_outer_block, timeindex_val,&
                                      i_am_in_0_plus_first_outer_block
    USE MPI
    
    IMPLICIT NONE

    INTEGER, ALLOCATABLE, SAVE     :: wp_neighbour_count(:)
    INTEGER, ALLOCATABLE, SAVE     :: we_neighbour_count(:)
    INTEGER, ALLOCATABLE, SAVE     :: wd_neighbour_count(:)
    INTEGER, ALLOCATABLE, SAVE     :: we_neighbour_store(:,:)

! coupling matrices for MPI breakdown of channels
    INTEGER, ALLOCATABLE, SAVE     :: wp_neighbour_me_count(:), wp_neighbour_recv_count(:)
    INTEGER, ALLOCATABLE, SAVE     :: we_neighbour_me_count(:), we_neighbour_recv_count(:)
    INTEGER, ALLOCATABLE, SAVE     :: wd_neighbour_me_count(:), wd_neighbour_recv_count(:)
    INTEGER, ALLOCATABLE, SAVE     :: wp_neighbour_me_store(:,:), wp_neighbour_recv_store(:,:)
    INTEGER, ALLOCATABLE, SAVE     :: we_neighbour_me_store(:,:), we_neighbour_recv_store(:,:)
    INTEGER, ALLOCATABLE, SAVE     :: we_neighbour_me_coup(:,:), we_neighbour_recv_coup(:,:)
    INTEGER, ALLOCATABLE, SAVE     :: wd_neighbour_me_store(:,:), wd_neighbour_recv_store(:,:)
    INTEGER, ALLOCATABLE, SAVE     :: my_channel_id(:) 
    integer, save  :: wd_size_me, wd_size_send, we_size_me, we_size_send, wp_size_me, wp_size_send
    integer, save  :: num_send_max
    LOGICAL, SAVE                  :: sendrecv_yes = .true.

    ! matrices and 'pointer' lists for MPI exchange of channel data with mpi_block_comm (no_of_pes_per_sector>1)
    INTEGER, ALLOCATABLE, SAVE     :: recv_index(:), my_recv_index(:), my_send_index(:), my_send_pe(:)
    INTEGER, ALLOCATABLE, SAVE     :: neighbour_recv_channels(:,:), neighbour_channel_index(:,:) 
    INTEGER, ALLOCATABLE, SAVE     :: neighbour_deriv_recv_channels(:,:), neighbour_deriv_channel_index(:,:) 
    INTEGER, ALLOCATABLE, SAVE     :: my_neighbour_recv_channels(:,:), my_neighbour_send_channels(:,:) 
    INTEGER, ALLOCATABLE, SAVE     :: my_neighbour_channel_rindex(:,:), my_neighbour_channel_sindex(:,:)
    INTEGER, ALLOCATABLE, SAVE     :: deriv_recv_index(:), my_deriv_recv_index(:), my_deriv_send_index(:), &
                                      my_deriv_send_pe(:)  
    INTEGER, ALLOCATABLE, SAVE     :: my_neighbour_deriv_recv_channels(:,:), my_neighbour_deriv_send_channels(:,:) 
    INTEGER, ALLOCATABLE, SAVE     :: my_neighbour_deriv_channel_rindex(:,:), my_neighbour_deriv_channel_sindex(:,:)
    INTEGER, ALLOCATABLE, SAVE     :: my_deriv_channel_rindex(:,:), my_deriv_channel_sindex(:,:)
    INTEGER, ALLOCATABLE, SAVE     :: my_send_previous(:), my_recv_previous(:), my_index_for_pe(:)
    INTEGER, ALLOCATABLE, SAVE     :: my_deriv_send_previous(:), my_deriv_recv_previous(:), &
                                      my_deriv_index_for_pe(:)
    COMPLEX(wp), ALLOCATABLE, SAVE :: psi_from_neighbours(:), psi_to_neighbours(:)
    COMPLEX(wp), ALLOCATABLE, SAVE :: deriv_psi_from_neighbours(:,:), deriv_psi_to_neighbours(:,:)
    INTEGER, SAVE                  :: my_send_dim, my_send_recv_dim, psi_dim_recv, psi_dim_send 
    INTEGER, SAVE                  :: my_deriv_send_dim, my_deriv_send_recv_dim, psi_deriv_dim_recv, &
                                      psi_deriv_dim_send, my_send_index_dim, my_recv_index_dim, &
                                      my_deriv_send_index_dim, my_deriv_recv_index_dim  
    
CONTAINS


    SUBROUTINE get_missing_2nd_deriv_bndries(psi_outer, R_bndry_hi)

        USE mpi_communications,        ONLY: get_my_block_group_id, last_block_group_id
        USE communications_parameters, ONLY: pe_id_last_outer
        COMPLEX(wp), INTENT(IN)      :: psi_outer(x_1st:x_last, my_channel_id_1st:my_channel_id_last, 1:numsols)
        COMPLEX(wp), INTENT(INOUT)   :: R_bndry_hi(x_last + 1:x_last + half_fd_order, &
                                                   my_channel_id_1st:my_channel_id_last, 1:numsols) 

        REAL(wp)   :: outer_boundary_ratio
        INTEGER    :: my_group_pe_id, my_block_group_id

        ! If I am the last processor in the outer region
        ! set R_bndry_hi(x_last+1) = zero
        ! and R_bndry_hi(x_last+2) = -1.0_wp * psi_outer(x_last)

        ! At the outer bndry, psi is odd for all L's:
        outer_boundary_ratio = -1.0_wp
        CALL get_my_block_group_id(my_block_group_id)
        IF (my_block_group_id .EQ. last_block_group_id) THEN
            R_bndry_hi(x_last + 1, my_channel_id_1st:my_channel_id_last, :) = zero
            R_bndry_hi(x_last + 2, my_channel_id_1st:my_channel_id_last, :) = &
                  outer_boundary_ratio * psi_outer(x_last, my_channel_id_1st:my_channel_id_last, :)
            IF (half_fd_order.eq.3) THEN
                R_bndry_hi(x_last + 3, my_channel_id_1st:my_channel_id_last, :) = &
                    outer_boundary_ratio * psi_outer(x_last-1, my_channel_id_1st:my_channel_id_last, :)
            END IF
        END IF

    END SUBROUTINE get_missing_2nd_deriv_bndries

!---------------------------------------------------------------------

    SUBROUTINE get_missing_1st_deriv_bndries(psi_outer, R_bndry_hi)

        USE mpi_communications, ONLY: get_my_block_group_id, last_block_group_id
        USE communications_parameters, ONLY: pe_id_last_outer

        COMPLEX(wp), INTENT(IN)      :: psi_outer(x_1st:x_last, my_channel_id_1st:my_channel_id_last, 1:numsols)
        COMPLEX(wp), INTENT(INOUT)   :: R_bndry_hi(x_last + 1:x_last + half_fd_order, &
                                                   my_channel_id_1st:my_channel_id_last, 1:numsols) 

        REAL(wp)   :: outer_boundary_ratio
        INTEGER    :: my_group_pe_id, my_block_group_id

        ! If I am the last processor in the outer region
        ! set R_bndry_hi(x_last+1) = zero
        ! and R_bndry_hi(x_last+2) = 0.0_wp * psi_outer(x_last)

        ! Hugo has changed 0.0_wp to -1.0_wp as he does not understand this
        outer_boundary_ratio = -1.0_wp
        CALL get_my_block_group_id(my_block_group_id)
        IF (my_block_group_id .EQ. last_block_group_id) THEN
            R_bndry_hi(x_last + 1, my_channel_id_1st:my_channel_id_last, :) = zero
            R_bndry_hi(x_last + 2, my_channel_id_1st:my_channel_id_last, :) = &
                 outer_boundary_ratio * psi_outer(x_last, my_channel_id_1st:my_channel_id_last, :)
            IF (half_fd_order.eq.3) THEN
                R_bndry_hi(x_last + 3, my_channel_id_1st:my_channel_id_last, :) = &
                    outer_boundary_ratio * psi_outer(x_last-1, my_channel_id_1st:my_channel_id_last, :)
            END IF
        END IF

    END SUBROUTINE get_missing_1st_deriv_bndries

!---------------------------------------------------------------------

    !> \brief Multiply the hamiltonian by the wavefunction (outer region)
    !>
    !> The multiplication to be performed is
    !> \f[ H\psi = \left( -\frac{1}{2} \frac{d^2}{dr^2} + \frac{l(l+1)}{2r^2} -
    !> \frac{(Z-N)}{r} \right) \psi  + \sum _{\gamma} (W_E +
    !> W_P + W_D) \psi_{\gamma} \f]
    !> where the sum is performed over all the channels \f$ \gamma \f$.
    !> Each term is computed in turn as required for a given calculation.
    !>
    !> The surface term in the TDSE needed for propagating the inner region coefficients looks like:
    !>  \f[ \frac{1}{2} \sum _{\gamma } \omega _{k,\gamma} \bar{f}_{\gamma}(b,t)
    !>  \f]
    !>  where
    !> \f[ \bar{f}_{\gamma}(b,t) = \left[ \frac{d}{dr} - \frac{\beta}{r}\right]
    !> f_{\gamma} + g\left(\frac{A(t)}{c}\right) \sum_{\gamma'} K_{\gamma,\gamma'} f_{\gamma'}\f]
    !> evaluated at \f$r=b\f$
    !>  where \f$ \beta\f$ is an arbitrary parameter and \f$ g=0 \f$ in length gauge
    !>  and \f$ g=1 \f$ in velocity gauge.
    !>
    !> Regardless of gauge and \f$ \beta \f$ we will always need the first derivative
    !> of f.
    !> Therefore we'll calculate it at the start of this subroutine and save it.
    !>
    !> NB this derivative is also needed for the laser-electron interaction in velocity gauge
    !> so we'll pass the derivatives to the incr_w_WP_Ham_x_Vec_out_vel subroutine
    !>
    !> Also in this routine- calculate \f$ \mathbf d | \psi \rangle \f$ and \f$
    !> \dot{\mathbf{d}} | \psi \rangle \f$, i.e. multiply the dipole and dipole
    !> velocity operators by the wavefunction with a view to evaluating their
    !> expectation value later on.
    !>
    !>
    !> To handle the several different cases (for which potentials to apply,
    !> which multiplications to perform etc.) this routine uses a number of
    !> logical flags. `get_expec` advises if the dipole expectation values are
    !> required (only calculated on the first call of each iteration). The value
    !> of `field_strength` is used to determine if the field coupling potential
    !> needs to be applied (.false. if field_strength = 0).
    !> 
    !> The routine has been adapted for MPI channel parallelisation (no_of_pes_per_sector > 1)
    !> and calls prep_w_comm_block_ham_x_vector_outer and prep_deriv_w_comm_block_ham_x_vector_outer
    !> (for MPI) before the main ham vec operations. The OpenMP is maintained and unaffected apart from 
    !> one case. In the new code, due to the need to call the prep_<…> routines, the OpenMP parallelisation 
    !> is over my_num_channels and the loop over solutions above it is serial. This means that for runs in
    !> which numsols > 1 pure OpenMP runs may be slightly less efficient as the numsols loop is
    !> embarrassingly parallel with no communications. This will be investigated further: the test results
    !> show that for large cases invoking parallelisation over channels the efficiency gains outweigh this
    !> loss.  
    SUBROUTINE ham_x_vector_outer(delR, Z_minus_N, field_strength, psi_outer, &
                                      h_psi_outer, R_bndry_lo, R_bndry_hi, &
                                      first_deriv_of_psi_at_b, t_step2,&
                                      WE_store, &
                                      expec_psi_outer, &
                                      get_expec)

        USE initial_conditions, ONLY: dipole_velocity_output, use_taylor_propagator
        USE lrpots,             ONLY: we_size

        REAL(wp), INTENT(IN)           :: delR, Z_minus_N, t_step2
        REAL(wp), INTENT(IN)           :: field_strength(3, numsols)
        COMPLEX(wp), INTENT(IN)        :: psi_outer(x_1st:x_last, my_channel_id_1st:my_channel_id_last, 1:numsols)
        COMPLEX(wp), INTENT(OUT)       :: h_psi_outer(x_1st:x_last, my_channel_id_1st:my_channel_id_last, 1:numsols)
        COMPLEX(wp), INTENT(INOUT)     :: R_bndry_lo(x_1st - half_fd_order:x_1st - 1, &
                                                     my_channel_id_1st:my_channel_id_last, 1:numsols)
        COMPLEX(wp), INTENT(INOUT)     :: R_bndry_hi(x_last + 1:x_last + half_fd_order, &
                                                     my_channel_id_1st:my_channel_id_last, 1:numsols)
        REAL(wp), INTENT(IN)           :: WE_store(x_1st:x_last, we_size, my_num_channels)
        COMPLEX(wp), INTENT(OUT)       :: first_deriv_of_psi_at_b(my_channel_id_1st:my_channel_id_last, 1:numsols)
        LOGICAL, INTENT(IN)            :: get_expec

        COMPLEX(wp), INTENT(INOUT)     :: &
             expec_psi_outer(x_1st:x_last,my_channel_id_1st:my_channel_id_last,1:numsols,1:3,1:2)

        INTEGER                        :: channel_id, little_L, total_ML, isol
        COMPLEX(wp)                    :: deriv_psi(x_1st:x_last)
        COMPLEX(wp)                    :: first_derivatives_of_psi(x_1st:x_last, &
                                          my_channel_id_1st:my_channel_id_last, 1:numsols)
        LOGICAL                        :: field_on
        REAL(wp)                       :: z_minus_n_over_r(x_1st:x_last)
        INTEGER                        :: number_grid_points
        INTEGER                        :: ic_me
        
        REAL(wp)                       :: second_deriv_coeff

        ! In the molecular case the array ltuu keeps the values of the irreducible
        ! representations (not angular momentum) so at any point in this module
        ! where ltuu is used L must be changed (in particular occurences of ``L+1").

        ! Get updated R_bndry_hi to reflect outer_boundary_ratio = 0.0 for 2nd deriv at the outer boundary
        h_psi_outer = (0.0_wp,0.0_wp)
        
        CALL get_missing_1st_deriv_bndries(psi_outer, R_bndry_hi)

        ! Check if the field is on or not
        IF (ALL(field_strength == 0.0_wp)) THEN
            field_on = .false.
        ELSE
            field_on = .true.
        END IF

        ! Calculate first derivatives: For surface term calculation and laser-electron vel gauge interaction
        first_deriv_of_psi_at_b = (0.0_wp, 0.0_wp)
        IF (i_am_in_first_outer_block .OR. dipole_velocity_output) THEN
            DO isol = 1, numsols
            !$OMP PARALLEL DO PRIVATE(channel_id, deriv_psi)
                DO channel_id = my_channel_id_1st, my_channel_id_last
                    IF (dipole_velocity_output) THEN
                        CALL get_1st_deriv( &
                        psi_outer(x_1st:x_last, channel_id, isol), &
                        deriv_psi, &
                        R_bndry_lo(:, channel_id, isol), &
                        R_bndry_hi(:, channel_id, isol), &
                        one_over_delR)
                        first_derivatives_of_psi(:, channel_id, isol) = deriv_psi
                    ELSE
!                        psi_outer(x_1st+1:x_1st + half_fd_order, channel_id, isol), &
                        CALL get_1st_deriv( &
                        psi_outer(x_1st:x_1st + half_fd_order, channel_id, isol), &
                        deriv_psi, &
                        R_bndry_lo(:, channel_id, isol), &
                        R_bndry_hi(:, channel_id, isol), &
                        one_over_delR)
                    END IF
                    first_deriv_of_psi_at_b(channel_id, isol) = deriv_psi(x_1st)
                END DO
            END DO

            IF (i_am_in_first_outer_block .AND. use_taylor_propagator) THEN
                first_deriv_of_psi_at_b = first_deriv_of_psi_at_b * t_step2
                    CALL send_fderivs_outer_to_inner(first_deriv_of_psi_at_b)
            END IF     
        END IF


        ! Get updated R_bndry_hi to reflect outer_boundary_ratio = -1.0 for 2nd deriv at the outer boundary
!       CALL get_missing_2nd_deriv_bndries(psi_outer, R_bndry_hi)
!  Seems to replicate earlier information...

        IF (get_expec) THEN
            expec_psi_outer = (0.0_wp, 0.0_wp)
        END IF

        z_minus_n_over_r = Z_minus_N * Rinverse
        number_grid_points = x_last - x_1st + 1

!       kinetic_E_coeff = 1.0_wp/(delR*delR)    !! due to finite difference rule for d2/dr2
!       atomic_ham_factor = -0.5_wp                     !! since Hamiltonian has -1/2 grad^2

        second_deriv_coeff = -0.5_wp/(delR*delR)

        DO isol = 1, numsols

            IF (no_of_pes_per_sector > 1) THEN
                ! These routines perform the necessary MPI sends/receives of off task  psi_outer and deriv_psi_outer.
                ! The set of tranfers is performed once, but the data is used many times below.               
                call prep_w_comm_block_ham_x_vector_outer(psi_outer(:,:,isol), number_grid_points)
                call prep_deriv_w_comm_block_ham_x_vector_outer(first_derivatives_of_psi(:,:,isol), number_grid_points)
            END IF           

            !$OMP PARALLEL DO PRIVATE(ic_me, channel_id, little_L, total_ML) 
            DO ic_me = 1, my_num_channels
                channel_id = my_channel_id(ic_me) 
                little_L = liuu(channel_id)
! not used              parity = lpuu(channel_id)
! not used              total_L = ltuu(channel_id)
                total_ML = lmuu(channel_id)  ! DDAC ADDED THIS

                ! Initialize h_psi_outer = - ( 1/2 grad^2 ) psi
                CALL init_w_atomicham_x_vector_outer &
                    (psi_outer(:, channel_id, isol), &
                    h_psi_outer(:, channel_id, isol), &
                    little_L, &
                    R_bndry_lo(:, channel_id, isol), &
                    R_bndry_hi(:, channel_id, isol), &
                    second_deriv_coeff)

                ! - ( (Z-N)/r + 1/2 k^2 ) psi
                CALL apply_centrifugal_and_energy_shift &
                    (number_grid_points,&
                    psi_outer(:,channel_id, isol),&
                    h_psi_outer(:,channel_id, isol),&
                    z_minus_n_over_r,&
                    ksq(channel_id))

                CALL apply_long_range_potential_matrices &
                    (psi_outer(:, :, isol), &
                    h_psi_outer(:,channel_id, isol), &
                    ic_me, &
                    total_ML, &
                    WE_store, &
                    channel_id, &
                    number_grid_points, &
                    field_strength(:, isol), &
                    RR,&
                    field_on)

                IF (get_expec) THEN ! This routine is only called for order 0 of the propagation
                    CALL get_Z_psi_outer &
                        (psi_outer(:, :, isol), &
                            expec_psi_outer(:, channel_id, isol, :, :), &
                            ic_me, &
                            total_ML, &
                            channel_id, &
                            first_derivatives_of_psi(:, :, isol))
                 END IF

            END DO
        END DO

    END SUBROUTINE ham_x_vector_outer

!------------------------------------------------------------------
    ! h_psi_outer = h_psi_outer + V' psi'
    ! V' = W_E + W_P + W_D
    ! W_E = Coulomb interaction
    ! W_D = long range potential coupling together channels in outer region via inner target
    !       states through laser interaction
    ! W_P = laser coupling to outer electron
    SUBROUTINE apply_long_range_potential_matrices (psi, &
                                                    h_psi, &
                                                    ic_me, &
                                                    total_ML, &
                                                    WE_store, &
                                                    channel_id, &
                                                    number_grid_points, &
                                                    field_strength, &
                                                    r_store,&
                                                    field_on)

        USE calculation_parameters, ONLY: use_we_ham_interactions, &
                                          use_wd_ham_interactions, &
                                          use_wp_ham_interactions
        USE lrpots,                 ONLY: we_size

        INTEGER, INTENT(IN)       :: number_grid_points
        INTEGER, INTENT(IN)       :: ic_me 
        INTEGER, INTENT(IN)       :: total_ML
        INTEGER, INTENT(IN)       :: channel_id
        REAL(wp), INTENT(IN)      :: WE_store(number_grid_points, we_size, my_num_channels)
        REAL(wp), INTENT(IN)      :: field_strength(3)
        REAL(wp), INTENT(IN)      :: r_store(number_grid_points)
        COMPLEX(wp), INTENT(IN)   :: psi(number_grid_points, my_channel_id_1st : my_channel_id_last)
        COMPLEX(wp), INTENT(INOUT):: H_psi(number_grid_points)
        LOGICAL, INTENT(IN)       :: field_on
!        integer :: my_group_pe_id
!        call get_my_group_pe_id(my_group_pe_id)
        
        ! h_psi= h_psi+ sum_channel'gamma' W_E psi_channel'gamma'
        IF (use_we_ham_interactions) THEN
            CALL incr_w_we_ham_x_vector_outer &
                (psi, &
                 H_psi, &
                 WE_store, &
                 ic_me, &
                 channel_id,&
                 number_grid_points)
        END IF

        IF (field_on) THEN
            ! h_psi= h_psi+ sum_channel'gamma' W_D psi_channel'gamma'
            IF (use_wd_ham_interactions) THEN
               CALL incr_w_wd_ham_x_vector_outer &
                    (psi, &
                     H_psi, &
                     ic_me, &
                     total_ML, &
                     channel_id, &
                     field_strength,&
                     number_grid_points)
            END IF

            ! h_psi= h_psi+ sum_channel'gamma' W_P psi_channel'gamma'
            IF (use_wp_ham_interactions) THEN
               CALL incr_w_wp_ham_x_vec_out_len &
                    (psi, &
                     H_psi, &
                     ic_me, &
                     total_ML, &
                     channel_id, &
                     field_strength,&
                     number_grid_points,&
                     r_store)
            END IF

        END IF

    END SUBROUTINE apply_long_range_potential_matrices


    SUBROUTINE init_w_atomicham_x_vector_outer(psi, H_psi, L, R_bndry_lo, R_bndry_hi, &
                    second_deriv_coeff)

        USE local_ham_matrix, ONLY: INCR_with_laplacian
        ! DDAC: Modified

        COMPLEX(wp), INTENT(IN)  :: psi(x_1st:x_last)
        COMPLEX(wp), INTENT(OUT) :: H_psi(x_1st:x_last)
        INTEGER, INTENT(IN)      :: L
        COMPLEX(wp), INTENT(IN)  :: R_bndry_lo(x_1st - half_fd_order:x_1st - 1)
        COMPLEX(wp), INTENT(IN)  :: R_bndry_hi(x_last + 1:x_last + half_fd_order)
        REAL(wp), INTENT(IN)     :: second_deriv_coeff

        REAL(wp) atomic_ham_factor
        !EX!

!       kinetic_E_coeff = 1.0_wp/(delR*delR)    !! due to finite difference rule for d2/dr2
        atomic_ham_factor = -0.5_wp                     !! since Hamiltonian has -1/2 grad^2

!       second_deriv_coeff = kinetic_E_coeff*atomic_ham_factor

        H_psi = (0.0_wp, 0.0_wp)

        CALL INCR_with_laplacian &
            (psi, &
             H_psi, &
             L, &
             second_deriv_coeff, &
             atomic_ham_factor, &
             Rinverse2, &
             R_Bndry_Lo, &
             R_bndry_hi)

    END SUBROUTINE init_w_atomicham_x_vector_outer


    SUBROUTINE apply_centrifugal_and_energy_shift (number_of_grid_points, psi, H_psi, Z_minus_N_over_R, k2)
        INTEGER, INTENT(IN)        :: number_of_grid_points
        COMPLEX(wp), INTENT(IN)    :: psi(number_of_grid_points)
        COMPLEX(wp), INTENT(INOUT) :: H_psi(number_of_grid_points)
        REAL(wp), INTENT(IN)       :: Z_minus_N_over_R(number_of_grid_points)
        COMPLEX(wp), INTENT(IN)    :: k2

        H_psi = H_psi - ( Z_minus_N_over_R + 0.5_wp * k2 ) *  psi

    END SUBROUTINE apply_centrifugal_and_energy_shift
!---------------------------------------------------------------------

    !> \brief   prepare off-task wavefunction and first derivative for use in ham-vec operations
    !> \authors M PLummer
    !> \date    2022
    !>
    !> If no_of_pes_per_sector > 1, outer region channels are split among tasks in the communicator
    !> mpi_comm_block for each sector ('block'). The ham-vec multiplication operations can require the
    !> wavefunction component (and the first derivative component) held by other PEs in the communicator.
    !> The specific requirements and associated indices for each task are worked out in test_outer_hamiltonian
    !> at the start of the run. The two routines prep_w_comm_block_ham_x_vector_outer and
    !> prep_deriv_w_comm_block_ham_x_vector_outer perform the required MPI exchanges for a given isol at the
    !> start of the loop over channels (now 'my_channels') in ham_x_vector_outer which performs the ham-vec
    !> multiplications. prep_w_comm_block_ham_x_vector_outer is also called in module
    !> outer_hamiltonian_atrlessthanb.

    SUBROUTINE prep_w_comm_block_ham_x_vector_outer(psi, number_grid_points)

        INTEGER, INTENT(IN)        :: number_grid_points
        COMPLEX(wp), INTENT(IN)    :: psi(number_grid_points, my_channel_id_1st:my_channel_id_last)
        INTEGER                    :: channel_j
        INTEGER                    :: i, ierr, i_prev, i_c
        INTEGER                    :: my_group_pe_id, my_block_group_pe_id
        INTEGER     :: send_count, pe_num, send_tag, recv_tag, my_send, my_recv, my_send_start, my_recv_start
        INTEGER     :: index, send_counts, recv_counts, status_array(MPI_STATUS_SIZE), i_req, req(my_send_dim*2)

!        call get_my_group_pe_id(my_group_pe_id)
        CALL get_my_block_group_pe_id(my_block_group_pe_id)
        IF (sendrecv_yes) THEN
! use sendrecv (saves some memory in psi_to_neighbours)
           DO send_count = 1, my_send_dim
              pe_num = my_send_pe(send_count)
              send_tag = pe_num
              recv_tag = my_block_group_pe_id
              my_send = my_send_index(send_count)
              my_recv = my_recv_index(send_count)
              my_send_start = 0
              my_recv_start = my_recv_previous(send_count) * number_grid_points
              DO i = 1, my_send
                 channel_j = my_neighbour_send_channels(i,send_count)
                 index = i
                 i_prev = (index - 1) * number_grid_points
                 psi_to_neighbours(i_prev+1:i_prev+number_grid_points) = psi(1:number_grid_points,channel_j)
              END DO
              send_counts = number_grid_points * my_send
              recv_counts = number_grid_points * my_recv
              CALL MPI_SENDRECV(psi_to_neighbours(my_send_start+1:my_send_start+send_counts), send_counts, &
                MPI_DOUBLE_COMPLEX, pe_num, send_tag, &
                psi_from_neighbours(my_recv_start+1:my_recv_start+recv_counts), recv_counts, &
                MPI_DOUBLE_COMPLEX, pe_num, recv_tag, &
                mpi_comm_block, status_array, ierr)
           END DO
        ELSE
! use isend and irecv: needs large array for psi_to_neighbours to avoid overwriting, and waitall at end.
! May be slightly faster           
           i_req = 0
           my_send_recv_dim = my_send_dim + my_send_dim
           DO send_count = 1, my_send_dim
              pe_num = my_send_pe(send_count)
              send_tag = pe_num
              recv_tag = my_block_group_pe_id
              my_send = my_send_index(send_count)
              my_recv = my_recv_index(send_count)
              my_send_start = my_send_previous(send_count) * number_grid_points 
              my_recv_start = my_recv_previous(send_count) * number_grid_points
              DO i = 1, my_send
                 channel_j = my_neighbour_send_channels(i,send_count)
                 index = my_send_previous(send_count) + i
                 i_prev = (index - 1) * number_grid_points
                 psi_to_neighbours(i_prev+1:i_prev+number_grid_points) = psi(1:number_grid_points,channel_j)
              END DO
              send_counts = number_grid_points * my_send
              recv_counts = number_grid_points * my_recv
              i_req = i_req + 1               
              CALL MPI_ISEND(psi_to_neighbours(my_send_start+x_1st), send_counts, MPI_DOUBLE_COMPLEX, &       
                          pe_num, send_tag, mpi_comm_block, req(i_req), ierr)
              i_req = i_req + 1               
              CALL MPI_IRECV(psi_from_neighbours(my_recv_start+x_1st), recv_counts, MPI_DOUBLE_COMPLEX, &
                          pe_num, recv_tag, mpi_comm_block, req(i_req), ierr)
           END DO

           IF (my_send_dim  /= 0) &
             CALL MPI_WAITALL(my_send_recv_dim, req(1:my_send_recv_dim), MPI_STATUSES_IGNORE, ierr)
        END IF    
    END SUBROUTINE prep_w_comm_block_ham_x_vector_outer
      
    SUBROUTINE prep_deriv_w_comm_block_ham_x_vector_outer(deriv_psi, number_grid_points)

        INTEGER, INTENT(IN)        :: number_grid_points
        COMPLEX(wp), INTENT(IN)    :: deriv_psi(number_grid_points, my_channel_id_1st:my_channel_id_last)
        INTEGER                    :: channel_j
        INTEGER                    :: i, ierr
        INTEGER                    :: my_group_pe_id, my_block_group_pe_id
        INTEGER     :: send_count, pe_num, send_tag, recv_tag, my_send, my_recv, my_send_start, my_recv_start
        INTEGER     :: index, send_counts, recv_counts, status_array(MPI_STATUS_SIZE), i_req, req(my_send_dim*2)

!        call get_my_group_pe_id(my_group_pe_id)
        CALL get_my_block_group_pe_id(my_block_group_pe_id)
        IF (sendrecv_yes) THEN
! use sendrecv (saves some memory in psi_to_neighbours)
           DO send_count = 1, my_deriv_send_dim
              pe_num = my_deriv_send_pe(send_count)
              send_tag = pe_num
              recv_tag = my_block_group_pe_id
              my_send = my_deriv_send_index(send_count)
              my_recv = my_deriv_recv_index(send_count)
              my_send_start = 1
              my_recv_start = my_deriv_recv_previous(send_count) + 1
              DO i = 1, my_send
                 channel_j = my_neighbour_deriv_send_channels(i,send_count)
                 index = i
                 deriv_psi_to_neighbours(:,index) = deriv_psi(:,channel_j)
              END DO
              send_counts = number_grid_points * my_send
              recv_counts = number_grid_points * my_recv
              CALL MPI_SENDRECV(deriv_psi_to_neighbours(x_1st,my_send_start), send_counts, &
                                MPI_DOUBLE_COMPLEX, pe_num, send_tag, &
                                deriv_psi_from_neighbours(x_1st,my_recv_start), recv_counts, &
                                MPI_DOUBLE_COMPLEX, pe_num, recv_tag, &
                                mpi_comm_block, status_array, ierr)
           END DO
     ELSE
! use isend and irecv: needs large array for psi_to_neighbours to avoid overwriting, and waitall at end.
! May be slightly faster           
           i_req = 0
           my_deriv_send_recv_dim = my_deriv_send_dim + my_deriv_send_dim
           DO send_count = 1, my_deriv_send_dim
              pe_num = my_deriv_send_pe(send_count)
              send_tag = pe_num
              recv_tag = my_block_group_pe_id
              my_send = my_deriv_send_index(send_count)
              my_recv = my_deriv_recv_index(send_count)
              my_send_start = my_deriv_send_previous(send_count) + 1
              my_recv_start = my_deriv_recv_previous(send_count) + 1
              DO i = 1, my_send
                 channel_j = my_neighbour_deriv_send_channels(i,send_count)
                 index = my_deriv_send_previous(send_count) + i
                 deriv_psi_to_neighbours(:,index) = deriv_psi(:,channel_j)
              END DO
              send_counts = number_grid_points * my_send
              recv_counts = number_grid_points * my_recv
              i_req = i_req + 1               
              CALL MPI_ISEND(deriv_psi_to_neighbours(x_1st,my_send_start), send_counts, MPI_DOUBLE_COMPLEX, &
                             pe_num, send_tag, mpi_comm_block, req(i_req), ierr)
              i_req = i_req + 1               
              CALL MPI_IRECV(deriv_psi_from_neighbours(x_1st,my_recv_start), recv_counts, MPI_DOUBLE_COMPLEX, &
                             pe_num, recv_tag, mpi_comm_block, req(i_req), ierr)
           END DO

           IF (my_deriv_send_dim  /= 0) &
              CALL MPI_WAITALL(my_deriv_send_recv_dim, req(1:my_deriv_send_recv_dim), MPI_STATUSES_IGNORE, ierr)
       END IF    
    END SUBROUTINE prep_deriv_w_comm_block_ham_x_vector_outer

!---------------------------------------------------------------------
    
    !> \brief   Apply WE potential on vector
    !> \authors G Armstrong, A Brown
    !> \date    2018
    !>
    !> Multiplies the vector \c Psi (corresponding to channel \c i) by the \f $W_E$ \f$ potential
    !> and adds the result to \c H_Psi (corresponding to channel \c j). The multiplication is only
    !> carried out if the targets in the two channels are coupled by the dipole operator. Note that
    !> the check for coupling is not performed here, but rather only those channels which are coupled
    !> by \f$ W_E \f$ are listed in the vector `we_neighbour_store` and these are the channels which
    !> are used
    !> Update (MP) 2023: for no_of_pes_per_sector > 1 the neighbour channels are spearated into
    !> those on the local task and those collected from ouher tasks in mpi_comm_block.
    SUBROUTINE incr_w_we_ham_x_vector_outer(psi, H_psi, WE_store, ic_me, channel_i,number_grid_points)

        USE lrpots,     ONLY: we_size

        INTEGER, INTENT(IN)        :: number_grid_points
        COMPLEX(wp), INTENT(IN)    :: psi(number_grid_points, my_channel_id_1st:my_channel_id_last)
        COMPLEX(wp), INTENT(INOUT) :: H_psi(number_grid_points)
        REAL(wp), INTENT(IN)       :: WE_store(number_grid_points, we_size, my_num_channels)
        integer, intent(in)        :: ic_me
        INTEGER, INTENT(IN)        :: channel_i
        INTEGER                    :: channel_j
        INTEGER                    :: ii, i, index

        INTEGER     :: num_send_count, send_to_pe, coupling_index, my_send_count, i_prev
!        integer                        :: my_group_pe_id
!        call get_my_group_pe_id(my_group_pe_id)

        DO ii = 1, we_neighbour_me_count(ic_me)
            channel_j = we_neighbour_me_store(ic_me,ii)
            coupling_index = we_neighbour_me_coup(ic_me, ii)
            H_psi(:) = H_psi(:) + WE_store(:,coupling_index,ic_me)*psi(:, channel_j)
        END DO

        num_send_count = we_neighbour_recv_count(ic_me)

        if (num_send_count /= 0) then 
           DO ii = 1, num_send_count
              channel_j = we_neighbour_recv_store(ic_me,ii)
              send_to_pe = pe_for_channel(channel_j)
              my_send_count = my_index_for_pe(send_to_pe)
              i = my_neighbour_channel_rindex(channel_j, my_send_count) 
              index = my_recv_previous(my_send_count) + i
              i_prev = (index - 1) * number_grid_points

              coupling_index = we_neighbour_recv_coup(ic_me, ii)
              H_psi(1:number_grid_points) = H_psi(1:number_grid_points) + &
               WE_store(1:number_grid_points,coupling_index,ic_me)*psi_from_neighbours(i_prev+1:i_prev+number_grid_points)
           END DO
        END IF 
    END SUBROUTINE incr_w_we_ham_x_vector_outer

!---------------------------------------------------------------------

    !> \brief Apply WD potential on vector (outer)
    !>
    !> Multiplies the vector \c Psi (corresponding to channel \c i) by the \f$ W_D \f$ potential
    !> and adds the result to \c H_Psi (corresponding to channel \c j). The multiplication is only
    !> carried out if the targets in the two channels are coupled by the dipole operator. Note that
    !> the check for coupling is not performed here, but rather only those channels which are coupled
    !> by \f$ W_D \f$ are listed in the vector `wd_neighbour_store` and these are the channels which
    !> are used.
    !> Update (MP) 2023: for no_of_pes_per_sector > 1 the neighbour channels are spearated into
    !> those on the local task and those collected from ouher tasks in mpi_comm_block.
    SUBROUTINE incr_w_wd_ham_x_vector_outer(psi, H_psi, ic_me, total_MLi, channel_i, field_strength, number_grid_points)

        USE coupling_rules, ONLY: field_contraction
        USE potentials,     ONLY: makewdpmat
        use mpi_communications, only: get_my_group_pe_id

        INTEGER, INTENT(IN)        :: number_grid_points
        COMPLEX(wp), INTENT(INOUT) :: H_psi(1:number_grid_points)
        COMPLEX(wp), INTENT(IN)    :: psi(1:number_grid_points, my_channel_id_1st:my_channel_id_last)
        INTEGER, INTENT(IN)        :: ic_me
        INTEGER, INTENT(IN)        :: total_MLi, channel_i
        REAL(wp), INTENT(IN)       :: field_strength(3)

        INTEGER     :: channel_j, total_MLj, ii, i, index
        COMPLEX(wp) :: WD_Ham(3), WD_Ham_Coeff
!        integer :: my_group_pe_id 

        INTEGER     :: num_send_count, send_to_pe, my_send_count, i_prev
       
!        call get_my_group_pe_id(my_group_pe_id)

        DO ii = 1, wd_neighbour_me_count(ic_me)
            channel_j = wd_neighbour_me_store(ic_me,ii)
            total_MLj = lmuu(channel_j)
            CALL makewdpmat(channel_i, channel_j, wd_ham)
            wd_ham_coeff = field_contraction(field_strength, wd_ham, total_MLj, total_MLi)
            IF (wd_ham_coeff /= 0) THEN
                H_psi(:) = H_psi(:) + wd_ham_coeff*psi(:, channel_j)
            END IF
        END DO

        num_send_count = wd_neighbour_recv_count(ic_me)
        if (num_send_count /= 0) then 
           DO ii = 1, num_send_count
              channel_j = wd_neighbour_recv_store(ic_me,ii)
              total_MLj = lmuu(channel_j)
              CALL makewdpmat(channel_i, channel_j, wd_ham)
              wd_ham_coeff = field_contraction(field_strength, wd_ham, total_MLj, total_MLi)
              IF (wd_ham_coeff /= 0) THEN
                  send_to_pe = pe_for_channel(channel_j)
                  my_send_count = my_index_for_pe(send_to_pe)
                  i = my_neighbour_channel_rindex(channel_j, my_send_count) 
                  index = my_recv_previous(my_send_count) + i
                  i_prev = (index - 1) * number_grid_points
                  H_psi(1:number_grid_points) = H_psi(1:number_grid_points) + &
                       wd_ham_coeff*psi_from_neighbours(i_prev+1:i_prev+number_grid_points)
              END IF
           END DO
        END IF

    END SUBROUTINE incr_w_wd_ham_x_vector_outer

!---------------------------------------------------------------------

    !> \brief Apply WD potential (velocity gauge) on vector (outer)
    !>
    !> Multiplies the vector \c Psi (corresponding to channel \c i) by the \f$ W_D \f$ potential
    !> and adds the result to \c H_Psi (corresponding to channel \c j). The multiplication is only
    !> carried out if the targets in the two channels are coupled by the dipole operator. Note that
    !> the check for coupling is not performed here, but rather only those channels which are coupled
    !> by \f$ W_D \f$ are listed in the vector `wd_neighbour_store` and these are the channels which
    !> are used.
    !> Update (MP) 2023: for no_of_pes_per_sector > 1 the neighbour channels are spearated into
    !> those on the local task and those collected from ouher tasks in mpi_comm_block.
    SUBROUTINE incr_w_wd_ham_x_vector_outer_vel(psi, H_psi, ic_me, total_MLi, channel_i,&
                                            field_strength, number_grid_points)

        USE coupling_rules, ONLY: field_contraction
        USE potentials,     ONLY: makewdpmat_v
        use mpi_communications, only: get_my_group_pe_id
        
        INTEGER, INTENT(IN)        :: number_grid_points
        COMPLEX(wp), INTENT(INOUT) :: H_psi(1:number_grid_points)
        COMPLEX(wp), INTENT(IN)    :: psi(1:number_grid_points, my_channel_id_1st:my_channel_id_last)
        INTEGER, INTENT(IN)        :: ic_me
        INTEGER, INTENT(IN)        :: total_MLi, channel_i
        REAL(wp), INTENT(IN)       :: field_strength(3)

        INTEGER     :: channel_j, total_MLj, ii, i, index
        COMPLEX(wp) :: WD_Ham(3), WD_Ham_Coeff
!        integer :: my_group_pe_id 
        
        INTEGER     :: num_send_count, my_send_count, send_to_pe, i_prev

!        call get_my_group_pe_id(my_group_pe_id)

        DO ii = 1, wd_neighbour_me_count(ic_me)
            channel_j = wd_neighbour_me_store(ic_me,ii)
            total_MLj = lmuu(channel_j)
            CALL makewdpmat_v(channel_i, channel_j, wd_ham)
            wd_ham_coeff = field_contraction(field_strength, wd_ham, total_MLj, total_MLi)
            IF (wd_ham_coeff /= 0) THEN
                H_psi(:) = H_psi(:) + wd_ham_coeff*psi(:, channel_j)
            END IF
        END DO

        num_send_count = wd_neighbour_recv_count(ic_me)
        if (num_send_count /= 0) then 
           DO ii = 1, num_send_count
              channel_j = wd_neighbour_recv_store(ic_me,ii)
              total_MLj = lmuu(channel_j)
              CALL makewdpmat_v(channel_i, channel_j, wd_ham)
              wd_ham_coeff = field_contraction(field_strength, wd_ham, total_MLj, total_MLi)
              IF (wd_ham_coeff /= 0) THEN
                 send_to_pe = pe_for_channel(channel_j)
                 my_send_count = my_index_for_pe(send_to_pe)
                 i = my_neighbour_channel_rindex(channel_j, my_send_count) 
                 index = my_recv_previous(my_send_count) + i
                 i_prev = (index - 1) * number_grid_points
                 H_psi(1:number_grid_points) = H_psi(1:number_grid_points) + &
                       wd_ham_coeff*psi_from_neighbours(i_prev+1:i_prev+number_grid_points)
              END IF
           END DO
        END IF
         
    END SUBROUTINE incr_w_wd_ham_x_vector_outer_vel

!---------------------------------------------------------------------

    SUBROUTINE incr_w_wp_ham_x_vec_out_vel(psi, H_psi, ic_me, total_MLi, channel_i,&
                                           field_strength, number_grid_points, first_derivs_of_psi)  ! Atomic

        ! DDAC: Modified
        USE coupling_rules, ONLY: field_contraction
        USE potentials,     ONLY : makewpmat, makewpmat2_v

        INTEGER,    INTENT(IN)     :: number_grid_points
        COMPLEX(wp), INTENT(INOUT) :: H_psi(1:number_grid_points)
        COMPLEX(wp), INTENT(IN)    :: psi(1:number_grid_points, my_channel_id_1st:my_channel_id_last)
        INTEGER, INTENT(IN)        :: ic_me
        INTEGER, INTENT(IN)        :: channel_i, total_MLi
        REAL(wp), INTENT(IN)       :: field_strength(3)

        COMPLEX(wp), INTENT(IN)    :: first_derivs_of_psi(1:number_grid_points, my_channel_id_1st:my_channel_id_last)

        INTEGER                    :: channel_j, total_MLj, ii, i, index, j, deriv_index, i_prev
        COMPLEX(wp)                :: wp_ham1(3), wp_ham2(3)
        COMPLEX(wp)                :: wp_ham_coeff1,wp_ham_coeff2

        INTEGER                    :: num_send_count, my_send_count, send_to_pe, my_deriv_send_count
!        integer :: my_group_pe_id 
!        call get_my_group_pe_id(my_group_pe_id)

        DO ii = 1, wp_neighbour_me_count(ic_me)
            channel_j = wp_neighbour_me_store(ic_me,ii)
            total_MLj = lmuu(channel_j)
            CALL makewpmat(channel_i, channel_j, wp_ham1)
            CALL makewpmat2_v(channel_i, channel_j, wp_ham2)
            wp_ham_coeff1 = field_contraction(field_strength(:), wp_ham1, total_MLj, total_MLi)
            wp_ham_coeff2 = field_contraction(field_strength(:), wp_ham2, total_MLj, total_MLi)
            IF ( (wp_ham_coeff1 /= 0) .OR. (wp_ham_coeff2 /=0) ) THEN
                H_psi(:) = H_psi(:) + &
                        (wp_ham_coeff1*first_derivs_of_psi(:, channel_j) + &
                            wp_ham_coeff2*Rinverse(:)*psi(:, channel_j))
            END IF
        END DO

        num_send_count = wp_neighbour_recv_count(ic_me)
        if (num_send_count /= 0) then 
           DO ii = 1, num_send_count
              channel_j = wp_neighbour_recv_store(ic_me,ii)
              total_MLj = lmuu(channel_j)
              CALL makewpmat(channel_i, channel_j, wp_ham1)
              CALL makewpmat2_v(channel_i, channel_j, wp_ham2)
              wp_ham_coeff1 = field_contraction(field_strength(:), wp_ham1, total_MLj, total_MLi)
              wp_ham_coeff2 = field_contraction(field_strength(:), wp_ham2, total_MLj, total_MLi)
              IF ( (wp_ham_coeff1 /= 0) .OR. (wp_ham_coeff2 /=0) ) THEN
                 send_to_pe = pe_for_channel(channel_j)
                 my_send_count = my_index_for_pe(send_to_pe)
                 i = my_neighbour_channel_rindex(channel_j, my_send_count) 
                 index = my_recv_previous(my_send_count) + i
                 i_prev = (index - 1) * number_grid_points
                 my_deriv_send_count = my_deriv_index_for_pe(send_to_pe)
                 j = my_neighbour_deriv_channel_rindex(channel_j, my_deriv_send_count) 
                 deriv_index = my_deriv_recv_previous(my_deriv_send_count) + j
                 H_psi(1:number_grid_points) = H_psi(1:number_grid_points) + &
                        (wp_ham_coeff1*deriv_psi_from_neighbours(1:number_grid_points,deriv_index) + &
                        wp_ham_coeff2*Rinverse(1:number_grid_points)* &
                         psi_from_neighbours(i_prev+1:i_prev+number_grid_points))
              END IF
           END DO
        END IF

    END SUBROUTINE incr_w_wp_ham_x_vec_out_vel

!---------------------------------------------------------------------

    !> \brief Apply WP potential on vector (outer)
    !>
    !> Multiplies the vector \c Psi (corresponding to channel \c i) by the \f$ W_P \f$ potential
    !> in length gauge and adds the result to \c H_Psi (corresponding to channel \c j). The multiplication is only
    !> carried out if the porjectiles in the two channels are coupled by the dipole operator. Note that
    !> the check for coupling is not performed here, but rather only those channels which are coupled
    !> by \f$ W_P \f$ are listed in the vector `wp_neighbour_store` and these are the channels which
    !> are used
    !> Update (MP) 2023: for no_of_pes_per_sector > 1 the neighbour channels are spearated into
    !> those on the local task and those collected from ouher tasks in mpi_comm_block.
    SUBROUTINE incr_w_wp_ham_x_vec_out_len(psi, H_psi, ic_me, total_MLi, channel_i, field_strength, number_grid_points, r_store)

        USE coupling_rules, ONLY: field_contraction
        USE potentials,     ONLY: makewpmat2
        use mpi_communications, only: get_my_group_pe_id 
        
        INTEGER, INTENT(IN)        :: number_grid_points
        COMPLEX(wp), INTENT(INOUT) :: H_psi(1:number_grid_points)
        COMPLEX(wp), INTENT(IN)    :: psi(1:number_grid_points, my_channel_id_1st:my_channel_id_last)
        INTEGER, INTENT(IN)        :: ic_me
        INTEGER, INTENT(IN)        :: channel_i, total_MLi
        REAL(wp), INTENT(IN)       :: field_strength(3)
        REAL(wp), INTENT(IN)       :: r_store(number_grid_points)

        INTEGER     :: channel_j, total_MLj, ii, i, index
        COMPLEX(wp) :: wp_ham2(3), wp_ham2_coeff

        INTEGER     :: num_send_count, my_send_count, send_to_pe, i_prev
!        integer :: my_group_pe_id
!        call get_my_group_pe_id(my_group_pe_id)

        DO ii = 1, wp_neighbour_me_count(ic_me)
            channel_j = wp_neighbour_me_store(ic_me,ii)
            total_MLj = lmuu(channel_j)
            CALL makewpmat2(channel_i, channel_j, wp_ham2)
            wp_ham2_coeff = field_contraction(field_strength, wp_ham2, total_MLj, total_MLi)
            IF (wp_ham2_coeff /= 0) THEN
               H_psi(:) = H_psi(:) + wp_ham2_coeff*r_store(:)*psi(:, channel_j)  ! WP Ham has form WP_Ham2 * r
            END IF
        END DO

        num_send_count = wp_neighbour_recv_count(ic_me)
        if (num_send_count /= 0) then 
           DO ii = 1, num_send_count
              channel_j = wp_neighbour_recv_store(ic_me,ii)
              total_MLj = lmuu(channel_j)
              CALL makewpmat2(channel_i, channel_j, wp_ham2)
              wp_ham2_coeff = field_contraction(field_strength, wp_ham2, total_MLj, total_MLi)
              IF (wp_ham2_coeff /= 0) THEN
                 send_to_pe = pe_for_channel(channel_j)
                 my_send_count = my_index_for_pe(send_to_pe)
                 i = my_neighbour_channel_rindex(channel_j, my_send_count) 
                 index = my_recv_previous(my_send_count) + i
                 i_prev = (index - 1) * number_grid_points
                 H_psi(1:number_grid_points) = H_psi(1:number_grid_points) + &
                      wp_ham2_coeff*r_store(1:number_grid_points) * &
                      psi_from_neighbours(i_prev+1:i_prev+number_grid_points)
                 ! WP Ham has form WP_Ham2 *r
              END IF
           END DO
        END IF

    END SUBROUTINE incr_w_wp_ham_x_vec_out_len

!---------------------------------------------------------------------

    !> \brief    Apply dipole operator on outer wave function
    !> \authors  A Brown, D Clarke, J Benda, J Wragg
    !> \date     2018 - 2019
    !>
    !> Calculates the product of the dipole operator (currently z-component) and the wave functions in the outer
    !> region. Optionally, if Dipole_Velocity_Output is on, it also evaluates the time derivative of the product.
    !>
    !> \note DDAC (06/2018): Modified for calculation of the z-component of the dipole moment
    !>
    SUBROUTINE get_Z_psi_outer(psi, expec_psi, ic_me, total_MLi, channel_i, first_derivs_of_psi)

        USE initial_conditions,        ONLY: dipole_velocity_output,        &
                                             dipole_dimensions_desired,     &
                                             length_gauge_id,               &
                                             velocity_gauge_id
        USE local_ham_matrix,          ONLY: windowed_RR

        COMPLEX(wp), INTENT(IN)    :: psi(x_1st:x_last, my_channel_id_1st:my_channel_id_last)
        COMPLEX(wp), INTENT(INOUT) :: expec_psi(x_1st:x_last,1:3,1:2)
        INTEGER, INTENT(IN)        :: ic_me
        INTEGER, INTENT(IN)        :: channel_i, total_MLi
        COMPLEX(wp), INTENT(IN)    :: first_derivs_of_psi(x_1st:x_last, my_channel_id_1st:my_channel_id_last)

        REAL(wp)                   :: proj(3)
        INTEGER :: Dimension_Counter
!        integer :: my_group_pe_id
!        call get_my_group_pe_id(my_group_pe_id)

        ! Set contraction factor to project WP_Ham2 and WD_Ham2 onto the requested axis, currently z-axis.
        ! Some day, this may be generalized also to other axes. The minus sign compensates for electron charge
        ! in the dipole operator.

        DO Dimension_Counter=1,3
            IF (dipole_dimensions_desired(Dimension_Counter)) THEN

                proj = (/ 0, 0, 0/)
                proj(Dimension_Counter)=-1

                CALL incr_w_wp_ham_x_vec_out_len (psi, expec_psi(:,Dimension_Counter,length_gauge_id), &
                                                ic_me, total_MLi, channel_i,  proj,&
                                                x_last-x_1st+1, windowed_RR )
                CALL incr_w_wd_ham_x_vector_outer(psi, expec_psi(:,Dimension_Counter,length_gauge_id), &
                                                ic_me, total_MLi, channel_i, proj,&
                                                x_last-x_1st+1)

                IF (dipole_velocity_output) THEN

                    CALL incr_w_wp_ham_x_vec_out_vel(psi, expec_psi(:,Dimension_Counter,velocity_gauge_id), &
                                                    ic_me, total_MLi, channel_i, proj, &
                                                    x_last-x_1st+1, first_derivs_of_psi)

                    CALL incr_w_wd_ham_x_vector_outer_vel(psi, expec_psi(:,Dimension_Counter,velocity_gauge_id), &
                                                        ic_me, total_MLi, channel_i, proj, &
                                                        x_last-x_1st+1)
                END IF

            END IF

        END DO

    END SUBROUTINE get_Z_psi_outer

!---------------------------------------------------------------------

    !> \brief Test relations between coupling matrix elements for long range potentials
    !>
    !> Checks that all non-zero elements of the long range potentials between individual channels
    !> conform to the expected coupling rules. Also, counts number of couplings for each channel
    !> (i.e. number of channels j coupled to channel i by \f$ W_E, W_D\f$ and \f$ W_P\f$ ). The
    !> channels j which couple to channel i via \f$ W_E \f$ are stored in `we_neighbour_store(i,:)`
    !> and the number of such couplings is stored in `we_neighbour_count` (same for wp and wd). This
    !> allows all couplings to be evaluated before the time loop and stored, to avoid testing for
    !> coupling repeatedly.
    !> In addition, if no_of_pes_per_sector > 1, the communications (indices and arrays) are
    !> set up for exchange of channel data needed during the ham-vec multiplications etc. An index
    !> is set up that accumulates the necessary channels and tasks without repetition, whose data will
    !> be used for several (eg we, wp, wd etc) calculations during each propagation step. The outer region
    !> channels are split among tasks in the communicator mpi_comm_block for each sector ('block').
    !> The ham-vec multiplication operations can require the wavefunction component (and the first derivative
    !> component) held by other PEs in the communicator. The two routines
    !> prep_w_comm_block_ham_x_vector_outer and prep_deriv_w_comm_block_ham_x_vector_outer, which perform
    !> the mpi operations in each call of ham_x_vector_outer, use the arrays and indices set up here.
    !> prep_w_comm_block_ham_x_vector_outer is also called in module
    !> outer_hamiltonian_atrlessthanb. The dimensions of the new 'psi_from_neighbours' array are set to cope
    !> with both applications. 
    
    SUBROUTINE test_outer_hamiltonian(i_can_do_io)
        USE lrpots,         ONLY: we_size, wp_size, wd_size
        USE coupling_rules, ONLY: target_coupled
        USE initial_conditions, ONLY : debug, dipole_velocity_output
        USE mpi_communications, only: get_my_pe_id, get_my_block_group_pe_id, get_my_block_group_id
        USE calculation_parameters, ONLY: nfdm, half_fd_order
        
        LOGICAL, INTENT(IN) :: i_can_do_io

        COMPLEX(wp) :: wp_ham1(3), wp_ham2(3), wd_ham(3)
        REAL(wp)    :: we_ham, nonzero = 1.0e-14_wp
        INTEGER     :: channel_i, channel_j, tgt_i, tgt_j, little_li, little_lj, little_mi, little_mj
        INTEGER     :: total_Si, total_Sj, parityi, parityj, total_Li, total_Lj, total_MLi, total_MLj

        CHARACTER(110) :: tmplt_i, tmplt_j
        INTEGER :: coupling_index, pe_num, send_count, send_tag, recv_tag
        INTEGER :: ic_me, my_group_pe_id, num_send_we, num_me_we, num_send_wp, num_me_wp, num_send_wd, &
             num_me_wd, my_block_group_pe_id
        LOGICAL :: new_channel, new_deriv_channel
        INTEGER     :: status_array(MPI_STATUS_SIZE), i, i_index, my_send, my_recv, number_channels, ierr
        INTEGER     :: sum_r, sum_s, my_deriv_send, my_deriv_recv, my_block_group_id, s_temp(2), r_temp(2)
        INTEGER     :: max_points_for_H_op, neighbour_dim  
        
        CALL allocate_coupling_matrices

        tmplt_i = '(" Channel ",I5,": Si = ",I3,", Li = ",I3,", MLi = ",I3,", Pi = ",I3,", Ti = ",I3,", li = ",I3,", mi = ",I3)'
        tmplt_j = '(" Channel ",I5,": Sj = ",I3,", Lj = ",I3,", MLj = ",I3,", Pj = ",I3,", Ti = ",I3,", lj = ",I3,", mj = ",I3)'

        IF (i_can_do_IO .AND. debug) WRITE (*, '(/,1x,A)') 'Test outer Hamiltonian'

        CALL get_my_group_pe_id(my_group_pe_id)
        CALL get_my_block_group_pe_id(my_block_group_pe_id)
        CALL get_my_block_group_id(my_block_group_id)
        
        allocate (recv_index(0:no_of_pes_per_sector-1), deriv_recv_index(0:no_of_pes_per_sector-1), &
             stat=ierr)
        IF (no_of_pes_per_sector > 1) allocate (&
             neighbour_recv_channels(channel_id_1st:channel_id_last,0:no_of_pes_per_sector-1), & ! ***** temp fix *****
             neighbour_channel_index(channel_id_1st:channel_id_last,0:no_of_pes_per_sector-1), &
             neighbour_deriv_recv_channels(channel_id_1st:channel_id_last,0:no_of_pes_per_sector-1), & ! ***** temp fix *****
             neighbour_deriv_channel_index(channel_id_1st:channel_id_last,0:no_of_pes_per_sector-1), &
             stat=ierr)
        recv_index(:) = 0
        deriv_recv_index(:) = 0 
        ! WE
        DO ic_me = 1, my_num_channels
            channel_i = my_channel_id(ic_me)
            coupling_index = 0
            num_send_we = 0
            num_me_we = 0
            total_Si = lsuu(channel_i); total_Li = ltuu(channel_i); total_MLi = lmuu(channel_i)
            parityi = lpuu(channel_i); little_li = liuu(channel_i); little_mi = miuu(channel_i)
            tgt_i = icuu(channel_i)

            DO channel_j = channel_id_1st, channel_id_last
                total_Sj = lsuu(channel_j); total_Lj = ltuu(channel_j); total_MLj = lmuu(channel_j)
                parityj = lpuu(channel_j); little_lj = liuu(channel_j); little_mj = miuu(channel_j)
                tgt_j = icuu(channel_j)

                IF (abs(channel_i - channel_j) <= we_size ) THEN
                    CALL makewemat(RR(x_1st), channel_i, channel_j, WE_ham)
                ELSE
                    WE_ham = 0.0_wp
                END IF

                IF  (ABS(WE_ham) > nonzero) THEN
                    ! WE coupling requires equal total quantum numbers

                   IF (total_Li == total_Lj .AND. total_MLi == total_MLj .AND. parityi == parityj) THEN
                        coupling_index = coupling_index +1
                        we_neighbour_count(channel_i) = coupling_index
                        we_neighbour_store(channel_i,coupling_index) = channel_j
                        pe_num = pe_for_channel(channel_j) 
                        IF (pe_num /= my_block_group_pe_id) then
                           new_channel = .true.
                           DO i_index = 1, recv_index(pe_num)
                              IF (channel_j == neighbour_recv_channels(i_index,pe_num)) then
                                 new_channel = .false.
                                 exit
                              END IF
                           END DO
                           IF (new_channel) then
                               recv_index(pe_num) = recv_index(pe_num) + 1
                               neighbour_recv_channels(recv_index(pe_num),pe_num) = channel_j
                               neighbour_channel_index(channel_j,pe_num) = recv_index(pe_num)
                           END IF
                           num_send_we = num_send_we + 1
                           we_neighbour_recv_count(ic_me) = num_send_we
                           we_neighbour_recv_store(ic_me, num_send_we) = channel_j
                           we_neighbour_recv_coup(ic_me, num_send_we) = coupling_index
                        ELSE
                           num_me_we = num_me_we + 1
                           we_neighbour_me_count(ic_me) = num_me_we
                           we_neighbour_me_store(ic_me, num_me_we) = channel_j
                           we_neighbour_me_coup(ic_me, num_me_we) = coupling_index
                        END IF
                    END IF
                END IF
             END DO
             
        END DO

        ! WP
        DO ic_me = 1, my_num_channels
            channel_i = my_channel_id(ic_me)
            num_send_wp = 0
            num_me_wp = 0
            coupling_index = 0
            total_Si = lsuu(channel_i); total_Li = ltuu(channel_i); total_MLi = lmuu(channel_i)
            parityi = lpuu(channel_i); little_li = liuu(channel_i); little_mi = miuu(channel_i)
            tgt_i = icuu(channel_i)

            DO channel_j = channel_id_1st, channel_id_last
                total_Sj = lsuu(channel_j); total_Lj = ltuu(channel_j); total_MLj = lmuu(channel_j)
                parityj = lpuu(channel_j); little_lj = liuu(channel_j); little_mj = miuu(channel_j)
                tgt_j = icuu(channel_j)

                IF (ABS(channel_i - channel_j) <= wp_size) THEN
                    IF (dipole_velocity_output) THEN
                        CALL makewpmat(channel_i, channel_j, wp_ham1)
                    ELSE
                        wp_ham1 = 0.0_wp
                    END IF
                    CALL makewpmat2(channel_i, channel_j, wp_ham2)
                ELSE
                    wp_ham1 = zero
                    wp_ham2 = zero
                END IF

                IF ( any(ABS(wp_ham1) > nonzero) .or. any(ABS(wp_ham2) > nonzero) ) THEN
                    ! WP coupling requires dipole coupled targets and projectiles, and equal targets
                    IF (projectile_lcoupled(channel_i, channel_j) .AND. tgt_i == tgt_j) THEN
                        coupling_index = coupling_index + 1
                        wp_neighbour_count(channel_i) = coupling_index
                        pe_num = pe_for_channel(channel_j) 
                        IF (pe_num /= my_block_group_pe_id) then
! first continue psi count
                           new_channel = .true.
                           DO i_index = 1, recv_index(pe_num)
                              IF (channel_j == neighbour_recv_channels(i_index,pe_num)) then
                                 new_channel = .false.
                                 exit
                              END IF
                           END DO
                           IF (new_channel) THEN
                               recv_index(pe_num) = recv_index(pe_num) + 1
                               neighbour_recv_channels(recv_index(pe_num),pe_num) = channel_j
                               neighbour_channel_index(channel_j,pe_num) = recv_index(pe_num)
                           END IF
! now the psi derivative count
                           new_deriv_channel = .true.
                           DO i_index = 1, deriv_recv_index(pe_num)
                              IF (channel_j == neighbour_deriv_recv_channels(i_index,pe_num)) then
                                 new_deriv_channel = .false.
                                 exit
                              END IF
                           END DO
                           IF (new_deriv_channel) then
                               deriv_recv_index(pe_num) = deriv_recv_index(pe_num) + 1
                               neighbour_deriv_recv_channels(deriv_recv_index(pe_num),pe_num) = channel_j
                               neighbour_deriv_channel_index(channel_j,pe_num) = deriv_recv_index(pe_num)
                           END IF
                           num_send_wp = num_send_wp + 1
                           wp_neighbour_recv_count(ic_me) = num_send_wp
                           wp_neighbour_recv_store(ic_me, num_send_wp) = channel_j
                        ELSE
                           num_me_wp = num_me_wp + 1
                           wp_neighbour_me_count(ic_me) = num_me_wp
                           wp_neighbour_me_store(ic_me, num_me_wp) = channel_j
                        END IF
                    END IF
                END IF
            END DO
        END DO

        ! WD
        DO ic_me = 1, my_num_channels
           channel_i = my_channel_id(ic_me)
            num_send_wd = 0
            num_me_wd = 0
            coupling_index = 0
            total_Si = lsuu(channel_i); total_Li = ltuu(channel_i); total_MLi = lmuu(channel_i)
            parityi = lpuu(channel_i); little_li = liuu(channel_i); little_mi = miuu(channel_i)
            tgt_i = icuu(channel_i)

            DO channel_j = channel_id_1st, channel_id_last
                total_Sj = lsuu(channel_j); total_Lj = ltuu(channel_j); total_MLj = lmuu(channel_j)
                parityj = lpuu(channel_j); little_lj = liuu(channel_j); little_mj = miuu(channel_j)
                tgt_j = icuu(channel_j)

                IF (ABS(channel_i - channel_j) <= wd_size) THEN
                    CALL makewdpmat(channel_i, channel_j, wd_ham)
                 ELSE
                    wd_ham = zero
                END IF

                IF (any(ABS(wd_ham) > nonzero)) THEN
                    ! WD coupling requires dipoled coupled (total) systems and equal channels
                    IF (target_coupled(channel_i, channel_j) .AND. little_li == little_lj &
                                                             .AND. little_mi == little_mj) THEN
                        coupling_index = coupling_index + 1
                        wd_neighbour_count(channel_i) = coupling_index
                        pe_num = pe_for_channel(channel_j) 
                        IF (pe_num /= my_block_group_pe_id) then
                           new_channel = .true.
                           DO i_index = 1, recv_index(pe_num)
                              IF (channel_j == neighbour_recv_channels(i_index,pe_num)) THEN
                                 new_channel = .false.
                                 exit
                              END IF
                           END DO
                           IF (new_channel) THEN
                               recv_index(pe_num) = recv_index(pe_num) + 1
                               neighbour_recv_channels(recv_index(pe_num),pe_num) = channel_j
                               neighbour_channel_index(channel_j,pe_num) = recv_index(pe_num)
                           END IF
                           num_send_wd = num_send_wd + 1
                           wd_neighbour_recv_count(ic_me) = num_send_wd
                           wd_neighbour_recv_store(ic_me, num_send_wd) = channel_j
                        ELSE
                           num_me_wd = num_me_wd + 1
                           wd_neighbour_me_count(ic_me) = num_me_wd
                           wd_neighbour_me_store(ic_me, num_me_wd) = channel_j
                        END IF
                    END IF
                END IF
            END DO
        END DO

        ierr = 0
        ! exchange recv_index etc with other pes in block communicator
    ! Each task in MPI_comm_block needs to know which channels to send to and receive from which other tasks
        my_send_dim = 0
        DO i = 0, no_of_pes_per_sector - 1
           if (recv_index(i) /= 0) my_send_dim = my_send_dim + 1
        END DO
        ! localize the index
        IF (my_send_dim /= 0) THEN
           my_send_recv_dim = my_send_dim + my_send_dim
           number_channels = channel_id_last - channel_id_1st + 1
           my_recv_index_dim = MAXVAL(recv_index(0:no_of_pes_per_sector-1))
           ALLOCATE(my_recv_index(my_send_dim), my_send_pe(my_send_dim), &
                my_neighbour_recv_channels(my_recv_index_dim,my_send_dim), &
                my_neighbour_channel_rindex(channel_id_1st:channel_id_last,my_send_dim), &
                my_neighbour_channel_sindex(channel_id_1st:channel_id_last,my_send_dim), &
                my_send_previous(my_send_dim), my_recv_previous(my_send_dim), &
                my_index_for_pe(0:no_of_pes_per_sector-1), my_send_index(my_send_dim), &
                stat=ierr)
           send_count = 0 
           DO pe_num = 0, no_of_pes_per_sector-1
              IF (recv_index(pe_num) /= 0) THEN
                 send_count = send_count + 1
                 my_recv_index(send_count) = recv_index(pe_num)
                 my_send_pe(send_count) = pe_num
                 my_index_for_pe(pe_num) = send_count
                 DO i = 1, recv_index(pe_num)
                    channel_j = neighbour_recv_channels(i,pe_num)
                    my_neighbour_recv_channels(i,send_count) = channel_j
                    my_neighbour_channel_rindex(channel_j,send_count) = i 
                 END DO 
              END IF
           END DO
           DEALLOCATE (neighbour_recv_channels, neighbour_channel_index, recv_index, stat=ierr)
           CALL get_my_block_group_id(my_block_group_id)

           my_send_index_dim = 0
           DO send_count = 1, my_send_dim
              pe_num = my_send_pe(send_count)
              send_tag = my_block_group_id * 10 + my_block_group_pe_id
              recv_tag = my_block_group_id * 10 + pe_num              
              my_recv = my_recv_index(send_count)
              s_temp(1) = my_recv
              s_temp(2) = my_recv_index_dim
              CALL MPI_sendrecv (s_temp, 2, MPI_INTEGER, pe_num, send_tag, & 
                   r_temp, 2, MPI_INTEGER, pe_num, recv_tag, &
                   mpi_comm_block, status_array, ierr)
              my_send = r_temp(1)
              my_send_index(send_count) = my_send
              my_send_index_dim = MAX(my_send_index_dim, r_temp(2))
           END DO    
           ALLOCATE(my_neighbour_send_channels(my_send_index_dim,my_send_dim), stat=ierr)
              
           DO send_count = 1, my_send_dim
              pe_num = my_send_pe(send_count)
              send_tag = my_block_group_id * 10 + my_block_group_pe_id
              recv_tag = my_block_group_id * 10 + pe_num
              my_recv = my_recv_index(send_count)
              my_send = my_send_index(send_count)
              CALL MPI_SENDRECV (my_neighbour_recv_channels(1:my_recv,send_count), my_recv, MPI_INTEGER, &
                   pe_num, send_tag, &
                   my_neighbour_send_channels(1:my_send,send_count), my_send, MPI_INTEGER, pe_num, recv_tag, &
                   mpi_comm_block, status_array, ierr)
              CALL MPI_SENDRECV (my_neighbour_channel_rindex(:,send_count), number_channels, MPI_INTEGER, &
                   pe_num, send_tag, &
                   my_neighbour_channel_sindex(:,send_count), number_channels, MPI_INTEGER, pe_num, recv_tag, &
                   mpi_comm_block, status_array, ierr)
           END DO
           sum_s = 0
           sum_r = 0
           DO send_count = 1, my_send_dim
              my_send_previous(send_count) = sum_s
              my_recv_previous(send_count) = sum_r
              sum_s = sum_s + my_send_index(send_count)
              sum_r = sum_r + my_recv_index(send_count)
           END DO   
           psi_dim_recv = sum_r
           ! The temporary storage arrays for MPI-exchanged channel data are used in both ham_x_vector_outer and
           ! ham_x_vector_outer_at_b (the boundary).
           ! For small sectors the boundary grid point number may be larger than the sector grid size
           max_points_for_H_op = 2 * nfdm - half_fd_order
           neighbour_dim = MAX(max_points_for_H_op, (x_last-x_1st+1))
           ALLOCATE(psi_from_neighbours(neighbour_dim * psi_dim_recv), stat = ierr)
           IF (sendrecv_yes) THEN
              psi_dim_send = MAXVAL(my_send_index(1:my_send_dim))
           ELSE   
              psi_dim_send = sum_s
           END IF
           ALLOCATE(psi_to_neighbours(neighbour_dim * psi_dim_send), stat = ierr)
           CALL assert(ierr == 0, 'problem in set-up of mpi_comm_block channel-passing arrays')
           CALL assert(ierr == 0, 'problem allocating psi or deriv _from_neighbours in test_outer') 
        END IF
        ierr = 0
        ! repeat whole procedure for the neighbour first derivatives:
        !exchange deriv_recv_index etc with other pes in block communicator.
        ! The repetition here is to save space in the deriv_psi_from/to_neighbours arrays.
        my_deriv_send_dim = 0
        DO i = 0, no_of_pes_per_sector-1
           IF (deriv_recv_index(i) /= 0) my_deriv_send_dim = my_deriv_send_dim + 1
        END DO
        IF (my_deriv_send_dim /= 0) THEN
           my_deriv_send_recv_dim = my_deriv_send_dim + my_deriv_send_dim
           number_channels = channel_id_last - channel_id_1st + 1
           my_deriv_recv_index_dim = MAXVAL(deriv_recv_index(0:no_of_pes_per_sector-1))
           ALLOCATE(my_deriv_recv_index(my_deriv_send_dim), my_deriv_send_pe(my_deriv_send_dim), &
                my_neighbour_deriv_recv_channels(my_deriv_recv_index_dim,my_deriv_send_dim), &
                my_neighbour_deriv_channel_rindex(channel_id_1st:channel_id_last,my_deriv_send_dim), &
                my_neighbour_deriv_channel_sindex(channel_id_1st:channel_id_last,my_deriv_send_dim), &
                my_deriv_send_previous(my_deriv_send_dim), my_deriv_recv_previous(my_deriv_send_dim), &
                my_deriv_index_for_pe(0:no_of_pes_per_sector-1),my_deriv_send_index(my_deriv_send_dim), &
                stat=ierr)
           send_count = 0 
           DO pe_num = 0, no_of_pes_per_sector-1
              IF (deriv_recv_index(pe_num) /= 0) THEN
                 send_count = send_count + 1
                 my_deriv_recv_index(send_count) = deriv_recv_index(pe_num)
                 my_deriv_send_pe(send_count) = pe_num
                 my_deriv_index_for_pe(pe_num) = send_count
                 DO i = 1, deriv_recv_index(pe_num)
                    channel_j = neighbour_deriv_recv_channels(i,pe_num)
                    my_neighbour_deriv_recv_channels(i,send_count) = channel_j
                    my_neighbour_deriv_channel_rindex(channel_j,send_count) = i 
                 END DO 
              END IF
           END DO
           DEALLOCATE (neighbour_deriv_recv_channels, neighbour_deriv_channel_index, deriv_recv_index, &
                stat=ierr)
           my_deriv_send_index_dim = 0
           DO send_count = 1, my_deriv_send_dim
              pe_num = my_deriv_send_pe(send_count)
              send_tag = my_block_group_pe_id
              recv_tag = pe_num
              s_temp(1) = my_deriv_recv_index(send_count)
              s_temp(2) = my_deriv_recv_index_dim
              CALL MPI_sendrecv (s_temp, 2, MPI_INTEGER, pe_num, send_tag, & 
                   r_temp, 2, MPI_INTEGER, pe_num, recv_tag, &
                   mpi_comm_block, status_array, ierr)
              my_deriv_send_index(send_count) = r_temp(1)
              my_deriv_send_index_dim = MAX(my_deriv_send_index_dim, r_temp(2))
           END DO
           ALLOCATE(my_neighbour_deriv_send_channels(my_deriv_send_index_dim,my_deriv_send_dim), stat=ierr)
           DO send_count = 1, my_deriv_send_dim
              pe_num = my_deriv_send_pe(send_count)
              send_tag = my_block_group_pe_id
              recv_tag = pe_num
              my_deriv_send = my_deriv_send_index(send_count)
              my_deriv_recv = my_deriv_recv_index(send_count)
              CALL MPI_SENDRECV (my_neighbour_deriv_recv_channels(1:my_deriv_recv,send_count), &
                   my_deriv_recv, MPI_INTEGER, pe_num, send_tag, &
                   my_neighbour_deriv_send_channels(1:my_deriv_send,send_count), my_deriv_send, &
                   MPI_INTEGER, pe_num, recv_tag, mpi_comm_block, status_array, ierr)
              CALL MPI_SENDRECV (my_neighbour_deriv_channel_rindex(:,send_count), number_channels, &
                   MPI_INTEGER, pe_num, send_tag, &
                   my_neighbour_deriv_channel_sindex(:,send_count), number_channels, MPI_INTEGER, &
                   pe_num, recv_tag, mpi_comm_block, status_array, ierr)
           END DO
           sum_s = 0
           sum_r = 0
           DO send_count = 1, my_deriv_send_dim
              my_deriv_send_previous(send_count) = sum_s
              my_deriv_recv_previous(send_count) = sum_r
              sum_s = sum_s + my_deriv_send_index(send_count)
              sum_r = sum_r + my_deriv_recv_index(send_count)
           END DO   
           psi_deriv_dim_recv = sum_r
           ALLOCATE(deriv_psi_from_neighbours(x_last-x_1st+1, psi_deriv_dim_recv), stat = ierr)
           IF (sendrecv_yes) THEN
              psi_deriv_dim_send = MAXVAL(my_deriv_send_index(1:my_deriv_send_dim))
           ELSE   
              psi_deriv_dim_send = sum_s
           END IF
           ALLOCATE(deriv_psi_to_neighbours(x_last-x_1st+1, psi_deriv_dim_send), stat = ierr)
           CALL assert(ierr == 0, 'problem in set-up of mpi_comm_block channel-passing arrays')
           CALL assert(ierr == 0, 'problem allocating psi or deriv _from_neighbours in test_outer') 
        END IF
        
        IF (no_of_pes_per_sector > 1 .and. debug) THEN
           print *, ' my_group_pe_id =', my_group_pe_id, &
           ' SUM(we_neighbour_recv_count(1:my_num_channels) =', SUM(we_neighbour_recv_count(1:my_num_channels)) 
           print *, ' my_group_pe_id =', my_group_pe_id, &
           ' SUM(wd_neighbour_recv_count(1:my_num_channels) =', SUM(wd_neighbour_recv_count(1:my_num_channels)) 
           print *, ' my_group_pe_id =', my_group_pe_id, &
           ' SUM(wp_neighbour_recv_count(1:my_num_channels) =', SUM(wp_neighbour_recv_count(1:my_num_channels)) 
        END IF

        ! Print number of neighbours
        IF (i_am_in_first_outer_block .AND. debug) THEN
            PRINT *, 'Number of neighbours:'
            DO channel_i = my_channel_id_1st, my_channel_id_last
                total_Si = lsuu(channel_i); total_Li = ltuu(channel_i); total_MLi = lmuu(channel_i)
                parityi = lpuu(channel_i); little_li = liuu(channel_i); little_mi = miuu(channel_i)
                tgt_i = icuu(channel_i)

                WRITE (*, tmplt_i, advance='no') &
                    channel_i, total_Si, total_Li, total_MLi, parityi, tgt_i, little_li, little_mi
                WRITE (*, '(": #WE = ",I4,", #WP = ",I4,", #WD = ",I4)') &
                    we_neighbour_count(channel_i), wp_neighbour_count(channel_i), wd_neighbour_count(channel_i)
            END DO
        END IF
         
! safety:  (could be got rid of)
        CALL mpi_barrier(mpi_comm_region, ierr)
        
        ! Reset sizes to actual width of the packed storage rather than the half-band estimate
        wd_size = MAXVAL(wd_neighbour_count)
        wd_size_me = MAXVAL(wd_neighbour_me_count)
        wd_size_send = MAXVAL(wd_neighbour_recv_count)
        we_size = MAXVAL(we_neighbour_count)
        we_size_me = MAXVAL(we_neighbour_me_count)
        we_size_send = MAXVAL(we_neighbour_recv_count)
        wp_size = MAXVAL(wp_neighbour_count)
        wp_size_me = MAXVAL(wp_neighbour_me_count)
        wp_size_send = MAXVAL(wp_neighbour_recv_count)
        num_send_max = MAX(wp_size_send, wd_size_send, we_size_send)
    END SUBROUTINE test_outer_hamiltonian


    !> \brief Allocate the arrays for the coupling information
    !>
    !> For each channel i, we will store the channels j to which i couples via
    !> the \f$ W_D, W_E \f$ and \f$ W_P\f$ matrices.
    SUBROUTINE allocate_coupling_matrices
        USE lrpots, ONLY: we_size, wp_size, wd_size
        IMPLICIT NONE
        INTEGER :: ierr, ic_me, my_start_minus_1

! check definitions of 'estimate' we_size etc
        ALLOCATE(we_neighbour_store(my_channel_id_1st:my_channel_id_last, 2*we_size + 1),&
                 wp_neighbour_count(my_channel_id_1st:my_channel_id_last),&
                 wd_neighbour_count(my_channel_id_1st:my_channel_id_last),&
                 we_neighbour_count(my_channel_id_1st:my_channel_id_last),&
                 stat=ierr)
        ALLOCATE(wp_neighbour_me_store(my_num_channels, 2*wp_size + 1),&
                 wd_neighbour_me_store(my_num_channels, 2*wd_size + 1),&
                 we_neighbour_me_store(my_num_channels, 2*we_size + 1),&
                 we_neighbour_me_coup(my_num_channels, 2*we_size + 1),&
                 wp_neighbour_me_count(my_num_channels),&
                 wd_neighbour_me_count(my_num_channels),&
                 we_neighbour_me_count(my_num_channels),&
                 wp_neighbour_recv_count(my_num_channels),&
                 wd_neighbour_recv_count(my_num_channels),&
                 we_neighbour_recv_count(my_num_channels),&
                 stat=ierr)
        ! Initialize
        WP_neighbour_count = 0  ;  
        WE_neighbour_count = 0  ;  WE_neighbour_store = (0,0)
        WD_neighbour_count = 0  ;  
        WP_neighbour_me_count = 0  ;  WP_neighbour_me_store = (0,0)
        WE_neighbour_me_count = 0  ;  WE_neighbour_me_store = (0,0) ; WE_neighbour_me_coup = 0
        WD_neighbour_me_count = 0  ;  WD_neighbour_me_store = (0,0)
        WP_neighbour_recv_count = 0  
        WE_neighbour_recv_count = 0  
        WD_neighbour_recv_count = 0  

        IF (no_of_pes_per_sector > 1) THEN
           ALLOCATE(wp_neighbour_recv_store(my_num_channels, 2*wp_size + 1),&
                    wd_neighbour_recv_store(my_num_channels, 2*wd_size + 1),&
                    we_neighbour_recv_store(my_num_channels, 2*we_size + 1),&
                    we_neighbour_recv_coup(my_num_channels, 2*we_size + 1),&
                    stat=ierr)
        ! Initialize
           WP_neighbour_recv_store = (0,0)
           WE_neighbour_recv_store = (0,0) ; WE_neighbour_recv_coup = 0
           WD_neighbour_recv_store = (0,0)
        END IF   

        ALLOCATE (my_channel_id(my_num_channels), stat=ierr)

        CALL assert((ierr == 0), 'allocation error in outer_hamiltonian')
        

        my_start_minus_1 = my_channel_id_1st - 1
        DO ic_me = 1, my_num_channels 
           my_channel_id(ic_me) = my_start_minus_1 + ic_me
        END DO   
        
    END SUBROUTINE allocate_coupling_matrices


    !> \brief Setup and store the WE array
    !> \authors  A Brown
    !>
    !> The array storing the WE potential coefficients  (coupling the electron to the residual
    !> ion) is set up outside of the time loop to avoid unnecessary steps in the
    !> time propagation.
    SUBROUTINE setup_WE_store (WE_store, r_store, number_grid_points)
        USE lrpots,     ONLY: we_size
        USE potentials, ONLY: makewemat
        INTEGER,  INTENT(IN)    :: number_grid_points
        REAL(wp), INTENT(INOUT) :: WE_store(number_grid_points, we_size, my_num_channels)
        REAL(wp), INTENT(IN)    :: r_store(number_grid_points)
        INTEGER                  :: channel_i, channel_j
        INTEGER                  :: ii, i, ic_me
        REAL(wp)                 :: WE_ham

        do ic_me = 1, my_num_channels
            channel_i = my_channel_id(ic_me)

            DO ii = 1, we_neighbour_count(channel_i)
                channel_j = we_neighbour_store(channel_i,ii)

                DO i = 1, number_grid_points
                    CALL makewemat(r_store(i), channel_i, channel_j, WE_ham)
                    WE_store(i,ii,ic_me) = WE_ham
                END DO
            END DO
        END DO

    END SUBROUTINE setup_WE_store

END MODULE outer_hamiltonian
