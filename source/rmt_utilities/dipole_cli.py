def read_command_line(objectstring='requested'):
    from argparse import ArgumentParser as AP
    parser = AP()
    parser.add_argument('files', nargs="*",
                        help="optional list of directories containing rmt\
                        calculations",
                        default=["."])
    parser.add_argument('-x', '--x', help='transform the  x component of the dipole',
                        action='store_true', default=False)
    parser.add_argument('-y', '--y', help='transform the y component of the dipole',
                        action='store_true', default=False)
    parser.add_argument('-z', '--z', help='transform the z component of the dipole',
                        action='store_true', default=False)
    parser.add_argument('-p', '--plot', help=f"show a plot of {objectstring} spectrum",
                        action='store_true', default=False)
    parser.add_argument('-o', '--output', action='store_true',
                        help=f'output the {objectstring} spectra to text files',
                        default=False)
    parser.add_argument('--pad_factor',
                        help='number of zeros to pad data with during calculations\
                        involving Fourier transforms i.e. pad signal up to length 2^n)',
                        type=int, default=8)
    parser.add_argument('-s', '--solutions_list',
                        help='list of solutions on which to operate, defaults to all solutions', default=None)
    parser.add_argument('-u', '--units',
                        help='units to use on the x-axis, "eV" or "au"', default="eV")
    parser.add_argument('-w', '--window_start',
                        help='the time step index at which to begin the linear window\
                        (defaults to half the total number of time steps), negative values\
                        disable windowing (for more complex windows, see the ``windowing``\
                        documentation)', type=int)

    return parser


def get_command_line(objectstring='requested'):
    from . import windowing

    parser = read_command_line(objectstring)
    args = parser.parse_args()
# If no dimension specified, operate on z.
    if not (args.x or args.y or args.z):
        args.z = True
# If no output/plot is specified, output to file
    if not args.plot and not args.output:
        args.output = True

    proportion = {'harmonic': 0.99,
                  'absorption': 0.5,
                  'requested': 1.0}

    if args.window_start is None:
        args.window = windowing.LinearProportion(proportion[objectstring])
    elif args.window_start < 0:
        args.window = None
    else:
        args.window = windowing.LinearStep(args.window_start)

    return args
