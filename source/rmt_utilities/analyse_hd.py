"""Tools for analysing the input files to RMT.

CLI
===

This module can act as a command line program. It is documented by its ``--help`` argument, but some examples of
use are::

    $ RMT_analyse_hd --converthd build/bin/convertHD --type H --input H_file_with_unusual_name summary
    $ RMT_analyse_hd summary file_already_converted.json
    $ convertHD H | RMT_analyse_hd summary
    $ RMT_analyse_hd -c build/bin/convertHD --input inputs/H wmat plot

API
===

Alternatively this module can be used programmatically. This is particularly useful if you want to inspect less
commonly examined parts of the data or work with the data in other ways (e.g. plot arrays).

The main entry points will be the ``load_*_file`` functions, from which you will obtain a Python object describing
the input file's format. For this example, however, we will use ``load_json_string`` to avoid issues with working
directories.

>>> H = load_json_string('''{
...     "nelc": 9,
...     "nz": 10,
...     "lrang2": 4,
...     "lamax": 4,
...     "ntarg": 2,
...     "rmatr": 20.0,
...     "bbloch": 0.0,
...     "etarg": {"format": "fortran", "shape": [2], "values": [-127.9277, -126.9274]},
...     "ltarg": {"format": "fortran", "shape": [2], "values": [1, 0]},
...     "starg": {"format": "fortran", "shape": [2], "values": [2, 2]},
...     "L_blocks": []
... }''')
>>> H.rmatr
20.0
"""
from __future__ import print_function

from matplotlib import pyplot

import argparse
import json
import numpy
import pathlib
import subprocess
import sys


def _convert_ndarray(format, values, shape):
    formats = {"fortran": "F"}
    order = formats[format]

    return numpy.reshape(numpy.asarray(values), shape, order=order)


class UnknownJSONObjectError(Exception):
    """The keys of this JSON object did not match any known types."""


def _object_hook(obj):
    NDARRAY_KEYS = ("format", "shape", "values")
    FILE_OBJECT_TYPES = (HFile, LBlock)

    keys = obj.keys()

    if keys == set(NDARRAY_KEYS):
        return _convert_ndarray(**obj)
    else:
        for file_object_type in FILE_OBJECT_TYPES:
            if keys == set(file_object_type.KEYS):
                return file_object_type(**obj)

        raise UnknownJSONObjectError(keys)


class HFile(object):

    HEADER_KEYS = ("nelc", "nz", "lrang2", "lamax", "ntarg", "rmatr", "bbloch")
    KEYS = HEADER_KEYS + ("etarg", "ltarg", "starg", "cfbut", "L_blocks")

    def __init__(
        self,
        nelc,
        nz,
        lrang2,
        lamax,
        ntarg,
        rmatr,
        bbloch,
        etarg,
        ltarg,
        starg,
        cfbut,
        L_blocks,
    ):
        self.nelc = nelc
        self.nz = nz
        self.lrang2 = lrang2
        self.lamax = lamax
        self.ntarg = ntarg
        self.rmatr = rmatr
        self.bbloch = bbloch
        self.etarg = etarg
        self.ltarg = ltarg
        self.starg = starg
        self.cfbut = cfbut
        self.L_blocks = L_blocks

        assert vars(self).keys() == set(self.KEYS)

    def __eq__(self, other):
        if isinstance(other, HFile):
            return (
                self.nelc == other.nelc
                and self.nz == other.nz
                and self.lrang2 == other.lrang2
                and self.lamax == other.lamax
                and self.ntarg == other.ntarg
                and self.rmatr == other.rmatr
                and self.bbloch == other.bbloch
                and numpy.all(self.etarg == other.etarg)
                and numpy.all(self.ltarg == other.ltarg)
                and numpy.all(self.starg == other.starg)
                and numpy.all(self.cfbut == other.cfbut)
                and all(s == o for (s, o) in zip(self.L_blocks, other.L_blocks))
            )
        else:
            return NotImplemented

    def summary(self, L_blocks=True):
        """Render a human-readable summary of the data.

        Parameters
        ==========
        L_blocks : bool
            Also render the summary data of this H file's L blocks.

        Returns
        =======
        summary : str
        """
        result = " | ".join("{} = {}".format(key, getattr(self, key)) for key in self.HEADER_KEYS)

        if L_blocks:
            result += "\n" + "\n".join(L_block.summary() for L_block in self.L_blocks)

        return result


class LBlock(object):

    HEADER_KEYS = ("lrgl", "nspn", "npty", "nchan", "nstat")
    KEYS = HEADER_KEYS + ("nconat", "l2p", "cf", "eig", "wmat")

    def __init__(self, lrgl, nspn, npty, nchan, nstat, nconat, l2p, cf, eig, wmat):
        self.lrgl = lrgl
        self.nspn = nspn
        self.npty = npty
        self.nchan = nchan
        self.nstat = nstat
        self.nconat = nconat
        self.l2p = l2p
        self.cf = cf
        self.eig = eig
        self.wmat = wmat

        assert vars(self).keys() == set(self.KEYS)

    def __eq__(self, other):
        if isinstance(other, LBlock):
            return (
                self.lrgl == other.lrgl
                and self.nspn == other.nspn
                and self.npty == other.npty
                and self.nchan == other.nchan
                and self.nstat == other.nstat
                and numpy.all(self.nconat == other.nconat)
                and numpy.all(self.l2p == other.l2p)
                and numpy.all(self.cf == other.cf)
                and numpy.all(self.eig == other.eig)
                and numpy.all(self.wmat == other.wmat)
            )
        else:
            return NotImplemented

    def summary(self):
        """Render a human-readable summary of the data.

        Returns
        =======
        summary : str
        """
        return " | ".join("{} = {}".format(key, getattr(self, key)) for key in self.HEADER_KEYS)


def load_json_string(string):
    """Load input file data from a JSON encoded string.

    The JSON data is expected to be in the format produced by ``convertHD`` and the file type is guessed based on
    structural sub-typing (i.e. whether the keys of the JSON object match the keys we expect that file type to
    have).

    Parameters
    ==========
    string : str
        The JSON encoded string to load.

    Returns
    =======
    An object representing the input file.
    """
    return json.loads(string, object_hook=_object_hook)


def load_json_stream(io):
    """Load input file data from a JSON encoded input stream.

    The JSON data is expected to be in the format produced by ``convertHD`` and the file type is guessed based on
    structural sub-typing (i.e. whether the keys of the JSON object match the keys we expect that file type to
    have).

    Parameters
    ==========
    io : io.TextIOWrapper
        A stream (e.g. as obtained by using ``open``) containing the JSON encoded data to load.

    Returns
    =======
    An object representing the input file.
    """
    return json.load(io, object_hook=_object_hook)


def load_json_file(path, _=None):
    """Load input file data from a JSON encoded file.

    The JSON data is expected to be in the format produced by ``convertHD`` and the file type is guessed based on
    structural sub-typing (i.e. whether the keys of the JSON object match the keys we expect that file type to
    have).

    Parameters
    ==========
    path : str or pathlib.Path
        The path to the JSON file.
    _
        An ignored parameter. Present so that this function's interface matches other ``load_*_file`` functions.

    Returns
    =======
    An object representing the input file.
    """
    with open(path) as f:
        return load_json_stream(f)


def load_H_file(path, converthd="convertHD"):
    """Convert then load input file data from an H file.

    Parameters
    ==========
    path : str or pathlib.Path
        The path to the H file.
    converthd : str or pathlib.Path
        The path to the ``convertHD`` binary on your system.

    Returns
    =======
    An object representing the input H file.
    """
    result = subprocess.run([converthd, path], capture_output=True, check=True)
    return load_json_string(result.stdout)


class UnknownFileTypeError(Exception):
    """The format of your input file could not be guessed from its name."""


def load_guessed_file(path, converthd="convertHD"):
    """Convert then load input file data from any valid file.

    The type of the file is guessed based on the file name, if your file has an unusual name you will have to use
    one of the more specific functions.

    Parameters
    ==========
    path : str or pathlib.Path
        The path to the input file.
    converthd : str or pathlib.Path
        The path to the ``convertHD`` binary on your system.

    Returns
    =======
    An object representing the input file.
    """
    path = pathlib.Path(path)

    if path.suffix == ".json":
        return load_json_file(path)
    elif path.name == "H":
        return load_H_file(path, converthd)
    else:
        raise UnknownFileTypeError(path)


_LOADERS = {"H": load_H_file, "JSON": load_json_file, "guess": load_guessed_file}


def _cli_summary(data, _=None):
    print(data.summary())


def summary(data):
    """Print a human-readable summary of the information in the input file to the standard output.

    Parameters
    ==========
    data
        The data from a loaded input file.
    """
    _cli_summary(data)


def _wmat_ascii(wmats, output):
    def inner(wmats, output):
        print("{:<10}  {:<10}  {:<10}  {:<15}".format("L Block", "Channel", "State", "Value"), file=output)
        for l_block, wmat in enumerate(wmats):
            for (channel, state), value in numpy.ndenumerate(wmat):
                # As close as I can reasonably get to Fortran's default formatting.
                print(
                    "{:>10}  {:>10}  {:>10}  {:< #15.8}".format(l_block + 1, channel + 1, state + 1, value),
                    file=output,
                )

    if output is None:
        inner(wmats, sys.stdout)
    elif isinstance(output, (str, pathlib.Path)):
        with open(output, "w") as f:
            inner(wmats, f)
    else:
        inner(wmats, output)


def _wmat_plot(wmats, output):
    def plot(L_block, wmat):
        # TODO: Ticks are often a bit awkward, especially when there's only one row in the matrix.
        # TODO: Can we 1-index this instead of the default 0-indexing?
        # TODO: Might a Hinton diagram (https://matplotlib.org/stable/gallery/specialty_plots/hinton_demo.html#sphx-glr-gallery-specialty-plots-hinton-demo-py) make more sense?  # noqa: E501
        pyplot.matshow(wmat, fignum=L_block)
        pyplot.suptitle(f"L Block: {L_block}")
        pyplot.xlabel("State")
        pyplot.ylabel("Channel")

        if output is not None:
            original = pathlib.Path(output)
            stem = original.stem
            suffix = original.suffix

            new = original.with_name("{}{:>04}{}".format(stem, L_block, suffix))
            pyplot.savefig(new)

    for L_block, wmat in enumerate(wmats):
        plot(L_block + 1, wmat)

    if output is None:
        pyplot.show()


_WMAT_ACTIONS = {"ascii": _wmat_ascii, "plot": _wmat_plot}


def _cli_wmat(data, args):
    action = _WMAT_ACTIONS[args.action]
    wmats = (L.wmat for L in data.L_blocks)
    action(wmats, args.output)


def wmat_ascii(data, output=None):
    """Output in an ASCII format the ``wmat`` fields of all L block in an H file.

    The format is a fixed-width file format, similar to the default output of a Fortran ``PRINT`` statement with
    list-directed I/O. The first row is a header row. Remaining rows list the L block index, the channel number,
    the state number, and the ``wmat`` value in that order.

    Parameters
    ==========
    data : HFile
        The H file data that you want to print.
    output
        Where you want to print the result. If ``None`` (the default) the print to the standard output, if a str
        or ``pathlib.Path`` then assume it is a filename to write, otherwise assume it is some IO type that can be
        written to. You may wish to use ``StringIO`` to save this data to a string.
    """
    _cli_wmat(data, argparse.Namespace(action="ascii", output=output))


def wmat_plot(data, output=None):
    """Plot the ``wmat`` fields of all L block in an H file.

    Each matrix is plotted using ``matplotlib.pyplot.matshow`` with default parameters. Note that the channel and
    state numbers will be 0-indexed, instead of the 1-indexing you might be expecting.

    Parameters
    ==========
    data : HFile
        The H file data that you want to print.
    output
        Where you want to print the result. If ``None`` (the default) a new matplotlib window is show for each
        matrix, if a ``str`` or ``pathlib.Path`` each matrix will be saved to a different file using the name
        given but with the L Block index added between the stem and extension of the file name.
    """
    _cli_wmat(data, argparse.Namespace(action="plot", output=output))


def _parser():
    parser = argparse.ArgumentParser(
        description="Tools for analysing the RMT input data.",
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )

    if parser.prog in ("RMT_analyse_hd", "analyse_hd.py"):
        # TODO: This is super dodgy, we first create the parser to get the program name then we change the
        # description dynamically using the program name. This will definitely break one day...
        parser.description += "\n\nexamples:"
        parser.description += f"\n  {parser.prog} --converthd build/bin/convertHD --type H --input \
H_file_with_unusual_name summary"
        parser.description += f"\n  {parser.prog} --input file_already_converted.json summary"
        parser.description += f"\n  convertHD H | {parser.prog} summary"

    parser.add_argument(
        "-c",
        "--converthd",
        help="the path to your convertHD binary (by default it is assumed to be in your PATH)",
        default="convertHD",
    )
    parser.add_argument(
        "-t",
        "--type",
        help="the kind of input file you have (by default we try to guess based on the file name)",
        default="guess",
        choices=_LOADERS.keys(),
    )
    parser.add_argument(
        "-i",
        "--input",
        help="the input file that you want to analyse (by default read from the standard input, though only JSON \
is supported when reading from the standard input)",
        nargs="?",
    )

    subparsers = parser.add_subparsers(help="what to do with the input file")

    subparser_summary = subparsers.add_parser("summary", help="summarise the data in the file")
    subparser_summary.set_defaults(function=_cli_summary)

    subparser_wmat = subparsers.add_parser("wmat", help="output the wmat data from the file")
    subparser_wmat.set_defaults(function=_cli_wmat)
    subparser_wmat.add_argument("action", help="how to output the wmat data", choices=_WMAT_ACTIONS.keys())
    subparser_wmat.add_argument(
        "-o",
        "--output",
        help="the file to which to output the data (by default it is output in some appropriate interactive way)",
    )

    return parser


def _main():
    parser = _parser()
    args = parser.parse_args()

    if args.input is None:
        data = load_json_stream(sys.stdin)
    else:
        data = _LOADERS[args.type](args.input, args.converthd)

    if hasattr(args, "function"):
        args.function(data, args)
    else:
        parser.print_help()


if __name__ == "__main__":
    _main()
